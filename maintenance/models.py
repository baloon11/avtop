# coding: utf-8
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.utils import timezone
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.core.cache import cache
from django.conf import settings


MESSAGE_TYPES = (
    (0, _(u'Announce for admins')),
    (5, _(u'Announce both for admins and users')),
    (10, _(u'Message for users')),
    (15, _(u'Full site down')),
)


class MaintenanceMessage(models.Model):
    message = models.TextField(_(u'Message'))
    start_time = models.DateTimeField(_(u'Start time'), default=timezone.now)
    end_time = models.DateTimeField(_(u'End time'), blank=True, null=True)

    type = models.IntegerField(
        _(u'Message type'), default=0, choices=MESSAGE_TYPES)
    stop_api = models.BooleanField(_(u'Stop API'), default=False)

    class Meta:
        verbose_name = _(u'Maintenance message')
        verbose_name_plural = _(u'Maintenance messages')

    def __unicode__(self):
        return self.message


# Invalidate the cache when a MaintenanceMessage is saved
@receiver(post_save, sender=MaintenanceMessage)
def invalidate_message_cache(sender, **kwargs):
    if not getattr(settings, 'MAINTENANCE_CACHE_MESSAGES', False):
        cache.delete('maintenance_messages')
