# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'MaintenanceMessage.type'
        db.add_column('maintenance_maintenancemessage', 'type',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)

        # Adding field 'MaintenanceMessage.stop_api'
        db.add_column('maintenance_maintenancemessage', 'stop_api',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'MaintenanceMessage.type'
        db.delete_column('maintenance_maintenancemessage', 'type')

        # Deleting field 'MaintenanceMessage.stop_api'
        db.delete_column('maintenance_maintenancemessage', 'stop_api')


    models = {
        'maintenance.maintenancemessage': {
            'Meta': {'object_name': 'MaintenanceMessage'},
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 7, 26, 0, 0)'}),
            'stop_api': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['maintenance']