# coding: utf-8
from django.conf.urls.defaults import patterns, url

# These url patterns use for admin interface
urlpatterns = patterns(
    'adminconfig.views',
    url(r'^restart-engine/(\w+)/$',
        'restart_engine', name="admin_config_restart_engine"),
    url(r'^$', 'config_index', name="admin_config_index"),
    url(r'^(\w+)/$', 'config_form', name="admin_config_form"),
)
