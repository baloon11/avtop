# coding: utf-8
import logging

from django.conf import settings
from django.http import HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
from django.contrib import messages

from lfs.core.utils import import_symbol


logger = logging.getLogger('sshop')


@permission_required(
    "core.manage_shop",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def config_index(request):
    return HttpResponseRedirect(
        reverse('admin_config_form', args=('general',)))


@permission_required(
    "core.manage_shop",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def config_form(request, config_group, template='adminconfig/index.html'):
    ADMIN_CONFIGURERS = getattr(settings, 'ADMIN_CONFIGURERS', tuple())
    config_groups = [(x[0], x[1]) for x in ADMIN_CONFIGURERS]
    config_class = [x[2] for x in ADMIN_CONFIGURERS if x[0] == config_group]
    config_groups = sorted(config_groups, key=lambda x: x[1])
    if len(config_class) > 0:
        if len(config_class) > 1:
            logger.warning(
                u'More than one config group "%(c_group)s" found.'
                u'Only first is taken.' % {
                    'c_group': config_group,
                })
        config_class = config_class[0]
    else:
        raise Http404

    try:
        config_class = import_symbol(config_class)
        configurer = config_class()
        configurer.load_data()

        form_class = configurer.get_form()

        if request.method == 'POST':
            form = form_class(request.POST)
            if form.is_multipart():
                form = form_class(request.POST, request.FILES)

            if form.is_valid():
                configurer.load_data_from_form(form.cleaned_data)
                configurer.save_data()
                messages.success(
                    request, _(u'Configuration saved successfully.'))
        else:
            form = form_class(initial=configurer.get_initial_data_for_form())
    except AttributeError as e:
        form = None
        logger.error(
            u'Cannot generate the config form for admin: %s' % unicode(e))

    return render_to_response(template, RequestContext(request, {
        'config_groups': config_groups,
        'active_group': config_group,
        'form': form,
    }))


@permission_required(
    "core.manage_shop",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def restart_engine(request, config_group):
    from sbits_plugins import mediator
    mediator.publish(None, 'restart_project')
    messages.success(request, _(u'Engine was restarted.'))
    return HttpResponseRedirect(
        reverse('admin_config_form', args=(config_group,)))
