# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _

from lfs.catalog.models import Product


class Question(models.Model):
    QUESTION_STATUSES = (
        (0, _(u'New')),
        (5, _(u'Sent')),
        (10, _(u'Sending failed')),
    )

    question = models.TextField(_(u'Question'))
    email = models.EmailField(_(u'Email'))
    product = models.ForeignKey(Product, related_name="questions")
    answer = models.TextField(_(u'Answer'), blank=True)
    status = models.IntegerField(
        _(u'Status'), default=0, choices=QUESTION_STATUSES)
    creation_date = models.DateTimeField(
        _(u"Creation date"), auto_now_add=True)
    answer_date = models.DateTimeField(
        _(u"Last answer date"), auto_now_add=True)

    class Meta:
        verbose_name = _(u'Question')
        verbose_name_plural = _(u'Questions')

    def __unicode__(self):
        return self.question
