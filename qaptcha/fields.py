# coding: utf-8

import datetime
import hashlib
import time

from django import forms
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.timezone import utc
from django.utils.simplejson import dumps
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

# app imports
from qaptcha.models import Qaptcha


class QaptchaInput(forms.Widget):
    def render(self, name='qaptcha', value='', attrs=None):
        captcha_hash = hashlib.sha1()
        captcha_hash.update(str(time.time()))
        default_options = {
            'disabledSubmit': False,
            'autoRevert': True,
            'autoSubmit': False,
            'PHPfile': reverse('validate_qaptcha'),
            'name': name
        }
        if attrs and isinstance(attrs, dict):
            default_options.update(attrs)
        js = """
            <script type="text/javascript">
              $(document).ready(function(){
                $('#%s_%s').QapTcha(%s);
              });
            </script>
        """
        js = js % (name, str(captcha_hash.hexdigest()), dumps(default_options))
        return mark_safe(
            u'\n'.join(
                ('<div class="QapTcha" id="%s_%s"></div>' % (
                    name, captcha_hash.hexdigest()), js)))

    def id_for_label(self, id_):
        return 'id_qaptcha_{}'.format(id_)

    class Media:
        css = {
            'all': (
                settings.STATIC_URL + 'css/QapTcha.jquery.css',
            )
        }
        js = (
            settings.STATIC_URL + 'js/jquery.ui.touch.js',
            settings.STATIC_URL + 'js/QapTcha.jquery.js',
        )


class QaptchaField(forms.Field):

    def __init__(self, *args, **kwargs):
        widget = QaptchaInput()
        super(QaptchaField, self).__init__(widget=widget, *args, **kwargs)

    def clean(self, key):
        try:
            qaptcha = Qaptcha.objects.get(key=key)
        except Qaptcha.DoesNotExist:
            raise forms.ValidationError(_(u'Qaptcha does not exist'))

        if qaptcha.expiration <= \
                datetime.datetime.utcnow().replace(tzinfo=utc):
            qaptcha.delete()
            raise forms.ValidationError(_(u'Qaptcha does not exist'))
        qaptcha.delete()
        return key
