from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.utils.simplejson import dumps
from qaptcha.models import Qaptcha
import datetime
from django.utils.timezone import utc
from django.conf import settings
from qaptcha.forms import TestForm
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def receive_key(request):
    error_response = dumps({'errors': True})
    accept_response = dumps({'errors': False})

    key = request.POST.get('qaptcha_key', None)
    action = request.POST.get('action', None)

    if not key or not action:
        return HttpResponse(error_response)

    if action != 'qaptcha':
        return HttpResponse(error_response)

    qaptcha = Qaptcha()
    qaptcha.key = key
    qaptcha.expiration = datetime.datetime.utcnow()\
        .replace(tzinfo=utc) + \
        datetime.timedelta(minutes=int(settings.CAPTCHA_TIMEOUT))
    qaptcha.save()
    return HttpResponse(accept_response)


@csrf_exempt
def test(request):
    if request.method == 'POST':
        form = TestForm(request.POST)
        if form.is_valid():
            return render_to_response(
                'qaptcha_response.html', {'value': form.data['text']})
    form = TestForm()
    return render_to_response('qaptcha_test.html', {'form': form})
