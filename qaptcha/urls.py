from django.conf.urls import *

urlpatterns = patterns(
    'qaptcha.views',
    url(r'^qaptcha/', 'receive_key', name='validate_qaptcha'),
    url(r'^test/', 'test', name='test_qaptcha'),
)
