# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Car'
        db.create_table('wheels_truck_car', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vendor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_truck.Vendor'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('wheels_truck', ['Car'])

        # Adding model 'Modification'
        db.create_table('wheels_truck_modification', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('car', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_truck.Car'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('str_tires', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('wheels_truck', ['Modification'])

        # Adding model 'TransportType'
        db.create_table('wheels_truck_transporttype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=1000)),
        ))
        db.send_create_signal('wheels_truck', ['TransportType'])

        # Adding model 'Vendor'
        db.create_table('wheels_truck_vendor', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('transport_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['wheels_truck.TransportType'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('wheels_truck', ['Vendor'])


    def backwards(self, orm):
        # Deleting model 'Car'
        db.delete_table('wheels_truck_car')

        # Deleting model 'Modification'
        db.delete_table('wheels_truck_modification')

        # Deleting model 'TransportType'
        db.delete_table('wheels_truck_transporttype')

        # Deleting model 'Vendor'
        db.delete_table('wheels_truck_vendor')


    models = {
        'wheels_truck.car': {
            'Meta': {'object_name': 'Car'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'vendor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_truck.Vendor']"})
        },
        'wheels_truck.modification': {
            'Meta': {'object_name': 'Modification'},
            'car': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_truck.Car']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'str_tires': ('django.db.models.fields.TextField', [], {})
        },
        'wheels_truck.transporttype': {
            'Meta': {'object_name': 'TransportType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1000'})
        },
        'wheels_truck.vendor': {
            'Meta': {'object_name': 'Vendor'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'transport_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['wheels_truck.TransportType']"})
        }
    }

    complete_apps = ['wheels_truck']