from django.db import models

class TransportType(models.Model):
    name = models.CharField(max_length=1000)

    def __unicode__(self):
        return self.name

class Vendor(models.Model):
    transport_type = models.ForeignKey('TransportType')
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class Car(models.Model):
    vendor = models.ForeignKey('Vendor')
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name

class Modification(models.Model):
    car = models.ForeignKey('Car')
    name = models.CharField(max_length=255)
    str_tires = models.TextField()

    def __unicode__(self):
        return self.name