# -*- coding: utf-8 -*-
from django.db import models

class Vendor(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class Modification(models.Model):
    vendor = models.ForeignKey('Vendor')
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class Car(models.Model):
    modification = models.ForeignKey('Modification')
    years = models.CharField(max_length=255)
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

class Param(models.Model):
    POSITION = (
        (0, u'перед'),
        (1, u'зад'),
        )

    PARAM_TYPE = (
        (0, u'Заводская комплектация'),
        (1, u'Варианты замены'),
        )

    car = models.ForeignKey('Car')
    position = models.IntegerField(default=0, choices=POSITION)
    param_type = models.IntegerField(default=0, choices=PARAM_TYPE)
    size = models.CharField(max_length=256)
    p1 = models.CharField(max_length=10)
    p2 = models.CharField(max_length=10)
    p3 = models.CharField(max_length=10)

    def __unicode__(self):
        return '%s %s %s %s' % (self.car, self.get_position_display(), self.get_param_type_display(), self.size)