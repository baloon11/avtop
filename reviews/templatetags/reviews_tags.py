# django imports
from django import template
from django.contrib.contenttypes.models import ContentType

from reviews import utils as reviews_utils
import hashlib
import time

register = template.Library()


@register.inclusion_tag('reviews/reviews_for_instance.html',
                        takes_context=True)
def reviews_for_instance(context, instance):
    """
    """
    request = context.get("request")
    ctype = ContentType.objects.get_for_model(instance)
    has_rated = reviews_utils.has_rated(request, instance)
    reviews = reviews_utils.get_reviews_for_instance(instance)

    return {
        "reviews": reviews,
        "has_rated": has_rated,
        "content_id": instance.id,
        "content_type_id": ctype.id,
        "MEDIA_URL": context.get("MEDIA_URL")
    }


@register.inclusion_tag('reviews/init_average_for_instance.html',
                        takes_context=True)
def average_for_instance(context, instance):
    """
    """
    ctype = ContentType.objects.get_for_model(instance)
    average_hash = hashlib.sha1()
    average_hash.update(str(time.time()))
    # average, amount = reviews_utils.get_average_for_instance(instance)
    return {
        # "average": average,
        # "amount": amount,
        # "init": True,
        "content_type_id": ctype.id,
        "content_id": instance.id,
        "average_hash": str(average_hash.hexdigest())
    }


def result_for_instance(instance):
    type_id = ContentType.objects.get_for_model(instance).id
    id = instance.id
    hash = str(hashlib.sha1(str(type_id)+str(id)).hexdigest())
    return {
        "instance": str(type_id) + ' ' + str(id),
        "hash": hash,
    }


@register.inclusion_tag('reviews/init_mark_for_instance.html')
def mark_for_instance(instance):
    return result_for_instance(instance)


@register.inclusion_tag('reviews/init_amount_feedbacks_for_instance.html')
def amount_feedbacks_for_instance(instance):
    return result_for_instance(instance)
