# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
# lfs imports
from reviews.models import Review


class ReviewAdmin(admin.ModelAdmin):
    list_display = [
        'comment',
        'score',
        'user',
        'content',
        'creation_date',
        'active'
    ]
    search_fields = ['comment']
    date_hierarchy = 'creation_date'
    list_filter = ['score']
    actions = ['moderate']

    def moderate(self, request, queryset):
        for review in queryset:
            review.active = True
            review.save()
    moderate.short_description = _(u'It has been moderated')

admin.site.register(Review, ReviewAdmin)
