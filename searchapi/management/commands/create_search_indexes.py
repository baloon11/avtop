# coding: utf-8
from django.core.management.base import BaseCommand
from south.db import db
from django.conf import settings


class Command(BaseCommand):
    args = ''
    help = 'Create search indexes'

    def handle(self, *args, **options):
        #cursor = connection.cursor()
        if not settings.ENABLE_FULL_TEXT_SEARCH:
            return
        db.execute("""
            DROP INDEX IF EXISTS description_tsv;
            DROP INDEX IF EXISTS name_idx_trgm;
            DROP INDEX IF EXISTS sku_manufacturer_idx_trgm;
            DROP INDEX IF EXISTS word_idx_trgm;

            CREATE INDEX word_idx_trgm on searchapi_uniqueword
            using gist (word gist_trgm_ops);
        """)
        print "Indexes for fuzzy search created already"

        print "Indexes created"
        print "Start parsing unique product names"
        from searchapi.searchconfigs import parse_unique_words
        parse_unique_words()
        print "Finish parsing unique product names"
