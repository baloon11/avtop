from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils import simplejson
from searchapi.models import UniqueWord

from searchapi.searchapibase import BaseSearchConfig
from lfs.catalog.models import Product
from lfs.catalog.settings import (
    STANDARD_PRODUCT, PRODUCT_WITH_VARIANTS, VARIANT)
from django.conf import settings


class SimpleSearchConfig(BaseSearchConfig):
    search_template = 'search_results.html'
    quick_search_template = 'livesearch_results.html'

    def get_products(self, query, order_by=None, is_quick_search=False):
        if not query and not is_quick_search:  # standard empty search
            return {
                "products": [],
                "q": query,
                "total": 0
            }
        if getattr(settings, 'CATEGORY_HIDE_VARIANTS', False):
            sub_types = (STANDARD_PRODUCT, PRODUCT_WITH_VARIANTS)
        else:
            sub_types = (STANDARD_PRODUCT, PRODUCT_WITH_VARIANTS, VARIANT)
        orm_query = Q(active=True) & \
            Q(status__is_searchable=True) & \
            Q(name__icontains=query) | \
            Q(manufacturer__name__icontains=query) | \
            Q(sku_manufacturer__icontains=query)
        if settings.SSHOP_USE_1C:
            orm_query = orm_query | Q(id_1c__icontains=query)
        elif query.isdigit():
            orm_query = orm_query | Q(id=query)
        products = Product.objects.filter(sub_type__in=sub_types)\
            .filter(orm_query)
        # check type search
        if is_quick_search:  # quick search
            total = len(products)
            products = products[0:5]
        else:   # standard search
            if order_by:
                order_by = [x.strip() for x in order_by.split(',')]
                products = products.order_by(*order_by)
            total = 0
            if products:
                total += len(products)
        # return data
        return {
            "products": products,
            "q": query,
            "total": total
        }

    def search(self, query, order_by=None):
        return render_to_response(
            self.search_template,
            RequestContext(
                self.request,
                self.get_products(query, order_by)))

    def quick_search(self, query):
        products = render_to_string(
            self.quick_search_template,
            RequestContext(
                self.request,
                self.get_products(query, is_quick_search=True)))

        result = simplejson.dumps({
            "state": "success",
            "products": products,
        })
        return HttpResponse(result)


class FuzzyTextSearchConfig(BaseSearchConfig):
    search_template = 'search_results.html'
    quick_search_template = 'livesearch_results.html'
    min_distance = float(settings.__getattr__('FUZZY_SEARCH_MIN_DISTANCE', .1))

    def get_products(self, query, order_by=None, is_quick_search=False):
        words = []
        query = query.strip().lower()
        for word in query.split():
            query_text = """
            SELECT word FROM searchapi_uniqueword
            WHERE
            similarity(word, '%s')>%s
            ORDER BY
            similarity(word, '%s') DESC LIMIT 1""" % (
                word, self.min_distance,  word)
            words.extend(UniqueWord.objects.raw(query_text))
        words = list(set(words))
        words_obj = words
        products_ids_list = [
            list(x.products.filter(active=True).values('id'))
            for x in words_obj]
        products_ids = sum(products_ids_list, [])
        ids = [x['id'] for x in products_ids]
        temp_dict = {}
        max_count = 0
        for i in ids:
            temp_dict[i] = ids.count(i)
            max_count = (
                temp_dict[i] if max_count < temp_dict[i] else max_count)
        for i in temp_dict.keys():
            if temp_dict[i] != max_count:
                del temp_dict[i]
        n = sorted(temp_dict, key=lambda key: temp_dict[key])
        n.reverse()
        additional_id = None
        if settings.SSHOP_USE_1C:
            additional_id = Q(id_1c__icontains=query.strip().split()[0])
        elif query.strip().split()[0].isdigit():
            additional_id = Q(id=query.strip().split()[0])
        for i in query.strip().split()[1:]:
            if settings.SSHOP_USE_1C:
                try:
                    additional_id |= Q(id_1c__icontains=i)
                except:
                    additional_id = Q(id_1c__icontains=i)
            elif i.isdigit():
                try:
                    additional_id |= Q(id=i)
                except:
                    additional_id = Q(id=i)
        if additional_id:
            products = Product.objects.filter(
                additional_id | Q(id__in=n), active=True)
        else:
            products = Product.objects.filter(id__in=n, active=True)
        # check search func
        if is_quick_search:
            total = len(products)
            products = products[0:5]
        else:
            if order_by:
                if ',' in order_by and isinstance(order_by, basestring):
                    order_by = [x.strip() for x in order_by.split(',')]
                else:
                    order_by = (order_by,)
                products = products.order_by(*order_by)
            total = 0
            if products:
                total += len(products)
        return {"products": products,
                "q": query,
                "total": total
                }
        ### repeat search func in two classes

    def search(self, query, order_by=None):
        return render_to_response(
            self.search_template,
            RequestContext(
                self.request,
                self.get_products(query, order_by)))

    def quick_search(self, query):
        products = render_to_string(
            self.quick_search_template,
            RequestContext(
                self.request,
                self.get_products(query, is_quick_search=True)))

        result = simplejson.dumps({
            "state": "success",
            "products": products,
        })
        return HttpResponse(result)


def parse_unique_words():
    """
        Fill table with unique words from field 'name' of Product objects
    """
    from models import UniqueWord
    products = Product.objects.all()
    words = {}
    for product in products:
        for word in product.name.split():
            if word.strip().lower() not in words:
                words[word.strip().lower()] = []
            words[word.strip().lower()].append(product)
    for key, value in words.iteritems():
        w = UniqueWord.objects.get_or_create(word=key)[0]
        w.save()
        w.products.clear()
        w.products.add(*value)
        w.save()
