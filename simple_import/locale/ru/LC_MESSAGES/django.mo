��    *      l  ;   �      �  X   �           #     =     Z     h     u     �     �     �     �     �     �     �     �     �            
         +     ;  :   @     {     �     �     �  2   �     �     �     �     �               %     7     C     S     Z  
   v     �     �  �  �  �   Z  E   �  :   *	  9   e	  !   �	  '   �	  '   �	     
     1
  &   I
  /   p
     �
     �
  (   �
  (   �
  .        J     ^     g  !   �     �  Z   �  #        1     K  !   c  ]   �  '   �            
   )  
   4  *   ?  2   j     �  %   �     �  '   �  $        A     T              %      	          $                "       #             (                                      &                  
       )                                               !   '   *                
        Import was undone. This is now a simulation, you can run the import again.
     Cannot undo this type of import! Clear field on blank cell Click here to run the import Column Header Column match Column matches Column name Content type Create New Records Create and Update Records Created Date Created Default Value Default value Download failed records Failed Field Field name Header position Home If cell is blank, clear out the field setting it to blank. Import Results Import data Import file Import settings Invalid file type. Must be xls, xlsx, ods, or csv. Match Columns Model Name Next Null on empty Only Update Records Run actual import Sample Data Simulate import Submit This was only a simulation. Update Key Updated User Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-02-19 11:49+0200
PO-Revision-Date: 2014-02-19 11:50+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 
        Импорт отменен. Это симуляция, вы можете запустить импорт повторно.
     Невозможно отменить этот тип импорта! Очистить поле при пустой ячейке Нажмите чтобы запустить импорт Заголовок колонки Соответствие колонки Соответствия колонок Название колонки Тип контента Создать новые записи Создать и обновить записи Создано Дата создания Значение по умолчанию Значение по умолчанию Скачать ошибочные записи Не удалось Поле Название поля Позиция заголовка Главная Если ячейка пустая, то очищать содержание ячейки. Результаты импорта Импорт данных Файл импорта Настройки импорта Недопустимый тип файла. Должен быть xls, xlsx, ods, или csv. Соответствие колонок Модель Название Далее Пусто Только обновить записи Запустить настоящий импорт Пример данных Симулировать импорт Подтвердить Это только симуляция. Ключ для обновления Обновлено Пользователь 