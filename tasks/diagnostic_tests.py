# coding: utf-8
from diagnostic.base import BaseDiagnosticTest
from django.utils.translation import ugettext as _

class FailJobTest(BaseDiagnosticTest):
    """
    """
    def get_description(self):
        return _(u'Repeat background jobs should not be completed with failed')

    def how_to_fix(self):
        return _(u'Remove background jobs, which completed with failed')

    def _get_items_with_problems(self):
        from models import Job
        _items = Job.objects.filter(status='failed').exclude(repeat_timeout=0)
        return _items

    def check(self):
        _items = self._get_items_with_problems()
        if _items:
            p_count = _items.count()
            return (
                False,
                _(u'There are %d jobs completed with failed.') % p_count)
        else:
            return (True, _(u'OK'))

    def get_report(self):
        _items = self._get_items_with_problems()
        _report = '<ul>'
        for i in _items:
            _report += '<li><pre><a href="%s" target="_blank">%s %s</a></pre></li>' % (
                '/superadmin/tasks/job/%d/' % i.pk, i.name, i.status)
        _report += '</ul>'
        return _report

    def fix(self, solution=0):
        _items = self._get_items_with_problems()
        if _items:
            for p in _items:
                p.delete()
            return (True, _(u'Fixed'))

        return (False, _(u'Failed'))
