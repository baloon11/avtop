# coding: utf-8
from django.utils.translation import ugettext as _
from django.contrib import admin

from tasks.models import Task, Job


# DEPRECATED in 0.3
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        'function_name', 'arguments',
        'added_at', 'finished_at', 'run_after',
        'status', 'error_msg')
admin.site.register(Task, TaskAdmin)


class JobAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'duty',
        'added',
        'scheduled',
        'started',
        'executed',
        'repeat_timeout',
        'get_status',
    )
    search_fields = ('name', 'id',)
    date_hierarchy = 'added'
    list_filter = ('duty', 'scheduled', 'executed', 'status')
    fieldsets = [
        (None, {
            'fields': ('name', 'sender', 'duty'),
        }),
        (_(u'Arguments'), {
            'fields': ('args', 'kwargs',),
        }),
        (_(u'Output'), {
            'fields': ('storage', 'result', 'status'),
        }),
        (_(u'Times'), {
            'fields': ('scheduled', 'started', 'executed', 'repeat_timeout'),
        }),
    ]
    actions = [
        'restart_jobs',
        'set_highest_priority',
        'set_normal_priority',
        'set_lowest_priority',
        'pause_jobs',
        'cancel_repeat',
        'repeat_every_hour',
        'repeat_every_6_hours',
    ]

    def get_status(self, obj):
        if obj.status == 'pending':
            return '<span class="label">%s</span>' % _('Pending')
        elif obj.status == 'running':
            return '<span class="label label-info">%s</span>' % _('Running')
        elif obj.status == 'success':
            return '<span class="label label-success">%s</span>' % _('Success')
        elif obj.status == 'failed':
            return '<span class="label label-important">%s</span>' %\
                _('Failed')
        elif obj.status == 'cancelled':
            return '<span class="label label-warning">%s</span>' %\
                _('Canceled')
        else:
            return '<span class="label label-warning">%s</span>' %\
                obj.status
    get_status.short_description = _('Status')
    get_status.allow_tags = True
    get_status.admin_order_field = 'status'

    def restart_jobs(self, request, queryset):
        for q in queryset:
            q.status = 'pending'
            q.save()
    restart_jobs.short_description = _(u'Restart jobs')

    def set_highest_priority(self, request, queryset):
        for q in queryset:
            q.duty = 9
            q.save()
    set_highest_priority.short_description = _(u'Set highest priority')

    def set_lowest_priority(self, request, queryset):
        for q in queryset:
            q.duty = 1
            q.save()
    set_lowest_priority.short_description = _(u'Set lowest priority')

    def pause_jobs(self, request, queryset):
        for q in queryset:
            q.duty = 0
            q.save()
    pause_jobs.short_description = _(u'Pause jobs')

    def set_normal_priority(self, request, queryset):
        for q in queryset:
            q.duty = 5
            q.save()
    set_normal_priority.short_description = _(u'Set normal priority')

    def cancel_repeat(self, request, queryset):
        for q in queryset:
            q.repeat_timeout = 0
            q.save()
    cancel_repeat.short_description = _(u'Cancel repeat')

    def repeat_every_hour(self, request, queryset):
        for q in queryset:
            q.repeat_timeout = 60
            q.save()
    repeat_every_hour.short_description = _(u'Repeat every hour')

    def repeat_every_6_hours(self, request, queryset):
        for q in queryset:
            q.repeat_timeout = 6*60
            q.save()
    repeat_every_6_hours.short_description = _(u'Repeat every 6 hours')


admin.site.register(Job, JobAdmin)
