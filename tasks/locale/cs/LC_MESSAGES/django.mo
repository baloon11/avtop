��    3      �  G   L      h  7   i     �     �  	   �  4   �     �               &     /     6     ;     A  !   I     k     y     }     �     �     �     �     �     �     �     �     �     �  
   �     �     �     �     �          '     9     H     U     \     d     k     �     �     �     �     �     �     �  %   �     �     �  �    =   �  	   �     �  	   �  D    	     E	     Y	     b	     �	  
   �	     �	     �	     �	  (   �	     �	     �	     �	     �	     �	     
     
     $
     +
  
   B
     M
     Y
     e
     m
     }
     �
     �
     �
     �
     �
     �
       	     
        *     6     T     r     �     �     �     �     �  *   �     �     �                                    '               .      &            (   0              "   ,   1   /   2      -                !   $             3   %                         
               #       )                     *   +   	                        A positive number. Bigger number means higher priority. Added Anonymous arguments Arguments Background job (pk=%(job)s) is failed: %(exception)s Cancel repeat Canceled Clear cache priority Executed Failed High High+ Highest If not set, will be executed ASAP In processing Job Jobs Low Low- Lowest Macros priority Name Named arguments Normal Normal+ Normal- Output Pause jobs Paused Pending Priority Rebuild counters priority Repeat every 6 hours Repeat every hour Repeat timeout Restart jobs Result Running Sender Set highest priority Set lowest priority Set normal priority Sheduled Started Status Storage Success Time in minutes for repeatable tasks. Times Update filters priority Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-23 17:14+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2
 Kladné číslo. Větší číslo znamená vyšší prioritu. Přidáno Anonymní argumenty Argumenty Úloha na pozadí (pk=%(job)s) proběhla neuspěšně: %(exception)s Zrušit opakování Zrušeno Očistit prioritu mezipaměti Začal Neúspěch Vysoký Vysoký+ Nejvyšší Pokud není nastaven, bude proveden ASAP Ve zpracování Úloha Úlohy Nízký Nízký- Nejnižší Priorita macrosu Jméno Pojmenované argumenty Normální Normální+ Normální- Výstup Zastavit úlohy Pozastaveno V očekávání Priorita Obnovit prioritu čítačů Opakovat každých 6 hodin Opakovat každou hodinu Opakovat timeout Restartovat úlohy Výsledek Běžící Odesílatel Nastavit nejvyšší prioritu Nastavit nejnižší prioritu Nastavit normální prioritu Zařazeno do rozvrhu Started Stav Skladování Úspěch Čas v minutách pro opakovatelné úkoly. Doby Obnovit prioritu filtrů 