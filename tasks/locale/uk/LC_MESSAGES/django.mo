��    2      �  C   <      H  7   I     �     �  	   �  4   �     �     �     �                         !  !   )     K     Y     ]     b     f     k     r     w     �     �     �     �  
   �     �     �     �     �     �     �     	          %     ,     4     ;     P     d     x     �     �     �     �  %   �     �     �  �  �  S   �     �  #   	     0	  O   C	  +   �	     �	  *   �	     �	     
     
     ,
     <
  I   M
     �
     �
     �
     �
     �
     �
  
   �
  !         "     7     M     c  '   v     �     �     �  @   �  .     '   J     r  +   �     �     �     �  8   �  :   /  :   j     �     �     �     �     �  E   �  
   A  6   L        "      .                                                   +   &            0   2   /         -                      
       )       	                          *         #      !   $                        (   %          1   '   ,    A positive number. Bigger number means higher priority. Added Anonymous arguments Arguments Background job (pk=%(job)s) is failed: %(exception)s Cancel repeat Canceled Clear cache priority Executed Failed High High+ Highest If not set, will be executed ASAP In processing Job Jobs Low Low- Lowest Name Named arguments Normal Normal+ Normal- Output Pause jobs Paused Pending Priority Rebuild counters priority Repeat every 6 hours Repeat every hour Repeat timeout Restart jobs Result Running Sender Set highest priority Set lowest priority Set normal priority Sheduled Started Status Storage Success Time in minutes for repeatable tasks. Times Update filters priority Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-12-16 13:03+0200
PO-Revision-Date: 2013-12-24 18:24+0300
Last-Translator: Natalia Potipko <natalia20061990@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 Додатнє число. Більше число - вищий пріоритет. Додано Невідомі аргументи Аргументи Фонова задача (pk=%(job)s) не виконалась: %(exception)s Відмінити повторювання Відмінено Пріоритет очистки кешу Виконано Невдало Високий Високий+ Найвищий Якщо не задано, виконається якнайшвидше В процесі Задача Задачі Низький Низький- Найнижчий Назва Названі аргументи Нормальний Нормальний+ Нормальний- Результат Призупинити завдання Зупинено В очікуванні Пріоритет Пріоритет перерахунку лічильників Повторювати кожні 6 годин Повторювати щогодини Повторити через Перезапустити завдання Результат В процесі Відправник Призначити найвищий пріоритет Призначити найнижчий пріоритет Призначити звичайний пріоритет За розкладом Розпочато Статус Сховище Успішно Час в хвилинах для повторюваних задач Разів Пріоритет оновлення фільтрів 