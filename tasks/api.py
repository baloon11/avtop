# coding: utf-8
import logging

from django.conf import settings
from django.db import transaction
from django.utils import timezone
from .models import Job
from .utils import non_unicode_kwarg_keys


logger = logging.getLogger('sshop')


class BaseTaskQueue(object):
    queue_name = 'base'

    def schedule(
            self, function, sender=None, args=None,
            kwargs=None, priority=5, run_after=None, repeat=0):
        raise NotImplemented

    def deschedule(self, job_id):
        raise NotImplemented

    def check_status(self, job_id):
        raise NotImplemented

    def get_result(self, job_id):
        raise NotImplemented

    def get_pending_tasks(self, regex=None):
        raise NotImplemented

    def health(self):
        raise NotImplemented

    def flush(self, jobs=getattr(settings, 'DEFAULT_FLUSH_JOB_COUNT', 5)):
        raise NotImplemented


class DummyTaskQueue(BaseTaskQueue):
    """A simple task queue backend. Call function immideatly when schedule.
    """
    queue_name = 'dummy'

    def schedule(
            self, function, sender=None, args=None,
            kwargs=None, priority=5, run_after=None, repeat=0):
        from .utils import full_name
        from lfs.core.utils import import_symbol
        _args = args or []
        _kwargs = non_unicode_kwarg_keys(kwargs or {})
        if type(function) is str:
            f = import_symbol(function)
            func_name = function
        else:
            f = function
            func_name = full_name(function)

        if func_name in getattr(settings, 'TASK_NAME_EXCLUDES', []):
            return 0

        transaction.commit_on_success(f)(*_args, **_kwargs)
        return 1

    def deschedule(self, job_id):
        return 1

    def check_status(self, job_id):
        return 'success'

    def get_pending_tasks(self, regex=None):
        return []

    def get_result(self, job_id):
        return None

    def health(self):
        return []

    def flush(self, jobs=getattr(settings, 'DEFAULT_FLUSH_JOB_COUNT', 5)):
        return []


class NullTaskQueue(BaseTaskQueue):
    """A simple task queue which does nothing. Ignore each schedule.
    """
    queue_name = 'null'

    def schedule(
            self, function, sender=None, args=None,
            kwargs=None, priority=5, run_after=None, repeat=0):
        return 1

    def deschedule(self, job_id):
        return 1

    def check_status(self, job_id):
        return 'success'

    def get_pending_tasks(self, regex=None):
        return []

    def get_result(self, job_id):
        return None

    def health(self):
        return []

    def flush(self, jobs=getattr(settings, 'DEFAULT_FLUSH_JOB_COUNT', 5)):
        return []


class DBTaskQueue(BaseTaskQueue):
    """A simple task queue backend. Use DB as queue broker.
    """
    queue_name = 'dummy'

    def schedule(
            self, function, sender=None, args=None,
            kwargs=None, priority=5, run_after=None, repeat=0):
        from .utils import full_name
        from lfs.core.utils import import_symbol
        _args = args or []
        _kwargs = non_unicode_kwarg_keys(kwargs or {})
        if sender:
            _sender = full_name(sender)
        else:
            _sender = None
        if type(function) is str:
            func_name = function
        else:
            func_name = full_name(function)

        if func_name in getattr(settings, 'TASK_NAME_EXCLUDES', []):
            return 0

        func = import_symbol(func_name)
        _allow_concatenate_args = getattr(func, 'allow_concatenate_args', None)

        pending_jobs = Job.objects.filter(
            name=func_name,
            # scheduled=run_after,
            duty=priority,
            repeat_timeout=repeat,
            status='pending',
        )
        if pending_jobs:
            for j in pending_jobs:
                if j.args == _args and j.kwargs == _kwargs:
                    return None
                else:
                    if _allow_concatenate_args:
                        t = j.args + _args
                        _args = t[:]
                        j.args = _args
                        j.save()
                        return j.id

        job = Job(
            name=func_name,
            sender=_sender,
            args=_args, kwargs=_kwargs,
            scheduled=run_after,
            duty=priority,
            repeat_timeout=repeat)
        job.save()
        return job.id

    def deschedule(self, job_id):
        try:
            job = Job.objects.get(pk=job_id)
            job.executed = timezone.now()
            job.status = 'cancelled'
            job.save()
            return job.id
        except Job.DoesNotExist:
            return 0

    def check_status(self, job_id):
        try:
            job = Job.objects.get(pk=job_id)
            return job.status
        except Job.DoesNotExist:
            return None

    def get_pending_tasks(self, regex=None):
        if regex is None:
            jobs = Job.objects.filter(status='pending')
        else:
            jobs = Job.objects.filter(name__regex=regex, status='pending')
        jobs = jobs.values('id')
        return [x['id'] for x in jobs]

    def get_result(self, job_id):
        try:
            job = Job.objects.get(pk=job_id)
            return job.result
        except Job.DoesNotExist:
            return None

    def health(self):
        return {
            'pending': Job.objects.filter(status='pending').count(),
            'running': Job.objects.filter(status='running').count(),
            'success': Job.objects.filter(status='success').count(),
            'failed': Job.objects.filter(status='failed').count(),
            'cancelled': Job.objects.filter(status='cancelled').count(),
            'all': Job.objects.all().count(),
            'executed': Job.objects.exclude(executed=None).count(),
            'non-executed': Job.objects.filter(executed=None).count(),
        }

    def flush(self, jobs=getattr(settings, 'DEFAULT_FLUSH_JOB_COUNT', 5)):
        import datetime
        running_jobs = Job.objects.filter(status='running')
        is_running = running_jobs.count() > 0

        now = timezone.now()
        check_time = now - datetime.timedelta(
            minutes=getattr(settings, 'AUTO_CANCEL_TASK_TIMEOUT', 120))
        for j in running_jobs:
            if j.started < check_time:
                j.status = 'cancelled'
                j.executed = now
                j.save()
        if is_running:
            return None

        job_list = Job.objects.filter(status='pending')\
            .exclude(scheduled__gt=timezone.now()).exclude(duty=0)\
            .order_by('-duty', 'added')[:jobs]
        for j in job_list.iterator():
            j.execute()
        return [x.id for x in job_list]


class TaskQueueManager(object):
    def __init__(self):
        from lfs.core.utils import import_symbol
        task_manager_name = getattr(
            settings, 'ACTIVE_TASK_MANAGER', 'tasks.api.DummyTaskQueue')
        self.task_queue = import_symbol(task_manager_name)()

    def schedule(
            self, function, sender=None, args=None,
            kwargs=None, priority=5, run_after=None, repeat=0):
        return self.task_queue.schedule(
            function, sender, args, kwargs, priority, run_after, repeat)

    def deschedule(self, job_id):
        return self.task_queue.deschedule(job_id)

    def check_status(self, job_id):
        return self.task_queue.check_status(job_id)

    def get_pending_tasks(self, regex=None):
        return self.task_queue.get_pending_tasks(regex)

    def get_result(self, job_id):
        return self.task_queue.get_result(job_id)

    def health(self):
        return self.task_queue.health()

    def flush(self, jobs=getattr(settings, 'DEFAULT_FLUSH_JOB_COUNT', 5)):
        return self.task_queue.flush(jobs)
