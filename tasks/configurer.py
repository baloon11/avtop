# coding: utf-8
from django import forms
from django.conf import settings
from django.utils.translation import ugettext as _

from adminconfig.utils import BaseConfig


class TaskPriorityConfigForm(forms.Form):
    clear_cache_priority = forms.IntegerField(
        label=_(u'Clear cache priority'),
        required=True)
    rebuild_counters_priority = forms.IntegerField(
        label=_(u'Rebuild counters priority'),
        required=True)
    update_filters_priority = forms.IntegerField(
        label=_(u'Update filters priority'),
        required=True)
    macros_priority = forms.IntegerField(
        label=_(u'Macros priority'),
        required=True)


class TaskPriorityConfig(BaseConfig):
    """Configurator for tasks options in config.
    """
    form_class = TaskPriorityConfigForm
    block_name = 'tasks'

    def __init__(self):
        super(TaskPriorityConfig, self).__init__()

        self.default_data = {
            'JOB_CLEAR_CACHE_PRIORITY':
            settings.JOB_CLEAR_CACHE_PRIORITY,
            'JOB_REBUILD_COUNTERS_PRIORITY':
            settings.JOB_REBUILD_COUNTERS_PRIORITY,
            'JOB_UPDATE_FILTERS_PRIORITY':
            settings.JOB_UPDATE_FILTERS_PRIORITY,
            'JOB_MACROS_PRIORITY':
            settings.JOB_MACROS_PRIORITY,
        }

        self.option_translation_table = (
            (
                'JOB_CLEAR_CACHE_PRIORITY',
                'clear_cache_priority',
            ),
            (
                'JOB_REBUILD_COUNTERS_PRIORITY',
                'rebuild_counters_priority',
            ),
            (
                'JOB_UPDATE_FILTERS_PRIORITY',
                'update_filters_priority',
            ),
            (
                'JOB_MACROS_PRIORITY',
                'macros_priority',
            ),
        )
