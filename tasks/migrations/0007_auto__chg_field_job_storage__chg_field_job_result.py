# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Job.storage'
        db.alter_column('tasks_job', 'storage', self.gf('json_field.fields.JSONField')(null=True))

        # Changing field 'Job.result'
        db.alter_column('tasks_job', 'result', self.gf('json_field.fields.JSONField')(null=True))

    def backwards(self, orm):

        # Changing field 'Job.storage'
        db.alter_column('tasks_job', 'storage', self.gf('json_field.fields.JSONField')())

        # Changing field 'Job.result'
        db.alter_column('tasks_job', 'result', self.gf('json_field.fields.JSONField')())

    models = {
        'tasks.job': {
            'Meta': {'object_name': 'Job'},
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'args': ('json_field.fields.JSONField', [], {'default': '[]'}),
            'duty': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'executed': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kwargs': ('json_field.fields.JSONField', [], {'default': '{}'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'repeat_timeout': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'result': ('json_field.fields.JSONField', [], {'default': "'null'", 'null': 'True', 'blank': 'True'}),
            'scheduled': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'started': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'pending'", 'max_length': '30'}),
            'storage': ('json_field.fields.JSONField', [], {'default': "'null'", 'null': 'True', 'blank': 'True'})
        },
        'tasks.task': {
            'Meta': {'object_name': 'Task'},
            'added_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'auto_now_add': 'True', 'blank': 'True'}),
            'arguments': ('django.db.models.fields.TextField', [], {'default': "'{}'"}),
            'error_msg': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'finished_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'function_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'run_after': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'auto_now_add': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['tasks']