# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Job.sender'
        db.add_column('tasks_job', 'sender',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True),
                      keep_default=False)


        # Changing field 'Job.name'
        db.alter_column('tasks_job', 'name', self.gf('django.db.models.fields.CharField')(max_length=256))

    def backwards(self, orm):
        # Deleting field 'Job.sender'
        db.delete_column('tasks_job', 'sender')


        # Changing field 'Job.name'
        db.alter_column('tasks_job', 'name', self.gf('django.db.models.fields.CharField')(max_length=100))

    models = {
        'tasks.job': {
            'Meta': {'object_name': 'Job'},
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'args': ('json_field.fields.JSONField', [], {'default': "'null'", 'blank': 'True'}),
            'executed': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kwargs': ('json_field.fields.JSONField', [], {'default': "'null'", 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'priority': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'repeat_timeout': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'result': ('json_field.fields.JSONField', [], {'default': "'null'", 'blank': 'True'}),
            'scheduled': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'sender': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True'}),
            'started': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'pending'", 'max_length': '30'}),
            'storage': ('json_field.fields.JSONField', [], {'default': "'null'", 'blank': 'True'})
        },
        'tasks.task': {
            'Meta': {'object_name': 'Task'},
            'added_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'auto_now_add': 'True', 'blank': 'True'}),
            'arguments': ('django.db.models.fields.TextField', [], {'default': "'{}'"}),
            'error_msg': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'finished_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'function_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'run_after': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'auto_now_add': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['tasks']