# coding: utf-8
from __future__ import unicode_literals
from datetime import datetime
import logging
from inspect import getmembers, getmodule, isfunction, ismethod
from django.utils import six

logger = logging.getLogger('sshop')


# DEPRECATED in 0.3
def add_task(function_name, arguments, run_after=datetime.now()):
    from .models import Task
    logger.warning('tasks.utils.add_task is deprecated.')
    param = {
        'function_name': function_name,
        'arguments': arguments,
        'status': 0,
    }
    task, created = Task.objects.get_or_create(**param)
    task.run_after = run_after
    task.save()


def full_name(item):
    """Return the full name of a something passed in so it can be retrieved
later on.
"""
    if isinstance(item, basestring):
        return item
    if ismethod(item):
        module_name = full_name(dict(getmembers(item))['im_self'])
    else:
        module_name = getmodule(item).__name__
    if isfunction(item):
        name = item.func_name
    else:
        name = item.__name__
    return '.'.join([module_name, name])


def object_at_end_of_path(path):
    """Attempt to return the Python object at the end of the dotted
path by repeated imports and attribute access.
"""
    access_path = path.split('.')
    module = None
    for index in xrange(1, len(access_path)-1):
        try:
            # import top level module
            module_name = '.'.join(access_path[:-index])
            module = __import__(module_name)
        except ImportError:
            continue
        else:
            for step in access_path[1:-1]:  # walk down it
                module = getattr(module, step)
            break
    if module:
        return getattr(module, access_path[-1])
    else:
        return globals()['__builtins__'][path]


def non_unicode_kwarg_keys(kwargs):
    """Convert all the keys to strings as Python won't accept unicode.
"""
    return dict([(six.b(k), v) for k, v in kwargs.items()]) if kwargs else {}


def test_job(
        text='Hello', timeout=0, error=False, extra_status=False, **kwargs):
    import time
    print text
    print kwargs
    if error:
        print 2/0
    if timeout > 0:
        print 'A:', text
        time.sleep(timeout)
        print 'B:', text
    if extra_status:
        return {
            'job_meta': {
                'storage': {
                    'extra_status': 'waiting for file',
                },
                'status': 'pending',
            },
        }
