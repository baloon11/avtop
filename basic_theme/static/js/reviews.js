$(function(){

function update_mark(current, res){
    general_width = $('.star-rating', current).width();
    $('.current-rating', current).width(general_width / 5 * res);
}

function update_amount_feedbacks(current, res){
    current.text(res);
}

function update_reviews(url, items, f){
    data = [];
    for(i=0; i<items.length; i++){
        data.push({
            'hash': items.eq(i).data('hash'),
            'instance': items.eq(i).data('instance')
        });
    }
    if(data.length){
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                'data': JSON.stringify(data),
            },
            dataType: "json",
            success: function(msg){
                data = msg['data'];
                for(i=0; i<items.length; i++){
                    current = items.eq(i);
                    hash = current.data('hash');
                    res = $.grep(data, function(e){ return e['hash'] == hash; });
                    f(current, res[0]['instance']);
                }
            },
        });
    }
}

update_reviews("/reviews/amount-feedbacks", $('.js-amount-feedbacks-for-instance'), update_amount_feedbacks);
update_reviews("/reviews/mark", $('.js-mark-for-instance'), update_mark);

});
