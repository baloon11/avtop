/**
 * Script file to execute all dynamic page actions
 *
 * @author D. Tiems (dennis-at-bigbase-dot-nl)
 * @version 1.0.0 - September 22nd 2012
 *
 * Converted for SSHOP by Oleh Korkh
 */

mediator.subscribe('document_ready', function() {
    // Show a message
    // console.log( 'All script will be executed now' );

    // Run the slideshows on the page
    if( $( '.nivoslider' ).length ) $( '.nivoslider' ).nivoSlider();

    $('#mega-menu').dcMegaMenu({
        rowItems: '3',
        speed: 'fast'
    });

    // Hover images with their lowsrc attr
    $( 'img[data-hover]' ).on( 'hover' , function(){
        var tmp = $(this).attr( 'src' );
        $(this).attr( 'src' , $(this).attr( 'data-hover' ) );
        $(this).attr( 'data-hover' , tmp );
    })
  
    // Apply the lightbox feature on images
    if( $( '.colorbox' ).length ) $( '.colorbox' ).colorbox();

    // Show google map
    if( $( '#googlemap1' ).length ) {
        var mapcfg = {
            zoom: 8,
            center: new google.maps.LatLng( -34.397 , 150.644 ),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map( document.getElementById( 'googlemap1' ) , mapcfg );
    }

});

function addLink() {
    var body_element = document.getElementsByTagName('body')[0];
    var selection;
    selection = window.getSelection();

    z = selection.getRangeAt();

    if (z.commonAncestorContainer.parentElement.tagName == "H1" ||
        z.commonAncestorContainer.parentElement.parentElement.tagName == "H1") {

        var pagelink = " Подробнее: " + document.location.href;
        var copytext = selection + pagelink;
        var newdiv = document.createElement('div');
        newdiv.style.position='absolute';
        newdiv.style.left='-99999px';
        body_element.appendChild(newdiv);
        newdiv.innerHTML = copytext;
        selection.selectAllChildren(newdiv);
        window.setTimeout(function() {
            body_element.removeChild(newdiv);
        },0);
    }

}

document.oncopy = addLink;
