# coding: utf-8
from django.template import Library
from django.utils.translation import ugettext as _
from lfs.core.utils import get_default_shop
from lfs.filters.models import FilterOption
from django.template.defaultfilters import stringfilter

register = Library()


@register.inclusion_tag('lfs/mail/mail_html_footer.html', takes_context=True)
def email_html_footer(context):
    request = context.get('request', None)
    shop = get_default_shop(request)
    return {
        "shop": shop
    }


@register.inclusion_tag('lfs/mail/mail_text_footer.html', takes_context=True)
def email_text_footer(context):
    request = context.get('request', None)
    shop = get_default_shop(request)
    return {
        "shop": shop
    }


@register.assignment_tag(takes_context=True)
def get_filteroption_name(context, request_get, filter_identifier, category):
    if request_get and context and filter_identifier and category:
        if filter_identifier in request_get.keys() and\
                len(request_get[filter_identifier].split(',')) == 1:
            f = FilterOption.objects.get(
                filter__identificator=filter_identifier,
                filter__category=category,
                identificator=request_get[filter_identifier])
            return f.title
    return ''


@register.filter
@stringfilter
def capitalize(value):
    return " ".join(value.strip().split()).capitalize()
