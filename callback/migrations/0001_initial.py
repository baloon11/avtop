# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Callback'
        db.create_table('callback_callback', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('time_from', self.gf('django.db.models.fields.TimeField')(default='', blank=True)),
            ('time_to', self.gf('django.db.models.fields.TimeField')(default='', blank=True)),
            ('exported', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('callback', ['Callback'])


    def backwards(self, orm):
        # Deleting model 'Callback'
        db.delete_table('callback_callback')


    models = {
        'callback.callback': {
            'Meta': {'object_name': 'Callback'},
            'exported': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'time_from': ('django.db.models.fields.TimeField', [], {'default': "''", 'blank': 'True'}),
            'time_to': ('django.db.models.fields.TimeField', [], {'default': "''", 'blank': 'True'})
        }
    }

    complete_apps = ['callback']