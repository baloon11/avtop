# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Callback.status'
        db.add_column('callback_callback', 'status',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=10, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Callback.status'
        db.delete_column('callback_callback', 'status')


    models = {
        'callback.callback': {
            'Meta': {'object_name': 'Callback'},
            'exported': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'status': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'blank': 'True'}),
            'time_from': ('django.db.models.fields.TimeField', [], {'default': "''", 'blank': 'True'}),
            'time_to': ('django.db.models.fields.TimeField', [], {'default': "''", 'blank': 'True'})
        }
    }

    complete_apps = ['callback']