# coding: utf-8
from django.conf.urls import url, patterns

urlpatterns = patterns(
    '',
    url('add_callback', 'callback.views.callback', name='add_callback',
        kwargs={'email': True, 'form_name': 'obratnyy_zvonok'}),
)
