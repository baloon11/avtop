# coding: utf-8


def run_macros_job(*macros_identifiers, **kwargs):
    from .models import Macros
    macroses = Macros.objects.filter(
        identifier__in=macros_identifiers).distinct()
    for macros in macroses:
        macros.run()

run_macros_job.allow_concatenate_args = True
