# coding: utf-8
from django import forms
from codemirror.widgets import CodeMirrorTextarea
from .models import Macros


class EditMacrosForm(forms.ModelForm):
    class Meta:
        model = Macros
        widgets = {
            'code': CodeMirrorTextarea(
                mode='python',
                config={
                    'fixedGutter': True,
                    'lineWrapping': True,
                    'indentUnit': 4,
                }),
            'output': CodeMirrorTextarea(
                config={
                    'fixedGutter': True,
                    'lineWrapping': True,
                    'readOnly': True,
                }),
        }
