# coding: utf-8


class BaseMacros(object):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    def run(self):
        pass

    def change_urlpatterns(self, urlpatterns):
        return None
