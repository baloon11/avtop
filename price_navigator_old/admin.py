# coding: utf-8
from django import forms
from django.contrib import admin
from price_navigator.models import PriceNavigator
from suit.admin import SortableModelAdmin
import autocomplete_light
from codemirror.widgets import CodeMirrorTextarea
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
import price_navigator.utils as utils


class PriceNavigatorForm(forms.ModelForm):
    class Meta:
        model = PriceNavigator
        widgets = {
            'categories':
            autocomplete_light.MultipleChoiceWidget('CategoryAutocomplete'),
            'template': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
        }


class PriceNavigatorAdmin(SortableModelAdmin, admin.ModelAdmin):
    form = PriceNavigatorForm
    sortable = 'order'
    list_display = ('name', 'active', 'file_name', 'update_rate')
    search_fields = ['name']
    actions = [
        'generate_price_navigator'
    ]

    def generate_price_navigator(self, request, queryset):
        for navigator in queryset:
            utils.check_tasks(navigator, True)

        self.message_user(
            request,
            _(u"The file for price navigator will be generated soon."))
        return HttpResponseRedirect(request.path)
    generate_price_navigator.short_description = _(u"Generate")

admin.site.register(PriceNavigator, PriceNavigatorAdmin)
