# coding: utf-8
import re
from urllib import urlencode

from django.core.exceptions import ObjectDoesNotExist

from .models import FilterUrlTemplate
from lfs.filters.models import Filter, FilterOption
from lfs.core.utils import import_symbol
from lfs.catalog.models import Category
from django.conf import settings
from lfs.caching.utils import lfs_filter_object, lfs_get_object_or_404


def unpack_hr_urls(category, url):
    if getattr(settings, 'USE_HR_URLS', False):
        futs = lfs_filter_object(FilterUrlTemplate, category=category)
    else:
        futs = []
    # find best template in templates
    best_dict = {}
    url_template = None
    best_count = 0
    for fut in futs:
        c = re.compile(fut.template)
        match = c.match(url)
        if match:
            _dict = match.groupdict()
            if best_count < len(_dict):
                best_count = len(_dict)
                best_dict = _dict
                url_template = fut

    filters = {}
    additional = {}
    for key, value in best_dict.items():
        try:
            f = Filter.objects.get(identificator=key, category=category)
            FilterOption.objects.get(filter=f, identificator=value)
            filters[key] = value
        except ObjectDoesNotExist:
            validator = url_template.get_validator_for_parameter(key)
            if validator:
                check_valid = import_symbol(validator)
                if check_valid(value, category.slug):
                    additional[key] = value

    return (urlencode(filters), additional)


def get_hr_urls(category, filters_string='', extras={}):
    if getattr(settings, 'USE_HR_URLS', False):
        futs = lfs_filter_object(FilterUrlTemplate, category=category)
    else:
        futs = []
    _dict = {}
    additional = {}
    objs = []
    if filters_string:
        objs = filters_string.split('&')
    if extras:
        objs += urlencode(extras).split('&')
    for f in objs:
        key, value = f.split('=')
        if len(value.split(',')) == 1:
            _dict[key] = value
        else:
            additional[key] = value
    # find best template in templates
    template = None
    best_count = 0
    if _dict:
        for fut in futs:
            if len(fut.template.split('/')) > len(_dict.keys()) + 1:
                continue
            c = re.compile(fut.template)
            keys = c.groupindex.keys()
            count = sum([1 for key in keys if key in _dict.keys()])
            if count > best_count and count == len(keys):
                template = fut.template
                best_count = count

    formated_url = '/'
    if template:
        keys = re.compile(template).groupindex.keys()
        # move extras from _dict to additional
        for key in _dict.keys():
            if key not in keys:
                additional[key] = _dict[key]
                del _dict[key]

        values = []
        #WTF this? refactor it!
        for value in template.split('/'):
            if not value:
                continue
            r = re.compile(r'<(.+?)>')
            items = r.findall(value)
            if items:
                values.append('{%s}' % items[0])
            else:
                values.append(value)
        formated_url += '/'.join(values)
        if template[-1] == '/':
            formated_url += '/'

        if additional:
            formated_url += '?%s' % urlencode(additional)
        return formated_url.format(**_dict)
    else:
        _dict.update(additional)
        url = urlencode(_dict)
        if url:
            return '?%s' % url
        else:
            return '/'


def url_validator(request, category_slug):
    if getattr(settings, 'USE_HR_URLS', False):
        category = lfs_get_object_or_404(Category, slug=category_slug)
        url_templates = lfs_filter_object(FilterUrlTemplate, category=category)
    else:
        url_templates = []

    for t in url_templates:
        parameters = t.get_all_template_parameters()
        f = True
        for p in parameters:
            if p in request.GET.keys():
                val = request.GET.get(p)
                if val.find(',') != -1:
                    f = False
                    break
            else:
                f = False
                break
        if f:
            return False

    return True


def get_hr_urls_to_client(request, category):
    hr_urls_templates = []
    if getattr(settings, 'USE_HR_URLS', False):
        futs = lfs_filter_object(FilterUrlTemplate, category=category)
    else:
        futs = []
    for f in futs:
        values = []
        formated_url = '/'
        for value in f.template.split('/'):
            if not value:
                continue
            r = re.compile(r'<(.+?)>')
            items = r.findall(value)
            if items:
                values.append('$%s' % items[0])
            else:
                values.append(value)
        formated_url += '/'.join(values)

        if f.template[-1] == '/':
            formated_url += '/'

        hr_urls_templates.append(formated_url)
    # get current url without category slug and get params
    get_pararms = ''
    url = '/'
    try:
        url = request.get_full_path()[len('/%s' % category.slug):]
        url, get_pararms = url.split('?')
    except:
        pass

    filters_string, extras = unpack_hr_urls(category, url)
    filters_string = '&'.join([
        p for p in [filters_string, urlencode(extras), get_pararms] if p])

    return hr_urls_templates, filters_string


def get_url_params_as_dicts(filters_string='', extras={}):
    _dict = {}
    additional = {}
    objs = []

    if filters_string:
        objs = filters_string.split('&')

    if extras:
        objs += urlencode(extras).split('&')

    for f in objs:
        key, value = f.split('=')
        if len(value.split(',')) == 1:
            _dict[key] = value
        else:
            additional[key] = value

    return _dict, additional


def get_best_template(category, _dict):
    futs = FilterUrlTemplate.objects.filter(category=category)

    # find best template in templates
    template = None
    best_count = 0
    for fut in futs:
        c = re.compile(fut.template)
        keys = c.groupindex.keys()
        count = sum([1 for key in keys if key in _dict.keys()])
        if count > best_count and count == len(keys):
            template = fut.template
            best_count = count

    return template


def get_formatted_url(template, param='', extra={}):
    _dict, additional = get_url_params_as_dicts(param, extra)

    formated_url = '/'
    if template:
        keys = re.compile(template).groupindex.keys()
        # move extras from _dict to additional
        for key in _dict.keys():
            if key not in keys:
                additional[key] = _dict[key]
                del _dict[key]

        values = []

        r = re.compile(r'<(.+?)>')
        for value in template.split('/'):
            if value:
                items = r.findall(value)
                if items:
                    values.append('{%s}' % items[0])
                else:
                    values.append(value)
        formated_url += '/'.join(values)
        if template[-1] == '/':
            formated_url += '/'

        if additional:
            formated_url += '?%s' % urlencode(additional)

        return formated_url.format(**_dict)
    else:
        _dict.update(additional)
        url = urlencode(_dict)
        if url:
            return '?%s' % url
        else:
            return '/'
