# -*- coding: utf-8 -*-
import logging

from django.core.management.base import BaseCommand


logger = logging.getLogger('sshop')


class Command(BaseCommand):
    args = ''
    help = 'Import 1C XML data to LFS'

    def handle(self, *args, **options):
        from sshop_import.models import Task
        from sshop_import.scripts.parse import parse_products
        from datetime import datetime
        from django.conf import settings

        SSHOP_1C_TASK_LIMIT_PER_CHECK = getattr(settings, 'SSHOP_1C_TASK_LIMIT_PER_CHECK', 1)
        try: #B:m:001
            if Task.objects.filter(status=30).count() == 0: #If none in processing
                ids = [task.id for task in Task.objects.filter(status=15).order_by('started_at')[:SSHOP_1C_TASK_LIMIT_PER_CHECK]]
                tasks = list(Task.objects.filter(id__in=ids))
                Task.objects.filter(id__in=ids).update(status=30)
                for task in tasks:
                    if task.files.count('\n') == 1:
                        path = task.files.split('\n')[0]
                        finish = parse_products(path)
                        if finish:
                            task.status = 20
                        else:
                            task.status = 25
                        task.finished_at = datetime.now()
                        task.save()
        except Exception, e:
            logger.error('B:m:001 %s' % unicode(e))