# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Task.finish'
        db.delete_column('sshop_import_task', 'finish')

        # Deleting field 'Task.filename'
        db.delete_column('sshop_import_task', 'filename')

        # Deleting field 'Task.performed'
        db.delete_column('sshop_import_task', 'performed')

        # Deleting field 'Task.time'
        db.delete_column('sshop_import_task', 'time')

        # Deleting field 'Task.path'
        db.delete_column('sshop_import_task', 'path')

        # Adding field 'Task.files'
        db.add_column('sshop_import_task', 'files',
                      self.gf('django.db.models.fields.TextField')(default=''),
                      keep_default=False)

        # Adding field 'Task.started_at'
        db.add_column('sshop_import_task', 'started_at',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, auto_now_add=True, blank=True),
                      keep_default=False)

        # Adding field 'Task.finished_at'
        db.add_column('sshop_import_task', 'finished_at',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now),
                      keep_default=False)

        # Adding field 'Task.status'
        db.add_column('sshop_import_task', 'status',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Task.finish'
        db.add_column('sshop_import_task', 'finish',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Task.filename'
        db.add_column('sshop_import_task', 'filename',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=150),
                      keep_default=False)

        # Adding field 'Task.performed'
        db.add_column('sshop_import_task', 'performed',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Task.time'
        db.add_column('sshop_import_task', 'time',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2012, 12, 2, 0, 0)),
                      keep_default=False)

        # Adding field 'Task.path'
        db.add_column('sshop_import_task', 'path',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=500),
                      keep_default=False)

        # Deleting field 'Task.files'
        db.delete_column('sshop_import_task', 'files')

        # Deleting field 'Task.started_at'
        db.delete_column('sshop_import_task', 'started_at')

        # Deleting field 'Task.finished_at'
        db.delete_column('sshop_import_task', 'finished_at')

        # Deleting field 'Task.status'
        db.delete_column('sshop_import_task', 'status')


    models = {
        'sshop_import.task': {
            'Meta': {'object_name': 'Task'},
            'files': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'finished_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'started_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'auto_now_add': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['sshop_import']