# coding: utf-8
from django.contrib import admin
from django_singleton_admin.admin import SingletonAdmin
from .models import OrderNumberGenerator


class OrderNumberGeneratorAdmin(SingletonAdmin):
    readonly_fields = ('id',)

admin.site.register(OrderNumberGenerator, OrderNumberGeneratorAdmin)
