��    Q      �  m   ,      �  	   �     �     �     �                    '     5     <     D     J     V     _     h     {     �  	   �     �     �     �     �     �     �     
          !     =     I     P     X     e     y          �     �  	   �     �     �     �     �     �     �     �     �     �  
   �  D   	     K	     h	     {	     �	     �	     �	     �	     �	     �	  
   �	     �	  #   �	     
     %
  
   ;
     F
     K
     S
     c
     o
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
       �       �     �     �  )   �  
   �     �  
   �     �                    +  	   1  	   ;     E     Z     q  
   z  ,   �  	   �  *   �  
   �     �          !     7  $   <     a     m     u     ~     �     �     �     �     �     �     �     �                 	         *     1     8  
   H  G   S  *   �     �     �     �     �               4     B     J     [  .   c     �     �     �     �     �     �       -   !     O     b     j     q     �     �  '   �     �     �  %   �          "               6          	   ,      1       
             '                    %   *         #   0   N      &   :      O   4   3   @   9   I           "   P       8                        A   B              2   /         C   J           !   H   +   G   F   7   M           L   =   D                          $                      5   -           E       )       <   >      .      K       ;   (      Q   ?    Aggregate Avg Contains Contains (case-insensitive) Copy Count Created Custom Fields Delete Delete? Desc? Description Distinct Download Download full xlsx Drag fields here Edit Ends with Ends with  (case-insensitive) Equals Equals (case-insensitive) Exclude? Expand Related Fields Export to Report Export to report Field Fields - drag fields to add Filter Type Format Formats Greater than Greater than equals Group Home Is null Last updated Less than Less than equals Manage all Reports Max Min Model Modified Name Position Preview Report Properties Python string format. Ex ${} would place a $ in front of the result. Reg. Exp. (case-insensitive) Regular Expression Removes duplicates Report Report Display Fields Report Filters Report builder Report list Reports Root model Save Save and continue to report builder Select and Download Set report fields for Short Name Sort Starred Starred Reports Starts with Starts with (case-insensitive) String Sum Total User created User modified Value View Starred Reports Week day Width Your starred reports in (comma seperated 1,2,3) range Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-23 17:05+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2
 Úhrn Průměr Obsahuje Obsahuje (necitlivé na velikost písmen) Kopírovat Počet Vytvořeno Vlastní pole Vymazat Vymazat? Podrobnosti? Popis Odlišný Stáhnout Stáhnout celý xlsx Přeráhněte pole sem Editovat Končí na Končí na  (necitlivé na velikost písmen) Rovná se Rovná se (necitlivé na velikost písmen) Vyloučit? Rozbalte příbuzná pole Exportovat do zpravy Ezportovat do zprávy Pole Pole - přetahnout pole nebo přidat Filter Type Formát Formáty Větší než Větší než se rovná Skupina Úvod Je null Naposledy aktualizováno Menší než Menší než se rovná Spravovat všechny zprávy Max Min Model Změněno Název Pozice Náhled zprávy Vlastnosti Formát textového řetězce Python. Ex ${} umisti  $ před výsledkem. Reg. Exp. (necitlivé na velikost písmen) Regulární výraz Odstraní duplicity Zpráva Reportovát zobrazená pole Flitry zpráv Konfigurátor zpráv Seznam zpráv Zprávy Kořenový model Uložit Uložit a pokračovat do konfigurátoru zpráv Vyberte a Stáhněte Nastavte pole pro zprávy pro Kratký název Seřadit Označene hvězdičkou Zprávy označené hvězdičkou Začíná na Začíná na (necitlivé na velikost písmen) Textový řetězec Součet Celkem Uživatel vytvořil Uživatel modifikoval Value Zobrazit zprávy označene hvězdičkou Den v týdnu Šířka Vaše zprávy označené hvězdičkou v (čárka odděleně 1,2,3) rozsah 