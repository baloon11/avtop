# coding: utf-8
from django.utils.translation import ugettext_lazy as ugettext
import logging
import os


logger = logging.getLogger('sshop')
# ugettext = lambda s: s

PROJECT_DIR = os.path.dirname(__file__)
PUBLIC_DIR = os.path.join(PROJECT_DIR, 'public')

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Administrator', 'error@the7bits.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'sshop',
        'USER': 'localdev',
        'PASSWORD': '123456',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

TIME_ZONE = 'Europe/Kiev'

LANGUAGES = (
    ('ru', ugettext(u'Русский')),
    ('uk', ugettext(u'Українська')),
    ('en', ugettext(u'English')),
    ('cs', ugettext(u'Čeština')),
)

LANGUAGE_CODE = 'ru'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PUBLIC_DIR, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(PUBLIC_DIR, 'static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
# STATICFILES_DIRS = (
#     os.path.join(PROJECT_DIR, 'static'),
# )

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'n!2b7zw#(1k8q6^i%(!)dz7ye_@7t=@x=nn=2#x74p0vulr&amp;io'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    # ('django.template.loaders.cached.Loader', (
    #     'django.template.loaders.filesystem.Loader',
    #     'django.template.loaders.app_directories.Loader',
    # )),
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #'django.template.loaders.cached.Loader',
    #     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'sshop.middleware.DisableClientCachingMiddleware',
    # 'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.doc.XViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'sshop.middleware.TimezoneMiddleware',
    'sshop.middleware.Http403Middleware',
    'lfs.utils.middleware.RedirectFallbackMiddleware',
    'pagination.middleware.PaginationMiddleware',
    # 'django.middleware.gzip.GZipMiddleware',
    'maintenance.middleware.MaintenanceMiddleware',
    'sshop.middleware.DeleteNoneMiddleware',
    'sshop.middleware.MakeLowerUrlMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    'django.core.context_processors.csrf',
    "django.contrib.messages.context_processors.messages",
    'django.core.context_processors.request',
    'lfs.core.context_processors.main',
)

ROOT_URLCONF = 'sshop.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'sshop.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, 'templates'),
)


INSTALLED_APPS = (
    'basic_theme',  # Shop theme
    'compressor',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'adminextras',
    'suit',
    'logtailer',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.webdesign',
    'django.contrib.markup',
    # 'django.contrib.comments',
    # 'django.contrib.admindocs',
    'django.contrib.redirects',
    'django_extensions',
    'djsupervisor',
    'filebrowser',
    'south',
    'mptt',
    'filer',
    'easy_thumbnails',
    'django_nose',
    # 'lettuce.django',
    'bootstrap_toolkit',
    'smuggler',
    'autofixture',
    'django_countries',
    'pagination',
    'reviews',
    'admininfo',
    'adminconfig',
    'diagnostic',
    'macroses',
    'portlets',
    'lfs',
    'lfs.tests',
    'lfs.core',
    'lfs.caching',
    'lfs.cart',
    'lfs.catalog',
    'lfs.filters',
    'lfs.checkout',
    'lfs.criteria',
    'lfs.customer',
    'lfs.discounts',
    'lfs.mail',
    'lfs.marketing',
    'lfs.manufacturer',
    'lfs.order',
    'lfs.page',
    'lfs.payment',
    'lfs.portlet',
    'lfs.search',
    'lfs.shipping',
    'lfs.utils',
    'lfs.voucher',
    'lfs_contact',
    'lfs_order_numbers',
    'lfs.fields',
    'genericadmin',
    'autocomplete_light',
    'django_select2',
    'django_mptt_admin',
    'treeadmin',
    'sshop_currencies',
    'sshop_import',
    'tasks',
    'vertical_menu',
    'lfs.faq',
    'searchapi',
    'loginza',
    'question',
    'callback',
    'sms',
    # 'price_navigator',
    'qaptcha',
    'form_designer',
    'hr_urls',
    'suit_ckeditor',
    'codemirror',
    'maintenance',
    'captchas',
    'datetimewidget',
    'django_singleton_admin',
    'report_builder',
    'simple_import',
    #'visual_search',
    # 'tire_shop.wheels_auto',
    # 'tire_shop.wheels_truck',
    # 'tire_shop.wheels_moto',
    # 'tire_shop.tires_variants',
)


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format':
            '%(levelname)s %(asctime)s %(pathname)s:%(lineno)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'file': {
            # 'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': os.path.join(PROJECT_DIR, 'error.log'),
            'formatter': 'verbose'
        },
        # 'console': {
        #     'level': 'DEBUG',
        #     'class': 'logging.StreamHandler',
        #     'formatter': 'simple',
        # },
    },
    'loggers': {
        'sshop': {
            'handlers': ['file'],
            # 'level': 'ERROR',
            'propagate': True,
        },
        # 'django': {
        #     'handlers': ['console'],
        #     'level': 'DEBUG',
        # },
    }
}

LOGTAILER_HISTORY_LINES = 20

SESSION_SAVE_EVERY_REQUEST = True

REVIEWS_SHOW_PREVIEW = False
REVIEWS_IS_NAME_REQUIRED = True
REVIEWS_IS_EMAIL_REQUIRED = False
REVIEWS_IS_MODERATED = False

FORCE_SCRIPT_NAME = ""
LOGIN_URL = "/login/"
LOGIN_REDIRECT_URL = '/'
LOGIN_REDIRECT_URLNAME = 'next'
# LOGIN_REDIRECT_URL = '/accounts/%(username)s/'
# LOGIN_URL = '/accounts/signin/'
# LOGOUT_URL = '/accounts/signout/'

AUTHENTICATION_BACKENDS = (
    # 'lfs.customer.auth.EmailBackend',
    'django.contrib.auth.backends.ModelBackend',
    'loginza.authentication.LoginzaBackend',
)

# For sql_queries
INTERNAL_IPS = (
    "127.0.0.1",
)

CACHES = {
    'default': {
        # 'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}
CACHE_MIDDLEWARE_KEY_PREFIX = 'sshop'

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
SOUTH_TESTS_MIGRATE = False

NOSE_ARGS = [
    # '--with-coverage',
    # '--cover-erase',
    # '--cover-branches',
    # '--cover-package=profiles',
    # '--cover-html',
    '--with-progressive',
    '--logging-clear-handlers',
    '--failed',
    '--stop',
    # '--cover-package=lfs.catalog,lfs.voucher,lfs.marketing,'
    # 'lfs.discounts',
]

LFS_AFTER_ADD_TO_CART = "lfs_added_to_cart"
LFS_RECENT_PRODUCTS_LIMIT = 5

LFS_ORDER_NUMBER_GENERATOR = "lfs_order_numbers.models.OrderNumberGenerator"
LFS_DOCS = "http://docs.getlfs.com/en/latest/"

LFS_INVOICE_COMPANY_NAME_REQUIRED = False
LFS_INVOICE_EMAIL_REQUIRED = True
LFS_INVOICE_PHONE_REQUIRED = True

LFS_SHIPPING_COMPANY_NAME_REQUIRED = False
LFS_SHIPPING_EMAIL_REQUIRED = False
LFS_SHIPPING_PHONE_REQUIRED = False

LFS_PRICE_CALCULATORS = [
    [
        'lfs.default_price.DefaultPriceCalculator',
        ugettext(u'Default price calculator')
    ],
]

SSHOP_PAYMENT_METHOD_PROCESSORS = []

SSHOP_DEFAULT_PRICE_CALCULATOR = 0

LFS_SHIPPING_METHOD_PRICE_CALCULATORS = [
    [
        'lfs.default_price.DefaultShippingPriceCalculator',
        ugettext(u'Default shipping price calculator')
    ],
]

LFS_UNITS = [
    u"м",
    u"кв.м",
    u"см",
    u"уп",
    u"шт",
]

LFS_PRICE_UNITS = LFS_BASE_PRICE_UNITS = LFS_UNITS

LFS_LOG_FILE = PUBLIC_DIR + '/../lfs.log'

PAGINATION_PAGE_NAME_TEMPLATE = "page_number"

REVIEWS_SHOW_PREVIEW = False
REVIEWS_IS_NAME_REQUIRED = True
REVIEWS_IS_EMAIL_REQUIRED = False
REVIEWS_IS_MODERATED = False

# FILE_UPLOAD_TEMP_DIR = os.path.join(PROJECT_DIR, "tmp/")
# FILE_UPLOAD_MAX_MEMORY_SIZE = 0
#TODO: fix this
# DIR_FOR_1C = '/home/drom/temp/'
# DIR_FOR_FILTERS = '/home/drom/temp/'
# DIR_FOR_ERRORS = '/home/drom/temp/'

FILE_UPLOAD_HANDLERS = (
    "django.core.files.uploadhandler.MemoryFileUploadHandler",
    "django.core.files.uploadhandler.TemporaryFileUploadHandler",
    "sshop_import.views.SshopFileUploadHandler",
    # "sshop_import.views.UploadProgressCachedHandler",
)
COMPRESS_PRECOMPILERS = (
    ('text/coffeescript', 'coffee --compile --stdio'),
    ('text/less', 'lessc {infile} {outfile}'),
    ('text/stylus', 'stylus < {infile} > {outfile}'),
)


SSHOP_SLIDER_FILTER_STEPS = 10
CACHE_VIEW_TIMEOUT = 60 * 50

# Default settings
ALLOW_REGIONS = False

ENABLE_FULL_TEXT_SEARCH = True

DEMO_MODE = False

ALLOW_BACKGROUND_REGISTRATION = False

SSHOP_VERSION = '0.3.2o'

SSHOP_IMAGE_SIZES = {
    'small': (60, 60),
    'medium': (100, 100),
    'large': (200, 200),
    'huge': (400, 400),
}

LOGINZA_DEFAULT_PROVIDERS_SET = 'google,facebook,twitter,vkontakte,yandex'

USE_HR_URLS = False

SSHOP_DEFAULT_SLUG = None

SSHOP_USE_1C = True


SEARCH_CONFIGS = [
    (
        'searchapi.searchconfigs.SimpleSearchConfig',
        ugettext(u'Simple search config')
    ),
    # (
    #     'searchapi.searchconfigs.FuzzyTextSearchConfig',  # DEPRECATED
    #     ugettext(u'Fuzzy fulltext search config')
    # ),
]
FUZZY_SEARCH_MIN_DISTANCE = .1  # DEPRECATED


# LETTUCE_APPS = (
#     'lfs.core',
#     'qaptcha',
#     'lfs.manage',
# )

#Qaptcha settings
#timeout in minutes
CAPTCHA_TIMEOUT = 20

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    #'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)

# Extend default static into base.html
EXTRA_JS = [
    'js/loadover.js',
]
EXTRA_CSS = [
    'css/loadover.css',
]
ADMIN_ROOT_PATH = 'superadmin/'

ADMIN_FILTER_OPTION_PER_PAGE = 10

FILTER_SORTERS = (
    ('asc', 'lfs.filters.sorters.AscFilterOptionSorter'),
    ('desc', 'lfs.filters.sorters.DescFilterOptionSorter'),
    None,
    ('count', 'lfs.filters.sorters.CountAZFilterOptionSorter'),
    ('-count', 'lfs.filters.sorters.CountZAFilterOptionSorter'),
    None,
    ('numberasc', 'lfs.filters.sorters.NumberAZFilterOptionSorter'),
    ('numberdesc', 'lfs.filters.sorters.NumberZAFilterOptionSorter'),
)

PROPERTY_SORTERS = (
    ('asc', 'lfs.catalog.sorters.AscPropertyOptionSorter'),
    ('desc', 'lfs.catalog.sorters.DescPropertyOptionSorter'),
    ('numberasc', 'lfs.catalog.sorters.NumberAZPropertyOptionSorter'),
    ('numberdesc', 'lfs.catalog.sorters.NumberZAPropertyOptionSorter'),
)

HELPBAR_DOCS_REPOSITORY = 'http://sitepanel.the7bits.com/pages/'

HELPBAR_URLS = (
    ((r'^/'+ADMIN_ROOT_PATH+'auth/user/$',), 'pcart-list-users'),
    ((r'^/'+ADMIN_ROOT_PATH+'auth/user/add/$',), 'pcart-add-user'),
    ((r'^/'+ADMIN_ROOT_PATH+'auth/user/\d+/$',), 'pcart-edit-user'),
    ((r'^/'+ADMIN_ROOT_PATH+'auth/user/\d+/password/$',),
        'pcart-change-password'),

    ((r'^/'+ADMIN_ROOT_PATH+'auth/group/$',), 'pcart-list-groups'),
    ((r'^/'+ADMIN_ROOT_PATH+'auth/group/add/$',), 'pcart-add-group'),
    ((r'^/'+ADMIN_ROOT_PATH+'auth/group/\d+/$',), 'pcart-edit-group'),

    ((r'^/'+ADMIN_ROOT_PATH+'loginza/identity/$',), 'pcart-list-identities'),
    ((r'^/'+ADMIN_ROOT_PATH+'loginza/identity/add/$',), 'pcart-add-identity'),
    ((r'^/'+ADMIN_ROOT_PATH+'loginza/identity/\d+/$',), 'pcart-edit-identity'),

    ((r'^/'+ADMIN_ROOT_PATH+'loginza/usermap/$',), 'pcart-list-usermaps'),
    ((r'^/'+ADMIN_ROOT_PATH+'loginza/usermap/add/$',), 'pcart-add-usermap'),
    ((r'^/'+ADMIN_ROOT_PATH+'loginza/usermap/\d+/$',), 'pcart-edit-usermap'),

    ((r'^/'+ADMIN_ROOT_PATH+'catalog/category/$',), 'pcart-list-categories'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/category/add/$',), 'pcart-add-category'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/category/\d+/$',), 'pcart-edit-category'),

    ((r'^/'+ADMIN_ROOT_PATH+'filters/autoconfigure/\d+$',),
        'pcart-autoconfigurer'),
    ((r'^/'+ADMIN_ROOT_PATH+'filters/filter/add/$',), 'pcart-add-filter'),
    ((r'^/'+ADMIN_ROOT_PATH+'filters/filter/\d+/$',), 'pcart-edit-filter'),
    ((r'^/'+ADMIN_ROOT_PATH+'filters/filteroption/add/$',),
        'pcart-add-filteroption'),
    ((r'^/'+ADMIN_ROOT_PATH+'filters/filteroption/\d+/$',),
        'pcart-edit-filteroption'),

    ((r'^/'+ADMIN_ROOT_PATH+'catalog/product/$',), 'pcart-list-products'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/product/add/$',), 'pcart-add-product'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/product/\d+/$',), 'pcart-edit-product'),

    ((r'^/'+ADMIN_ROOT_PATH+'catalog/property/$',), 'pcart-list-properties'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/property/add/$',), 'pcart-edit-property'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/property/\d+/$',),
        'pcart-edit-property'),

    ((r'^/'+ADMIN_ROOT_PATH+'manufacturer/manufacturer/$',),
        'pcart-list-manufacturers'),
    ((r'^/'+ADMIN_ROOT_PATH+'manufacturer/manufacturer/add/$',),
        'pcart-add-manufacturer'),
    ((r'^/'+ADMIN_ROOT_PATH+'manufacturer/manufacturer/\d+/$',),
        'pcart-edit-manufacturer'),

    ((r'^/'+ADMIN_ROOT_PATH+'payment/paymentmethod/$',),
        'pcart-list-paymentmethods'),
    ((r'^/'+ADMIN_ROOT_PATH+'payment/paymentmethod/add/$',),
        'pcart-add-paymentmethod'),
    ((r'^/'+ADMIN_ROOT_PATH+'payment/paymentmethod/\d+/$',),
        'pcart-edit-paymentmethod'),

    ((r'^/'+ADMIN_ROOT_PATH+'shipping/shippingmethod/$',),
        'pcart-list-shippingmethods'),
    ((r'^/'+ADMIN_ROOT_PATH+'shipping/shippingmethod/add/$',),
        'pcart-add-shippingmethod'),
    ((r'^/'+ADMIN_ROOT_PATH+'shipping/shippingmethod/\d+/$',),
        'pcart-edit-shippingmethod'),

    ((r'^/'+ADMIN_ROOT_PATH+'sshop_currencies/currency/$',),
        'pcart-list-currency'),
    ((r'^/'+ADMIN_ROOT_PATH+'sshop_currencies/currency/add/$',),
        'pcart-add-currency'),
    ((r'^/'+ADMIN_ROOT_PATH+'sshop_currencies/currency/\d+/$',),
        'pcart-edit-currency'),

    ((r'^/'+ADMIN_ROOT_PATH+'page/page/$',), 'pcart-list-pages'),
    ((r'^/'+ADMIN_ROOT_PATH+'page/page/add/$',), 'pcart-add-page'),
    ((r'^/'+ADMIN_ROOT_PATH+'page/page/\d+/$',), 'pcart-edit-page'),

    ((r'^/'+ADMIN_ROOT_PATH+'core/action/$',), 'pcart-list-actions'),
    ((r'^/'+ADMIN_ROOT_PATH+'core/action/add/$',), 'pcart-add-action'),
    ((r'^/'+ADMIN_ROOT_PATH+'core/action/\d+/$',),
        'pcart-edit-action'),

    ((r'^/'+ADMIN_ROOT_PATH+'catalog/sorttype/$',), 'pcart-list-sorttypes'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/sorttype/add/$',), 'pcart-add-sorttype'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/sorttype/\d+/$',),
        'pcart-edit-sorttype'),

    # ((r'^/'+ADMIN_ROOT_PATH+'price_navigator/pricenavigator/$',),
    #     'pcart-list-pricenavigators'),
    # ((r'^/'+ADMIN_ROOT_PATH+'price_navigator/pricenavigator/add/$',),
    #     'pcart-add-pricenavigator'),
    # ((r'^/'+ADMIN_ROOT_PATH+'price_navigator/pricenavigator/\d+/$',),
    #     'pcart-edit-pricenavigator'),

    ((r'^/'+ADMIN_ROOT_PATH+'question/question/$',), 'pcart-list-questions'),
    ((r'^/'+ADMIN_ROOT_PATH+'question/question/add/$',),
        'pcart-edit-question'),
    ((r'^/'+ADMIN_ROOT_PATH+'question/question/\d+/$',),
        'pcart-edit-question'),

    ((r'^/'+ADMIN_ROOT_PATH+'info/system-info/$',), 'pcart-info-systeminfo'),

    ((r'^/'+ADMIN_ROOT_PATH+'config/general/$',), 'pcart-config-general'),
    ((r'^/'+ADMIN_ROOT_PATH+'config/seo/$',), 'pcart-config-seo'),
    ((r'^/'+ADMIN_ROOT_PATH+'config/captcha/$',), 'pcart-config-captcha'),
    ((r'^/'+ADMIN_ROOT_PATH+'config/login/$',), 'pcart-config-login'),
    ((r'^/'+ADMIN_ROOT_PATH+'config/registration/$',),
        'pcart-config-registration'),
    ((r'^/'+ADMIN_ROOT_PATH+'config/customer/$',), 'pcart-config-customer'),
    ((r'^/'+ADMIN_ROOT_PATH+'config/email/$',), 'pcart-config-email'),
    ((r'^/'+ADMIN_ROOT_PATH+'config/checkout/$',), 'pcart-config-checkout'),
    ((r'^/'+ADMIN_ROOT_PATH+'config/message_sending/$',),
        'pcart-config-message_sending'),
    ((r'^/'+ADMIN_ROOT_PATH+'config/catalog/$',), 'pcart-config-catalog'),
    ((r'^/'+ADMIN_ROOT_PATH+'config/tasks/$',), 'pcart-config-tasks'),

    ((r'^/'+ADMIN_ROOT_PATH+'redirects/redirect/$',), 'pcart-list-redirect'),
    ((r'^/'+ADMIN_ROOT_PATH+'redirects/redirect/add/$',),
        'pcart-edit-redirect'),
    ((r'^/'+ADMIN_ROOT_PATH+'redirects/redirect/\d+/$',),
        'pcart-edit-redirect'),

    ((r'^/'+ADMIN_ROOT_PATH+'sites/site/$',), 'pcart-list-site'),
    ((r'^/'+ADMIN_ROOT_PATH+'sites/site/add/$',),
        'pcart-edit-site'),
    ((r'^/'+ADMIN_ROOT_PATH+'sites/site/\d+/$',),
        'pcart-edit-site'),

    ((r'^/'+ADMIN_ROOT_PATH+'portlets/portletregistration/$',),
        'pcart-list-portletregistration'),
    ((r'^/'+ADMIN_ROOT_PATH+'portlets/portletregistration/add/$',),
        'pcart-edit-portletregistration'),
    ((r'^/'+ADMIN_ROOT_PATH+'portlets/portletregistration/\d+/$',),
        'pcart-edit-portletregistration'),

    ((r'^/'+ADMIN_ROOT_PATH+'portlets/slot/$',),
        'pcart-list-slot'),
    ((r'^/'+ADMIN_ROOT_PATH+'portlets/slot/add/$',),
        'pcart-edit-slot'),
    ((r'^/'+ADMIN_ROOT_PATH+'portlets/slot/\d+/$',),
        'pcart-edit-slot'),

    ((r'^/'+ADMIN_ROOT_PATH+'filer/folder/$',),
        'pcart-filer-folder'),

    ((r'^/'+ADMIN_ROOT_PATH+'tasks/job/$',),
        'pcart-list-job'),
    ((r'^/'+ADMIN_ROOT_PATH+'tasks/job/add/$',),
        'pcart-edit-job'),
    ((r'^/'+ADMIN_ROOT_PATH+'tasks/job/\d+/$',),
        'pcart-edit-job'),

    ((r'^/'+ADMIN_ROOT_PATH+'maintenance/maintenancemessage/$',),
        'pcart-list-maintenancemessage'),
    ((r'^/'+ADMIN_ROOT_PATH+'maintenance/maintenancemessage/add/$',),
        'pcart-edit-maintenancemessage'),
    ((r'^/'+ADMIN_ROOT_PATH+'maintenance/maintenancemessage/\d+/$',),
        'pcart-edit-maintenancemessage'),

    ((r'^/'+ADMIN_ROOT_PATH+'admin/logentry/$',),
        'pcart-admin-logentry'),

    ((r'^/'+ADMIN_ROOT_PATH+'logtailer/logfile/$',),
        'pcart-list-logfile'),
    ((r'^/'+ADMIN_ROOT_PATH+'logtailer/logfile/add/$',),
        'pcart-add-logfile'),
    ((r'^/'+ADMIN_ROOT_PATH+'logtailer/logfile/\d+/$',),
        'pcart-edit-logfile'),
    ((r'^/'+ADMIN_ROOT_PATH+'logtailer/filter/$',),
        'pcart-list-logfilter'),
    ((r'^/'+ADMIN_ROOT_PATH+'logtailer/filter/add/$',),
        'pcart-edit-logfilter'),
    ((r'^/'+ADMIN_ROOT_PATH+'logtailer/filter/\d+/$',),
        'pcart-edit-logfilter'),
    ((r'^/'+ADMIN_ROOT_PATH+'logtailer/logsclipboard/$',),
        'pcart-list-logsclipboard'),
    ((r'^/'+ADMIN_ROOT_PATH+'logtailer/logsclipboard/add/$',),
        'pcart-edit-logsclipboard'),
    ((r'^/'+ADMIN_ROOT_PATH+'logtailer/logsclipboard/\d+/$',),
        'pcart-edit-logsclipboard'),

    ((r'^/'+ADMIN_ROOT_PATH+'filebrowser/browse/$',),
        'pcart-admin-filebrowser'),

    ((r'^/'+ADMIN_ROOT_PATH+'voucher/vouchergroup/$',),
        'pcart-list-vouchergroup'),
    ((r'^/'+ADMIN_ROOT_PATH+'voucher/vouchergroup/add/$',),
        'pcart-edit-vouchergroup'),
    ((r'^/'+ADMIN_ROOT_PATH+'voucher/vouchergroup/\d+/$',),
        'pcart-edit-vouchergroup'),
    ((r'^/'+ADMIN_ROOT_PATH+'voucher/voucher/$',),
        'pcart-list-voucher'),
    ((r'^/'+ADMIN_ROOT_PATH+'voucher/voucher/add/$',),
        'pcart-edit-voucher'),
    ((r'^/'+ADMIN_ROOT_PATH+'voucher/voucher/\d+/$',),
        'pcart-edit-voucher'),

    ((r'^/'+ADMIN_ROOT_PATH+'discounts/discount/$',),
        'pcart-list-discount'),
    ((r'^/'+ADMIN_ROOT_PATH+'discounts/discount/add/$',),
        'pcart-add-discount'),
    ((r'^/'+ADMIN_ROOT_PATH+'discounts/discount/\d+/$',),
        'pcart-edit-discount'),

    ((r'^/'+ADMIN_ROOT_PATH+'marketing/featuredproduct/$',),
        'pcart-list-featuredproduct'),
    ((r'^/'+ADMIN_ROOT_PATH+'marketing/featuredproduct/add/$',),
        'pcart-edit-featuredproduct'),
    ((r'^/'+ADMIN_ROOT_PATH+'marketing/featuredproduct/\d+/$',),
        'pcart-edit-featuredproduct'),
    ((r'^/'+ADMIN_ROOT_PATH+'marketing/topseller/$',),
        'pcart-list-topseller'),
    ((r'^/'+ADMIN_ROOT_PATH+'marketing/topseller/add/$',),
        'pcart-edit-topseller'),
    ((r'^/'+ADMIN_ROOT_PATH+'marketing/topseller/\d+/$',),
        'pcart-edit-topseller'),

    ((r'^/'+ADMIN_ROOT_PATH+'marketing/productlist/$',),
        'pcart-list-productlist'),
    ((r'^/'+ADMIN_ROOT_PATH+'marketing/productlist/add/$',),
        'pcart-edit-productlist'),
    ((r'^/'+ADMIN_ROOT_PATH+'marketing/productlist/\d+/$',),
        'pcart-edit-productlist'),
    ((r'^/'+ADMIN_ROOT_PATH+'marketing/productlistitem/$',),
        'pcart-list-productlistitem'),
    ((r'^/'+ADMIN_ROOT_PATH+'marketing/productlistitem/add/$',),
        'pcart-edit-productlistitem'),
    ((r'^/'+ADMIN_ROOT_PATH+'marketing/productlistitem/\d+/$',),
        'pcart-edit-productlistitem'),

    ((r'^/'+ADMIN_ROOT_PATH+'faq/topic/$',),
        'pcart-list-faqtopic'),
    ((r'^/'+ADMIN_ROOT_PATH+'faq/topic/add/$',),
        'pcart-edit-faqtopic'),
    ((r'^/'+ADMIN_ROOT_PATH+'faq/topic/\d+/$',),
        'pcart-edit-faqtopic'),
    ((r'^/'+ADMIN_ROOT_PATH+'faq/question/$',),
        'pcart-list-faqquestion'),
    ((r'^/'+ADMIN_ROOT_PATH+'faq/question/add/$',),
        'pcart-edit-faqquestion'),
    ((r'^/'+ADMIN_ROOT_PATH+'faq/question/\d+/$',),
        'pcart-edit-faqquestion'),

    ((r'^/'+ADMIN_ROOT_PATH+'customer/customer/$',),
        'pcart-list-customer'),
    ((r'^/'+ADMIN_ROOT_PATH+'customer/customer/add/$',),
        'pcart-edit-customer'),
    ((r'^/'+ADMIN_ROOT_PATH+'customer/customer/\d+/$',),
        'pcart-edit-customer'),
    ((r'^/'+ADMIN_ROOT_PATH+'cart/cart/$',),
        'pcart-list-cart'),
    ((r'^/'+ADMIN_ROOT_PATH+'cart/cart/add/$',),
        'pcart-edit-cart'),
    ((r'^/'+ADMIN_ROOT_PATH+'cart/cart/\d+/$',),
        'pcart-edit-cart'),

    ((r'^/'+ADMIN_ROOT_PATH+'order/order/$',),
        'pcart-list-order'),
    ((r'^/'+ADMIN_ROOT_PATH+'order/order/add/$',),
        'pcart-edit-order'),
    ((r'^/'+ADMIN_ROOT_PATH+'order/order/\d+/$',),
        'pcart-edit-order'),
    ((r'^/'+ADMIN_ROOT_PATH+'order/order-preview/\d+/$',),
        'pcart-edit-orderpreview'),

    ((r'^/'+ADMIN_ROOT_PATH+'reviews/review/$',),
        'pcart-list-review'),
    ((r'^/'+ADMIN_ROOT_PATH+'reviews/review/add/$',),
        'pcart-edit-review'),
    ((r'^/'+ADMIN_ROOT_PATH+'reviews/review/\d+/$',),
        'pcart-edit-review'),

    ((r'^/'+ADMIN_ROOT_PATH+'catalog/staticblock/$',),
        'pcart-list-staticblock'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/staticblock/add/$',),
        'pcart-edit-staticblock'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/staticblock/\d+/$',),
        'pcart-edit-staticblock'),

    ((r'^/'+ADMIN_ROOT_PATH+'mail/mailtemplate/$',),
        'pcart-list-mailtemplate'),
    ((r'^/'+ADMIN_ROOT_PATH+'mail/mailtemplate/add/$',),
        'pcart-edit-mailtemplate'),
    ((r'^/'+ADMIN_ROOT_PATH+'mail/mailtemplate/\d+/$',),
        'pcart-edit-mailtemplate'),

    ((r'^/'+ADMIN_ROOT_PATH+'form_designer/formlog/$',),
        'pcart-list-formlog'),
    ((r'^/'+ADMIN_ROOT_PATH+'form_designer/formdefinition/$',),
        'pcart-list-formdefinition'),
    ((r'^/'+ADMIN_ROOT_PATH+'form_designer/formdefinition/add/$',),
        'pcart-edit-formdefinition'),
    ((r'^/'+ADMIN_ROOT_PATH+'form_designer/formdefinition/\d+/$',),
        'pcart-edit-formdefinition'),

    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertyvalueicon/$',),
        'pcart-list-propertyvalueicon'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertyvalueicon/add/$',),
        'pcart-edit-propertyvalueicon'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertyvalueicon/\d+/$',),
        'pcart-edit-propertyvalueicon'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertyunit/$',),
        'pcart-list-propertyunit'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertyunit/add/$',),
        'pcart-edit-propertyunit'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertyunit/\d+/$',),
        'pcart-edit-propertyunit'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertyoption/$',),
        'pcart-list-propertyoption'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertyoption/add/$',),
        'pcart-edit-propertyoption'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertyoption/\d+/$',),
        'pcart-edit-propertyoption'),

    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertyset/$',),
        'pcart-list-propertyset'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertyset/add/$',),
        'pcart-edit-propertyset'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertyset/\d+/$',),
        'pcart-edit-propertyset'),

    ((r'^/'+ADMIN_ROOT_PATH+'core/shop/$',),
        'pcart-list-shop'),
    ((r'^/'+ADMIN_ROOT_PATH+'core/shop/add/$',),
        'pcart-add-shop'),
    ((r'^/'+ADMIN_ROOT_PATH+'core/shop/\d+/$',),
        'pcart-edit-shop'),
    ((r'^/'+ADMIN_ROOT_PATH+'core/actiongroup/$',),
        'pcart-list-actiongroup'),
    ((r'^/'+ADMIN_ROOT_PATH+'core/actiongroup/add/$',),
        'pcart-add-actiongroup'),
    ((r'^/'+ADMIN_ROOT_PATH+'core/actiongroup/\d+/$',),
        'pcart-edit-actiongroup'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/productstatus/$',),
        'pcart-list-productstatus'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/productstatus/add/$',),
        'pcart-edit-productstatus'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/productstatus/\d+/$',),
        'pcart-edit-productstatus'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/deliverytime/$',),
        'pcart-list-deliverytime'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/deliverytime/add/$',),
        'pcart-edit-deliverytime'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/deliverytime/\d+/$',),
        'pcart-edit-deliverytime'),

    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertytype/$',),
        'pcart-list-propertytype'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertytype/add/$',),
        'pcart-edit-propertytype'),
    ((r'^/'+ADMIN_ROOT_PATH+'catalog/propertytype/\d+/$',),
        'pcart-edit-propertytype'),

    ((r'^/simple_import/start_import/$',),
        'pcart-start-simpleimport'),
    ((r'^/'+ADMIN_ROOT_PATH+'sessions/session/$',),
        'pcart-list-session'),

    ((r'^/'+ADMIN_ROOT_PATH+'macroses/macros/$',),
        'pcart-list-macros'),
    ((r'^/'+ADMIN_ROOT_PATH+'macroses/macros/add/$',),
        'pcart-edit-macros'),
    ((r'^/'+ADMIN_ROOT_PATH+'macroses/macros/\d+/$',),
        'pcart-edit-macros'),
    (
        (r'^/'+ADMIN_ROOT_PATH+'config/compatibility/$',),
        'pcart-config-compatibility'
    ),
    (
        (r'^/'+ADMIN_ROOT_PATH+'config/reviews/$',),
        'pcart-config-reviews'
    ),
    (
        (r'^/'+ADMIN_ROOT_PATH+'config/pcart_page/$',),
        'pcart-config-pages'
    ),
    (
        (r'^/'+ADMIN_ROOT_PATH+'config/filters/$',),
        'pcart-config-filters'
    ),
    (
        (r'^/'+ADMIN_ROOT_PATH+'config/pagination/$',),
        'pcart-config-pagination'
    ),
    (
        (r'^/'+ADMIN_ROOT_PATH+'config/portlets_errors/$',),
        'pcart-config-portlets-errors'
    ),
)

HELPBAR_INDEX_URL = 'p-cart'

DEFAULT_CATEGORY_MODE = 'list'  # list or table
DEFAULT_SORTING = 'effective_price'

ADMIN_PROPERTY_OPTION_FOR_PAGE = 10

SMART_DEFINE_PROPERTY_TYPE = False
IGNORE_EMPTY_PROPERTY = False
FILTER_SHOW_ALL_PRODUCTS = True

AJAX_LOOKUP_CHANNELS = {
    'property': ('lfs.catalog.lookups', 'PropertyLookup'),
}

PROPERTY_TYPE_VALIDATORS = (
    ('', ugettext(u'Standard regex and rules')),
)

SEO_DATA_GENERATORS = (
    ('lfs.core.seo.CoreSEOGenerator', 0),
    ('lfs.page.seo.PageSEOGenerator', 5),
    ('lfs.catalog.seo.ProductSEOGenerator', 10),
    ('lfs.catalog.seo.CategorySEOGenerator', 15),

)

PRODUCT_PROPERTY_TRANSLATORS = {
    'test': {
        u'test_value': u'test_value - test_description',
    },
}

# Extend this option if you want to add new sitemaps to sitemap.xml
# For example:
#
# EXTRA_SITEMAPS = (
#     {
#         'url': r'^sitemap/news\.xml$',
#         'module': 'lfs.core.sitemap',
#         'view': 'pages_sitemap',
#     },
# )

EXTRA_SITEMAPS = tuple()

# SEO meta templates

# SEO_USE_DJANGO_TEMPLATES = True  # DEPRECATED
SHOW_SEO_TEXT_ON_FILTERED_PAGES = False
DEFAULT_PRODUCT_META_H1_TEMPLATE = u'{{ name }}'
DEFAULT_CATEGORY_META_H1_TEMPLATE = u'''
{{ name }}{% if filtered %} ({{ products_count }}){% endif %}'''
DEFAULT_PAGE_META_H1_TEMPLATE = u'{{ title }}'

DEFAULT_SHOP_META_TITLE_TEMPLATE = u'{{ shop_name }}'
DEFAULT_CATEGORY_META_TITLE_TEMPLATE = u'{{ name }} - {{ shop_name }}'
DEFAULT_PRODUCT_META_TITLE_TEMPLATE = u'{{ name }} - {{ shop_name }}'
DEFAULT_PAGE_META_TITLE_TEMPLATE = u'{{ name }} - {{ shop_name }}'
DEFAULT_OTHER_META_TITLE_TEMPLATE = u'{{ name }} - {{ shop_name }}'

DEFAULT_SHOP_META_KEYWORDS_TEMPLATE = u''
DEFAULT_CATEGORY_META_KEYWORDS_TEMPLATE = u''
DEFAULT_PRODUCT_META_KEYWORDS_TEMPLATE = u''
DEFAULT_PAGE_META_KEYWORDS_TEMPLATE = u''
DEFAULT_OTHER_META_KEYWORDS_TEMPLATE = u''

DEFAULT_SHOP_META_DESCRIPTION_TEMPLATE = u''
DEFAULT_CATEGORY_META_DESCRIPTION_TEMPLATE = u''
DEFAULT_PRODUCT_META_DESCRIPTION_TEMPLATE = u''
DEFAULT_PAGE_META_DESCRIPTION_TEMPLATE = u''
DEFAULT_OTHER_META_DESCRIPTION_TEMPLATE = u''

DEFAULT_SHOP_META_TEXT_TEMPLATE = u''
DEFAULT_CATEGORY_META_TEXT_TEMPLATE = u''
DEFAULT_PRODUCT_META_TEXT_TEMPLATE = u''
DEFAULT_PAGE_META_TEXT_TEMPLATE = u''
DEFAULT_OTHER_META_TEXT_TEMPLATE = u''


CODEMIRROR_THEME = 'neat'

ADMIN_AJAX_FORBIDDEN_URL = '/admin-ajax-forbidden/'

# Configurers for admin interface

ADMIN_CONFIGURERS = (
    ('general', ugettext(u'General'), 'lfs.core.configurer.GeneralShopConfig'),
    ('seo', ugettext(u'SEO'), 'lfs.core.configurer.SeoConfig'),
    ('captcha', ugettext(u'Captcha'), 'captchas.configurer.CaptchaConfig'),
    ('login', ugettext(u'Login'), 'lfs.customer.configurer.LoginConfig'),
    ('registration', ugettext(u'Registration'),
        'lfs.customer.configurer.RegistrationConfig'),
    ('customer', ugettext(u'Customers'),
        'lfs.customer.configurer.CustomerConfig'),
    # FIX: send_mail use str, but stored unicode
    # ('email', ugettext(u'Email'),
    #     'lfs.mail.configurer.EmailConfig'),
    ('checkout', ugettext(u'Checkout'),
        'lfs.checkout.configurer.CheckoutConfig'),
    ('message_sending', ugettext(u'Message sending'),
        'lfs.mail.configurer.MessageSendingConfig'),
    ('catalog', ugettext(u'Catalog'),
        'lfs.catalog.configurer.CatalogConfig'),
    ('tasks', ugettext(u'Task priorities'),
        'tasks.configurer.TaskPriorityConfig'),
    ('compatibility', ugettext(u'Compatibility'),
        'lfs.core.configurer.CompatibilityConfig'),
    ('reviews', ugettext(u'Reviews'),
        'reviews.configurer.ReviewsConfig'),
    ('pcart_page', ugettext(u'Pages'),
        'lfs.page.configurer.PcartPagesConfig'),
    ('filters', ugettext(u'Filter'), 'lfs.filters.configurer.FiltersConfig'),
    ('pagination', ugettext(u'Pagination'),
        'lfs.core.configurer.PaginationConfig'),
    ('portlets_errors', ugettext(u'Portlets'),
        'lfs.portlet.configurer.PortletsConfig'),
)

GLOBAL_JSON_CONFIG = os.path.join(PROJECT_DIR, 'config.json')

# Compatibility settings

COMPATIBILITY_USE_TOPSELLER_AND_FEATURED = False
COMPATIBILITY_USE_PARENT_STATUS = True
COMPATIBILITY_CHEAPEST_PRICE_DEPENDS_SORTING = True

# Background task management

TASK_MANAGERS = (
    ('tasks.api.DummyTaskQueue', ugettext(u'Dummy task queue')),
    ('tasks.api.NullTaskQueue', ugettext(u'Null task queue')),
    ('tasks.api.DBTaskQueue', ugettext(u'DB task queue')),
)
ACTIVE_TASK_MANAGER = 'tasks.api.DBTaskQueue'
DEFAULT_FLUSH_JOB_COUNT = 5
AUTO_CANCEL_TASK_TIMEOUT = 120  # minutes
TASK_NAME_EXCLUDES = []  # use for ignore some names

# Default priorities for background jobs

JOB_CLEAR_CACHE_PRIORITY = 1
JOB_REBUILD_COUNTERS_PRIORITY = 2
JOB_UPDATE_FILTERS_PRIORITY = 3
JOB_MACROS_PRIORITY = 5

# Captchas

CAPTCHAS = (
    ('', ugettext(u'Do not use captcha')),
    ('qaptcha.fields.QaptchaField', ugettext(u'Slider')),
)
ACTIVE_CAPTCHA = ''
CAPTCHA_USE_FOR_REGISTER = True
CAPTCHA_USE_FOR_ANON_CHECKOUT = True

# Customer registration settings

CUSTOMER_AUTH_CHOICES = (
    ('email', ugettext(u'Email')),
    ('phone', ugettext(u'Phone')),
    ('username', ugettext(u'Username')),
)
CUSTOMER_AUTH_BY = 'email'
CUSTOMER_AUTH_LABEL = ugettext(u'Email')
CUSTOMER_AUTH_PLACEHOLDER = ''
CUSTOMER_AUTH_ENABLED = True
CUSTOMER_AUTH_MERGE = True

CUSTOMER_NAME_REQUIRED = True
# True if you want to use separated fields for first name and last name
CUSTOMER_NAME_SEPARATED = True
CUSTOMER_ASK_BIRTHDAY = True
CUSTOMER_BIRTHDAY_REQUIRED = True

CUSTOMER_PHONE_NUMBER_REGION = 'UA'
CUSTOMER_PHONE_HELP_TEXT = ugettext(u'Enter full number with regional code.')
CUSTOMER_PHONE_PLACEHOLDER = ''

REGISTER_REDIRECT_URL = '/'
CUSTOMER_PHONE_TEMPLATE = '+%s%s(%s%s%s)%s%s%s-%s%s-%s%s'

CUSTOMER_FINDERS = (
    ('lfs.customer.finder.EmailFinder', ugettext(u'By default email')),
    ('lfs.customer.finder.PhoneFinder', ugettext(u'By default phone')),
)
CUSTOMER_FINDER = 'lfs.customer.finder.EmailFinder'

EMAIL_FORMAT_CHOICES = (
    ('text', ugettext(u'Plain text')),
    ('html', ugettext(u'HTML')),
    ('html+images', ugettext(u'HTML with images')),
)
EMAIL_DEFAULT_FORMAT = 'html+images'


DISCOUNT_CALCULATORS = (
    (
        'lfs.discounts.api.BaseDiscountCalculator',
        ugettext(u'Discount on cart')
    ),
    (
        'lfs.discounts.api.ProductDiscountCalculator',
        ugettext(u'Discount on product')
    ),
    (
        'lfs.discounts.api.CategoryDiscountCalculator',
        ugettext(u'Discount on category')
    ),
)
DISCOUNT_CALCULATOR = 'lfs.discounts.api.BaseDiscountCalculator'

CRITERIONS = [
    'lfs.criteria.models.criteria.PaymentMethodCriterion',
    'lfs.criteria.models.criteria.ShippingMethodCriterion',
    'lfs.criteria.models.criteria.CartPriceCriterion',
    'lfs.criteria.models.criteria.UserCriterion',
    'lfs.criteria.models.criteria.CustomCriterion',
    'lfs.criteria.models.criteria.ProductAmountCriterion',
    'lfs.criteria.models.criteria.ProductAmountCategoryCriterion',
    'lfs.criteria.models.criteria.DateTimeCriterion',
    'lfs.criteria.models.criteria.RegisteredUserCriterion',
    'lfs.criteria.models.criteria.GroupUserCriterion',
]

CHECKOUT_AUTH_ONLY = False
CHECKOUT_PHONE_NUMBER_REGION = 'UA'
CHECKOUT_PHONE_HELP_TEXT = ugettext(u'Enter full number with regional code.')
CHECKOUT_PHONE_PLACEHOLDER = ''

CHECKOUT_SHOW_SHIPPING = True
CHECKOUT_SHOW_PAYMENT = True
CHECKOUT_SHOW_COMMENT = True
CHECKOUT_SHOW_VOUCHER = True

CHECKOUT_USE_DEFAULT_SHIPPING = False
CHECKOUT_USE_DEFAULT_PAYMENT = False

CHECKOUT_REQUIRE_EMAIL = False
CHECKOUT_REQUIRE_PHONE = True
CHECKOUT_VALIDATE_PHONE = True
REGISTRATION_VALIDATE_PHONE = True
CHECKOUT_THANK_YOU_TEXT = ugettext(
    u'Soon you will get an order confirmation via E-Mail.')

# Message sending

LFS_SEND_ORDER_MAIL_ON_CHECKOUT = True

# Category

CATEGORY_SHOW_PRODUCT_WITH_VARIANTS = False
CATEGORY_SHOW_404_FOR_INVALID_PAGE = False
CATEGORY_MAKE_301_REDIRECT_FOR_INVALID_PAGE = False
PRODUCT_CANT_BE_BOUGHT_MESSAGE = ""
# Filters
SHOW_FILTER_COUNTERS = True

# Graphic module
# TODO: add to settings
AVAILABLE_GRAPHIC_PROCESSOR = (
    ('PIL', 'PIL'),
    ('ImageMagick', 'ImageMagick'),
)
GRAPHIC_PROCESSOR = 'PIL'

WATERMARK_X_DELTA = 0
WATERMARK_Y_DELTA = 0

# Report builder
REPORT_BUILDER_GLOBAL_EXPORT = True

# Diagnostic

DIAGNOSTIC_TESTS = [
    'lfs.customer.diagnostic_tests.UserWithoutCustomersTest',
    'lfs.catalog.diagnostic_tests.StandardWithVariantsTest',
    'lfs.catalog.diagnostic_tests.VariantWithVariantsTest',
    'lfs.catalog.diagnostic_tests.NotActiveParentButActiveChildrenTest',
    'lfs.catalog.diagnostic_tests.PropertiesWithBlankNamesTest',
    'lfs.catalog.diagnostic_tests.StatusForParentsTest',
    'lfs.catalog.diagnostic_tests.ZeroPriceTest',
    'lfs.catalog.diagnostic_tests.ProductWithoutStatusTest',
    'lfs.catalog.diagnostic_tests.HasParentTest',
    'lfs.catalog.diagnostic_tests.WrongProductPropertiesTest',
    'portlets.diagnostic_tests.PortletAssignmentErrorTest',
    'tasks.diagnostic_tests.FailJobTest',
]

try:
    LOCAL_SETTINGS
except NameError:
    try:
        from local_settings import *
    except ImportError:
        pass

# Regions
# if ALLOW_REGIONS:
#     INSTALLED_APPS += (
#         'regions',
#     )

#     LFS_PRICE_CALCULATORS += [
#         [
#             'regions.RegionPriceCalculator',
#             ugettext(u'Regions price calculator')],
#     ]

#     LFS_SHIPPING_METHOD_PRICE_CALCULATORS += [
#         [
#             'regions.RegionShippingPriceCalculator',
#             ugettext(u'Regions shipping price calculator')],
#     ]

SUIT_CONFIG = {
    'ADMIN_NAME': ugettext(u'P-Cart'),
    'HEADER_DATE_FORMAT': 'l, j F Y',   # Saturday, 16 March 2013
    'HEADER_TIME_FORMAT': 'H:i',        # 18:42
    # FIX
    # 'SEARCH_URL': 'admin:accounts_account_changelist',

    'MENU': (
        {
            'label': ugettext(u'Accounts'),
            'icon': 'icon-user',
            'tag': 'accounts',
            'models': (
                'auth.user',
                'auth.group',
                'loginza.identity',
                'loginza.usermap',
            )
        },
        {
            'label': ugettext(u'Shop'),
            'icon': 'icon-home',
            'tag': 'shop',
            'models': (
                'core.shop',
                'core.actiongroup',
                'core.action',
                'payment.paymentmethod',
                'shipping.shippingmethod',
                'sshop_currencies.currency',
                'catalog.productstatus',
                'catalog.sorttype',
                'catalog.deliverytime',
            )
        },
        {
            'label': ugettext(u'Catalog'),
            'icon': 'icon-align-justify',
            'tag': 'catalog',
            'models': (
                'manufacturer.manufacturer',
                'catalog.category',
                'catalog.product',
                'catalog.property',
                'catalog.propertyset',
                'catalog.propertytype',
                'catalog.propertyoption',
                'catalog.propertyunit',
                'catalog.propertyvalueicon',
            )
        },
        {
            'label': ugettext(u'HTML'),
            'icon': 'icon-file',
            'tag': 'html',
            'models': (
                'page.page',
                'catalog.staticblock',
                'mail.mailtemplate',
                'form_designer.formlog',
                'form_designer.formdefinition',
            )
        },
        {
            'label': ugettext(u'Customers and orders'),
            'icon': 'icon-shopping-cart',
            'tag': 'customers',
            'models': (
                'customer.customer',
                'cart.cart',
                'order.orderstatus',
                'order.order',
                'lfs_order_numbers.ordernumbergenerator',
                'reviews.review',
                'question.question',
            )
        },
        {
            'label': ugettext(u'Marketing'),
            'icon': 'icon-bullhorn',
            'tag': 'marketing',
            'models': (
                'marketing.featuredproduct',
                'marketing.topseller',
                'marketing.productlist',
                'marketing.productlistitem',
                {'model': 'faq.topic', 'label': ugettext(u'FAQ topics')},
                {'model': 'faq.question', 'label': ugettext(u'FAQ questions')},
            )
        },
        {
            'label': ugettext(u'Vouchers and discounts'),
            'icon': 'icon-heart',
            'tag': 'discounts',
            'models': (
                'voucher.vouchergroup',
                'voucher.voucher',
                'discounts.discount',
            )
        },
        {
            'label': ugettext(u'Reports and analytics'),
            'icon': 'icon-briefcase',
            'tag': 'reports',
            'models': (
                'report_builder.report',
                'report_builder.format',
                'marketing.productsales',
            ),
        },
        {
            'label': ugettext(u'Service'),
            'icon': 'icon-leaf',
            'tag': 'service',
            'models': (
                {
                    'model': 'filer.folder',
                    'label': ugettext(u'File management')},
                {
                    'url': 'simple_import-start_import',
                    'label': ugettext(u'Import data')},
                {'model': 'tasks.job', 'label': ugettext(u'Jobs')},
                'maintenance.maintenancemessage',
                'admin.logentry',
                'sessions.session',
                'logtailer.logfile',
                'logtailer.filter',
                'logtailer.logsclipboard',
                'macroses.macros',
                {
                    'url': 'admin_diagnose',
                    'label': ugettext(u'Diagnose')},
            )
        },
        {
            'label': ugettext(u'Settings'),
            'icon': 'icon-cog',
            'tag': 'settings',
            'models': (
                {
                    'url': 'admin_system_info',
                    'label': ugettext(u'System info')},
                {
                    'url': 'admin_config_index',
                    'label': ugettext(u'Configuration')},
                'redirects.redirect',
                'sites.site',
                'portlets.portletregistration',
                'portlets.slot',
            )
        },
    )
}

REPLY_TO_CUSTOMER = False

# Load settings from extensions
ENTRY_POINT_GROUP = 'sshop.plugins.settings.entrypoints'

import pkg_resources

for entrypoint in pkg_resources.iter_entry_points(group=ENTRY_POINT_GROUP):
    if entrypoint.name == 'update_settings':
        module_name = entrypoint.module_name
        # if 'haystack' in module_name: continue
        try:
            update_settings = entrypoint.load()
        except Exception, e:
            logger.error(
                u'Trouble when looking for init settings for extension '
                u'"%(module)s": %(exception)s' % {
                    'module': module_name,
                    'exception': unicode(e),
                })
        new_settings = update_settings(globals())
        for i in new_settings.keys():
            globals()[i] = new_settings[i]

try:
    from server_settings import *
except ImportError:
    pass