# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-02 19:39+0300\n"
"PO-Revision-Date: 2014-03-05 12:53+0300\n"
"Last-Translator: Oleh Korkh <korkholeh@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 1.5.4\n"

#: admin.py:45
msgid "Groups"
msgstr "Групи"

#: settings.py:36
msgid "Русский"
msgstr ""

#: settings.py:37
msgid "Українська"
msgstr ""

#: settings.py:38
msgid "English"
msgstr ""

#: settings.py:363
msgid "Default price calculator"
msgstr "Стандартиний калькулятор ціни"

#: settings.py:374
msgid "Default shipping price calculator"
msgstr "Стандартний калькулятор ціни доставки"

#: settings.py:450
msgid "Simple search config"
msgstr "Простий пошук"

#: settings.py:881
msgid "Standard regex and rules"
msgstr "Стандартні регулярні вирази і правила"

#: settings.py:952
msgid "General"
msgstr "Загальне"

#: settings.py:953
msgid "SEO"
msgstr ""

#: settings.py:954
msgid "Captcha"
msgstr "Каптча"

#: settings.py:955
msgid "Login"
msgstr "Логін"

#: settings.py:956
msgid "Registration"
msgstr "Реєстрація"

#: settings.py:958
msgid "Customers"
msgstr "Клієнти"

#: settings.py:963
msgid "Checkout"
msgstr "Оформлення замовлення"

#: settings.py:965
msgid "Message sending"
msgstr "Відсилання повідомлень"

#: settings.py:967 settings.py:1207
msgid "Catalog"
msgstr "Каталог"

#: settings.py:969
msgid "Task priorities"
msgstr "Пріоритети завдань"

#: settings.py:971
msgid "Compatibility"
msgstr "Сумісність"

#: settings.py:973
msgid "Reviews"
msgstr ""

#: settings.py:975
msgid "Pages"
msgstr ""

#: settings.py:977
msgid "Filter"
msgstr ""

#: settings.py:978
msgid "Pagination"
msgstr "Нумерація сторінок"

#: settings.py:993
msgid "Dummy task queue"
msgstr "Запускати задачі негайно"

#: settings.py:994
msgid "Null task queue"
msgstr "Ігнорувати задачі"

#: settings.py:995
msgid "DB task queue"
msgstr "Черга в БД"

#: settings.py:1012
msgid "Do not use captcha"
msgstr "Не використовувати каптчу"

#: settings.py:1013
msgid "Slider"
msgstr "Слайдер"

#: settings.py:1022 settings.py:1027
msgid "Email"
msgstr "Електронна пошта"

#: settings.py:1023
msgid "Phone"
msgstr "Телефон"

#: settings.py:1024
msgid "Username"
msgstr "Ім’я користувача"

#: settings.py:1039 settings.py:1090
msgid "Enter full number with regional code."
msgstr "Введіть повний номер з регіональним кодом."

#: settings.py:1046
msgid "By default email"
msgstr "За стандартним email"

#: settings.py:1047
msgid "By default phone"
msgstr "За стандартним телефоном"

#: settings.py:1052
msgid "Plain text"
msgstr "Звичайний текст"

#: settings.py:1053 settings.py:1223
msgid "HTML"
msgstr ""

#: settings.py:1054
msgid "HTML with images"
msgstr "HTML із зображеннями"

#: settings.py:1062
msgid "Discount on cart"
msgstr "Знижка на кошик"

#: settings.py:1066
msgid "Discount on product"
msgstr "Знижка на продукт"

#: settings.py:1070
msgid "Discount on category"
msgstr "Знижка на категорію"

#: settings.py:1104
msgid "Soon you will get an order confirmation via E-Mail."
msgstr ""

#: settings.py:1172
msgid "P-Cart"
msgstr ""

#: settings.py:1180
msgid "Accounts"
msgstr "Облікові записи"

#: settings.py:1191
msgid "Shop"
msgstr "Магазин"

#: settings.py:1235
msgid "Customers and orders"
msgstr "Клієнти та замовлення"

#: settings.py:1249
msgid "Marketing"
msgstr "Маркетинг"

#: settings.py:1257
msgid "FAQ topics"
msgstr "Теми FAQ"

#: settings.py:1258
msgid "FAQ questions"
msgstr "Питання FAQ"

#: settings.py:1262
msgid "Vouchers and discounts"
msgstr "Сертифікати і знижки"

#: settings.py:1272
msgid "Reports and analytics"
msgstr "Звіти та аналітика"

#: settings.py:1282
msgid "Service"
msgstr "Сервіс"

#: settings.py:1288
msgid "File management"
msgstr "Керування файлами"

#: settings.py:1291
msgid "Import data"
msgstr "Імпорт даних"

#: settings.py:1292
msgid "Jobs"
msgstr "Завдання"

#: settings.py:1302
msgid "Diagnose"
msgstr "Діагностика"

#: settings.py:1306
msgid "Settings"
msgstr "Налаштування"

#: settings.py:1312
msgid "System info"
msgstr "Про систему"

#: settings.py:1315
msgid "Configuration"
msgstr "Конфігурація"

#: templates/admin/user_change_form.html:8
msgid "Customer"
msgstr "Клієнт"

#: templates/admin/sessions/change_list.html:8
msgid "Set task for clear sessions"
msgstr "Встановити задачу для очистки сессій"

#~ msgid "Cart discount"
#~ msgstr "Знижка на кошик"

#~ msgid "Product discount"
#~ msgstr "Знижка на товар"

#~ msgid "Regions price calculator"
#~ msgstr "Регіональний калькулятор ціни"

#~ msgid "Regions shipping price calculator"
#~ msgstr "Регіональний калькулятор ціни доставки"

#~ msgid "File browser"
#~ msgstr "Файловий менеджер"
