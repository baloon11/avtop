��    4      �  G   \      x     y     �     �     �     �     �     �     �     �  	   �     �            !   (     J     S     d     y     �     �     �  %   �     �  
   �     �                         +     7     <  	   B     L     \  
   l     w  
   }     �     �     �     �     �     �     �     �     �               .     7  �  N     	  9   5	  &   o	  
   �	     �	  !   �	     �	     �	     
     
     "
  !   A
  4   c
  E   �
     �
      �
  $        ;  (   X  +   �  !   �  U   �     %     8  #   E  
   i     t     �     �     �     �  
   �     �  #     4   %  !   Z     |     �     �  "   �     �  D   �     1     D  6   S     �  %   �     �     �     �  &                                   1      -   '                 	   +                                     !   (       )              0   %           "   *   &      4                          #             
           /      $      3          ,   2   .          Accounts By default email By default phone Captcha Catalog Checkout Compatibility Configuration Customer Customers Customers and orders DB task queue Default price calculator Default shipping price calculator Diagnose Discount on cart Discount on category Discount on product Do not use captcha Dummy task queue Email Enter full number with regional code. FAQ questions FAQ topics File management General Groups HTML HTML with images Import data Jobs Login Marketing Message sending Null task queue Pagination Phone Plain text Registration Reports and analytics Service Set task for clear sessions Settings Shop Simple search config Slider Standard regex and rules System info Task priorities Username Vouchers and discounts Project-Id-Version: LFS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-10-02 19:39+0300
PO-Revision-Date: 2014-03-05 12:52+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Учетные записи Электронная почта по умолчанию Телефон по умолчанию Капча Каталог Оформление заказа Совместимость Конфигурация Клиент Клиенты Клиенты и заказы Очередь задач в БД Стандартный калькулятор цен Стандартный калькулятор цен доставки Диагностика Скидка на корзину Скидка на категорию Скидка на товар Не использовать капчу Немедленное исполнение Электронная почта Введите полный номер телефона с кодом региона. FAQ вопросы FAQ темы Управление файлами Общие Группы Контент HTML с картинками Импорт данных Фоновые задачи Логин Маркетинг Отправка сообщений Игнорирование фоновых задач Нумерация страниц Телефон Простой текст Регистрация Отчеты и аналитика Сервис Установить задачу для очистки сессий Настройки Магазин Конфигурация обычного поиска Слайдер Стандартные правила О системе Приоритеты задач Имя пользователя Сертификаты и скидки 