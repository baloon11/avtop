$(document).ready(function() {
        Suit.$('.autocomplete-light-widget').on(
            'click', '.yourlabs-autocomplete [data-create-choice]', function() {
                var widget = $(this).parents('.autocomplete-light-widget[data-bootstrap]');
                var url = widget.data('autocompleteUrl');

                $.ajax(url, {
                    async: false,
                    type: 'POST',
                    data: $(this).data(),
                    success: function(data, textStatus, jqXHR) {
                        var widgetInstance = widget.yourlabsWidget();
                        widgetInstance.selectChoice($(data.replace(/[\n\r]/g, '')));
                    }
                });
            }
        );
    });