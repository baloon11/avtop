# coding: utf-8
from django.contrib import admin
from .widgets import (
    VerboseForeignKeyRawIdWidget, VerboseManyToManyRawIdWidget)
from django.contrib.admin.sites import site
from django.contrib.admin.models import LogEntry, DELETION
from django.utils.html import escape
from django.core.urlresolvers import reverse


class LinkedInline(admin.TabularInline):
    template = 'adminextras/linked_inline/tabular.html'

    admin_model_path = None
    admin_app_label = None

    def __init__(self, *args):
        super(LinkedInline, self).__init__(*args)
        if self.admin_model_path is None:
            self.admin_model_path = self.model.__name__.lower()
        if self.admin_app_label is None:
            self.admin_app_label = self.model._meta.app_label


class ImproveRawIdFieldsForm(admin.ModelAdmin):
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name in self.raw_id_fields:
            kwargs.pop("request", None)
            type = db_field.rel.__class__.__name__
            if type == "ManyToOneRel":
                kwargs['widget'] = \
                    VerboseForeignKeyRawIdWidget(db_field.rel, site)
            elif type == "ManyToManyRel":
                kwargs['widget'] = \
                    VerboseManyToManyRawIdWidget(db_field.rel, site)
            return db_field.formfield(**kwargs)
        return super(ImproveRawIdFieldsForm, self)\
            .formfield_for_dbfield(db_field, **kwargs)


class LogEntryAdmin(admin.ModelAdmin):
    list_display = [
        'action_time',
        'user',
        'content_type',
        'object_link',
        'action_text',
        'change_message',
    ]
    readonly_fields = LogEntry._meta.get_all_field_names()
    date_hierarchy = 'action_time'
    search_fields = [
        'object_repr',
        'change_message'
    ]
    list_filter = [
        'user',
        'content_type',
        'action_flag'
    ]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser and request.method != 'POST'

    def has_delete_permission(self, request, obj=None):
        return False

    def object_link(self, obj):
        if obj.action_flag == DELETION:
            link = escape(obj.object_repr)
        else:
            ct = obj.content_type
            link = u'<a href="%s">%s</a>' % (
                reverse(
                    'admin:%s_%s_change' % (ct.app_label, ct.model),
                    args=[obj.object_id]),
                escape(obj.object_repr),
            )
        return link
    object_link.allow_tags = True
    object_link.admin_order_field = 'object_repr'
    object_link.short_description = u'object'

    def action_text(self, obj):
        if obj.action_flag == 1:
            return u'<span class="label label-success">INSERT</span>'
        elif obj.action_flag == 2:
            return u'<span class="label label-info">UPDATE</span>'
        elif obj.action_flag == 3:
            return u'<span class="label label-important">DELETE</span>'
        else:
            return u'<span class="label">UNKNOWN</span>'
    action_text.allow_tags = True

    def queryset(self, request):
        return super(LogEntryAdmin, self).queryset(request) \
            .prefetch_related('content_type')

admin.site.register(LogEntry, LogEntryAdmin)
