# coding: utf-8
from django.template import Library
from adminextras.settings import (
    HELPBAR_DOCS_REPOSITORY, HELPBAR_URLS, HELPBAR_INDEX_URL)
import re

register = Library()


@register.simple_tag(takes_context=True)
def get_help_doc_url(context):
    path = context['request'].path
    for item in HELPBAR_URLS:
        for url in item[0]:
            if re.match(url, path):
                return HELPBAR_DOCS_REPOSITORY + item[1]
    return HELPBAR_DOCS_REPOSITORY + HELPBAR_INDEX_URL
