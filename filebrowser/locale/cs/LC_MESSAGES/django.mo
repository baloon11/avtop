��    K      t  e   �      `     a     }     �     �     �  ,   �  *   �     �                         $     7  	   <     F     K     Q     ]     k     �     �  5   �     �  >   �     '     3     <     C     J     M     T     Y     ^     d     r     u     x     �  
   �     �     �     �  C   �     �     	     	  $   '	     L	     S	     _	     x	     	     �	     �	     �	     �	  '   �	     �	  R   
  %   X
  '   ~
  "   �
  $   �
  	   �
  	   �
                              /  
   5     @  �  F     �     �     �     �       .     *   E     p     v  
     
   �     �     �     �  
   �     �     �     �     �     �             E   -     s  D   z     �     �     �     �     �     �                         %     (     +     A     H     V     c     u  I   �     �     �       %        A     N  &   `     �     �     �     �     �     �  '   �     	  Q      "   r  %   �     �      �  	   �                              %     4     :     L     B          A          .                      /      9   J          1                    2          3   :      ?   %                      I   *      E   0   >   =       F      8       +   5      "              ;                ,       )   G       
           	          H   7              6       !      4   @   '   &   $   D   C   K   <       -                #       (    %(full_result_count)s total All Allowed An Error occured Any Date Are you sure you want to delete this Folder? Are you sure you want to delete this file? Audio BROWSE By Date By Type Clear Queue Clear Restrictions Code Completed Date Debug Delete File Delete Folder Do you want to replace the file Document Error creating folder. Error finding Upload-Folder. Maybe it does not exist? Error. Extension %(ext)s is not allowed. Only %(allowed)s is allowed. FileBrowser Filename Filter Folder Go Height Help Home Image Image Version KB MB Max. Filesize Name New Folder New Name No Items No Items Found Only letters, numbers, underscores, spaces and hyphens are allowed. Past 30 days Past 7 days Permission denied. Please correct the following errors. Rename Rename "%s" Renaming was successful. Search Select Select files to upload Size Submit The File already exists. The Folder %s was successfully created. The Folder already exists. The Name will be converted to lowercase. Spaces will be replaced with underscores. The file %s was successfully deleted. The folder %s was successfully deleted. The requested File does not exist. The requested Folder does not exist. This year Thumbnail Today Type Upload Versions Versions for "%s" Video View Image Width Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-23 16:59+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2
 %(full_result_count)s celkem Vše Povoleno Nastala chyba Jakékoliv datum Jste si jisti, že chcete smazat tento soubor? Are you sure you want to delete this file? Audio PŘEHLED podle data Podle typu Vymazat řadu Vymazat Omezení Kód Dokončeno Datum Odladit Delete File Smazat soubor Chcete nahradit soubor Dokument Chyba při vytváření složky. Chyba při hledání Upload-Složky. Možná, že složka neexistuje? Chyba. Rozšíření %(ext)s není povoleno. Je povoleno pouze %(allowed)s. Souborový manažér Název souboru Filtr Složka Spustit Výška Pomoc Úvod Obrázek Verze obrázku KB MB Max. velikost souboru Název Nová složka Nový název Žádné položky Žádné položky nenalezeny Pouze písmena, číslice, podtržítka, mezery a pomlčky jsou povoleny. Posledních 30 dnů Posledních 7 dní Přístup byl odepřen. Opravte prosím následující chyby. Přejmenovat Přejmenovat "%s" Přejmenování proběhlo úspěšně. Vyhledávání Vybrat Vyberte soubory k nahrání Velikost Potvrdit Soubor již existuje. Složka %s byla úspěšně vytvořena. Složka již existuje. Jméno bude konvertováno na malá písmena. Mezery budou nahrazeny podtržítky. Soubor %s byl úspěšně smazán. Složka %s byla úspěšně smazána. Požadovaný soubor neexistuje. Požadovaná složka neexistuje. Tento rok Náhled Dnes Typ Nahrát Verze Verze pro "%s" Video Zobrazit obrázek Šířka 