from django.db import models
from django.utils.translation import ugettext_lazy as _


class Region(models.Model):
    name = models.CharField(_(u"Name"), max_length=100)
    coefficient = models.FloatField(default=1)
    is_default = models.BooleanField(default=False)

    def __unicode__(self):
        return "%s" % self.name
