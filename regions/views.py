# coding: utf-8
from django.http import HttpResponseRedirect
from regions.models import Region


def set_region_view(request):
    if 'region' in request.GET:
        region = Region.objects.get(pk=request.GET.get('region'))
        request.session['region'] = region.id
    return HttpResponseRedirect(request.GET.get('next', '/'))
