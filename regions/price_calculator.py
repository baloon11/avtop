from lfs.plugins import PriceCalculator, ShippingMethodPriceCalculator
from regions.models import Region


class RegionPriceCalculator(PriceCalculator):
    def get_price_coefficient(self):
        if 'region' in self.request.session:
            return Region.objects.get(
                pk=self.request.session.get('region')).coefficient
        else:
            return Region.objects.get(is_default=True).coefficient

    def get_price(self, with_properties=True):
        coefficient = self.get_price_coefficient()
        return super(RegionPriceCalculator, self)\
            .get_price(with_properties) * coefficient

    def get_standard_price(self, with_properties=True):
        coefficient = self.get_price_coefficient()
        return super(RegionPriceCalculator, self)\
            .get_standard_price(with_properties) * coefficient

    def get_for_sale_price(self, with_properties=True):
        coefficient = self.get_price_coefficient()
        return super(RegionPriceCalculator, self)\
            .get_for_sale_price(with_properties) * coefficient


class RegionShippingPriceCalculator(ShippingMethodPriceCalculator):
    def get_price_coefficient(self):
        if 'region' in self.request.session:
            return Region.objects.get(
                pk=self.request.session.get('region')).coefficient
        else:
            return Region.objects.get(is_default=True).coefficient

    def get_price(self):
        coefficient = self.get_price_coefficient()
        return super(RegionShippingPriceCalculator, self)\
            .get_price() * coefficient
