��          �   %   �      0     1     8  ,   V  O   �  %   �     �        
          (        ?     S     d     v     �     �     �     �     �  G   �                 �       �  3   �  G   %  �   m  J        S     g     |  
   �  W   �  )   �  %   &  '   L  %   t  '   �     �     �  
   �  
   �  a   �     R	     e	     l	             	                    
                                                                                              Active Blocked in inheritors`s slots Class name of the portlet, e.g. TextPortlet. Database may not has PortletAssignment objects without link to correct portlet. Delete incorrect items from database. Failed Fixed Horizontal Name Name of the portlet which used in admin. Portlet assignments Portlet blocking Portlet blockings Portlet registration Portlet registrations Position Slot Slots Text There are %d PortletAssignment objects without link to correct portlet. Title Type Vertical Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-08-02 16:50+0300
PO-Revision-Date: 2014-08-02 16:52+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Активно Блокувати в дочірніх слотах Назва класу портлету, наприклад TextPortlet. База даних не повинна містити об’єкти PortletAssignment з некоректними лінками на портлети. Видалити некоректні записи з бази даних. Не вдалося Виправлено Горизонтальний Назва Назва портлету, яка використовується в адмінці. Розташування портлету Блокування портлету Блокування портлетів Реєстрація портлету Реєстрації портлетів Позиція Слот Слоти Текст Знайдено %d об’єктів PortletAssignment з некоректними даними. Заголовок Тип Вертикальний 