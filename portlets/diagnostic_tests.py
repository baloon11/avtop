# coding: utf-8
from diagnostic.base import BaseDiagnosticTest
from django.utils.translation import ugettext as _


class PortletAssignmentErrorTest(BaseDiagnosticTest):
    """
    """
    def get_description(self):
        return _(u'Database may not has PortletAssignment objects without link to correct portlet.')

    def how_to_fix(self):
        return _(u'Delete incorrect items from database.')

    def _get_items_with_problems(self):
        from .models import PortletAssignment
        _pa = PortletAssignment.objects.filter(portlet_id=None)
        return _pa

    def get_report(self):
        _items = self._get_items_with_problems()
        _report = '<ul>'
        for i in _items:
            _report += '<li><a href="{0}" target="_blank">PortletAssignment.pk={1}</a></li>'.format(
                '/superadmin/portlets/portletassignment/%d/' % i.pk,
                i.pk)
        _report += '</ul>'
        return _report

    def check(self):
        _items = self._get_items_with_problems()
        if _items:
            p_count = _items.count()
            return (
                False,
                _(u'There are %d PortletAssignment objects without link to correct portlet.') % p_count)
        else:
            return (True, _(u'OK'))

    def fix(self, solution=0):
        _items = self._get_items_with_problems()
        if _items:
            _items.delete()
            return (True, _(u'Fixed'))

        return (False, _(u'Failed'))
