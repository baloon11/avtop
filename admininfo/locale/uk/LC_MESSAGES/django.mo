��          �   %   �      P     Q     _  !   r     �     �     �     �     �  0   �     $     )     6     M     ^     m     �     �     �     �     �     �     �     �            �  &     �       S     E   s     �  "   �     �  &     H   /     x  (   �  A   �  #   �       ,   *     W  '   k  %   �  %   �  #   �     	     	  %   8	     ^	  $   g	                                                                  
   	                                                        Cache backend Cache was cleared. Cannot find default cache config. Cannot find default database. Clear cache Database driver Django version Engine was restarted. Garbage collector cleared the collected objects. Home Memory usage Memory used by process Operating system P-Cart version Process command line Python version Restart engine SSHOP_VERSION is not defined. System info System user Thread number Total memory Virtual environment bytes sys.prefix is not defined. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-12-16 12:55+0200
PO-Revision-Date: 2014-01-29 21:28+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Бекенд кешу Кеш очищено. Неможливо знайти дефолтну конфігурацію кешу. Неможливо знайти дефолтну базу даних. Очистити кеш Драйвер бази даних Версія Django Движок перезапущено. Збирач сміття очистив зібрані об’єкти. Головна Використання пам’яті Пам’ять використовується процесом Операційна система Версія P-Cart Командний рядок процесу Версія Python Перезапустити движок SSHOP_VERSION невизначена. Системна інформація Користувач системи Номер потоку Всього пам’яті Віртуальне оточення байт sys.prefix невизначений. 