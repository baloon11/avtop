��          �   %   �      p     q       !   �     �     �     �     �     �       0   +     \     a     n     �     �     �     �     �     �     �     �                     -     D     X  �  ^     #     9  ;   N  /   �     �  $   �     �  $        0  \   J     �  %   �  .   �  '        3  +   G  0   s     �  '   �  (   �     		  '   	  #   C	     g	      	  )   �	     �	                                                          
          	                                                       Cache backend Cache was cleared. Cannot find default cache config. Cannot find default database. Clear cache Database driver Django version Engine was restarted. Force garbage collector Garbage collector cleared the collected objects. Home Memory usage Memory used by process Operating system P-Cart version PID Process command line Python version Restart engine SSHOP_VERSION is not defined. System info System user Thread number Total memory VIRTUAL_ENV not found. Virtual environment bytes Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-17 10:58+0300
PO-Revision-Date: 2013-07-17 11:00+0300
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Бэкенд кеша Кеш очищен. Не могу найти конфигурацию кеша. Не могу найти базу данных. Очистить кеш Драйвер базы данных Версия Django Движок перезапущен. Удалить мусор Сборщик мусора удалил все неиспользуемые объекты. Главная Использовано памяти Память занятая процессом Операционная система Версия P-Cart Идентификатор процесса Командная строка процесса Версия Python Перезапустить движок SSHOP_VERSION не определена. О системе Пользователь системы Количество потоков Всего памяти VIRTUAL_ENV не найдена. Виртуальное окружение байт 