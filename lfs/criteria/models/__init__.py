# Separated criteria and criteria_objects to prevent cyclic import:
# PaymentMethod -> CriteriaObjects | PaymentCriteria -> PaymentMethod
# from criteria import *
from criteria_objects import *
"""
For making migrations you need to uncomment upper lines.
Then you need comment they again for preventing cyclic import.
"""
