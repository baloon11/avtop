# coding: utf-8
import logging

# TODO: try to fix it
# need import criterions for load content_types from that classes
from .models import criteria
from lfs.discounts.models import Discount

logger = logging.getLogger('sshop')


def is_valid(request, object, product=None, **kwargs):
    """Returns True if the given object is valid. This is calculated via the
    attached criteria.

    Passed object is an object which can have criteria. At the momemnt are are
    shipping or payment methods.
    """

    for criterion_object in object.criteria_objects.all():
        if criterion_object.criterion.is_valid(request, product, **kwargs)\
                is False:
            return False
    return True


def get_first_valid(request, objects, product=None, **kwargs):
    """Returns the first valid object of given objects.

    Passed objects are objects which can have criteria.
    At the momement these are shipping or payment methods.
    """
    for object in objects:
        if is_valid(request, object, product, **kwargs):
            return object
    return None
