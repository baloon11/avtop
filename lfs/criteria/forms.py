# coding: utf-8
from django import forms
from django.contrib.contenttypes.models import ContentType


class SelectCriteriaForm(forms.Form):
    criterion = forms.ModelChoiceField(
        queryset=ContentType.objects.filter(app_label='criteria')
        .exclude(app_label='criteria', model='criteriaobjects'))
