# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

# from ..caching.utils import lfs_get_object_or_404
from ..core.managers import ActiveManager
from ..core.utils import get_default_shop
from ..catalog.models import Product, Category


class Page(models.Model):
    """An simple HTML page, which may have an optional file to download.
    """
    active = models.BooleanField(_(u"Active"), default=False)
    title = models.CharField(_(u"Title"), max_length=100)
    slug = models.SlugField(_(u"Slug"), unique=True, max_length=100)
    position = models.IntegerField(_(u"Position"), default=999)
    exclude_from_navigation = models.BooleanField(
        _(u"Exclude from navigation"), default=False)
    short_text = models.TextField(_(u"Short text"), blank=True)
    body = models.TextField(_(u"Text"), blank=True)
    associated_products = models.ManyToManyField(
        Product, blank=True, related_name="pages")
    associated_categories = models.ManyToManyField(
        Category, blank=True, related_name="pages")

    meta_h1 = models.TextField(
        _(u"H1 header"), blank=True,
        default=getattr(
            settings, 'DEFAULT_PAGE_META_H1_TEMPLATE', '{{title}}'),
    )
    meta_title = models.TextField(
        _(u"Meta title"), blank=True,
        default=getattr(
            settings, 'DEFAULT_PAGE_META_TITLE_TEMPLATE', '{{title}}'),
    )
    meta_keywords = models.TextField(
        _(u"Meta keywords"),
        default=getattr(
            settings, 'DEFAULT_PAGE_META_KEYWORDS_TEMPLATE', ''),
        blank=True)
    meta_description = models.TextField(
        _(u"Meta description"),
        default=getattr(
            settings, 'DEFAULT_PAGE_META_DESCRIPTION_TEMPLATE', ''),
        blank=True)
    meta_seo_text = models.TextField(
        _(u"Meta SEO text"),
        default=getattr(settings, 'DEFAULT_PAGE_META_TEXT_TEMPLATE', ''),
        blank=True)

    # DEPRECATED
    file = models.FileField(_(u"File"), blank=True, upload_to="files")

    objects = ActiveManager()

    class Meta:
        ordering = ("position", )
        verbose_name = _(u'Page')
        verbose_name_plural = _(u'Pages')

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        """
        Overwritten to save seo data.
        """
        if not self.meta_h1:
            self.meta_h1 = getattr(
                settings, 'DEFAULT_PAGE_META_H1_TEMPLATE', '')
        if not self.meta_title:
            self.meta_title = getattr(
                settings, 'DEFAULT_PAGE_META_TITLE_TEMPLATE', '')
        if not self.meta_keywords:
            self.meta_keywords = getattr(
                settings, 'DEFAULT_PAGE_META_KEYWORDS_TEMPLATE', '')
        if not self.meta_description:
            self.meta_description = getattr(
                settings, 'DEFAULT_PAGE_META_DESCRIPTION_TEMPLATE', '')
        if not self.meta_seo_text:
            self.meta_seo_text = getattr(
                settings, 'DEFAULT_PAGE_META_TEXT_TEMPLATE', '')
        super(Page, self).save(*args, **kwargs)

    def get_image(self):  # WTF?
        """Returns the image for the page.
        """
        shop = get_default_shop()
        return shop.image

    def get_absolute_url(self):
        return ("lfs_page_view", (), {"slug": self.slug})
    get_absolute_url = models.permalink(get_absolute_url)

    def get_parent_for_portlets(self):
        """Returns the parent for parents.
        """
        return get_default_shop()

    def _get_data_for_meta_info(self, **kwargs):
        """
        Get common data for meta templates.
        """
        from ..core.utils import get_default_shop
        shop = get_default_shop()
        data = {
            'name': self.title,
            'title': self.title,
            'shop_name': shop.name,
            'shop': shop,
            'page': self,
        }
        data.update(kwargs)
        return data

    def get_meta_h1(self, **kwargs):
        """Returns the H1 description of the page.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_h1, self._get_data_for_meta_info(**kwargs))

    def get_meta_title(self, **kwargs):
        """Returns the meta title of the page.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_title, self._get_data_for_meta_info(**kwargs))

    def get_meta_keywords(self, **kwargs):
        """Returns the meta keywords of the page.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_keywords, self._get_data_for_meta_info(**kwargs))

    def get_meta_description(self, **kwargs):
        """Returns the meta description of the page.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_description, self._get_data_for_meta_info(**kwargs))

    def get_meta_seo_text(self, **kwargs):
        """Returns the meta seo text of the page.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_seo_text, self._get_data_for_meta_info(**kwargs))

    def get_categories(self):
        return self.associated_categories.all()


class SeoPage(Page):
    def get_meta_title(self, request=None, context=None):
        from lfs.core.utils import render_meta_info
        import lfs.core.utils
        shop = lfs.core.utils.get_default_shop()
        data = {
            'name': settings.PCART_PAGE_NAME_PAGE,
            'shop_name': shop.name
        }
        title = settings.PCART_PAGE_META_TITLE
        return render_meta_info(title, data)

    def get_meta_keywords(self, request=None, context=None):
        from lfs.core.utils import render_meta_info
        import lfs.core.utils
        shop = lfs.core.utils.get_default_shop()
        data = {
            'name': settings.PCART_PAGE_NAME_PAGE,
            'shop_name': shop.name
        }
        keywords = settings.PCART_PAGE_META_KEYWORDS
        return render_meta_info(keywords, data)

    def get_meta_description(self, request=None, context=None):
        from lfs.core.utils import render_meta_info
        import lfs.core.utils
        shop = lfs.core.utils.get_default_shop()
        data = {
            'name': settings.PCART_PAGE_NAME_PAGE,
            'shop_name': shop.name
        }
        description = settings.PCART_PAGE_META_DESCRIPTION
        return render_meta_info(description, data)

    def get_meta_seo_text(self, request=None, context=None):
        from lfs.core.utils import render_meta_info
        import lfs.core.utils
        shop = lfs.core.utils.get_default_shop()
        data = {
            'name': settings.PCART_PAGE_NAME_PAGE,
            'shop_name': shop.name
        }
        meta_seo_text = settings.PCART_PAGE_META_TEXT
        return render_meta_info(meta_seo_text, data)

    def get_parent_for_portlets(self):
        """
        Returns the parent for portlets.
        """
        from lfs.core.utils import get_default_shop
        return get_default_shop()
