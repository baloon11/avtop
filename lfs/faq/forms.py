# coding: utf-8
from django.forms import ModelForm
from lfs.faq.models import Question


class FaqAddForm(ModelForm):
    class Meta:
        model = Question
        fields = ("question", "topic",)

    def __init__(self, *args, **kwargs):
        super(FaqAddForm, self).__init__(*args, **kwargs)
