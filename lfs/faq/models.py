# coding: utf-8
import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from pytils.translit import slugify
from django.db.models.signals import pre_save


class Topic(models.Model):
    name = models.CharField(_(u'Name'), max_length=150)
    slug = models.SlugField(_(u'Slug'), max_length=150)
    sort_order = models.IntegerField(
        _(u'Sort order'), default=0,
        help_text=_(u'Sort order for topics.'))

    class Meta:
        ordering = ['sort_order']
        verbose_name = _(u'Topic')
        verbose_name_plural = _(u'Topics')

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('faq_topic_detail', [self.slug])


class Question(models.Model):
    question = models.TextField(_(u'Question'), help_text=_(u'Question text.'))
    answer = models.TextField(
        _(u'Answer'), blank=True, help_text=_(u'Question answer.'))
    slug = models.SlugField(
        _(u'Slug'), max_length=100, blank=True, default='')
    topic = models.ForeignKey(
        Topic, verbose_name=_(u'Topic'), related_name='questions')
    protected = models.BooleanField(
        _(u'Protected'), default=False,
        help_text=_(
            u"Mark this if this question should be visible "
            u"only for authorized users."
        ))
    creation_date = models.DateTimeField(
        _(u"Creation date"), auto_now_add=True)
    answer_date = models.DateTimeField(_(u"Answer date"), auto_now_add=True)
    created_by = models.ForeignKey(
        User, verbose_name=_(u'Asked by'),
        null=True, related_name="questions")
    answered_by = models.ForeignKey(
        User, verbose_name=_(u'Answered by'),
        blank=True, null=True, related_name="answers")
    published = models.BooleanField(_(u'Published'), default=False)
    sort_order = models.IntegerField(
        _(u'Sort order'), default=0,
        help_text=_(u'Sort order for questions.'))

    class Meta:
        ordering = ['sort_order']
        verbose_name = _(u'Question')
        verbose_name_plural = _(u'Questions')

    def __unicode__(self):
        return self.question

    @models.permalink
    def get_absolute_url(self):
        return ('faq_question_detail', [self.topic.slug, self.slug])


def pre_save_handler(sender, *args, **kwargs):
    obj = kwargs['instance']
    obj.answer_date = datetime.datetime.now()
    # Create a unique slug, if needed.
    if not obj.slug:
        suffix = 0
        potential = base = slugify(obj.question[:20])
        while not obj.slug:
            if suffix:
                potential = "%s-%s" % (base, suffix)
            if not Question.objects.filter(slug=potential).exists():
                obj.slug = potential
            # We hit a conflicting slug; increment the suffix and try again.
            suffix += 1

pre_save.connect(pre_save_handler, sender=Question)
