# coding: utf-8
from django import forms
from django.db import models
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _

# portlets
from portlets.models import Portlet

CATEGORY_TYPE_CHOICES = (
    (1, _(u'Tree')),
    (2, _(u'Flat')),
)


class CategoriesPortlet(Portlet):
    """Portlet to display categories.
    """
    menu_type = models.IntegerField(
        _(u'Category type'), default=1, choices=CATEGORY_TYPE_CHOICES)

    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        """Renders the portlet as html.
        """
        from ...catalog.models import Category
        from django.db.models import Q
        import operator

        request = context.get("request")

        product = context.get("product")
        category = context.get("category")
        obj = category or product

        if self.menu_type == 1:
            if obj and obj.content_type == "category":
                tree_ids = [
                    (c['id'], c['level']) for c in obj.get_ancestors(
                        include_self=True).values('id', 'level')[1:]]
                categories = obj.get_root().get_descendants(include_self=True)\
                    .exclude(level__gt=obj.level+1)
                for t in tree_ids:
                    categories = categories.exclude(
                        Q(level=t[1]+1) & ~Q(parent_id=t[0]))
            elif obj and obj.content_type == 'product':
                categories = obj.categories.all()
                if categories:
                    filters = []
                    for n in categories:
                        filters.append(
                            Q(tree_id=n.tree_id,
                                lft__lte=n.lft, rght__gte=n.rght))
                    q = reduce(operator.or_, filters)
                    categories = Category.objects.filter(q)
                else:
                    categories = []
            else:
                categories = []
        elif self.menu_type == 2:
            if obj and obj.content_type == "category":
                categories = Category.objects.get(id=obj.id).children.all()
            elif obj and obj.content_type == 'product':
                categories = []
            else:
                categories = []

        result = render_to_string(
            "lfs/portlets/categories.html",
            RequestContext(request, {
                "title": self.title,
                "categories": categories,
                'slot_name': context.get('slot_name'),
            }))
        return result

    def form(self, **kwargs):
        return CategoriesPortletForm(instance=self, **kwargs)


class CategoriesPortletForm(forms.ModelForm):
    """Form for CategoriesPortlet.
    """
    class Meta:
        model = CategoriesPortlet
