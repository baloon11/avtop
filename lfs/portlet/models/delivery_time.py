# coding: utf-8
from django import forms
from django.template import RequestContext
from django.template.loader import render_to_string

from portlets.models import Portlet


# DEPRECATED
class DeliveryTimePortlet(Portlet):
    """Portlet to display delivery time.
    """
    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        """Renders the portlet as html.
        """
        request = context.get("request")

        # This portlet does nothing
        d = {
            "display": False,
        }

        d['slot_name'] = context.get('slot_name')
        return render_to_string(
            "lfs/portlets/delivery_time.html",
            RequestContext(request, d))

    def form(self, **kwargs):
        return DeliveryTimeForm(instance=self, **kwargs)


class DeliveryTimeForm(forms.ModelForm):
    """
    """
    class Meta:
        model = DeliveryTimePortlet
