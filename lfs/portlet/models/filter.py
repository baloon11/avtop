# coding: utf-8
from django import forms
from django.db import models
from django.conf import settings
from django.core.cache import cache
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _

from portlets.models import Portlet

from ...catalog.models import Product
from ...catalog.settings import STANDARD_PRODUCT
from ...catalog.settings import PRODUCT_WITH_VARIANTS
from ...catalog.utils import get_product_filters
from ...filters.models import Filter

from urllib import urlencode


class FilterPortlet(Portlet):
    """A portlet to display filters.
    """
    show_product_filters = models.BooleanField(  # DEPRECATED
        _(u"Show product filters"), default=True)
    show_price_filters = models.BooleanField(  # DEPRECATED
        _(u"Show price filters"), default=True)
    use_after_choose = models.BooleanField(
        _(u"Use filter right after choose"), default=True)
    load_related_options = models.BooleanField(
        _(u"Load related options"), default=False
    )

    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        """Renders the portlet as html.
        """
        request = context.get("request")
        category = context.get("category")
        if getattr(settings, 'FILTER_AJAX_LOADING', False) and category:
            get_params = urlencode(request.GET).replace('%2C', ',')
            cache_key = "%s-filter-ajax-loading-%s-%s-%s" %\
                (
                    settings.CACHE_MIDDLEWARE_KEY_PREFIX,
                    self.id,
                    category.id,
                    get_params,
                )
            result = cache.get(cache_key)

            if result:
                return result

            result = render_to_string(
                "lfs/portlets/filter_ajax.html",
                RequestContext(
                    request,
                    {
                        'id': self.id,
                        'slot_name': context.get('slot_name'),
                        'category_id': category.id,
                        'get_params': get_params,
                    }))
            return result

        sorting = request.session.get("sorting")

        if category is None:
            return render_to_string("lfs/portlets/filter.html", {
                "show": False,
            })

        # get saved filters
        set_product_filters = dict(request.GET)
        product_filters = get_product_filters(
            category, set_product_filters, sorting)
        # ajax load some filters
        children = Filter.objects.filter(
            category=category).exclude(parent=None)
        load = []
        for f in Filter.objects.filter(
                category=category, parent=None,
                identificator__in=set_product_filters.keys(),
                show_as_tree=True):
            for o in f.filteroption_set.all().order_by('-is_popular'):
                if f.identificator in set_product_filters and \
                    o.identificator in set_product_filters[f.identificator] \
                        and str(o.id) not in load:
                    load.append(str(o.id))

        products = Product.objects.filter(
            active=True,
            status__is_visible=True,
            categories__in=[category, ],
            sub_type__in=[STANDARD_PRODUCT, PRODUCT_WITH_VARIANTS])\
            .distinct().select_related()

        for f in children:
            for o in f.filteroption_set.all().order_by('-is_popular'):
                if f.identificator in set_product_filters and \
                        o.identificator in \
                        set_product_filters[f.identificator]:
                    for po in f.parent.filteroption_set.all()\
                            .order_by('-is_popular'):
                        if products.filter(
                            filteroption__id__in=[o.id, po.id]
                        ).count() > 0:
                            if str(po.id) not in load:
                                load.append(str(po.id))

        ajax_load = ','.join(load)
        return render_to_string(
            "lfs/portlets/filter.html",
            RequestContext(request, {
                "show": True,
                "title": self.title,
                'slot_name': context.get('slot_name'),
                'category': category,
                "product_filters": product_filters,
                'use_after_choose': self.use_after_choose,
                'portlet_id': self.id,
                'ajax_load': ajax_load,
                'load_related_options': self.load_related_options
            }))

    def form(self, **kwargs):
        return FilterPortletForm(instance=self, **kwargs)


class FilterPortletForm(forms.ModelForm):
    """Form for the FilterPortlet.
    """
    class Meta:
        model = FilterPortlet
