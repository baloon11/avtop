# coding: utf-8
from django import forms
from django.template import RequestContext
from django.template.loader import render_to_string

from portlets.models import Portlet


class AssociatedPagesPortlet(Portlet):
    """Portlet to display associated pages.
    """

    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        request = context.get("request")
        product = context.get("product")
        category = context.get("category")
        obj = product or category
        if obj is None:
            pages = []
        else:
            pages = obj.pages.all()

        return render_to_string(
            "lfs/portlets/associated_pages.html",
            RequestContext(request, {
                'pages': pages,
                'slot_name': context.get('slot_name'),
                'main_page': context.get('main_page'),
            }))

    def form(self, **kwargs):
        return AssociatedPagesForm(instance=self, **kwargs)


class AssociatedPagesForm(forms.ModelForm):
    """Form for the CarouselPortlet.
    """
    class Meta:
        model = AssociatedPagesPortlet
