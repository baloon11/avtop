# coding: utf-8
from django import forms
from django.template import RequestContext
from django.template.loader import render_to_string
from django.db import models
from django.utils.translation import ugettext_lazy as _

from portlets.models import Portlet


class EmptySpacePortlet(Portlet):
    """Portlet to display carousel.
    """

    size = models.TextField(_(u"Empty size(px)"), blank=True)

    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        request = context.get("request")
        try:
            size = int(filter(lambda x: x.isdigit(), self.size))
        except:
            size = 0
        return render_to_string(
            "lfs/portlets/empty_space.html",
            RequestContext(request, {
                'size': size,
                'slot_name': context.get('slot_name'),
                'main_page': context.get('main_page'),
            }))

    def form(self, **kwargs):
        return EmptySpaceForm(instance=self, **kwargs)


class EmptySpaceForm(forms.ModelForm):
    """Form for the CarouselPortlet.
    """
    class Meta:
        model = EmptySpacePortlet
