# coding: utf-8
from django import forms
from django.template import RequestContext
from django.template.loader import render_to_string
from django.db import models
from django.utils.translation import ugettext_lazy as _

from portlets.models import Portlet
from codemirror.widgets import CodeMirrorTextarea


class CustomHTMLPortlet(Portlet):
    """Portlet to display the custom html
    """
    custom_html = models.TextField(_(u'Custom HTML'))

    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        """Renders the portlet as html.
        """
        request = context.get('request')

        return render_to_string(
            "lfs/portlets/custom_html.html",
            RequestContext(request, {
                'custom_html': self.custom_html,
                'slot_name': context.get('slot_name'),
            }))

    def form(self, **kwargs):
        return CustomHTMLPortletForm(instance=self, **kwargs)


class CustomHTMLPortletForm(forms.ModelForm):
    """Form for CustomHTMLPortlet.
    """
    custom_html = forms.CharField(
        widget=CodeMirrorTextarea(
            mode="xml", config={'fixedGutter': True}))

    class Meta:
        model = CustomHTMLPortlet
