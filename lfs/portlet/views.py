# coding: utf-8
from django.conf import settings
from django.http import HttpResponse
from django.core.cache import cache
from django.utils import simplejson
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt

from urlparse import parse_qs
from portlets.models import PortletAssignment
from lfs.filters.models import Filter
from lfs.catalog.models import Category
from lfs.core.decorators import ajax_required
from lfs.catalog.utils import get_product_filters


from lfs.catalog.models import Product
from lfs.catalog.settings import STANDARD_PRODUCT
from lfs.catalog.settings import PRODUCT_WITH_VARIANTS


def portlet_panginator(request, pa_id):
    try:
        pa = PortletAssignment.objects.get(id=pa_id)
    except PortletAssignment.DoesNotExist:
        response = simplejson.dumps({'html': None, 'status': False})
        return HttpResponse(response)

    page = request.GET.get('page', 1)
    portlet = pa.portlet
    context = {
        'request': request,
        'category': None,
        'product': None,
        'slot_name': pa.slot.name,
        'MEDIA_URL': getattr(settings, 'MEDIA_URL', ''),
    }

    result = portlet.render(context, page, ajax_mode=True)
    response = simplejson.dumps({'html': result, 'status': True})

    return HttpResponse(response)


@ajax_required
def filter_reload_ajax(request):
    sorting = request.session.get("sorting")
    category = Category.objects.get(id=int(request.POST['category_id']))
    set_product_filters = {}
    identificators = [
        f['identificator'] for f in Filter.objects.filter(
            type=5).values('identificator')]
    for key, value in parse_qs(request.POST['get_params']).iteritems():
        if value != [u'none'] and key in identificators:
            set_product_filters[key] = value
    product_filters = get_product_filters(
        category, set_product_filters, sorting)
    # product_filters_choice = []
    html = {}
    for pf in product_filters:
        if pf['type'] == 5:
            # product_filters_choice.append(pf)
            html[pf['identificator']] = render_to_string(
                'lfs/filters/representations/property_5.html',
                {
                    'filter': pf
                }
            )
    return HttpResponse(simplejson.dumps({
        'html': html
    }))


@ajax_required
def filter_ajax_load(request, portlet_id, category_id):
    from .models import FilterPortlet
    self = FilterPortlet.objects.get(pk=portlet_id)
    sorting = request.session.get("sorting")

    category = Category.objects.get(id=category_id)
    if category is None:
        html = render_to_string("lfs/portlets/filter.html", {
            "show": False,
        })
    else:
        # get saved filters
        set_product_filters = dict(request.GET)
        product_filters = get_product_filters(
            category, set_product_filters, sorting)
        # ajax load some filters
        children = Filter.objects.filter(
            category=category).exclude(parent=None)
        load = []
        for f in Filter.objects.filter(
                category=category, parent=None,
                identificator__in=set_product_filters.keys(),
                show_as_tree=True):
            for o in f.filteroption_set.all().order_by('-is_popular'):
                if f.identificator in set_product_filters and \
                    o.identificator in set_product_filters[f.identificator] \
                        and str(o.id) not in load:
                    load.append(str(o.id))

        products = Product.objects.filter(
            active=True,
            status__is_visible=True,
            categories__in=[category, ],
            sub_type__in=[STANDARD_PRODUCT, PRODUCT_WITH_VARIANTS])\
            .distinct().select_related()

        for f in children:
            for o in f.filteroption_set.all().order_by('-is_popular'):
                if f.identificator in set_product_filters and \
                        o.identificator in \
                        set_product_filters[f.identificator]:
                    for po in f.parent.filteroption_set.all()\
                            .order_by('-is_popular'):
                        if products.filter(
                            filteroption__id__in=[o.id, po.id]
                        ).count() > 0:
                            if str(po.id) not in load:
                                load.append(str(po.id))

        ajax_load = ','.join(load)
        html = render_to_string(
            "lfs/portlets/filter.html",
            RequestContext(request, {
                "show": True,
                "title": self.title,
                'category': category,
                "product_filters": product_filters,
                'use_after_choose': self.use_after_choose,
                'portlet_id': self.id,
                'ajax_load': ajax_load,
                'load_related_options': self.load_related_options
            }))

    cache_key = "%s-filter-ajax-loading-%s-%s" %\
        (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX,
            self.id,
            category.id
        )
    cache.set(cache_key, html)
    return HttpResponse(simplejson.dumps({
        'html': html
    }))
