# coding: utf-8
import simplejson

from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
from django.contrib.contenttypes.models import ContentType

from portlets.models import (
    PortletAssignment,
    PortletRegistration,
    Slot,
    PortletBlocking,
)
from portlets.utils import (
    get_slots,
)
from ..core.utils import LazyEncoder
from .utils import invalidate_portlet_cache


def _render_portlets(request, content_type_id, object_id):
    content_type = ContentType.objects.get(id=content_type_id)
    object = content_type.get_object_for_this_type(id=object_id)
    slots = get_slots(object)

    return render_to_string('admin/portlets_includes/portlets_list.html', {
        'object_id': object_id,
        'content_type_id': content_type_id,
        'slots': slots,
    })


@permission_required(
    "core.manage_portlets",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def portlets_for_object(request, content_type_id, object_id):
    return HttpResponse(_render_portlets(request, content_type_id, object_id))


def move_portlet(request, portlet_assignment_id):
    if request.user.has_perm('core.manage_portlets'):
        pa = PortletAssignment.objects.get(id=portlet_assignment_id)
        direction = request.GET.get("direction", "0")
        if direction == "1":
            pa.position += 15
        else:
            pa.position -= 15
            if pa.position < 0:
                pa.position = 10
        pa.save()
        update_portlet_positions(pa)

        content_type = pa.content_type
        object_id = pa.content_id
        obj = content_type.get_object_for_this_type(pk=object_id)
        invalidate_portlet_cache(pa.slot.name, obj)

        result = simplejson.dumps({
            "html": [[
                "#portlets-list-wrapper",
                _render_portlets(
                    request, pa.content_type.id, pa.content_id)]]
        },
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#portlets-list-wrapper",
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


def delete_portlet(request, portlet_assignment_id):
    """Deletes the field.
    """
    if request.user.has_perm('core.manage_portlets'):
        pa = PortletAssignment.objects.get(id=portlet_assignment_id)
        pa.delete()

        content_type = pa.content_type
        object_id = pa.content_id
        obj = content_type.get_object_for_this_type(pk=object_id)
        invalidate_portlet_cache(pa.slot.name, obj)

        result = simplejson.dumps({
            "html": [[
                "#portlets-list-wrapper",
                _render_portlets(request, pa.content_type.id, pa.content_id)]],
        },
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#portlets-list-wrapper",
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


@permission_required(
    "core.manage_portlets",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def change_portlet(
        request, portlet_assignment_id,
        template_name="admin/portlets_includes/change_portlet.html"):
    pa = PortletAssignment.objects.get(id=portlet_assignment_id)
    if request.POST:
        form = pa.portlet.form(data=request.POST)
        if form.is_multipart():
            form = pa.portlet.form(data=request.POST, files=request.FILES)
        if form.is_valid():
            portlet = form.save()

            pa.blocked_in_inheritor = portlet.blocked_in_inheritor
            pa.slot_id = request.POST.get("slot")
            pa.save()

            content_type = pa.content_type
            object_id = pa.content_id
            obj = content_type.get_object_for_this_type(pk=object_id)
            invalidate_portlet_cache(pa.slot.name, obj)

            return HttpResponse(
                '<html><script>opener.on_portlets_popup_close();'
                'window.close();</script></html>')
    else:
        form = pa.portlet.form()
    slots = Slot.objects.all()
    for slot in slots:
        if slot.pk == pa.slot.pk:
            slot.selected = True

    return render_to_response(template_name, RequestContext(request, {
        'form': form,
        'is_popup': True,
        'has_file_field': True,
        'slots': slots,
    }))


@permission_required(
    "core.manage_portlets",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def add_portlet(
        request, content_type_id, object_id,
        model_id, template_name="admin/portlets_includes/change_portlet.html"):
    content_type = ContentType.objects.get(id=content_type_id)
    model_class = ContentType.objects.get(
        model=PortletRegistration.objects.get(id=model_id).type)

    if request.POST:
        portlet = model_class.model_class()

        form = portlet().form(prefix="portlet", data=request.POST)
        if form.is_multipart():
            form = portlet().form(
                prefix="portlet", data=request.POST, files=request.FILES)
        if form.is_valid():
            p = form.save()
            slot = request.POST.get("slot")
            portlet = PortletAssignment.objects.filter(
                content_type=content_type,
                content_id=object_id,
                slot_id=slot).order_by('-position')
            if portlet:
                position = portlet[0].position + 10
            else:
                position = 10
            pa = PortletAssignment()
            pa.portlet = p
            pa.content_type = content_type
            pa.content_id = object_id
            pa.position = position
            pa.slot_id = slot
            pa.save()

            obj = content_type.get_object_for_this_type(pk=object_id)
            invalidate_portlet_cache(slot, obj)
            return HttpResponse(
                '<html><script>opener.on_portlets_popup_close();'
                'window.close();</script></html>')
    else:
        portlet = model_class.model_class()
        form = portlet().form(prefix="portlet")
    slots = Slot.objects.all()
    return render_to_response(template_name, RequestContext(request, {
        'form': form,
        'is_popup': True,
        'has_file_field': True,
        'slots': slots,
    }))


def delete_all_portlets(request, content_type_id, object_id):
    if request.user.has_perm('core.manage_portlets'):
        content_type = ContentType.objects.get(id=content_type_id)
        object = content_type.get_object_for_this_type(id=object_id)
        portlets = PortletAssignment.objects.filter(
            content_type=content_type, content_id=object_id)

        slots = set()
        for p in portlets:
            slots.add(p.slot.name)
            p.delete()

        for slot in slots:
            invalidate_portlet_cache(slot, object)

        result = simplejson.dumps({
            "html": [[
                "#portlets-list-wrapper",
                _render_portlets(request, content_type_id, object_id)]],
        },
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#portlets-list-wrapper",
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


def update_portlet_positions(pa):
    for i, pa in enumerate(
            PortletAssignment.objects.filter(
                content_type=pa.content_type,
                content_id=pa.content_id, slot=pa.slot)):
        pa.position = (i + 1) * 10
        pa.save()


def blocked_slot(request, content_type_id, object_id):
    if request.user.has_perm('core.manage_portlets'):
        slot_id = request.POST['slot_id'][0]
        if 'checked' in request.POST:
            pb = PortletBlocking.objects.get_or_create(
                slot_id=slot_id,
                content_type_id=content_type_id, content_id=object_id)
        else:
            pb = PortletBlocking.objects.get(
                slot_id=slot_id,
                content_type=content_type_id, content_id=object_id)
            pb.delete()
        result = simplejson.dumps({
            "html": 'good',
        },
            cls=LazyEncoder
        )

        content_type = ContentType.objects.get(id=content_type_id)
        object = content_type.get_object_for_this_type(id=object_id)
        slot = Slot.objects.get(pk=slot_id)
        invalidate_portlet_cache(slot.name, object)
    else:
        result = simplejson.dumps({
            "html": 'bad',
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)
