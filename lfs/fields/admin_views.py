# coding: utf-8
import logging
import simplejson

from django.conf import settings
from django.utils.translation import ugettext as _
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.contrib.auth.decorators import permission_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.contenttypes.models import ContentType

from ..core.utils import LazyEncoder
from .models.fields import ChoiceField, CharField
from .models.fields_objects import FieldsObjects


logger = logging.getLogger('sshop')


def _render_fields(request, content_type_id, object_id):
    content_type = ContentType.objects.get(id=content_type_id)
    fields = FieldsObjects.objects.filter(
        content_id=object_id, content_type=content_type)
    return render_to_string('admin/fields_list.html', {
        'fields': fields,
    })


@permission_required(
    "core.manage_fields",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def fields(request, content_type_id, object_id):
    return HttpResponse(
        _render_fields(request, content_type_id, object_id))


def move_field(request, field_id):
    """
    Moves a field up/down in portlet.

    **Parameters:**

        field_id
            The id of field which should be moved.

    **Query String:**

        direction
            The direction to which the portlet should be moved. One of 0 (up)
            or 1 (down).
    """
    try:
        f = FieldsObjects.objects.get(pk=field_id)
    except FieldsObjects.DoesNotExist:
        return HttpResponse(_(u'Field object does not exist.'))

    if request.user.has_perm('core.manage_fields'):
        direction = request.GET.get("direction", "0")
        if direction == "1":
            f.position += 15
        else:
            f.position -= 15
            if f.position < 0:
                f.position = 10
        f.save()
        update_field_positions(f.content_type, f.content_id)
        result = simplejson.dumps({
            "html": [[
                "#fields-list-wrapper",
                _render_fields(request, f.content_type.id, f.content_id)]]
        },
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#fields-list-wrapper",
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


def delete_field(request, field_id):
    """Deletes the field.
    """
    try:
        f = FieldsObjects.objects.get(pk=field_id)
    except FieldsObjects.DoesNotExist:
        return HttpResponse(_(u'Field object does not exist.'))

    if request.user.has_perm('core.manage_fields'):
        f.delete()
        update_field_positions(f.content_type, f.content_id)
        result = simplejson.dumps({
            "html": [[
                "#fields-list-wrapper",
                _render_fields(
                    request, f.content_type.id, f.content_id)]],
        },
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#fields-list-wrapper",
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


@permission_required(
    "core.manage_fields",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def add_field(
        request, content_type_id, object_id,
        model_id, template_name="admin/change_field.html"):
    model_class = ContentType.objects.get(app_label="fields", id=model_id)
    if request.POST:
        _field = model_class.model_class()
        form = _field().form(data=request.POST)
        if form.is_valid():
            field = form.save()
            content_object = ContentType.objects.get(id=content_type_id)
            object = content_object.get_object_for_this_type(id=object_id)
            f = FieldsObjects(
                content=object,
                field=field,
            )
            try:
                f.save()
            except Exception, e:
                logger.error(u'%s' % unicode(e))
            return HttpResponse(
                '<html><script>opener.on_fields_popup_close();'
                'window.close();</script></html>')
    else:
        _field = model_class.model_class()
        form = _field().form()
    return render_to_response(template_name, RequestContext(request, {
        'field': _field,
        'form': form,
        'is_popup': True,
    }))


@permission_required(
    "core.manage_fields",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def change_field(request, field_id, template_name="admin/change_field.html"):
    f = FieldsObjects.objects.get(pk=field_id)
    if request.POST:
        form = f.field.form(data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse(
                '<html><script>opener.on_fields_popup_close();'
                'window.close();</script></html>')
    else:
        form = f.field.form()
    return render_to_response(template_name, RequestContext(request, {
        'field': f.field,
        'form': form,
        'is_popup': True,
    }))


def delete_all_fields(request, content_type_id, object_id):
    if request.user.has_perm('core.manage_fields'):
        content_type = ContentType.objects.get(id=content_type_id)
        fields = FieldsObjects.objects.filter(
            content_type=content_type, content_id=object_id)
        for field in fields:
            field.delete()
        result = simplejson.dumps({
            "html": [[
                "#fields-list-wrapper",
                _render_fields(request, content_type_id, object_id)]],
        },
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#fields-list-wrapper",
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


def update_field_positions(content_type, content_id):
    for i, obj in enumerate(FieldsObjects.objects.filter(
            content_type=content_type, content_id=content_id)):
        obj.position = (i + 1) * 10
        obj.save()
