# coding: utf-8
from django import forms
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _, ugettext

from datetimewidget.widgets import DateTimeWidget
from ..arrays import FieldsDataManager


class Field(object):
    """Base class for all sshop field.
    """
    class Meta:
        app_label = "fields"


class CharField(models.Model, Field):
    """Class for char field
    """
    field_name = models.CharField(
        _(u"Name"), blank=False, max_length=100, default='')
    identificator = models.CharField(
        _(u"Identificator"), blank=True, max_length=100)
    field_value = models.TextField(_(u"Value"), blank=True)
    required = models.BooleanField(_(u"Required"), default=False)
    disabled_by_default = models.BooleanField(
        _(u'Disabled by default'), default=False)
    css_class = models.CharField(
        _(u'Custom CSS class'), max_length=50, blank=True, default='')

    widget_name = ugettext(u"Text input")
    widget_icon = '/static/icons/ui-text-field.png'

    def __unicode__(self):
        return ugettext(
            "CharField: %(name)s %(value)s") % {
                'name': self.field_name,
                'value': self.field_value}

    @property
    def content_type(self):
        """Returns the content_type of the field as lower string.

        This is for instance used to select the appropriate form for the
        field.
        """
        return u"char"

    @property
    def name(self):
        """Returns the descriptive name of the field.
        """
        return self.widget_name

    def is_valid(self, request, product=None):
        """Returns True if the field is valid.
        """
        return True

    @property
    def value(self):
        """Returns the value of the field.
        """
        return self.field_value

    def get_field_object(self, container_object=None, initial=None, post={}):
        if type(initial) is list:
            initial = ''.join(initial)
        if self.disabled_by_default and not initial:
            return None
        elif initial == '!TOUCH':
            initial = ''

        return {
            'name': self.identificator,
            'obj': forms.CharField(
                label=self.field_name,
                required=self.required,
                initial=initial,
            ),
        }

    def form(self, **kwargs):
        return CharFieldForm(instance=self, **kwargs)


class CharFieldForm(forms.ModelForm):
    class Meta:
        model = CharField
        widgets = {
            'field_value': forms.TextInput,
        }


class ChoiceField(models.Model, Field):
    """Class for choice field
    """
    field_name = models.CharField(
        _(u"Name"), blank=False, max_length=100, default='')
    identificator = models.CharField(
        _(u"Identificator"), blank=True, max_length=100)
    field_value = models.TextField(_(u"Value"), blank=True)
    required = models.BooleanField(_(u"Required"), default=False)
    disabled_by_default = models.BooleanField(
        _(u'Disabled by default'), default=False)
    css_class = models.CharField(
        _(u'Custom CSS class'), max_length=50, blank=True, default='')

    widget_name = ugettext(u"Select box")
    widget_icon = '/static/icons/ui-combo-box.png'

    def __unicode__(self):
        return ugettext(
            "ChoiceField: %(name)s %(value)s") % {
                'name': self.field_name,
                'value': self.field_value}

    @property
    def content_type(self):
        """Returns the content_type of the field as lower string.

        This is for instance used to select the appropriate form for the
        field.
        """
        return u"choice"

    @property
    def name(self):
        """Returns the descriptive name of the field.
        """
        return self.widget_name

    def is_valid(self, request, product=None):
        """Returns True if the field is valid.
        """
        return True

    @property
    def value(self):
        """Returns the value of the field.
        """
        return self.field_value

    def get_field_object(self, container_object=None, initial=None, post={}):
        d = FieldsDataManager(data=container_object.extra)
        choices = []
        widget_type = 'simple'
        actions = []
        if type(initial) is list:
            initial = ''.join(initial)
        if self.disabled_by_default and not initial:
            return None
        elif initial == '!TOUCH':
            initial = ''

        w = None
        if self.field_value:
            w = d.get_array(self.field_value)
        elif ('%s__choices' % self.identificator) in post:
            w = d.get_array(post['%s__choices' % self.identificator])

        if w is not None:
            for x in w:
                choices.append((x['value'], x['value']))
                if x['type'] == 'action':
                    widget_type = 'action'
                    if initial and initial == x['value']:
                        actions = x['actions']
                    elif initial is None:
                        t = w[0]
                        if t['type'] == 'action':
                            actions = t['actions']

        if self.disabled_by_default and not choices:
            return None

        return {
            'name': self.identificator,
            'obj': forms.ChoiceField(
                label=self.field_name,
                required=self.required,
                choices=choices,
                widget=forms.Select(attrs={
                    'class': 'custom-form__widget--%s' % widget_type,
                }),
                initial=initial,
            ),
            'actions': actions,
        }

    def form(self, **kwargs):
        return ChoiceFieldForm(instance=self, **kwargs)


class ChoiceFieldForm(forms.ModelForm):
    class Meta:
        model = ChoiceField
        widgets = {
            'field_value': forms.TextInput,
        }


class DateField(models.Model, Field):
    """Class for choice field
    """
    field_name = models.CharField(
        _(u"Name"), blank=False, max_length=100, default='')
    identificator = models.CharField(
        _(u"Identificator"), blank=True, max_length=100)
    field_value = models.TextField(_(u"Value"), blank=True)
    required = models.BooleanField(_(u"Required"), default=False)
    disabled_by_default = models.BooleanField(
        _(u'Disabled by default'), default=False)
    css_class = models.CharField(
        _(u'Custom CSS class'), max_length=50, blank=True, default='')

    widget_name = ugettext(u"Calendar")
    widget_icon = '/static/icons/ui-combo-box-calendar.png'

    def __unicode__(self):
        return ugettext(
            "DateField: %(name)s %(value)s") % {
                'name': self.field_name,
                'value': self.field_value}

    @property
    def content_type(self):
        """Returns the content_type of the field as lower string.

        This is for instance used to select the appropriate form for the
        field.
        """
        return u"date"

    @property
    def name(self):
        """Returns the descriptive name of the field.
        """
        return self.widget_name

    def is_valid(self, request, product=None):
        """Returns True if the field is valid.
        """
        return True

    @property
    def value(self):
        """Returns the value of the field.
        """
        return self.field_value

    def get_field_object(self, container_object=None, initial=None, post={}):
        import datetime
        if initial is None:
            if '%s_0' % self.identificator in post:
                initial = post['%s_0' % self.identificator]
        if type(initial) is list:
            initial = ''.join(initial)
        if self.disabled_by_default and not initial:
            return None
        elif initial == '!TOUCH':
            initial = ''

        if initial:
            date = datetime.datetime.strptime(initial, "%d/%m/%Y")
            initial = date

        date_options = {
            'format': 'dd/mm/yyyy',
            'autoclose': 'true',
            'todayBtn': 'true',
            'startDate': '01/01/1900',
            'minView': '2',
        }

        initial_value = timezone.now()
        if initial:
            initial_value = initial
        elif self.field_value:
            initial_value = datetime.datetime.strptime(
                self.field_value, "%d/%m/%Y")

        return {
            'name': self.identificator,
            'obj': forms.DateField(
                label=self.field_name,
                initial=initial_value,
                widget=DateTimeWidget(
                    options=date_options,
                ),
                required=self.required),
        }

    def form(self, **kwargs):
        return DateFieldForm(instance=self, **kwargs)


class DateFieldForm(forms.ModelForm):
    class Meta:
        model = DateField
        widgets = {
            'field_value': forms.TextInput,
        }


class CheckboxField(models.Model, Field):
    """Class for check box field
    """
    field_name = models.CharField(
        _(u"Name"), blank=False, max_length=100, default='')
    identificator = models.CharField(
        _(u"Identificator"), blank=True, max_length=100)
    field_value = models.BooleanField(_(u"Default value"), blank=False)
    required = models.BooleanField(_(u"Required"), default=False)
    disabled_by_default = models.BooleanField(
        _(u'Disabled by default'), default=False)
    css_class = models.CharField(
        _(u'Custom CSS class'), max_length=50, blank=True, default='')

    widget_name = ugettext(u"Checkbox")
    widget_icon = '/static/icons/ui-check-box.png'

    def __unicode__(self):
        return ugettext(
            "CheckboxField: %(name)s %(value)s") % {
                'name': self.field_name,
                'value': self.field_value}

    @property
    def content_type(self):
        """Returns the content_type of the field as lower string.

        This is for instance used to select the appropriate form for the
        field.
        """
        return u"checkbox"

    @property
    def name(self):
        """Returns the descriptive name of the field.
        """
        return self.widget_name

    def is_valid(self, request, product=None):
        """Returns True if the field is valid.
        """
        return True

    @property
    def value(self):
        """Returns the value of the field.
        """
        return self.field_value

    def get_field_object(self, container_object=None, initial=None, post={}):
        if type(initial) is list:
            initial = bool(''.join(initial))
        if self.disabled_by_default and not initial:
            return None
        elif initial == '!TOUCH':
            initial = False

        return {
            'name': self.identificator,
            'obj': forms.BooleanField(
                label=self.field_name,
                required=self.required,
                initial=initial,
            ),
        }

    def form(self, **kwargs):
        return CheckboxFieldForm(instance=self, **kwargs)


class CheckboxFieldForm(forms.ModelForm):
    class Meta:
        model = CheckboxField
        widgets = {
            'field_value': forms.CheckboxInput,
        }
