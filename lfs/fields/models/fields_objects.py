# django imports
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from django.db import models
from django.utils.translation import ugettext_lazy as _


class FieldsObjects(models.Model):
    """Assigns arbitrary field to arbitrary content objects.
    """
    class Meta:
        ordering = ["position"]
        verbose_name_plural = "Field objects"
        app_label = "fields"

    field_type = models.ForeignKey(
        ContentType, verbose_name=_(u"Field type"), related_name="field")
    field_id = models.PositiveIntegerField(_(u"Content id"))
    field = generic.GenericForeignKey(
        ct_field="field_type", fk_field="field_id")

    content_type = models.ForeignKey(
        ContentType, verbose_name=_(u"Content type"), related_name="fields")
    content_id = models.PositiveIntegerField(_(u"Content id"))
    content = generic.GenericForeignKey(
        ct_field="content_type", fk_field="content_id")

    position = models.PositiveIntegerField(_(u"Position"), default=999)

    def __unicode__(self):
        return u'%s: %s' % (
            self.field.field_name,
            self.field.identificator,
        )
