# coding: utf-8
from django.conf.urls.defaults import *

# These url patterns use for admin interface
urlpatterns = patterns(
    'lfs.fields.admin_views',
    url(
        r'^fields/(?P<content_type_id>[-\d]*)/(?P<object_id>[-\d]*)/$$',
        'fields', name="admin_fields"),
    url(
        r'^move-field/(?P<field_id>[-\d]*)/$',
        'move_field', name="admin_move_field"),
    url(
        r'^delete-field/(?P<field_id>\d+)$',
        "delete_field", name="admin_delete_field"),
    url(
        r'^add-field/(?P<content_type_id>[-\d]*)/(?P<object_id>[-\d]*)/'
        '(?P<model_id>[-\d]*)/$', 'add_field',
        name="admin_add_field"),
    url(
        r'^change-field/(?P<field_id>[-\d]*)$',
        "change_field", name="admin_change_field"),
    url(
        r'^delete-all-fields/(?P<content_type_id>[-\d]*)/'
        '(?P<object_id>[-\d]*)/$',
        'delete_all_fields', name="admin_delete_all_fields"),
)
