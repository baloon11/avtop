# coding: utf-8
from django.conf.urls.defaults import *

# These url patterns use for admin interface
urlpatterns = patterns(
    'lfs.mail.views',
    url(
        r'^preview-mail-text-template/(?P<template_id>[-\d]*)/$',
        'preview_txt', name="admin_mail_preview_txt"),
    url(
        r'^preview-mail-html-template/(?P<template_id>[-\d]*)/$',
        'preview_html', name="admin_mail_preview_html"),
)
