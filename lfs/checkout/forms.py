# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from ..core.utils import get_default_shop, import_symbol


class OnePageCheckoutForm(forms.Form):
    """
    """
    message = forms.CharField(
        label=_(u"Your message to us"),
        widget=forms.Textarea(attrs={
            'cols': '80',
            'class': 'input-xxlarge'}),
        required=False)

    def __init__(
            self, customer=None, request=None, *args, **kwargs):
        from ..fields.utils import get_field_object_list

        shop = get_default_shop()
        super(OnePageCheckoutForm, self).__init__(*args, **kwargs)

        self.customer_fields = shop.checkout_form_fields.split(',')
        fields = get_field_object_list(shop, request=request)

        for f in fields:
            if f['name'] in self.customer_fields:
                self.fields[f['name']] = f['obj']

        if customer is not None:
            if shop.ask_email_when_checkout:
                emails = customer.emails.order_by('-default')
                if emails.count() > 0:
                    email_choices = [
                        (x.id, x.email) for x in emails
                    ]
                    email_choices.append(('!NEW', _(u'Add another...')))
                    self.fields['email_choice'] = forms.ChoiceField(
                        label=_(u'Email'),
                        choices=email_choices,
                        widget=forms.Select(
                            attrs={
                                'class': '__action__checkout__select-email',
                            })
                    )
                    self.fields['email'] = forms.EmailField(
                        label='',
                        required=False,
                    )
                else:
                    self.fields['email'] = forms.EmailField(
                        label=_(u'Email'),
                        required=getattr(
                            settings,
                            'CHECKOUT_REQUIRE_EMAIL',
                            False),
                    )
        else:
            if shop.ask_email_when_checkout:
                self.fields['email'] = forms.EmailField(
                    label=_(u'Email'),
                    required=getattr(
                        settings,
                        'CHECKOUT_REQUIRE_EMAIL',
                        False),
                )

        if customer is not None:
            if shop.ask_phone_when_checkout:
                phones = customer.phones.order_by('-default')
                if phones.count() > 0:
                    phone_choices = [
                        (x.id, x.get_formatted_number()) for x in phones
                    ]
                    phone_choices.append(('!NEW', _(u'Add another...')))
                    self.fields['phone_choice'] = forms.ChoiceField(
                        label=_(u'Phone'),
                        help_text=getattr(
                            settings, 'CHECKOUT_PHONE_HELP_TEXT', ''),
                        choices=phone_choices,
                        widget=forms.Select(
                            attrs={
                                'class': '__action__checkout__select-phone',
                            })
                    )
                    self.fields['phone'] = forms.CharField(
                        label='',
                        required=False,
                        widget=forms.TextInput(
                            attrs={
                                'placeholder':
                                getattr(
                                    settings, 'CHECKOUT_PHONE_PLACEHOLDER', '')
                            })
                    )
                else:
                    self.fields['phone'] = forms.CharField(
                        label=_(u'Phone'),
                        required=getattr(
                            settings,
                            'CHECKOUT_REQUIRE_PHONE',
                            False),
                        help_text=getattr(
                            settings, 'CHECKOUT_PHONE_HELP_TEXT', ''),
                        widget=forms.TextInput(
                            attrs={
                                'placeholder':
                                getattr(
                                    settings, 'CHECKOUT_PHONE_PLACEHOLDER', '')
                            })
                    )
        else:
            if shop.ask_phone_when_checkout:
                self.fields['phone'] = forms.CharField(
                    label=_(u'Phone'),
                    required=getattr(
                        settings,
                        'CHECKOUT_REQUIRE_PHONE',
                        True),
                    help_text=getattr(
                        settings, 'CHECKOUT_PHONE_HELP_TEXT', ''),
                    widget=forms.TextInput(
                        attrs={
                            'placeholder':
                            getattr(settings, 'CHECKOUT_PHONE_PLACEHOLDER', '')
                        })
                )

        if customer is not None:
            addresses = customer.addresses.order_by('-default')
            if addresses.count() > 0:
                address_choices = [
                    (x.id, x.get_customer_address()) for x in addresses
                ]
                self.fields['address_choice'] = forms.ChoiceField(
                    label=_(u'Address'),
                    choices=address_choices,
                    widget=forms.Select(
                        attrs={
                            'class': '__action__checkout__select-address',
                        })
                )

        self.order_fields = shop.order_form_fields.split(',')
        for f in fields:
            if f['name'] in self.order_fields:
                self.fields[f['name']] = f['obj']

        active_captcha = getattr(settings, 'ACTIVE_CAPTCHA', '')
        if active_captcha and \
                getattr(settings, 'CAPTCHA_USE_FOR_ANON_CHECKOUT', False) and \
                request.user.is_anonymous():
            captcha_class = import_symbol(active_captcha)
            self.fields['captcha'] = captcha_class()

    def _validate_phone(self, phone):
        import phonenumbers
        if phone:
            if getattr(settings, 'CHECKOUT_VALIDATE_PHONE', True):
                try:
                    phone_obj = phonenumbers.parse(
                        phone,
                        getattr(
                            settings, 'CUSTOMER_PHONE_NUMBER_REGION', 'UA'),
                    )
                except phonenumbers.NumberParseException:
                    raise forms.ValidationError(
                        _(u'The string is not a valid phone number.'))
                phone_str = '%s%s' % (
                    phone_obj.country_code,
                    phone_obj.national_number,
                )
                phone_type = phonenumbers.number_type(phone_obj)
                if phone_type == 99:
                    raise forms.ValidationError(
                        _(u"Invalid phone number."))
                return phone_str
            else:
                return phone
        else:
            return phone

    def clean_phone(self):
        """ Check the phone requirement
        """
        phone_choice = self.cleaned_data.get('phone_choice')
        phone = self.cleaned_data.get('phone')

        if phone_choice is not None:
            if phone_choice == '!NEW':
                return self._validate_phone(phone)
            else:
                return ''
        else:
            return self._validate_phone(phone)

    def clean_email(self):
        """ Check the email requirement
        """
        email_choice = self.cleaned_data.get('email_choice')
        email = self.cleaned_data.get('email')

        if email_choice is not None:
            if email_choice == '!NEW' and email.strip() == '':
                raise forms.ValidationError(
                    _(u"Email is required."))
        return email

    def get_customer_fields(self):
        for name in self.fields:
            if name in self.customer_fields:
                yield self[name]

    def get_order_fields(self):
        for name in self.fields:
            if name in self.order_fields:
                yield self[name]


class ShippingMethodForm(forms.Form):
    def __init__(
            self,
            shipping_method, request=None, address=None, *args, **kwargs):
        from ..fields.utils import get_field_object_list
        super(ShippingMethodForm, self).__init__(*args, **kwargs)

        fields = get_field_object_list(
            shipping_method, request=request, address=address)
        for f in fields:
            self.fields[f['name']] = f['obj']


class PaymentMethodForm(forms.Form):
    def __init__(
            self,
            payment_method, request=None, address=None, *args, **kwargs):
        from ..fields.utils import get_field_object_list
        super(PaymentMethodForm, self).__init__(*args, **kwargs)

        fields = get_field_object_list(
            payment_method, request=request, address=address)
        for f in fields:
            self.fields[f['name']] = f['obj']
