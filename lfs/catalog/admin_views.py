# coding: utf-8
import simplejson

from ..core.utils import LazyEncoder
from django.utils.translation import ugettext as _
from django.template.loader import render_to_string
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import permission_required
from django.views.decorators.http import require_POST
from django.conf import settings
from .models import (
    Category,
    Product,
    PropertyOptionRelation,
    PropertyType
)
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .settings import ADMIN_CATEGORY_PRODUCT_COUNT
from pytils.translit import slugify


def _render_products(request, category_id):
    category = Category.objects.get(pk=category_id)
    products = category.products.all().order_by('status', '-price', 'name')
    paginator = Paginator(products, ADMIN_CATEGORY_PRODUCT_COUNT)

    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        products = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        products = paginator.page(paginator.num_pages)

    return render_to_string('admin/catalog_includes/product_list.html', {
        'category': category,
        'products': products,
    })


def _render_properties_options(request, current_property_type, query=None):
    if query:
        all_property_options_relation = PropertyOptionRelation.objects.filter(
            property_type=current_property_type,
            property_option__name__icontains=query).order_by('position')
    else:
        all_property_options_relation = PropertyOptionRelation.objects.filter(
            property_type=current_property_type).order_by('position')
    start = request.GET.get('start')
    try:
        start = int(start)
    except (ValueError, TypeError):
        start = 1
    amount = getattr(settings, 'ADMIN_PROPERTY_OPTION_PER_PAGE', 10)
    paginator = Paginator(all_property_options_relation, amount)
    try:
        current_page = paginator.page(start)
    except (EmptyPage, InvalidPage):
        current_page = paginator.page(paginator.num_pages)

    property_options = current_page.object_list
    return render_to_string(
        'admin/catalog_includes/properties-options.html',
        {
            'property_type': current_property_type,
            'property_options': property_options,
            'paginator': paginator,
            'current_page': current_page,
        })


@permission_required(
    "catalog.change_product",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def products_for_category(request, category_id):
    return HttpResponse(_render_products(request, category_id))


@permission_required(
    "catalog.change_product",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def delete_product_from_category(request, category_id, product_id):
    category = Category.objects.get(pk=category_id)
    product = Product.objects.get(pk=product_id)
    category.products.remove(product)

    result = simplejson.dumps({
        "html": [[
            "#products-list-wrapper",
            _render_products(request, category_id)]]
    },
        cls=LazyEncoder
    )
    return HttpResponse(result)


@require_POST
def generate_slug(request):
    title = request.POST['title']
    slug = slugify(title)
    response = simplejson.dumps({'slug': slug})
    return HttpResponse(response)


def sort_property_options(request, property_type_id):
    from ..core.utils import import_symbol
    try:
        property_type = PropertyType.objects.get(id=property_type_id)
    except PropertyType.DoesNotExist:
        return HttpResponse(_(u'Property option does not exist.'))

    if request.user.has_perm('catalog.change_propertyoption'):
        pors = PropertyOptionRelation.objects.filter(
            property_type_id=property_type_id)
        sort_type = request.GET.get('sort_type', 'asc')
        sort_methods = dict(
            (x for x in settings.PROPERTY_SORTERS if x is not None))
        sorter_class_name = sort_methods[sort_type]
        sorter_class = import_symbol(sorter_class_name)

        sorter = sorter_class()
        sorter.sort(pors)
        update_properties_options_positions(property_type)
        html = _render_properties_options(request, property_type)
        response = simplejson.dumps(
            {'html': [['#properties-options-list-wrapper', html]]})
    else:
        response = _(u'Property type is not specified.')
    return HttpResponse(response)


@permission_required(
    "catalog.change_propertytype",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def option_list_for_property_type(request):
    if 'property_type_id' in request.GET:
        property_type_id = request.GET['property_type_id']
        current_property_type = PropertyType.objects.get(pk=property_type_id)
        args_list = [request, current_property_type]
        if 'q' in request.GET and request.GET['q']:
            args_list += [request.GET['q']]
        html = _render_properties_options(*args_list)
        response = simplejson.dumps(
            {'html': [['#properties-options-list-wrapper', html]]})
    else:
        response = _(u'Property type is not specified.')
    return HttpResponse(response)


def update_properties_options_positions(current_property_type):
    for i, obj in enumerate(PropertyOptionRelation.objects.filter(property_type=current_property_type)):
        obj.position = (i + 1) * 10
        obj.save()


def move_property_option(request):
    option_id = request.GET['option_id']
    try:
        o = PropertyOptionRelation.objects.get(pk=option_id)
    except PropertyOption.DoesNotExist:
        return HttpResponse(_(u'Property option does not exist.'))

    if request.user.has_perm('catalog.change_propertyoption'):
        direction = request.GET.get("direction", "0")
        if direction == "1":
            o.position += 15
        else:
            o.position -= 15
            if o.position < 0:
                o.position = 10
        o.save()
        update_properties_options_positions(o.property_type)
        html = _render_properties_options(request, o.property_type)
        response = simplejson.dumps(
            {'html': [['#properties-options-list-wrapper', html]]})
    else:
        response = _(u'Property type is not specified.')
    return HttpResponse(response)


def delete_property_option(request):
    option_id = request.GET['option_id']
    try:
        o = PropertyOptionRelation.objects.get(pk=option_id)
    except PropertyOptionRelation.DoesNotExist:
        return HttpResponse((u'Property option does not exist.'))

    if request.user.has_perm('catalog.delete_propertyoptionrelation'):
        o.delete()
        update_properties_options_positions(o.property_type)
        html = _render_properties_options(request, o.property_type)
        response = simplejson.dumps(
            {'html': [['#properties-options-list-wrapper', html]]})
    else:
        response = _(u'Property type is not specified.')
    return HttpResponse(response)
