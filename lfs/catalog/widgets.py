from django.utils.safestring import mark_safe
from django import forms


class ReSlug(forms.TextInput):
    def __init__(self,  attrs=None):
        super(ReSlug, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        output = super(ReSlug, self)\
            .render(name, value, attrs)
        output += (
            '<i class="icon-refresh" id="generate_slug" style="cursor:pointer"></i>'
            '<script type="text/javascript">'
            '    $(\'#generate_slug\').click(function(){'
            '        button=$(this);'
            '        $.ajax({'
            '            url: "/superadmin/catalog/generate-slug",'
            '            type: "POST",'
            '            dataType: "json",'
            '            data: {'
            '                \'title\': button.parents(\'#product_form\').find(\'input[name=name]\').attr(\'value\'),'
            '            },'
            '            success: function(msg){'
            '                button.parents(\'#product_form\').find(\'input[name=slug]\').attr(\'value\', msg[\'slug\'])'
            '            }'
            '        });'
            '    });'
            '</script>'
        )

        return mark_safe(u''.join(output))
