# coding: utf-8
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

QUANTITY_FIELD_INTEGER = 0
QUANTITY_FIELD_DECIMAL_1 = 1
QUANTITY_FIELD_DECIMAL_2 = 2

QUANTITY_FIELD_TYPES = [
    (QUANTITY_FIELD_INTEGER, _(u"Integer")),
    (QUANTITY_FIELD_DECIMAL_1, _(u"Decimal 0.1")),
    (QUANTITY_FIELD_DECIMAL_2, _(u"Decimal 0.01")),
]

CHOICES_STANDARD = 0
CHOICES_YES = 2
CHOICES_NO = 3
CHOICES = [
    (CHOICES_STANDARD, _(u"Standard")),
    (CHOICES_YES, _(u"Yes")),
    (CHOICES_NO, _(u"No")),
]

STANDARD_PRODUCT = "0"
PRODUCT_WITH_VARIANTS = "1"
VARIANT = "2"
# CONFIGURABLE_PRODUCT = "3"

PRODUCT_TYPE_LOOKUP = {
    STANDARD_PRODUCT: _(u"Standard"),
    PRODUCT_WITH_VARIANTS: _(u"Product with variants"),
    VARIANT: _(u"Variant"),
    # CONFIGURABLE_PRODUCT: _(u"Configurable product")
}

PRODUCT_TYPE_CHOICES = [
    (STANDARD_PRODUCT, _(u"Standard")),
    (PRODUCT_WITH_VARIANTS, _(u"Product with variants")),
    (VARIANT, _(u"Variant")),
    # (CONFIGURABLE_PRODUCT, _(u"Configurable product")),
]

PRODUCT_TYPE_FORM_CHOICES = [
    (STANDARD_PRODUCT, _(u"Standard")),
    (PRODUCT_WITH_VARIANTS, _(u"Product with variants")),
    # (CONFIGURABLE_PRODUCT, _(u"Configurable product")),
]

CATEGORY_VARIANT_DEFAULT = -1
CATEGORY_VARIANT_CHEAPEST_PRICE = -2
CATEGORY_VARIANT_CHEAPEST_BASE_PRICE = -3
CATEGORY_VARIANT_CHEAPEST_PRICES = -4
CATEGORY_VARIANT_CHOICES = [
    (CATEGORY_VARIANT_DEFAULT, _(u"Default")),
    (CATEGORY_VARIANT_CHEAPEST_PRICE, _(u"Cheapest price")),
    (CATEGORY_VARIANT_CHEAPEST_BASE_PRICE, _(u"Cheapest base price")),
    (CATEGORY_VARIANT_CHEAPEST_PRICES, _(u"Cheapest prices")),
]

# LIST = 0
# SELECT = 1
# VARIANTS_DISPLAY_TYPE_CHOICES = [
#     (LIST, _(u"List")),
#     (SELECT, _(u"Select")),
# ]

CONTENT_PRODUCTS = 0
CONTENT_CATEGORIES = 1

CONTENT_CHOICES = (
    (CONTENT_PRODUCTS, _(u"Products")),
    (CONTENT_CATEGORIES, _(u"Categories")),
)

DELIVERY_TIME_UNIT_HOURS = 1
DELIVERY_TIME_UNIT_DAYS = 2
DELIVERY_TIME_UNIT_WEEKS = 3
DELIVERY_TIME_UNIT_MONTHS = 4

DELIVERY_TIME_UNIT_CHOICES = (
    (DELIVERY_TIME_UNIT_HOURS, _(u"hours")),
    (DELIVERY_TIME_UNIT_DAYS, _(u"days")),
    (DELIVERY_TIME_UNIT_WEEKS, _(u"weeks")),
    (DELIVERY_TIME_UNIT_MONTHS, _(u"months")),
)

DELIVERY_TIME_UNIT_SINGULAR = {
    DELIVERY_TIME_UNIT_HOURS: _(u"hour"),
    DELIVERY_TIME_UNIT_DAYS: _(u"day"),
    DELIVERY_TIME_UNIT_WEEKS: _(u"week"),
    DELIVERY_TIME_UNIT_MONTHS: _(u"month"),
}

PROPERTY_VALUE_TYPE_FILTER = 0
PROPERTY_VALUE_TYPE_DEFAULT = 1
PROPERTY_VALUE_TYPE_DISPLAY = 2
PROPERTY_VALUE_TYPE_VARIANT = 3

PROPERTY_NUMBER_FIELD = 1
PROPERTY_TEXT_FIELD = 2
PROPERTY_SELECT_FIELD = 3

PROPERTY_FIELD_CHOICES = (
    (PROPERTY_NUMBER_FIELD, _(u"Float field")),
    (PROPERTY_TEXT_FIELD, _(u"Text field")),
    (PROPERTY_SELECT_FIELD, _(u"Select field")),
)

PROPERTY_STEP_TYPE_AUTOMATIC = 1
PROPERTY_STEP_TYPE_FIXED_STEP = 2
PROPERTY_STEP_TYPE_MANUAL_STEPS = 3

PROPERTY_STEP_TYPE_CHOICES = (
    (PROPERTY_STEP_TYPE_AUTOMATIC, _(u"Automatic")),
    (PROPERTY_STEP_TYPE_FIXED_STEP, _(u"Fixed step")),
    (PROPERTY_STEP_TYPE_MANUAL_STEPS, _(u"Manual steps")),
)


CAT_PRODUCT_PATH = "lfs/catalog/categories/product"   # category with products
CAT_CATEGORY_PATH = "lfs/catalog/categories/category"
    # category with subcategories
VARIANTS_PATH = "lfs/catalog/variants"   # product variants templates
PRODUCT_PATH = "lfs/catalog/products"   # product templates
IMAGES_PATH = "/media/lfs/icons"  # Path to template preview images


# Template configuration for variants display
VARIANTS_DISPLAY_TYPE_TEMPLATES = (
    (0, {
        "file": "%s/%s" % (VARIANTS_PATH, "as_table.html"),
        "name": _(u"As table")},),
)
VARIANTS_DISPLAY_TYPE_TEMPLATES = getattr(
    settings,
    'VARIANTS_DISPLAY_TYPE_TEMPLATES',
    VARIANTS_DISPLAY_TYPE_TEMPLATES)


# Template configuration for category display
CATEGORY_TEMPLATES = (
    (0, {
        "file": "%s/%s" % (CAT_PRODUCT_PATH, "default.html"),
        "image": IMAGES_PATH + "/product_default.png",
        "name": _(u"Category with products"),
    }),
    (1, {
        "file": "%s/%s" % (CAT_CATEGORY_PATH, "default.html"),
        "image": IMAGES_PATH + "/category_square.png",
        "name": _(u"Category with subcategories"),
    }),
)
CATEGORY_TEMPLATES = getattr(
    settings, 'CATEGORY_TEMPLATES', CATEGORY_TEMPLATES)

# Template configuration for product display
PRODUCT_TEMPLATES = (
    (0, {
        "file": "%s/%s" % (PRODUCT_PATH, "product_inline.html"),
        "image": IMAGES_PATH + "/product_default.png",
        "name": _(u"Default")
    },),
)
PRODUCT_TEMPLATES = getattr(settings, 'PRODUCT_TEMPLATES', PRODUCT_TEMPLATES)


sizes = getattr(
    settings, 'SSHOP_IMAGE_SIZES',
    {
        'small': (60, 60),
        'medium': (100, 100),
        'large': (200, 200),
        'huge': (400, 400),
    })

THUMBNAIL_SIZES = (
    sizes['small'],
    sizes['medium'],
    sizes['large'],
    sizes['huge']
)

watermarked = getattr(
    settings, 'SSHOP_IMAGE_WATERMARKED',
    {
        'small': False,
        'medium': False,
        'large': False,
        'huge': True,
    })

watermarked_zoom = getattr(
    settings, 'SSHOP_IMAGE_WATERMARKED_ZOOM',
    {
        'small': 0.15,
        'medium': 0.25,
        'large': 0.5,
        'huge': 1,
    })

PROPERTY_TYPE_STRING = 0
PROPERTY_TYPE_INTEGER = 5
PROPERTY_TYPE_FLOAT = 10
PROPERTY_TYPE_CHOICE = 15
PROPERTY_TYPE_BOOLEAN = 20

PROPERTY_TYPE_CHOICES = [
    (PROPERTY_TYPE_STRING, _(u'String')),
    (PROPERTY_TYPE_INTEGER, _(u'Integer')),
    (PROPERTY_TYPE_FLOAT, _(u'Float')),
    (PROPERTY_TYPE_CHOICE, _(u'Choice')),
    (PROPERTY_TYPE_BOOLEAN, _(u'Boolean'))
]

DEFAULT_PROPERTY_TYPE_REGEX = u'''%s
%s
%s''' % (
    '@strip',
    r'(?P<value>\d+[.,]?\d*)',
    '@replace:value:,:.',
)

ADMIN_CATEGORY_PRODUCT_COUNT = 20

PROPERTY_TYPE_VALIDATORS = getattr(
    settings,
    'PROPERTY_TYPE_VALIDATORS',
    (('', _(u'Standard regex and rules')),))
