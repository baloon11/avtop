# coding: utf-8
from django.core import urlresolvers
from django.conf import settings
from django.contrib import admin
from django.template import RequestContext
from django.utils.translation import ugettext as _
from django.shortcuts import render_to_response
from django.contrib.admin import SimpleListFilter
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect
from django.conf.urls.defaults import patterns, url
from django.contrib.admin.views.main import ChangeList
from django.core.paginator import InvalidPage
from django.contrib.admin.options import IncorrectLookupParameters
from django.http import HttpResponse

import re

from treeadmin.admin import TreeAdmin
from genericadmin.admin import GenericAdminModelAdmin, GenericTabularInline
from suit.admin import SortableModelAdmin, SortableTabularInline

from hr_urls.models import FilterUrlTemplate
from adminextras.admin import LinkedInline
from ..filters.models import Filter
from .models import (
    Category,
    Image,
    Product,
    ProductAccessories,
    ProductAttachment,
    Property,
    PropertyOption,
    ProductPropertyValue,
    StaticBlock,
    DeliveryTime,
    ProductStatus,
    SortType,
    PropertyValueIcon,
    Manufacturer,
    PropertySet,
    PropertySetRelation,
    PropertyUnit,
    PropertyType,
    PropertyOptionRelation,
    get_unique_id_str
)
from .forms import (
    AddCategoryForm,
    EditCategoryForm,
    AddProductForm,
    EditStandardProductForm,
    EditProductWithVariantsForm,
    EditVariantForm,
    ProductPropertyValueForm,
    PropertyValueIconForm,
    StaticBlockForm,
    PropertySetRelationForm,
    ProductAccessoryForm,
    PropertyTypeForm,
    ChangeSubtypeForm,
    ApplyPropertySetForm,
    CreatePropertySetFromProductForm,
    FilterUrlTemplateForm,
    ChangeStatusForm,
    ImageInlineForm,
    ChangePriceForm,
    ChangeManufacturerForm,
    ChangeCategoriesForm,
    PropertyOptionRelationForm,
    CategoryTemplateForm
)
from ..marketing.models import ProductListItem
from .settings import STANDARD_PRODUCT
from .settings import VARIANT
from .settings import PRODUCT_WITH_VARIANTS
from django.shortcuts import render


class FilterInline(SortableTabularInline, LinkedInline):
    model = Filter
    suit_classes = 'suit-tab suit-tab-filters'
    fields = ('displayed_title',)
    sortable = 'position'


class URLProxyInline(SortableTabularInline):
    model = FilterUrlTemplate
    form = FilterUrlTemplateForm
    suit_classes = 'suit-tab suit-tab-urlproxy'
    extra = 0


class CategoryAdmin(TreeAdmin):

    """
    """
    list_display = (
        'name', 'slug', 'product_count', 'exclude_from_navigation',)
    mptt_level_indent = 40
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ['name']
    list_editable = ('exclude_from_navigation',)
    filter_include_ancestors = True

    edit_form = EditCategoryForm
    add_form = AddCategoryForm

    inlines = [URLProxyInline]

    base_readonly_fields = ('uid',)
    edit_readonly_fields = ('position', 'level', 'product_count',)

    edit_fieldsets = [
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': [
                'name', 'slug', 'uid', 'parent', 'exclude_from_navigation']
        }),
        (_('Info'), {
            'classes': ('suit-tab suit-tab-general',),
            'description': _(u'Additional information for category.'),
            'fields': ['icon', 'image', 'short_description', 'description']
        }),
        (_('Extras'), {
            'classes': ('suit-tab suit-tab-general collapse',),
            'description': _(u'Extra data. Not for manual editing.'),
            'fields': ['extras']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-view',),
            'fields': [
                'template', 'show_all_products', 'active_formats',
                'product_cols', 'product_rows', 'category_cols',
                'static_block']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-seo',),
            'fields': [
                'meta_h1',
                'meta_title',
                'meta_keywords', 'meta_description', 'seo']
        }),
    ]
    exclude = ['products', 'product_count']

    edit_suit_tabs = (
        ('general', _(u'General')),
        ('view', _(u'View')),
        ('seo', _(u'SEO')),
        ('products', _(u'Products')),
        ('urlproxy', _(u'URL proxy')),
        ('filters', _(u'Filters')),
        ('portlets', _(u'Portlets')),
    )
    add_suit_form_includes = None
    edit_suit_form_includes = (
        ('admin/catalog_includes/products.html', 'top', 'products'),
        ('admin/catalog_includes/filters.html', 'top', 'filters'),
        ('admin/portlets_includes/portlets.html', 'top', 'portlets'),
        ('admin/catalog_includes/hr-urls-info.html', 'top', 'urlproxy'),
    )

    actions = [
        'rebuild_counters',
        'apply_default_meta_h1',
        'apply_default_meta_title',
        'apply_default_meta_keywords',
        'apply_default_meta_description',
        'apply_default_meta_text',
        'update_filters',
        'set_category_template',
    ]

    class Media:
        css = {
            "all": ("/static/css/loadover.css",)
        }
        js = (
            '/static/jquery/jquery.cookie.js',
            '/static/js/sco.collapse.js',
            '/static/admin/js/ajax_links.js',
            '/static/js/loadover.js',
        )

    def _actions_column(self, instance):
        actions = super(CategoryAdmin, self)._actions_column(instance)
        return actions + [
            '<a title="%s" href="%s" target="blank">'
            '<i class="icon-hand-right"></i></a>' % (
                _(u'Go to page on site'),
                instance.get_absolute_url(),
            ),
        ]

    def get_form(self, request, obj=None, **kwargs):
        """
        Use special form during category creation
        """
        defaults = {}
        if obj is None:
            defaults.update({
                'form': self.add_form,
            })
            self.suit_form_tabs = None
            self.suit_form_includes = self.add_suit_form_includes
            self.readonly_fields = tuple()
        else:
            defaults.update({
                'form': self.edit_form,
            })
            self.suit_form_tabs = self.edit_suit_tabs
            self.suit_form_includes = self.edit_suit_form_includes
            self.readonly_fields = self.base_readonly_fields + \
                self.edit_readonly_fields
        defaults.update(kwargs)
        return super(CategoryAdmin, self).get_form(request, obj, **defaults)

    def get_fieldsets(self, request, obj=None, **kwargs):
        if obj is None:
            return super(CategoryAdmin, self).get_fieldsets(
                request, obj, **kwargs)
        else:
            return self.edit_fieldsets

    def rebuild_counters(self, request, queryset):
        for q in queryset:
            q.recount_products(save=True)
    rebuild_counters.short_description = _(u'Rebuild counters')

    def apply_default_meta_h1(self, request, queryset):
        queryset.update(
            meta_h1=getattr(
                settings, 'DEFAULT_CATEGORY_META_H1_TEMPLATE', ''))
    apply_default_meta_h1.short_description = _(u'Apply default H1 header')

    def apply_default_meta_title(self, request, queryset):
        queryset.update(
            meta_title=getattr(
                settings, 'DEFAULT_CATEGORY_META_TITLE_TEMPLATE', ''))
    apply_default_meta_title.short_description = _(u'Apply default META title')

    def apply_default_meta_keywords(self, request, queryset):
        queryset.update(
            meta_keywords=getattr(
                settings, 'DEFAULT_CATEGORY_META_KEYWORDS_TEMPLATE', ''))
    apply_default_meta_keywords.short_description = \
        _(u'Apply default META keywords')

    def apply_default_meta_description(self, request, queryset):
        queryset.update(
            meta_description=getattr(
                settings, 'DEFAULT_CATEGORY_META_DESCRIPTION_TEMPLATE', ''))
    apply_default_meta_description.short_description = \
        _(u'Apply default META description')

    def apply_default_meta_text(self, request, queryset):
        queryset.update(
            seo=getattr(settings, 'DEFAULT_CATEGORY_META_TEXT_TEMPLATE', ''))
    apply_default_meta_text.short_description = _(u'Apply default SEO text')

    def update_filters(self, request, queryset):
        from tasks.api import TaskQueueManager
        task_manager = TaskQueueManager()
        for q in queryset:
            task_manager.schedule(
                'lfs.filters.jobs.update_filters_job',
                args=[q.id],
                priority=getattr(settings, 'JOB_UPDATE_FILTERS_PRIORITY', 1))
    update_filters.short_description = _(u'Add task for update filters')

    def set_category_template(self, request, queryset):
        if 'apply' in request.POST:
            categories = Category.objects.filter(
                uid__in=request.POST['categories'].split(';'))
            form = CategoryTemplateForm(request.POST)
            if form.is_valid():
                template = form.cleaned_data['template']
                for c in categories:
                    if c.children.count() != 0:
                        c.template = template
                        c.save()
        else:
            form = CategoryTemplateForm()
            return render(
                request,
                'admin/set_category_template.html',
                {
                    'form': form,
                    'selected_action': request.POST['_selected_action'],
                    'categories': ";".join([q.uid for q in queryset])
                }
            )
    set_category_template.short_description = _(u'Set category template')


admin.site.register(Category, CategoryAdmin)


class CategoryFilter(SimpleListFilter):
    title = _(u'Categories')
    parameter_name = 'categories'

    def lookups(self, request, model_admin):
        mptt_opts = Category._mptt_meta
        queryset = Category.objects.all().order_by(
            mptt_opts.tree_id_attr, mptt_opts.left_attr)
        return [(
            c.pk,
            '%s %s (%d)' % (
                '---' * c.level,
                c.name,
                c.product_count)) for c in queryset]

    def queryset(self, request, queryset):
        if self.value():
            try:
                return queryset.filter(categories__pk=self.value())
            except:
                return queryset.filter(category__pk=self.value())


class ManufacturerFilter(SimpleListFilter):
    title = _(u'Manufacturers')
    parameter_name = 'manufacturer'

    def lookups(self, request, model_admin):
        queryset = Manufacturer.objects.all().order_by('name')
        return [(
            m.pk, '%s (%d)' % (m.name, m.products.count())) for m in queryset]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(manufacturer__pk=self.value())


class ImageInline(SortableTabularInline, GenericTabularInline):
    model = Image
    form = ImageInlineForm
    suit_classes = 'suit-tab suit-tab-view'
    ct_field = 'content_type'
    ct_fk_field = 'content_id'
    sortable = 'position'
    extra = 0


class ProductListInline(admin.TabularInline):
    model = ProductListItem
    suit_classes = 'suit-tab suit-tab-linked'
    extra = 0


class AccessoryInline(SortableTabularInline):
    model = ProductAccessories
    form = ProductAccessoryForm
    suit_classes = 'suit-tab suit-tab-linked'
    fk_name = 'product'
    sortable = 'position'
    raw_id_fields = ('accessory',)
    extra = 0


class PropertyValueInline(SortableTabularInline):
    model = ProductPropertyValue
    form = ProductPropertyValueForm
    suit_classes = 'suit-tab suit-tab-properties'
    sortable = 'position'
    readonly_fields = ('value_as_float',)
    extra = 0


class ProductAttachmentInline(SortableTabularInline):
    model = ProductAttachment
    suit_classes = 'suit-tab suit-tab-view'
    sortable = 'position'
    extra = 0


class ProductVariantsInline(SortableTabularInline, LinkedInline):
    model = Product
    fk_name = 'parent'
    prepopulated_fields = {"slug": ("name", )}
    suit_classes = 'suit-tab suit-tab-view'
    sortable = 'variant_position'
    fields = ('name', 'slug', 'effective_price')
    readonly_fields = ('effective_price',)
    extra = 0


class ProductAdmin(GenericAdminModelAdmin):
    prepopulated_fields = {"slug": ("name", )}
    list_display = (
        'id', 'get_name', 'manufacturer', 'id_1c', 'get_categories',
        'active', 'get_status', 'for_sale', 'get_price',
        'copy_products_button') \
        if getattr(settings, 'SSHOP_USE_1C', False) else \
        (
            'id', 'name', 'manufacturer', 'get_categories', 'active',
            'get_status', 'for_sale', 'get_price')
    list_display_links = ('id',)
    search_fields = ['name', 'slug', 'uid'] + ['id_1c'] \
        if getattr(settings, 'SSHOP_USE_1C', False) else ['id']
    list_filter = (
        CategoryFilter,
        'manufacturer',
        'active',
        'status',
        'sub_type',
    )
    # list_editable = ('active',)
    ordering = ['status__order', 'name']
    change_form_template = 'admin/product_change_form.html'

    base_inlines = [
        ImageInline,
        ProductAttachmentInline,
        AccessoryInline,
        PropertyValueInline,
        ProductListInline,
    ]
    with_variants_inlines = [ProductVariantsInline] + base_inlines

    base_readonly_fields = ('uid',) + ('id_1c',) \
        if getattr(settings, 'SSHOP_USE_1C', False) else tuple()
    edit_standard_readonly = (
        'effective_price',
        'min_variant_price', 'max_variant_price',)
    edit_withvariants_readonly = (
        'effective_price',
        'min_variant_price', 'max_variant_price',)
    edit_variant_readonly = (
        'effective_price',
        'min_variant_price', 'max_variant_price',)

    add_form = AddProductForm
    edit_standard_form = EditStandardProductForm
    edit_withvariants_form = EditProductWithVariantsForm
    edit_variant_form = EditVariantForm

    standard_fieldsets = [
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['name', 'short_name', 'slug', 'sub_type', ] + (
                ['id_1c'] if getattr(
                    settings, 'SSHOP_USE_1C', False) else []) +
                ['active', 'status', 'sku', 'uid', 'categories']
        }),
        (_(u'Manufacturer'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['manufacturer', 'sku_manufacturer']
        }),
        (_(u'Description'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['short_description', 'description']
        }),
        (_(u'Price'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': [
                'effective_price',
                'min_variant_price', 'max_variant_price',
                'price', 'price_unit',
                'price_calculator',
                ('for_sale', 'for_sale_price'),
                'guarantee',
            ]
        }),
        (_(u'Amount'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['default_value', 'unit', 'type_of_quantity_field']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-view',),
            'fields': ['static_block', 'template', 'variants_display_type']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-seo',),
            'fields': [
                'meta_h1',
                'meta_title',
                'meta_keywords', 'meta_description', 'meta_seo_text']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-linked',),
            'fields': ['related_products']
        }),
        (_(u'Extra data'), {
            'classes': ('suit-tab suit-tab-general collapse',),
            'description':
            _(u'Extra data. Reserved for non standard purposes.'),
            'fields': [
                'recommended_position',
                'extra_text_1',
                'extra_text_2',
                'extra_int_1',
                'extra_int_2',
                'extra_float_1',
                'extra_float_2',

                'rank_1',
                'rank_2',
                'rank_3',
            ]
        }),
    ]
    withvariants_fieldsets = [
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': [
                'name', 'short_name', 'slug', 'sub_type'] + (
                    ['id_1c'] if getattr(
                        settings, 'SSHOP_USE_1C', False) else []) +
                    ['active', 'status', 'sku', 'uid', 'categories']
        }),
        (_(u'Manufacturer'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['manufacturer', 'sku_manufacturer']
        }),
        (_(u'Description'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['short_description', 'description']
        }),
        (_(u'Price'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': [
                'effective_price',
                'min_variant_price', 'max_variant_price',
                'price', 'price_unit',
                'price_calculator',
                ('for_sale', 'for_sale_price'),
                'guarantee',
            ]
        }),
        (_(u'Amount'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['default_value', 'unit', 'type_of_quantity_field']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-view',),
            'fields': [
                'static_block', 'template', 'variants_display_type']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-seo',),
            'fields': [
                'meta_h1',
                'meta_title',
                'meta_keywords', 'meta_description', 'meta_seo_text']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-linked',),
            'fields': ['related_products']
        }),
        (_(u'Extra data'), {
            'classes': ('suit-tab suit-tab-general collapse',),
            'description':
            _(u'Extra data. Reserved for non standard purposes.'),
            'fields': [
                'recommended_position',
                'extra_text_1',
                'extra_text_2',
                'extra_int_1',
                'extra_int_2',
                'extra_float_1',
                'extra_float_2',

                'rank_1',
                'rank_2',
                'rank_3',
            ]
        }),
    ]
    variant_fieldsets = [
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': [
                'name',
                'short_name',
                'slug',
                'parent',
                'sub_type'
            ] + (['id_1c'] if getattr(
                settings, 'SSHOP_USE_1C', False) else []) +
            ['active', 'status', 'sku', 'uid', 'categories']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': [
                'variant_position',
            ],
        }),
        (_(u'Manufacturer'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['manufacturer', 'sku_manufacturer']
        }),
        (_(u'Description'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': [
                'active_short_description',
                'short_description',
                'active_description',
                'description']
        }),
        (_(u'Price'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': [
                'effective_price',
                'min_variant_price', 'max_variant_price',
                'price', 'price_unit',
                'price_calculator',
                'for_sale',
                'for_sale_price',
                'guarantee',
            ]
        }),
        (_(u'Amount'), {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['default_value', 'unit', 'type_of_quantity_field']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-view',),
            'fields': [
                'active_static_block',
                'static_block',
                'variants_display_type',
                'template',
            ]
        }),
        (None, {
            'classes': ('suit-tab suit-tab-seo',),
            'fields': [
                'meta_h1',
                'meta_title',
                'meta_keywords', 'meta_description', 'meta_seo_text']
        }),
        (None, {
            'classes': ('suit-tab suit-tab-linked',),
            'fields': ['related_products']
        }),
        (_(u'Extra data'), {
            'classes': ('suit-tab suit-tab-general collapse',),
            'description':
            _(u'Extra data. Reserved for non standard purposes.'),
            'fields': [
                'recommended_position',
                'extra_text_1',
                'extra_text_2',
                'extra_int_1',
                'extra_int_2',
                'extra_float_1',
                'extra_float_2',

                'rank_1',
                'rank_2',
                'rank_3',
            ]
        }),
    ]

    standard_suit_form_tabs = (
        ('general', _(u'General')),
        ('view', _(u'View')),
        ('seo', _(u'SEO')),
        ('properties', _(u'Properties')),
        ('linked', _(u'Linked products')),
        ('portlets', _(u'Portlets')),
    )

    add_suit_form_includes = None
    edit_suit_form_includes = (
        ('admin/portlets_includes/portlets.html', 'top', 'portlets'),
    )

    withvariants_suit_form_tabs = standard_suit_form_tabs
    variant_suit_form_tabs = standard_suit_form_tabs
    actions = [
        'change_subtype_action',
        'change_status_action',
        'apply_propertyset_action',
        'create_propertyset_action',
        'apply_default_meta_h1',
        'apply_default_meta_title',
        'apply_default_meta_keywords',
        'apply_default_meta_description',
        'apply_default_meta_text',
        'make_active',
        'make_inactive',
        'make_inactive_cascading',
        'set_for_sale',
        'unset_for_sale',
        'change_price_action',
        'change_price_for_sale_action',
        'set_price_for_sale_action',
        'change_manufacturer_action',
        'change_categories_action',
        'add_categories_action',
        'resave_product_images_action',
    ]

    class Media:
        css = {
            "all": ("/static/css/loadover.css",)
        }
        js = (
            '/static/jquery/jquery.cookie.js',
            '/static/js/sco.collapse.js',
            '/static/admin/js/ajax_links.js',
            '/static/js/loadover.js',
        )

    def get_urls(self):
        urls = super(ProductAdmin, self).get_urls()
        my_urls = patterns(
            '',
            url(
                r'^copy-product/(?P<product_id>[-\w]*)/$',
                self.admin_site.admin_view(self.copy_product)
            ),
        )
        return my_urls + urls

    def apply_default_meta_h1(self, request, queryset):
        queryset.update(
            meta_h1=getattr(
                settings, 'DEFAULT_PRODUCT_META_H1_TEMPLATE', ''))
    apply_default_meta_h1.short_description = _(u'Apply default H1 header')

    def apply_default_meta_title(self, request, queryset):
        queryset.update(
            meta_title=getattr(
                settings, 'DEFAULT_PRODUCT_META_TITLE_TEMPLATE', ''))
    apply_default_meta_title.short_description = _(u'Apply default META title')

    def apply_default_meta_keywords(self, request, queryset):
        queryset.update(
            meta_keywords=getattr(
                settings, 'DEFAULT_PRODUCT_META_KEYWORDS_TEMPLATE', ''))
    apply_default_meta_keywords.short_description = \
        _(u'Apply default META keywords')

    def apply_default_meta_description(self, request, queryset):
        queryset.update(
            meta_description=getattr(
                settings, 'DEFAULT_PRODUCT_META_DESCRIPTION_TEMPLATE', ''))
    apply_default_meta_description.short_description = \
        _(u'Apply default META description')

    def apply_default_meta_text(self, request, queryset):
        queryset.update(
            meta_seo_text=getattr(
                settings, 'DEFAULT_PRODUCT_META_TEXT_TEMPLATE', ''))
    apply_default_meta_text.short_description = _(u'Apply default SEO text')

    def set_for_sale(self, request, queryset):
        for q in queryset:
            q.for_sale = True
            q.save()
    set_for_sale.short_description = _(u'Set for sale')

    def unset_for_sale(self, request, queryset):
        for q in queryset:
            q.for_sale = False
            q.save()
    unset_for_sale.short_description = _(u'Unset for sale')

    def make_inactive_cascading(self, request, queryset):
        for q in queryset:
            variants = q.get_variants()
            for v in variants:
                v.active = False
                v.save()
            q.active = False
            q.save()
    make_inactive_cascading.short_description = _(u'Make inactive cascading')

    def make_inactive(self, request, queryset):
        for q in queryset:
            q.active = False
            q.save()
    make_inactive.short_description = _(u'Make inactive')

    def make_active(self, request, queryset):
        for q in queryset:
            q.active = True
            q.save()
    make_active.short_description = _(u'Make active')

    def add_categories_action(self, request, queryset):
        if 'do_action' in request.POST:
            form = ChangeCategoriesForm(request.POST)
            if form.is_valid():
                categories = form.cleaned_data['categories']
                for q in queryset:
                    for c in categories.all():
                        q.categories.add(c)
                    q.save()
                return
        else:
            form = ChangeCategoriesForm()
        return render_to_response(
            'admin/catalog_includes/add-categories-action.html', {
                'title': _(u'Add categories'),
                'objects': queryset,
                'form': form},
            context_instance=RequestContext(request))
    add_categories_action.short_description = _(u'Add categories')

    def change_categories_action(self, request, queryset):
        if 'do_action' in request.POST:
            form = ChangeCategoriesForm(request.POST)
            if form.is_valid():
                categories = form.cleaned_data['categories']
                for q in queryset:
                    q.categories = categories
                    q.save()
                return
        else:
            form = ChangeCategoriesForm()
        return render_to_response(
            'admin/catalog_includes/change-categories-action.html', {
                'title': _(u'Change categories'),
                'objects': queryset,
                'form': form},
            context_instance=RequestContext(request))
    change_categories_action.short_description = _(u'Change categories')

    def change_price_action(self, request, queryset):
        if 'do_action' in request.POST:
            form = ChangePriceForm(request.POST)
            if form.is_valid():
                change_action = form.cleaned_data['change_action']
                price = form.cleaned_data['price']
                for q in queryset:
                    if change_action == '0':  # Absolute value
                        q.price = price
                    elif change_action == '5':  # Absolute delta
                        q.price += price
                    elif change_action == '10':  # Percentage delta
                        q.price = q.price + q.price * price / 100.
                    q.save()
                return
        else:
            form = ChangePriceForm()
        return render_to_response(
            'admin/catalog_includes/change-price-action.html', {
                'title': _(u'Change price'),
                'objects': queryset,
                'form': form},
            context_instance=RequestContext(request))
    change_price_action.short_description = _(u'Change price')

    def change_price_for_sale_action(self, request, queryset):
        if 'do_action' in request.POST:
            form = ChangePriceForm(request.POST)
            if form.is_valid():
                change_action = form.cleaned_data['change_action']
                price = form.cleaned_data['price']
                for q in queryset:
                    if change_action == '0':  # Absolute value
                        q.for_sale_price = price
                    elif change_action == '5':  # Absolute delta
                        q.for_sale_price += price
                    elif change_action == '10':  # Percentage delta
                        q.for_sale_price = q.for_sale_price + \
                            q.for_sale_price * price / 100.
                    q.save()
                return
        else:
            form = ChangePriceForm()
        return render_to_response(
            'admin/catalog_includes/change-price-for-sale-action.html', {
                'title': _(u'Change price for sale'),
                'objects': queryset,
                'form': form},
            context_instance=RequestContext(request))
    change_price_for_sale_action.short_description = \
        _(u'Change price for sale')

    def set_price_for_sale_action(self, request, queryset):
        if 'do_action' in request.POST:
            form = ChangePriceForm(request.POST)
            if form.is_valid():
                change_action = form.cleaned_data['change_action']
                price = form.cleaned_data['price']
                for q in queryset:
                    if change_action == '0':  # Absolute value
                        q.for_sale = True
                        q.for_sale_price = price
                    elif change_action == '5':  # Absolute delta
                        q.for_sale = True
                        q.for_sale_price = q.price + price
                    elif change_action == '10':  # Percentage delta
                        q.for_sale = True
                        q.for_sale_price = q.price + \
                            q.price * price / 100.
                    q.save()
                return
        else:
            form = ChangePriceForm()
        return render_to_response(
            'admin/catalog_includes/set-price-for-sale-action.html', {
                'title': _(u'Set price for sale'),
                'objects': queryset,
                'form': form},
            context_instance=RequestContext(request))
    set_price_for_sale_action.short_description = \
        _(u'Set price for sale')

    def change_manufacturer_action(self, request, queryset):
        if 'do_action' in request.POST:
            form = ChangeManufacturerForm(request.POST)
            if form.is_valid():
                manufacturer = form.cleaned_data['manufacturer']
                for q in queryset:
                    q.manufacturer = manufacturer
                    q.save()
                return
        else:
            form = ChangeManufacturerForm()
        return render_to_response(
            'admin/catalog_includes/change-manufacturer-action.html', {
                'title': _(u'Change product manufacturer'),
                'objects': queryset,
                'form': form},
            context_instance=RequestContext(request))
    change_manufacturer_action.short_description = \
        _(u'Change product manufacturer')

    def change_status_action(self, request, queryset):
        if 'do_action' in request.POST:
            form = ChangeStatusForm(request.POST)
            if form.is_valid():
                status = form.cleaned_data['status']
                for q in queryset:
                    q.status = status
                    q.save()
                return
        else:
            form = ChangeStatusForm()
        return render_to_response(
            'admin/catalog_includes/change-status-action.html', {
                'title': _(u'Change product status'),
                'objects': queryset,
                'form': form},
            context_instance=RequestContext(request))
    change_status_action.short_description = _(u'Change product status')

    def change_subtype_action(self, request, queryset):
        if 'do_action' in request.POST:
            form = ChangeSubtypeForm(request.POST)
            if form.is_valid():
                sub_type = form.cleaned_data['sub_type']
                queryset.update(sub_type=sub_type)
                return
        else:
            form = ChangeSubtypeForm()
        return render_to_response(
            'admin/catalog_includes/change-subtype-action.html', {
                'title': _(u'Change product sub-type'),
                'objects': queryset,
                'form': form},
            context_instance=RequestContext(request))
    change_subtype_action.short_description = _(u'Change product sub-type')

    def apply_propertyset_action(self, request, queryset):
        if 'do_action' in request.POST:
            form = ApplyPropertySetForm(request.POST)
            if form.is_valid():
                property_set = form.cleaned_data['property_set']
                for obj in queryset:
                    obj.apply_property_set(property_set)
                return
        else:
            form = ApplyPropertySetForm()
        return render_to_response(
            'admin/catalog_includes/apply-propertyset-action.html', {
                'title': _(u'Apply property set'),
                'objects': queryset,
                'form': form},
            context_instance=RequestContext(request))
    apply_propertyset_action.short_description = _(u'Apply property set')

    def create_propertyset_action(self, request, queryset):
        if 'do_action' in request.POST:
            form = CreatePropertySetFromProductForm(request.POST)
            if form.is_valid():
                name = form.cleaned_data['name']
                if queryset:
                    obj = queryset[0]
                    obj.create_property_set_from_product(name)
                return
        else:
            form = CreatePropertySetFromProductForm()
        return render_to_response(
            'admin/catalog_includes/create-propertyset-action.html', {
                'title': _(u'Create property set'),
                'objects': queryset,
                'form': form},
            context_instance=RequestContext(request))
    create_propertyset_action.short_description = _(u'Create property set')

    def resave_product_images_action(self, request, queryset):
        if queryset.count() > 20:
            from tasks.api import TaskQueueManager
            task_manager = TaskQueueManager()
            task_manager.schedule(
                'lfs.core.jobs.resave_product_images_job',
                args=list(zip(*queryset.values_list('id'))[0]),
                priority=getattr(
                    settings, 'JOB_RESAVE_IMAGES_PRIORITY', 1))
        else:
            for q in queryset:
                for image in q.images.all():
                    image.image.resave()
    resave_product_images_action.short_description =\
        _(u'Resave images')

    def save_model(self, request, obj, form, change):
        obj.save()
        if 'categories' in form.cleaned_data:
            obj.categories = form.cleaned_data['categories']
        if 'propertyset' in form.cleaned_data \
                and form.cleaned_data['propertyset']:
            obj.apply_property_set(form.cleaned_data['propertyset'])

    def get_status(self, obj):
        return '<span class="label %s">%s</span>' % (
            obj.status.css_class, obj.status.name)

    get_status.short_description = _(u'Status')
    get_status.allow_tags = True
    get_status.admin_order_field = 'status__order'

    def get_name(self, obj):
        if obj.sub_type == STANDARD_PRODUCT:  # Standard product
            return '<a href="%s">%s</a>' % (
                urlresolvers.reverse(
                    'admin:%s_%s_change' % (
                        'catalog', 'product'), args=[obj.pk]),
                obj.get_name(),
            )
        elif obj.sub_type == VARIANT:
            return '<img title="%s" src="/static/icons/document-small.png"'\
                '/>&nbsp;<a href="%s">%s</a>' % (
                    _(u'Variant'),
                    urlresolvers.reverse(
                        'admin:%s_%s_change' % (
                            'catalog', 'product'), args=[obj.pk]),
                    obj.get_name(),
                )
        elif obj.sub_type == PRODUCT_WITH_VARIANTS:
            # variants_str = ''
            # for v in obj.variants.all():
            #     variants_str += u'<li><img title="%s" '\
            #         u'src="/static/icons/category-item.png"/>&nbsp;'\
            #         u'<a href="%s">%s</a></li>' % (
            #         _(u'Variant'),
            #         urlresolvers.reverse(
            #             'admin:%s_%s_change' % (
            #                 'catalog', 'product'), args=[v.pk]),
            #         v.get_name(),
            #         )

            return u'''<img src="/static/icons/box-document.png" title="%s"/>
            <a href="%s">%s</a>
            <span class="pull-right">[<a href="%s">%s</a>]</span>
            ''' % (
                _(u'Product with variants'),
                urlresolvers.reverse(
                    'admin:%s_%s_change' % (
                        'catalog', 'product'), args=[obj.pk]),
                obj.name,
                '%s%s' % (
                    urlresolvers.reverse(
                        'admin:%s_%s_changelist' % (
                            'catalog', 'product')),
                    '?parent__exact=%s' % obj.pk),
                _(u'All variants'),
            )
            # return u'''<a href="%s"><strong>%s</strong></a>&nbsp;
            # <img src="/static/icons/control-270-small.png" title="%s"/>
            # <ul class="unstyled">%s</ul>
            # ''' % (
            #     urlresolvers.reverse(
            #         'admin:%s_%s_change' % (
            #             'catalog', 'product'), args=[obj.pk]),
            #     obj.name,
            #     _(u'Product with variants'),
            #     variants_str,
            # )

    get_name.short_description = _(u'Name')
    get_name.allow_tags = True
    get_name.admin_order_field = 'name'

    def get_categories(self, obj):
        categories = obj.categories.all()
        return ', '.join(['<a href="%s">%s</a>' % (
            urlresolvers.reverse(
                'admin:%s_%s_change' % ('catalog', 'category'), args=[c.pk]),
            c.name,
        ) for c in categories])
    get_categories.short_description = _(u'Categories')
    get_categories.allow_tags = True
    get_categories.admin_order_field = 'categories__name'

    def suit_row_attributes(self, obj):
        if obj.categories.all().count() == 0:
            return {'class': 'error'}

    def get_price(self, obj):
        from ..core.templatetags.sshop_tags import format_currency
        return format_currency(None, obj.effective_price)
    get_price.short_description = _(u'Price')
    get_price.admin_order_field = 'effective_price'

    def copy_products_button(self, obj):
        return '<a href="copy-product/%(product_id)s/">%(button_caption)s</a>' % {
            'product_id': obj.id,
            'button_caption': _(u'Copy')
        }
    copy_products_button.allow_tags = True
    copy_products_button.short_description = ''

    def copy_product(self, request, product_id):
        p = Product.objects.get(id=product_id)
        old_id = p.id
        categories = p.categories.all()
        new_product = p
        new_product.id = None
        new_product.uid = get_unique_id_str()
        new_product.effective_price = 0
        new_product.id_1c = ''
        new_product.price = 0
        new_product.status = ProductStatus.objects.all().order_by(
            'show_buy_button',
            'show_ask_button',
            'is_visible',
            'is_searchable'
        )[0]
        stop = False
        new_slug = new_product.slug + '_'
        counter = 1
        while not stop:
            try:
                Product.objects.get(slug=new_slug + str(counter))
                counter += 1
            except:
                stop = True
        new_product.slug = new_slug + str(counter)
        new_product.save()
        for category in categories:
            category.products.add(new_product)
            category.save()
        images = Image.objects.filter(
            content_type=ContentType.objects.get_for_model(p),
            content_id=old_id)
        if not images and p.parent:
            images = Image.objects.filter(
                content_type=ContentType.objects.get_for_model(p.parent),
                content_id=p.parent.id)
        for im in images:
            im.id = None
            im.content_id = new_product.id
            im.save()
        properties = ProductPropertyValue.objects.filter(
            product__id=old_id
        )
        new_product.property_values.all().delete()
        for prop in properties:
            prop.id = None
            prop.product = new_product
            prop.save()
        from django.contrib.sites.models import Site
        site = "http://%s" % Site.objects.get(id=settings.SITE_ID)
        from django.contrib.admin.models import LogEntry, ADDITION
        from django.utils.encoding import force_unicode
        if request:
            user = request.user
            LogEntry.objects.log_action(
                user_id=user.pk,
                content_type_id=ContentType.objects.get_for_model(
                    p).pk,
                object_id=p.pk,
                object_repr=force_unicode(p),
                action_flag=ADDITION,
            )
        return HttpResponseRedirect(
            '%s/superadmin/%s/%s/%s/?new_product=true' % (
                site,
                new_product._meta.app_label,
                new_product._meta.module_name,
                new_product.id
            ))

    def get_form(self, request, obj=None, **kwargs):
        """
        Use special form during category creation
        """
        defaults = {}
        if obj is None:
            defaults.update({
                'form': self.add_form,
            })
            self.suit_form_tabs = None
            self.readonly_fields = tuple()
            self.suit_form_includes = self.add_suit_form_includes
        else:
            self.suit_form_includes = self.edit_suit_form_includes
            if obj.sub_type == STANDARD_PRODUCT:  # Standard product
                defaults.update({
                    'form': self.edit_standard_form,
                })
                self.inlines = self.base_inlines
                self.suit_form_tabs = self.standard_suit_form_tabs
                self.readonly_fields = self.base_readonly_fields + \
                    self.edit_standard_readonly
            elif obj.sub_type == PRODUCT_WITH_VARIANTS:
                defaults.update({
                    'form': self.edit_withvariants_form,
                })
                self.inlines = self.with_variants_inlines
                self.suit_form_tabs = self.withvariants_suit_form_tabs
                self.readonly_fields = self.base_readonly_fields + \
                    self.edit_withvariants_readonly
            elif obj.sub_type == VARIANT:
                defaults.update({
                    'form': self.edit_variant_form,
                })
                self.inlines = self.base_inlines
                self.suit_form_tabs = self.variant_suit_form_tabs
                self.readonly_fields = self.base_readonly_fields + \
                    self.edit_variant_readonly

        defaults.update(kwargs)
        return super(ProductAdmin, self).get_form(request, obj, **defaults)

    def get_fieldsets(self, request, obj=None, **kwargs):
        if obj is None:
            return super(ProductAdmin, self).get_fieldsets(
                request, obj, **kwargs)
        else:
            if obj.sub_type == STANDARD_PRODUCT:
                return self.standard_fieldsets
            elif obj.sub_type == PRODUCT_WITH_VARIANTS:
                return self.withvariants_fieldsets
            elif obj.sub_type == VARIANT:
                return self.variant_fieldsets

    def get_changelist(self, request, **kwargs):
        return CustomProductSearchChangeList

    def queryset(self, request):
        qs = super(ProductAdmin, self).queryset(request)
        reg = re.compile(r'/superadmin/catalog/product/(.+?)/')
        v = reg.search(request.META.get('HTTP_REFERER', ''))
        reg2 = re.compile(r'/superadmin/catalog/product/$')
        all_products_page = reg2.search(request.path)
        if v and all_products_page:
            result, page_num = request.session.get(
                'admin_search_filters', ({}, 0))
            result = dict(result)
            if 'q' in result:
                del result['q']
            if 'o' in result:
                del result['o']
            if 'all' in result:
                del result['all']
            qs = Product.objects.filter(**result)
        return qs


class CustomProductSearchChangeList(ChangeList):
    # def get_query_string(self, new_params=None, remove=None):
    #     from django.utils.http import urlencode
    #     if new_params is None: new_params = {}
    #     if remove is None: remove = []
    #     p = self.params.copy()
    #     for r in remove:
    #         for k in p.keys():
    #             if k.startswith(r):
    #                 del p[k]
    #     for k, v in new_params.items():
    #         if v is None:
    #             if k in p:
    #                 del p[k]
    #         else:
    #             p[k] = v
    #     return '?'

    def get_results(self, request):
        paginator = self.model_admin.get_paginator(
            request, self.query_set, self.list_per_page)
        # Get the number of objects, with admin filters applied.
        result_count = paginator.count

        # Get the total number of objects, with no admin filters applied.
        # Perform a slight optimization: Check to see whether any filters were
        # given. If not, use paginator.hits to calculate the number of objects,
        # because we've already done paginator.hits and the value is cached.
        reg = re.compile(r'/superadmin/catalog/product/(.+?)/')
        v = reg.search(request.META.get('HTTP_REFERER', ''))
        if v:
            full_result_count = self.model.objects.count()
        elif not self.query_set.query.where:
            full_result_count = result_count
        else:
            full_result_count = self.root_query_set.count()

        can_show_all = result_count <= self.list_max_show_all
        multi_page = result_count > self.list_per_page

        # Get the list of objects to display on this page.
        if (self.show_all and can_show_all) or not multi_page:
            result_list = self.query_set._clone()
        else:
            try:
                result_list = paginator.page(self.page_num + 1).object_list
            except InvalidPage:
                raise IncorrectLookupParameters

        self.result_count = result_count
        self.full_result_count = full_result_count
        self.result_list = result_list
        self.can_show_all = can_show_all
        self.multi_page = multi_page
        self.paginator = paginator

    def get_query_set(self, request):
        reg = re.compile(r'/superadmin/catalog/product/(.+?)/')
        v = reg.search(request.META.get('HTTP_REFERER', ''))
        if v:
            result, page_num = request.session.get('admin_search_filters')
            self.params = result
            self.page_num = page_num

            for key, value in result.items():
                if key == 'q':
                    continue
                request.GET[key] = value
            if result:
                self.query = result['q'] if 'q' in result else self.query
        else:
            result = (self.params, self.page_num)
            request.session['admin_search_filters'] = result
        return super(
            CustomProductSearchChangeList, self
        ).get_query_set(request)

admin.site.register(Product, ProductAdmin)


class StaticBlockAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ['name']
    form = StaticBlockForm
admin.site.register(StaticBlock, StaticBlockAdmin)


class DeliveryTimeAdmin(admin.ModelAdmin):
    list_display = ('get_name', 'min', 'max', 'unit', 'description')

    def get_name(self, obj):
        return obj.__unicode__()
    get_name.short_description = _(u'Name')

admin.site.register(DeliveryTime, DeliveryTimeAdmin)


class ProductStatusAdmin(SortableModelAdmin):
    list_display = (
        'name', 'css_class', 'delivery_time', 'show_buy_button',
        'show_ask_button', 'is_visible', 'is_searchable',)
    search_fields = ['name']
    sortable = 'order'
    list_filter = ['delivery_time']


admin.site.register(ProductStatus, ProductStatusAdmin)


class PropertySetRelationInline(SortableTabularInline):
    model = PropertySetRelation
    form = PropertySetRelationForm
    sortable = 'position'
    extra = 0


class PropertySetAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ['name']
    ordering = ['name']
    inlines = [PropertySetRelationInline]

admin.site.register(PropertySet, PropertySetAdmin)


class PropertyAdmin(admin.ModelAdmin):
    list_display = ('name', 'identificator', 'is_group', 'uid',)
    search_fields = ['name', 'identificator', 'uid']
    list_filter = ('is_group',)
    ordering = ['-is_group', 'name']
    readonly_fields = ('uid',)
    prepopulated_fields = {'identificator': ('name',)}
    fieldsets = [
        (None, {
            'fields': ['name', 'is_group', 'identificator', 'uid']
        }),
    ]

admin.site.register(Property, PropertyAdmin)


class SortTypeAdmin(SortableModelAdmin):
    list_display = ('name', 'sortable_fields')
    search_fields = ['name']
    sortable = 'order'

admin.site.register(SortType, SortTypeAdmin)


class PropertyValueIconAdmin(SortableModelAdmin):
    form = PropertyValueIconForm
    list_display = [
        'title', 'get_icon', 'category',
        'get_properties', 'get_product_count']
    search_fields = ['title']
    sortable = 'position'
    exclude = ['products']
    list_filter = (CategoryFilter,)
    actions = [
        'parse_action',
    ]

    def parse_action(self, request, queryset):
        for q in queryset:
            q.parse()
    parse_action.short_description = _(u'Start parsing')

    def get_properties(self, obj):
        properties = obj.properties.all()
        return ', '.join(['<a href="%s">%s</a>' % (
            urlresolvers.reverse(
                'admin:%s_%s_change' % ('catalog', 'property'), args=[c.pk]),
            c.name,
        ) for c in properties])
    get_properties.short_description = _(u'Properties')
    get_properties.allow_tags = True

    def get_icon(self, obj):
        return '<img src="/media/%s" alt="%s" />' % (
            obj.icon,
            obj.title.encode('utf8'),
        )
    get_icon.short_description = _(u'Icon')
    get_icon.allow_tags = True

    def get_product_count(self, obj):
        return obj.products.count()
    get_product_count.short_description = _(u'Product count')


admin.site.register(PropertyValueIcon, PropertyValueIconAdmin)


class PropertyUnitAdmin(TreeAdmin):
    list_display = ['name', 'identifier', 'coefficient']
    mptt_level_indent = 40
    search_fields = ['name', 'identifier']
    prepopulated_fields = {'identifier': ('name',)}

admin.site.register(PropertyUnit, PropertyUnitAdmin)


class PropertyOptionAdmin(admin.ModelAdmin):
    fields = ('name', 'identifier')
    search_fields = ['name', 'identifier']
    list_display = ('name', 'identifier', 'property_types', 'categories')
    ordering = ['name']
    prepopulated_fields = {'identifier': ('name',)}

    def property_types(self, obj):
        names = obj.propertyoptionrelation_set.all()\
            .values('property_type__property__name')
        return ', '.join([n['property_type__property__name'] for n in names])
    property_types.short_description = _(u'Property type')

    def categories(self, obj):
        categories = []
        for por in obj.propertyoptionrelation_set.all():
            for category in por.property_type.categories.all():
                categories.append(category)
        categories = list(set(categories))
        return ', '.join([c.name for c in categories])
    categories.short_description = _(u'Categories')


class PropertyOptionRelationInline(SortableTabularInline):
    model = PropertyOptionRelation
    form = PropertyOptionRelationForm
    sortable = 'position'
    extra = 0
    #suit_classes = 'suit-tab suit-tab-realtionpropertyoprion'


class PropertyOptionRelationAdmin(admin.ModelAdmin):
    model = PropertyOptionRelation
    form = PropertyOptionRelationForm

    def response_add(self, request, obj, post_url_continue=None):
        if "_popup" in request.POST:
            return HttpResponse(
                '<!DOCTYPE html><html><head><title></title></head><body>'
                '<script type="text/javascript">'
                'opener.on_property_options_popup_close(window);'
                '</script></body></html>')

    def response_change(self, request, obj):
        if "_popup" in request.POST:
            return HttpResponse(
                '<!DOCTYPE html><html><head><title></title></head><body>'
                '<script type="text/javascript">'
                'opener.on_property_options_popup_close(window);'
                '</script></body></html>')
    '''
    def get_form(self, request, obj=None, **kwargs):
        defaults = {}

        class PropertyOptionRelationFormProxy(PropertyOptionRelationForm):
            def __new__(cls, *args, **kwargs):
                kwargs['request'] = request
                return PropertyOptionRelationForm(*args, **kwargs)
        defaults.update({
            'form': PropertyOptionRelationFormProxy,
        })
        defaults.update(kwargs)
        return super(PropertyOptionRelationAdmin, self)\
            .get_form(request, obj, **defaults)
    '''


class PropertyTypeAdmin(admin.ModelAdmin):
    form = PropertyTypeForm
    list_display = (
        'property', 'get_categories', 'type', 'is_hidden', 'is_searchable',
        'is_active', 'visible_in_list', 'do_sync')
    #inlines = [PropertyOptionRelationInline]
    search_fields = ['property__name']
    list_filter = (CategoryFilter, 'type', 'is_hidden', 'is_active')
    add_fieldsets = [
        (None, {
            'fields': ['categories', 'property', 'type']
        }),
        (_(u'Units'), {
            'fields': ['unit', 'available_units']
        }),
        (_(u'Advanced'), {
            'fields': [
                'is_hidden', 'is_searchable', 'visible_in_list', 'do_sync']
        }),
        (_(u'Active property'), {
            'fields': ['is_active', 'actions']
        }),
        (_(u'Validation'), {
            'fields': ['regex', 'validator']
        }),
        (_(u'Numeric'), {
            'fields': ['min_value', 'max_value']
        }),
        (_(u'Boolean'), {
            'fields': ['true_value', 'false_value']
        }),
    ]

    edit_fieldsets = [
        (None, {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ['categories', 'property', 'type']
        }),
        (_(u'Units'), {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ['unit', 'available_units']
        }),
        (_(u'Advanced'), {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': [
                'is_hidden', 'is_searchable', 'visible_in_list', 'do_sync']
        }),
        (_(u'Active property'), {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ['is_active', 'actions']
        }),
        (_(u'Validation'), {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ['regex', 'validator']
        }),
        (_(u'Numeric'), {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ['min_value', 'max_value']
        }),
        (_(u'Boolean'), {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ['true_value', 'false_value']
        }),
    ]

    edit_suit_tabs = (('general',  _(u'General')),
                      ('realtionpropertyoprion',  _(u'Relation property option')))

    edit_suit_includes = (
        ('admin/catalog_includes/properties-options-block.html',
         '', 'realtionpropertyoprion'),
    )

    def get_form(self, request, obj=None, **kwargs):
        if obj is None:
            self.suit_form_tabs = None
            self.suit_form_includes = None
            self.fieldsets = self.add_fieldsets
        else:
            self.suit_form_tabs = self.edit_suit_tabs
            self.suit_form_includes = self.edit_suit_includes
            self.fieldsets = self.edit_fieldsets
        return super(PropertyTypeAdmin, self).get_form(request, obj, **kwargs)

    def get_categories(self, obj):
        categories = obj.categories.all()
        return ', '.join(['<a href="%s">%s</a>' % (
            urlresolvers.reverse(
                'admin:%s_%s_change' % ('catalog', 'category'), args=[c.pk]),
            c.name,
        ) for c in categories])
    get_categories.short_description = _(u'Categories')
    get_categories.allow_tags = True

    class Media:
        css = {
            "all": ("/static/css/loadover.css",)
        }
        js = (
            '/static/jquery/jquery.cookie.js',
            '/static/js/sco.collapse.js',
            '/static/admin/js/ajax_links.js',
            '/static/js/loadover.js',
        )


admin.site.register(PropertyType, PropertyTypeAdmin)
admin.site.register(PropertyOption, PropertyOptionAdmin)
admin.site.register(PropertyOptionRelation, PropertyOptionRelationAdmin)
