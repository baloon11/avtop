# coding: utf-8
from django.conf import settings
from django.test import TestCase
from django.test.client import RequestFactory
from django.contrib.sessions.backends.file import SessionStore

from ..core.signals import product_changed, category_changed
from ..core.utils import get_default_shop
from .settings import (
    DELIVERY_TIME_UNIT_SINGULAR,
    DELIVERY_TIME_UNIT_CHOICES,
    DELIVERY_TIME_UNIT_HOURS,
    DELIVERY_TIME_UNIT_WEEKS,
    DELIVERY_TIME_UNIT_DAYS,
    DELIVERY_TIME_UNIT_MONTHS,

    STANDARD_PRODUCT,
    PRODUCT_WITH_VARIANTS,
    VARIANT,

    QUANTITY_FIELD_INTEGER,
    QUANTITY_FIELD_DECIMAL_1,
    QUANTITY_FIELD_DECIMAL_2,

    CONTENT_PRODUCTS,
    CONTENT_CATEGORIES,
    CAT_PRODUCT_PATH,
    CAT_CATEGORY_PATH,

    PRODUCT_PATH,

    PROPERTY_TYPE_STRING,
    PROPERTY_TYPE_INTEGER,
    PROPERTY_TYPE_FLOAT,
    PROPERTY_TYPE_CHOICE,
    PROPERTY_TYPE_BOOLEAN,
)

from .models import (
    Image,
    Product,
    Category,
    Property,
    ProductPropertyValue,
    ProductStatus,
    DeliveryTime,
    StaticBlock,
    ProductAccessories,
    ProductAttachment,
    PropertySet,
    PropertyOption,
    PropertyType,
    PropertyUnit,
    PropertyValueIcon,
)
from .exceptions import InvalidFilters


class CategoryTestCase(TestCase):
    """Tests the Category of the lfs.catalog.
    """

    fixtures = ['lfs_shop.xml']

    def setUp(self):
        """
        """
        self.s1 = ProductStatus.objects.get(pk=1)
        self.s2 = ProductStatus.objects.get(pk=2)
        self.s3 = ProductStatus.objects.get(pk=3)

        # Create some products
        self.p1 = Product.objects.create(
            name="Product 1",
            slug="product-1",
            price=5,
            active=True,
            status=self.s1)
        self.p2 = Product.objects.create(
            name="Product 2",
            slug="product-2",
            price=3,
            active=True,
            status=self.s1)
        self.p3 = Product.objects.create(
            name="Product 3",
            slug="product-3",
            price=1,
            active=True,
            status=self.s1)

        # Create a category hierachy
        self.c1 = Category.objects.create(
            name="Category 1", slug="category-1",
            short_description="Short description category 1")
        self.c11 = Category.objects.create(
            name="Category 11",
            slug="category-11", parent=self.c1)
        self.c111 = Category.objects.create(
            name="Category 111",
            slug="category-111", parent=self.c11)
        self.c12 = Category.objects.create(
            name="Category 12",
            slug="category-12", parent=self.c1)
        Category.objects.rebuild()

        # Add products to categories
        self.c111.products = [self.p1, self.p2]
        self.c111.save()

        self.c12.products = [self.p2, self.p3]
        self.c12.save()

    def test_get_data_for_meta_info(self):
        info = self.c1._get_data_for_meta_info()
        self.assertEqual(info.keys(), [
            'category', 'shop',
            'name', 'shop_name',
        ])

    def test_meta_title(self):
        """
        """
        self.assertEqual(self.c1.meta_title, "{{ name }} - {{ shop_name }}")

        self.c1.meta_title = "T1 T2 T3 - {{ shop_name }}"
        self.assertEqual(self.c1.get_meta_title(), "T1 T2 T3 - P-Cart")

        self.c1.meta_title = "{{ name }} T1 T2 T3 - {{ shop_name }}"
        self.assertEqual(
            self.c1.get_meta_title(), "Category 1 T1 T2 T3 - P-Cart")

        self.c1.meta_title = "T1 {{ name }} T2 T3 - {{ shop_name }}"
        self.assertEqual(
            self.c1.get_meta_title(), "T1 Category 1 T2 T3 - P-Cart")

        self.c1.meta_title = "T1 T2 {{ name }} T3 - {{ shop_name }}"
        self.assertEqual(
            self.c1.get_meta_title(), "T1 T2 Category 1 T3 - P-Cart")

        self.c1.meta_title = "T1 T2 T3 {{ name }} - {{ shop_name }}"
        self.assertEqual(
            self.c1.get_meta_title(), "T1 T2 T3 Category 1 - P-Cart")

        self.c1.meta_title = "{{ name }}"
        self.assertEqual(self.c1.get_meta_title(), "Category 1")

        # Test the default meta templates
        self.c1.meta_title = ''
        self.c1.save()
        self.assertEqual(self.c1.meta_title, "{{ name }} - {{ shop_name }}")
        self.assertEqual(self.c1.get_meta_title(), "Category 1 - P-Cart")

    def test_meta_keywords(self):
        """
        """
        self.c1.meta_keywords = "KW1 KW2 KW3"
        self.c1.save()
        self.assertEqual(self.c1.get_meta_keywords(), "KW1 KW2 KW3")

        self.c1.meta_keywords = "{{ name }} KW1 KW2 KW3"
        self.c1.save()
        self.assertEqual(self.c1.get_meta_keywords(), "Category 1 KW1 KW2 KW3")

        self.c1.meta_keywords = "KW1 {{ name }} KW2 KW3"
        self.c1.save()
        self.assertEqual(self.c1.get_meta_keywords(), "KW1 Category 1 KW2 KW3")

        self.c1.meta_keywords = "KW1 KW2 KW3 {{ name }}"
        self.c1.save()
        self.assertEqual(self.c1.get_meta_keywords(), "KW1 KW2 KW3 Category 1")

        self.c1.meta_keywords = "{{ name }}"
        self.c1.save()
        self.assertEqual(self.c1.get_meta_keywords(), "Category 1")

        self.c1.meta_keywords = "{{ category.short_description }} KW1 KW2 KW3"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_keywords(),
            "Short description category 1 KW1 KW2 KW3")

        self.c1.meta_keywords = "KW1 {{ category.short_description }} KW2 KW3"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_keywords(),
            "KW1 Short description category 1 KW2 KW3")

        self.c1.meta_keywords = "KW1 KW2 KW3 Short description category 1"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_keywords(),
            "KW1 KW2 KW3 Short description category 1")

        self.c1.meta_keywords = "{{ category.short_description }}"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_keywords(),
            "Short description category 1")

    def test_meta_description(self):
        """
        """
        self.c1.meta_description = "Meta description category 1"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_description(), "Meta description category 1")

        self.c1.meta_description = "{{ name }} Meta description category 1"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_description(),
            "Category 1 Meta description category 1")

        self.c1.meta_description = "Meta {{ name }} description"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_description(),
            "Meta Category 1 description")

        self.c1.meta_description = "Meta description category 1 {{ name }}"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_description(),
            "Meta description category 1 Category 1")

        self.c1.meta_description = "{{ name }}"
        self.c1.save()
        self.assertEqual(self.c1.get_meta_description(), "Category 1")

        self.c1.meta_description = \
            "{{ category.short_description }} Meta description category 1"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_description(),
            "Short description category 1 Meta description category 1")

        self.c1.meta_description = "{{ category.short_description }}"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_description(), "Short description category 1")

    def test_meta_seo_text(self):
        """
        """
        self.c1.seo = "Meta seo text category 1"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_seo_text(), "Meta seo text category 1")

        self.c1.seo = "{{ name }} Meta seo text category 1"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_seo_text(),
            "Category 1 Meta seo text category 1")

        self.c1.seo = "Meta {{ name }} seo text"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_seo_text(),
            "Meta Category 1 seo text")

        self.c1.seo = "Meta seo text category 1 {{ name }}"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_seo_text(),
            "Meta seo text category 1 Category 1")

        self.c1.seo = "{{ name }}"
        self.c1.save()
        self.assertEqual(self.c1.get_meta_seo_text(), "Category 1")

        self.c1.seo = \
            "{{ category.short_description }} Meta seo text category 1"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_seo_text(),
            "Short description category 1 Meta seo text category 1")

        self.c1.seo = "{{ category.short_description }}"
        self.c1.save()
        self.assertEqual(
            self.c1.get_meta_seo_text(), "Short description category 1")

    def test_get_products(self):
        """
        """
        product_ids = [p.id for p in self.c1.get_products()]
        self.assertEqual(len(product_ids), 0)

        product_ids = [p.id for p in self.c11.get_products()]
        self.assertEqual(len(product_ids), 0)

        product_ids = [p.id for p in self.c111.get_products()]
        self.assertEqual(len(product_ids), 2)
        self.assertEqual(product_ids, [self.p1.id, self.p2.id])

        product_ids = [p.id for p in self.c12.get_products()]
        self.assertEqual(len(product_ids), 2)
        self.assertEqual(product_ids, [self.p2.id, self.p3.id])

        # Test the caching
        product_ids_1 = [p.id for p in self.c12.get_products()]
        self.assertEqual(product_ids, product_ids_1)

    def test_get_all_products(self):
        """
        """
        product_ids = [p.id for p in self.c1.get_all_products()]
        self.assertEqual(product_ids, [self.p1.id, self.p2.id, self.p3.id])

        product_ids = [p.id for p in self.c11.get_all_products()]
        self.assertEqual(len(product_ids), 2)
        self.assertEqual(product_ids, [self.p1.id, self.p2.id])

        product_ids = [p.id for p in self.c111.get_all_products()]
        self.assertEqual(len(product_ids), 2)
        self.assertEqual(product_ids, [self.p1.id, self.p2.id])

        product_ids = [p.id for p in self.c12.get_all_products()]
        self.assertEqual(len(product_ids), 2)
        self.assertEqual(product_ids, [self.p2.id, self.p3.id])

        # Test the caching
        product_ids_1 = [p.id for p in self.c12.get_all_products()]
        self.assertEqual(product_ids, product_ids_1)

    def test_get_parent_for_portlets(self):
        """
        """
        shop = get_default_shop()

        self.assertEqual(
            self.c1.get_parent_for_portlets(), shop)
        self.assertEqual(
            self.c11.get_parent_for_portlets(), self.c1)
        self.assertEqual(
            self.c111.get_parent_for_portlets(), self.c11)
        self.assertEqual(
            self.c12.get_parent_for_portlets(), self.c1)

    def test_get_template_name(self):
        """
        """
        self.assertEqual(
            self.c1.get_template_name(),
            '%s/%s' % (CAT_PRODUCT_PATH, 'default.html'))

        self.assertEqual(
            self.c11.get_template_name(),
            '%s/%s' % (CAT_PRODUCT_PATH, 'default.html'))
        self.assertEqual(
            self.c111.get_template_name(),
            '%s/%s' % (CAT_PRODUCT_PATH, 'default.html'))
        self.assertEqual(
            self.c12.get_template_name(),
            '%s/%s' % (CAT_PRODUCT_PATH, 'default.html'))

        self.c1.template = CONTENT_CATEGORIES
        self.assertEqual(
            self.c1.get_template_name(),
            '%s/%s' % (CAT_CATEGORY_PATH, 'default.html'))

        self.c1.template = None
        self.assertEqual(
            self.c1.get_template_name(),
            '%s/%s' % (CAT_PRODUCT_PATH, 'default.html'))

    def test_get_content(self):
        """
        """
        self.assertEqual(
            self.c1.get_content(),
            CONTENT_PRODUCTS)

        self.c1.template = CONTENT_CATEGORIES
        self.assertEqual(
            self.c1.get_content(),
            CONTENT_CATEGORIES)

        self.c1.template = None
        self.assertEqual(
            self.c1.get_content(),
            CONTENT_PRODUCTS)

    def test_get_all_children(self):
        """
        """
        children_names = [c.name for c in self.c1.get_all_children()]
        self.assertEqual(
            children_names, [u"Category 11", u"Category 111", u"Category 12"])

        children_names = [c.name for c in self.c11.get_all_children()]
        self.assertEqual(children_names, [u"Category 111"])

        children_names = [c.name for c in self.c111.get_children()]
        self.assertEqual(children_names, [])

        children_names = [c.name for c in self.c12.get_children()]
        self.assertEqual(children_names, [])

    def test_get_children(self):
        """
        """
        children_names = [c.name for c in self.c1.get_children()]
        self.assertEqual(children_names, [u"Category 11", u"Category 12"])

        children_names = [c.name for c in self.c11.get_children()]
        self.assertEqual(children_names, [u"Category 111"])

        children_names = [c.name for c in self.c111.get_children()]
        self.assertEqual(children_names, [])

        children_names = [c.name for c in self.c12.get_children()]
        self.assertEqual(children_names, [])

    def test_get_format_info(self):
        """
        """
        # format informations are inherited from shop default values
        self.assertEqual(self.c1.active_formats, False)
        self.assertEqual({
            'category_cols': 1,
            'product_cols': 1,
            'product_rows': 10}, self.c1.get_format_info())

        # Add new format informations to c1
        self.c1.category_cols = 11
        self.c1.product_cols = 22
        self.c1.product_rows = 33
        self.c1.save()

        # But c1 still inherits from shop as active_formats is False by default
        self.assertEqual({
            'category_cols': 1,
            'product_cols': 1,
            'product_rows': 10}, self.c1.get_format_info())

        # Set active_formats to True
        self.c1.active_formats = True
        self.c1.save()

        # c1 has it own formats now
        self.assertEqual(self.c1.active_formats, True)
        self.assertEqual({
            'category_cols': 11,
            'product_cols': 22,
            'product_rows': 33}, self.c1.get_format_info())

        # c11 inherits from c1
        self.assertEqual(self.c11.active_formats, False)
        self.assertEqual({
            'category_cols': 11,
            'product_cols': 22,
            'product_rows': 33}, self.c11.get_format_info())

        # Add new format informations
        self.c11.category_cols = 111
        self.c11.product_cols = 222
        self.c11.product_rows = 333
        self.c11.save()

        # But c11 still inherits from c1 as active_formats is False by default
        self.assertEqual(self.c11.active_formats, False)
        self.assertEqual({
            'category_cols': 11,
            'product_cols': 22,
            'product_rows': 33}, self.c11.get_format_info())

        # Set active_formats to True
        self.c11.active_formats = True
        self.c11.save()

        # c11 has it own formats now
        self.assertEqual(self.c11.active_formats, True)
        self.assertEqual({
            'category_cols': 111,
            'product_cols': 222,
            'product_rows': 333}, self.c11.get_format_info())

    def test_get_absolute_url(self):
        """
        """
        self.assertEqual(
            self.c1.get_absolute_url(), "/%s" % self.c1.slug)

    def test_unicode(self):
        """
        """
        self.assertEqual(
            self.c1.__unicode__(), "%s" % self.c1.name)

    def test_get_content_type(self):
        """
        """
        self.assertEqual(self.c1.content_type, u"category")

    def test_get_icon(self):
        """
        """
        self.assertEqual(self.c1.get_icon(), None)
        self.assertEqual(self.c11.get_icon(), None)
        self.assertEqual(self.c12.get_icon(), None)

        # Add an icon to c1
        self.c1.icon = Image.objects.create(title="Icon 1", position=1)
        self.assertEqual(self.c1.get_icon().title, "Icon 1")

        # c11 should not inherit the image from c1
        self.assertEqual(self.c11.get_icon(), None)

        # c12 should not inherit the image from c1
        self.assertEqual(self.c12.get_icon(), None)

    def test_get_image(self):
        """
        """
        self.assertEqual(self.c1.get_image(), None)
        self.assertEqual(self.c11.get_image(), None)
        self.assertEqual(self.c12.get_image(), None)
        self.assertEqual(self.c111.get_image(), None)

        # Add an image to c1
        self.c1.image = Image.objects.create(title="Image 1", position=1)
        self.assertEqual(self.c1.get_image().title, "Image 1")

        # c11 should inherit the image from c1
        self.assertEqual(self.c11.get_image().title, "Image 1")

        # c12 should inherit the image from c1
        self.assertEqual(self.c12.get_image().title, "Image 1")

        # c111 should inherit the image from c1
        self.assertEqual(self.c111.get_image().title, "Image 1")

        # Add an image to c11
        self.c11.image = Image.objects.create(title="Image 2", position=1)

        # c1 should still have it's own one
        self.assertEqual(self.c1.get_image().title, "Image 1")

        # c11 should have it's own one now
        self.assertEqual(self.c11.get_image().title, "Image 2")

        # c12 should still inherit the image from c1
        self.assertEqual(self.c12.get_image().title, "Image 1")

        # c111 should inherit the image from c11 now
        self.assertEqual(self.c111.get_image().title, "Image 2")

        # Add an image to c111
        self.c111.image = Image.objects.create(title="Image 3", position=1)

        # c1 should still have it's own one
        self.assertEqual(self.c1.get_image().title, "Image 1")

        # c12 should still inherit the image from c1
        self.assertEqual(self.c12.get_image().title, "Image 1")

        # c11 should have it's own one now
        self.assertEqual(self.c11.get_image().title, "Image 2")

        # c111 should have it's own one now
        self.assertEqual(self.c111.get_image().title, "Image 3")

    def test_get_ancestors(self):
        """
        """
        self.assertEqual(
            list(self.c1.get_ancestors(ascending=True)), [])

        parent_names = [c.name for c in self.c11.get_ancestors(ascending=True)]
        self.assertEqual(parent_names, ["Category 1"])

        parent_names = [c.name for c in self.c12.get_ancestors(ascending=True)]
        self.assertEqual(parent_names, ["Category 1"])

        parent_names = [
            c.name for c in self.c111.get_ancestors(ascending=True)]
        self.assertEqual(parent_names, ["Category 11", "Category 1"])

    def test_get_static_block(self):
        """
        """
        result = self.c1.get_static_block()
        self.assertEqual(result, None)

        result = self.c11.get_static_block()
        self.assertEqual(result, None)

        # Add static_block to c1
        sb1 = StaticBlock.objects.create(name="SB1")
        self.c1.static_block = sb1

        result = self.c1.get_static_block()
        self.assertEqual(result, sb1)

        result = self.c11.get_static_block()
        self.assertEqual(result, None)

        # Add another static_block to c11
        sb2 = StaticBlock.objects.create(name="SB2")
        self.c11.static_block = sb2

        result = self.c1.get_static_block()
        self.assertEqual(result, sb1)

        result = self.c11.get_static_block()
        self.assertEqual(result, sb2)

    def test_get_filtered_products(self):
        """
        """
        with self.assertRaises(InvalidFilters):
            self.c111.get_filtered_products(
                {'filter1': 'option1'},
                'id')


class DeliveryTimeTestCase(TestCase):
    """Tests attributes and methods of DeliveryTime objects.
    """
    def setUp(self):
        """
        """
        self.dm1 = DeliveryTime.objects.create(
            min=1.0, max=3.0, unit=DELIVERY_TIME_UNIT_HOURS)
        self.dm2 = DeliveryTime.objects.create(
            min=1.0, max=3.0, unit=DELIVERY_TIME_UNIT_DAYS)
        self.dm3 = DeliveryTime.objects.create(
            min=1.0, max=3.0, unit=DELIVERY_TIME_UNIT_WEEKS)
        self.dm4 = DeliveryTime.objects.create(
            min=1.0, max=3.0, unit=DELIVERY_TIME_UNIT_MONTHS)

    def test_as_hours(self):
        """
        """
        self.assertEqual(self.dm1.as_hours().min, 1)
        self.assertEqual(self.dm1.as_hours().max, 3)
        self.assertEqual(self.dm2.as_hours().min, 24)
        self.assertEqual(self.dm2.as_hours().max, 72)
        self.assertEqual(self.dm3.as_hours().min, 168)
        self.assertEqual(self.dm3.as_hours().max, 504)
        self.assertEqual(self.dm4.as_hours().min, 720)
        self.assertEqual(self.dm4.as_hours().max, 2160)

    def test_as_days(self):
        """
        """
        self.assertEqual(self.dm1.as_days().min, 1.0 / 24)
        self.assertEqual(self.dm1.as_days().max, 3.0 / 24)
        self.assertEqual(self.dm2.as_days().min, 1)
        self.assertEqual(self.dm2.as_days().max, 3)
        self.assertEqual(self.dm3.as_days().min, 7)
        self.assertEqual(self.dm3.as_days().max, 21)
        self.assertEqual(self.dm4.as_days().min, 30)
        self.assertEqual(self.dm4.as_days().max, 90)

    def test_as_weeks(self):
        """
        """
        self.assertEqual(self.dm1.as_weeks().min, 1.0 / (24 * 7))
        self.assertEqual(self.dm1.as_weeks().max, 3.0 / (24 * 7))
        self.assertEqual(self.dm2.as_weeks().min, 1.0 / 7)
        self.assertEqual(self.dm2.as_weeks().max, 3.0 / 7)
        self.assertEqual(self.dm3.as_weeks().min, 1)
        self.assertEqual(self.dm3.as_weeks().max, 3)
        self.assertEqual(self.dm4.as_weeks().min, 4)
        self.assertEqual(self.dm4.as_weeks().max, 12)

    def test_as_month(self):
        """
        """
        self.assertEqual(self.dm1.as_months().min, 1.0 / (24 * 30))
        self.assertEqual(self.dm1.as_months().max, 3.0 / (24 * 30))
        self.assertEqual(self.dm2.as_months().min, 1.0 / 30)
        self.assertEqual(self.dm2.as_months().max, 3.0 / 30)
        self.assertEqual(self.dm3.as_months().min, 1.0 / 4)
        self.assertEqual(self.dm3.as_months().max, 3.0 / 4)
        self.assertEqual(self.dm4.as_months().min, 1)
        self.assertEqual(self.dm4.as_months().max, 3)

    def test_as_reasonable_unit(self):
        """
        """
        d = DeliveryTime(min=24, max=48, unit=DELIVERY_TIME_UNIT_HOURS)
        d = d.as_reasonable_unit()
        self.assertEqual(d.min, 24)
        self.assertEqual(d.max, 48)
        self.assertEqual(d.unit, DELIVERY_TIME_UNIT_HOURS)

        d = DeliveryTime(min=96, max=120, unit=DELIVERY_TIME_UNIT_HOURS)
        d = d.as_reasonable_unit()
        self.assertEqual(d.min, 4)
        self.assertEqual(d.max, 5)
        self.assertEqual(d.unit, DELIVERY_TIME_UNIT_DAYS)

        d = DeliveryTime(min=7, max=14, unit=DELIVERY_TIME_UNIT_DAYS)
        d = d.as_reasonable_unit()
        self.assertEqual(d.min, 1)
        self.assertEqual(d.max, 2)
        self.assertEqual(d.unit, DELIVERY_TIME_UNIT_WEEKS)

        d = DeliveryTime(min=6, max=10, unit=DELIVERY_TIME_UNIT_WEEKS)
        d = d.as_reasonable_unit()
        self.assertEqual(d.min, 1)
        self.assertEqual(d.max, 2)
        self.assertEqual(d.unit, DELIVERY_TIME_UNIT_MONTHS)

    def test_as_string(self):
        """
        """
        # Hour
        d = DeliveryTime(min=0, max=1, unit=DELIVERY_TIME_UNIT_HOURS)
        self.assertEqual(d.as_string(), u"1 час")

        d = DeliveryTime(min=1, max=1, unit=DELIVERY_TIME_UNIT_HOURS)
        self.assertEqual(d.as_string(), u"1 час")

        d = DeliveryTime(min=2, max=2, unit=DELIVERY_TIME_UNIT_HOURS)
        self.assertEqual(d.as_string(), u"2 часов")

        d = DeliveryTime(min=2, max=3, unit=DELIVERY_TIME_UNIT_HOURS)
        self.assertEqual(d.as_string(), u"2-3 часов")
        # Days
        d = DeliveryTime(min=0, max=1, unit=DELIVERY_TIME_UNIT_DAYS)
        self.assertEqual(d.as_string(), u"1 день")

        d = DeliveryTime(min=1, max=1, unit=DELIVERY_TIME_UNIT_DAYS)
        self.assertEqual(d.as_string(), u"1 день")

        d = DeliveryTime(min=2, max=2, unit=DELIVERY_TIME_UNIT_DAYS)
        self.assertEqual(d.as_string(), u"2 дней")

        d = DeliveryTime(min=2, max=3, unit=DELIVERY_TIME_UNIT_DAYS)
        self.assertEqual(d.as_string(), u"2-3 дней")

        # Weeks
        d = DeliveryTime(min=0, max=1, unit=DELIVERY_TIME_UNIT_WEEKS)
        self.assertEqual(d.as_string(), u"1 неделя")

        d = DeliveryTime(min=1, max=1, unit=DELIVERY_TIME_UNIT_WEEKS)
        self.assertEqual(d.as_string(), u"1 неделя")

        d = DeliveryTime(min=2, max=2, unit=DELIVERY_TIME_UNIT_WEEKS)
        self.assertEqual(d.as_string(), u"2 недель")

        d = DeliveryTime(min=2, max=3, unit=DELIVERY_TIME_UNIT_WEEKS)
        self.assertEqual(d.as_string(), u"2-3 недель")

        # Months
        d = DeliveryTime(min=0, max=1, unit=DELIVERY_TIME_UNIT_MONTHS)
        self.assertEqual(d.as_string(), u"1 месяц")

        d = DeliveryTime(min=1, max=1, unit=DELIVERY_TIME_UNIT_MONTHS)
        self.assertEqual(d.as_string(), u"1 месяц")

        d = DeliveryTime(min=2, max=2, unit=DELIVERY_TIME_UNIT_MONTHS)
        self.assertEqual(d.as_string(), u"2 месяцев")

        d = DeliveryTime(min=2, max=3, unit=DELIVERY_TIME_UNIT_MONTHS)
        self.assertEqual(d.as_string(), u"2-3 месяцев")

    def test_add(self):
        """Tests the adding of delivery times.
        """
        # ### hours

        # hours + hours
        result = self.dm1 + self.dm1
        self.assertEqual(result.min, 2)
        self.assertEqual(result.max, 6)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_HOURS)

        # hours + days
        result = self.dm1 + self.dm2
        self.assertEqual(result.min, 25)
        self.assertEqual(result.max, 75)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_HOURS)

        # hours + weeks
        result = self.dm1 + self.dm3
        self.assertEqual(result.min, 169)
        self.assertEqual(result.max, 507)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_HOURS)

        # hours + month
        result = self.dm1 + self.dm4
        self.assertEqual(result.min, 721)
        self.assertEqual(result.max, 2163)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_HOURS)

        # ### Days

        # days + hours
        result = self.dm2 + self.dm1
        self.assertEqual(result.min, 25)
        self.assertEqual(result.max, 75)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_HOURS)

        # days + days
        result = self.dm2 + self.dm2
        self.assertEqual(result.min, 2)
        self.assertEqual(result.max, 6)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_DAYS)

        # days + weeks
        result = self.dm2 + self.dm3
        self.assertEqual(result.min, 192)
        self.assertEqual(result.max, 576)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_HOURS)

        # days + months
        result = self.dm2 + self.dm4
        self.assertEqual(result.min, 744)
        self.assertEqual(result.max, 2232)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_HOURS)

        # ### Weeks

        # weeks + hours
        result = self.dm3 + self.dm1
        self.assertEqual(result.min, 169)
        self.assertEqual(result.max, 507)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_HOURS)

        # weeks + days
        result = self.dm3 + self.dm2
        self.assertEqual(result.min, 192)
        self.assertEqual(result.max, 576)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_HOURS)

        # weeks + weeks
        result = self.dm3 + self.dm3
        self.assertEqual(result.min, 2)
        self.assertEqual(result.max, 6)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_WEEKS)

        # weeks + months
        result = self.dm3 + self.dm4
        self.assertEqual(result.min, 888)
        self.assertEqual(result.max, 2664)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_HOURS)

        # ### Months

        # months + hours
        result = self.dm4 + self.dm1
        self.assertEqual(result.min, 721)
        self.assertEqual(result.max, 2163)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_HOURS)

        # months + days
        result = self.dm4 + self.dm2
        self.assertEqual(result.min, 744)
        self.assertEqual(result.max, 2232)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_HOURS)

        # months + weeks
        result = self.dm4 + self.dm3
        self.assertEqual(result.min, 888)
        self.assertEqual(result.max, 2664)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_HOURS)

        # months + months
        result = self.dm4 + self.dm4
        self.assertEqual(result.min, 2)
        self.assertEqual(result.max, 6)
        self.assertEqual(result.unit, DELIVERY_TIME_UNIT_MONTHS)

    def test_round(self):
        """
        """
        # round down
        d = DeliveryTime(min=1.1, max=2.1, unit=DELIVERY_TIME_UNIT_HOURS)
        d = d.round()
        self.assertEqual(d.min, 1)
        self.assertEqual(d.max, 2)
        self.assertEqual(d.unit, DELIVERY_TIME_UNIT_HOURS)

        # .5 should be rounded up
        d = DeliveryTime(min=1.5, max=2.5, unit=DELIVERY_TIME_UNIT_HOURS)
        d = d.round()
        self.assertEqual(d.min, 2)
        self.assertEqual(d.max, 3)
        self.assertEqual(d.unit, DELIVERY_TIME_UNIT_HOURS)

        # round up
        d = DeliveryTime(min=1.6, max=2.6, unit=DELIVERY_TIME_UNIT_HOURS)
        d = d.round()
        self.assertEqual(d.min, 2)
        self.assertEqual(d.max, 3)
        self.assertEqual(d.unit, DELIVERY_TIME_UNIT_HOURS)

    def test_subtract_days(self):
        """
        """
        d = DeliveryTime(min=48, max=72, unit=DELIVERY_TIME_UNIT_HOURS)
        d = d.subtract_days(1)
        self.assertEqual(d.min, 24)
        self.assertEqual(d.max, 48)
        self.assertEqual(d.unit, DELIVERY_TIME_UNIT_HOURS)

        d = DeliveryTime(min=5, max=6, unit=DELIVERY_TIME_UNIT_DAYS)
        d = d.subtract_days(1)
        self.assertEqual(d.min, 4)
        self.assertEqual(d.max, 5)
        self.assertEqual(d.unit, DELIVERY_TIME_UNIT_DAYS)

        d = DeliveryTime(min=5, max=6, unit=DELIVERY_TIME_UNIT_WEEKS)
        d = d.subtract_days(7)
        self.assertEqual(d.min, 4)
        self.assertEqual(d.max, 5)
        self.assertEqual(d.unit, DELIVERY_TIME_UNIT_WEEKS)

        d = DeliveryTime(min=5, max=6, unit=DELIVERY_TIME_UNIT_MONTHS)
        d = d.subtract_days(30)
        self.assertEqual(d.min, 4)
        self.assertEqual(d.max, 5)
        self.assertEqual(d.unit, DELIVERY_TIME_UNIT_MONTHS)


class ProductTestCase(TestCase):
    """Tests attributes and methods of Products
    """
    fixtures = ['lfs_shop.xml']

    def setUp(self):
        """
        """
        self.request = RequestFactory().get("/")
        self.request.session = SessionStore()

        # Get status "In stock"
        self.s1 = ProductStatus.objects.get(pk=1)
        # Get status "For order"
        self.s2 = ProductStatus.objects.get(pk=2)
        # Get status "Archive"
        self.s3 = ProductStatus.objects.get(pk=3)

        # A product with properties
        self.p1 = Product.objects.create(
            name=u"Product 1",
            slug=u"product-1",
            sku=u"SKU P1",
            description=u"Description",
            short_description=u"Short description product 1",
            meta_description=u"Meta description product 1",
            meta_keywords=u"Meta keywords product 1",
            sub_type=PRODUCT_WITH_VARIANTS,
            price=1.0,
            for_sale_price=0.5,
            active=True,
            status=self.s1)

        # Products without properties
        self.p2 = Product.objects.create(
            name=u"Product 2", slug=u"product-2", active=True)
        self.p3 = Product.objects.create(
            name=u"Product 3", slug=u"product-3", active=True)

        # Create property groups
        self.ppg1 = Property.objects.create(
            name='Property group 1',
            identificator='property_group_1',
            is_group=True)
        self.ppg2 = Property.objects.create(
            name='Property group 2',
            identificator='property_group_2',
            is_group=True)

        # Create properties
        self.pp11 = Property.objects.create(
            name='Property 11', identificator='property_11')
        self.pp12 = Property.objects.create(
            name='Property 12', identificator='property_12')
        self.pp13 = Property.objects.create(
            name='Property 13', identificator='property_13')

        self.pp21 = Property.objects.create(
            name='Property 21', identificator='property_21')
        self.pp22 = Property.objects.create(
            name='Property 22', identificator='property_22')
        self.pp23 = Property.objects.create(
            name='Property 23', identificator='property_23')

        self.ppgv1 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.ppg1,
            position=1)
        self.ppv11 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp11,
            value='Value 11',
            position=2)
        self.ppv12 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp12,
            value='Value 12',
            position=3)
        self.ppv13 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp13,
            value='Value 13',
            position=4)
        self.ppgv2 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.ppg2,
            position=5)
        self.ppv21 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp21,
            value='Value 21',
            position=6)
        self.ppv22 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp22,
            value='Value 22',
            position=7)
        self.ppv23 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp23,
            value='Value 23',
            position=8)

        self.v1 = Product.objects.create(
            name=u"Variant 1",
            slug=u"variant-1",
            sku=u"SKU V1",
            description=u"This is the description of variant 1",
            meta_description=u"Meta description of variant 1",
            meta_keywords=u"Meta keywords variant 1",
            sub_type=VARIANT,
            price=2.0,
            for_sale_price=1.5,
            parent=self.p1,
            active=True,
            status=self.s1)

        self.v2 = Product.objects.create(
            name=u"Variant 2",
            slug=u"variant-2",
            sku=u"SKU V2",
            description=u"This is the description of variant 2",
            meta_description=u"Meta description of variant 2",
            meta_keywords=u"Meta keywords variant 2",
            sub_type=VARIANT,
            price=3.0,
            for_sale_price=1.0,
            parent=self.p1,
            active=True,
            status=self.s2)

        # Add related products to p1
        self.p1.related_products.add(self.p2, self.p3)

        # self.p1.min_variant_price = 2.0
        # self.p1.max_variant_price = 3.0
        self.p1.save()

        # Assign accessories to products
        self.pa_1_2 = ProductAccessories.objects.create(
            product=self.p1, accessory=self.p2, position=1)
        self.pa_1_3 = ProductAccessories.objects.create(
            product=self.p1, accessory=self.p3, position=2)

        self.pa_2_1 = ProductAccessories.objects.create(
            product=self.p2, accessory=self.p1, position=1)
        self.pa_2_3 = ProductAccessories.objects.create(
            product=self.p2, accessory=self.p3, position=2)

        self.pa_3_1 = ProductAccessories.objects.create(
            product=self.p3, accessory=self.p1, position=1)
        self.pa_3_2 = ProductAccessories.objects.create(
            product=self.p3, accessory=self.p2, position=2)

        # Create some categories
        self.c1 = Category.objects.create(
            name="Category 1", slug="category-1")
        self.c11 = Category.objects.create(
            name="Category 11", slug="category-11", parent=self.c1)
        self.c111 = Category.objects.create(
            name="Category 111", slug="category-111", parent=self.c11)
        self.c2 = Category.objects.create(
            name="Category 2", slug="category-2")
        Category.objects.rebuild()

        # Assign products to categories. Note: p2 has two top level categories
        self.c111.products = (self.p1, self.p2)
        self.c2.products = (self.p2, )

        # Create some dummy images
        self.i1 = Image.objects.create(title="Image 1", position=1)
        self.i2 = Image.objects.create(title="Image 2", position=2)
        self.i3 = Image.objects.create(title="Image 3", position=3)
        self.i4 = Image.objects.create(title="Image 4", position=1)
        self.i5 = Image.objects.create(title="Image 5", position=2)

        # Assign images to product
        self.p1.images.add(self.i1, self.i2, self.i3)

        # Assign images to variant
        self.v1.images.add(self.i4, self.i5)

        # setup attachments test stuff
        self.setupAttachments()

    def test_defaults(self):
        """Tests the default value after a product has been created
        """
        p = Product.objects.create(
            name="Product", slug="product", sku="4711", price=42.0)

        self.assertEqual(p.name, "Product")
        self.assertEqual(p.slug, "product")
        self.assertEqual(p.sku, "4711")
        self.assertEqual(p.price, 42.0)
        self.assertEqual(p.effective_price, 42.0)
        self.assertEqual(p.short_description, "")
        self.assertEqual(p.description, "")
        self.assertEqual(len(p.images.all()), 0)

        self.assertEqual(p.meta_title, "{{ name }} - {{ shop_name }}")
        self.assertEqual(p.meta_description, "")
        self.assertEqual(p.meta_keywords, "")

        self.assertEqual(len(p.related_products.all()), 0)
        self.assertEqual(len(p.accessories.all()), 0)

        self.assertEqual(p.for_sale, False)
        self.assertEqual(p.for_sale_price, 0.0)
        self.assertEqual(p.active, False)

        self.assertEqual(p.sub_type, STANDARD_PRODUCT)
        self.assertEqual(p.variants_display_type, 0)
        self.assertEqual(p.parent, None)

    def test_unicode(self):
        self.assertEqual(unicode(self.p1), 'Product 1')

    def test_get_absolute_url(self):
        self.assertEqual(
            self.p1.get_absolute_url(),
            '/product-1')

    def test_content_type(self):
        self.assertEqual(self.p1.content_type, u"product")

    def test_has_accessories(self):
        self.assertEqual(self.p1.has_accessories(), True)
        self.assertEqual(self.p2.has_accessories(), True)
        self.assertEqual(self.p3.has_accessories(), True)

        self.p1.accessories.clear()

        self.assertEqual(self.p1.has_accessories(), False)
        self.assertEqual(self.p2.has_accessories(), True)
        self.assertEqual(self.p3.has_accessories(), True)

        self.p2.accessories.clear()

        self.assertEqual(self.p1.has_accessories(), False)
        self.assertEqual(self.p2.has_accessories(), False)
        self.assertEqual(self.p3.has_accessories(), True)

        self.p3.accessories.clear()

        self.assertEqual(self.p1.has_accessories(), False)
        self.assertEqual(self.p2.has_accessories(), False)
        self.assertEqual(self.p3.has_accessories(), False)

    def test_get_accessories(self):
        """Tests the get_accessories method. Takes into account the retrieving
        of accessories in the correct order. Tests also the inheritance of
        accessories for variant.
        """
        names = [a.accessory.name for a in self.p1.get_accessories()]
        self.assertEqual(names, ["Product 2", "Product 3"])

        # By default the variant has the same accessory as the parent product
        names = [a.accessory.name for a in self.v1.get_accessories()]
        self.assertEqual(names, ["Product 2", "Product 3"])

        # Now we change the position
        self.pa_1_2.position = 3
        self.pa_1_2.save()

        # The order has been changed
        names = [a.accessory.name for a in self.p1.get_accessories()]
        self.assertEqual(names, ["Product 3", "Product 2"])

        # Also for the variant of course
        names = [a.accessory.name for a in self.v1.get_accessories()]
        self.assertEqual(names, ["Product 3", "Product 2"])

        # Now we assign own accessories for the variant
        self.v1.active_accessories = True
        self.v1.save()

        # We assign the same products but in another order
        ProductAccessories.objects.create(
            product=self.v1, accessory=self.p2, position=1)
        ProductAccessories.objects.create(
            product=self.v1, accessory=self.p3, position=2)

        names = [a.accessory.name for a in self.v1.get_accessories()]
        self.assertEqual(names, ["Product 2", "Product 3"])

        # Now we test quickly the other products
        names = [a.accessory.name for a in self.p2.get_accessories()]
        self.assertEqual(names, ["Product 1", "Product 3"])

        names = [a.accessory.name for a in self.p3.get_accessories()]
        self.assertEqual(names, ["Product 1", "Product 2"])

    def test_get_category(self):
        """
        """
        # Note: p1 has category c111; p2 has c111 and c2 (s. above)
        self.assertEqual(self.p1.get_category(), self.c111)
        self.assertEqual(self.p2.get_category(), self.c111)
        self.assertEqual(self.v1.get_category(), self.c111)

        self.c111.products.remove(self.p1)
        self.assertEqual(self.p1.get_category(), None)

        self.c111.products.remove(self.p2)
        self.assertEqual(self.p2.get_category(), self.c2)

        self.c2.products.remove(self.p2)
        self.assertEqual(self.p2.get_category(), None)

    def test_get_categories(self):
        """
        """
        # Get categories without parents (implicit)
        names = [c.name for c in self.p1.get_categories()]
        self.assertEqual(names, ["Category 111"])

        # Get categories without parents (explicit)
        names = [c.name for c in self.p1.get_categories(with_parents=False)]
        self.assertEqual(names, ["Category 111"])

        # Get categories with parents
        names = [c.name for c in self.p1.get_categories(with_parents=True)]
        self.assertEqual(names, ["Category 111", "Category 11", "Category 1"])

        ###  Now the same for Product 2 which has two categories
        # Get categories without parents (implicit)
        names = [c.name for c in self.p2.get_categories()]
        self.assertEqual(names, ["Category 111", "Category 2"])

        # Get categories without parents (explicit)
        names = [c.name for c in self.p2.get_categories(with_parents=False)]
        self.assertEqual(names, ["Category 111", "Category 2"])

        # Get categories with parents
        names = [c.name for c in self.p2.get_categories(with_parents=True)]
        self.assertEqual(
            names,
            ["Category 111", "Category 11", "Category 1", "Category 2"])

        self.assertEqual(
            [c.name for c in self.v1.get_categories(with_parents=False)],
            ['Category 111']
        )
        self.assertEqual(
            [c.name for c in self.v1.get_categories(with_parents=True)],
            ["Category 111", "Category 11", "Category 1"]
        )

    def test_get_current_category(self):
        from ..core.testutils import RequestMock, SessionMock
        r = RequestFactory().get("/")
        r.session = SessionStore()

        self.assertEqual(
            self.p2.get_current_category(r),
            self.c111)

        r.session['last_category'] = self.c2

        self.assertEqual(
            self.p2.get_current_category(r),
            self.c2)

        self.assertEqual(
            self.p1.get_current_category(r),
            self.c111)

        r.session['last_category'] = self.c1

        self.assertEqual(
            self.p2.get_current_category(r),
            self.c111)

        self.p1.categories.clear()
        self.assertEqual(
            self.p1.get_current_category(r),
            None)

    def test_has_description(self):
        """
        """
        # Test product
        self.assertEqual(self.p1.has_description(), True)

        self.p1.description = ''
        self.assertEqual(self.p1.has_description(), False)

    def test_get_description(self):
        """
        """
        # Test product
        self.assertEqual(self.p1.get_description(), u"Description")

        # Test variant. By default the description of a variant is inherited
        # from parent product.
        self.assertEqual(self.v1.get_description(), u"Description")

        # Now we switch to active description.
        self.v1.active_description = True
        self.v1.save()

        # Now we get the description of the variant
        self.assertEqual(
            self.v1.get_description(),
            u"This is the description of variant 1")

    def test_get_short_description(self):
        """
        """
        # Test product
        self.assertEqual(
            self.p1.get_short_description(), u"Short description product 1")

        # Test variant. By default the description of a variant is inherited
        # from parent product.
        self.assertEqual(
            self.v1.get_short_description(), u"Short description product 1")

        # Now we switch to active description.
        self.v1.active_short_description = True
        self.v1.short_description = \
            u'This is the short description of variant 1'
        self.v1.save()

        # Now we get the description of the variant
        self.assertEqual(
            self.v1.get_short_description(),
            u"This is the short description of variant 1")

    def test_get_image(self):
        """
        """
        image = self.p1.get_image()
        self.assertEqual(image.title, "Image 1")

        # We change the position of the image
        self.i1.position = 5
        self.i1.save()

        # We have to sent product_changed in order to refresh cache
        product_changed.send(self.p1)

        # We get another main images
        image = self.p1.get_image()
        self.assertEqual(image.title, "Image 2")

        # By default variants use it own images instead the parent product
        image = self.v1.get_image()
        self.assertEqual(image.title, "Image 4")

        # Clear the variant images
        self.v1.images.clear()

        # We have to sent product_changed in order to refresh cache
        product_changed.send(self.v1)

        # Now variant has to use the images of parent product
        image = self.v1.get_image()
        self.assertEqual(image.title, "Image 2")

    def test_get_image_by_type(self):
        from django.core.files import File
        import os
        image = self.p1.get_image()
        file_name = settings.PUBLIC_DIR + \
            '/test_data/images/test_product_image.jpeg'
        image.image = File(open(file_name, 'rb'))
        image.save()

        os.remove(os.path.join(
            settings.MEDIA_ROOT,
            'images/test_product_image.jpeg'))
        os.remove(os.path.join(
            settings.MEDIA_ROOT,
            'images/test_product_image.60x60.jpeg'))
        os.remove(os.path.join(
            settings.MEDIA_ROOT,
            'images/test_product_image.100x100.jpeg'))
        os.remove(os.path.join(
            settings.MEDIA_ROOT,
            'images/test_product_image.200x200.jpeg'))
        os.remove(os.path.join(
            settings.MEDIA_ROOT,
            'images/test_product_image.400x400.jpeg'))

        # Flush the cache
        product_changed.send(self.p1)

        url = self.p1.get_image_by_type('small')
        self.assertEqual(url, '/media/images/test_product_image.60x60.jpeg')
        url = self.p1.get_image_by_type('medium')
        self.assertEqual(url, '/media/images/test_product_image.100x100.jpeg')
        url = self.p1.get_image_by_type('large')
        self.assertEqual(url, '/media/images/test_product_image.200x200.jpeg')
        url = self.p1.get_image_by_type('huge')
        self.assertEqual(url, '/media/images/test_product_image.400x400.jpeg')

        self.assertEqual(
            self.p1.small_image_url(),
            '/media/images/test_product_image.60x60.jpeg')
        self.assertEqual(
            self.p1.medium_image_url(),
            '/media/images/test_product_image.100x100.jpeg')
        self.assertEqual(
            self.p1.large_image_url(),
            '/media/images/test_product_image.200x200.jpeg')
        self.assertEqual(
            self.p1.huge_image_url(),
            '/media/images/test_product_image.400x400.jpeg')

    def test_get_images(self):
        """
        """
        titles = [i.title for i in self.p1.get_images()]
        self.assertEqual(titles, ["Image 1", "Image 2", "Image 3"])

        # We change the position of the image
        self.i1.position = 5
        self.i1.save()

        # We have to sent product_changed in order to refresh cache
        product_changed.send(self.p1)

        # We get another order of the images
        titles = [i.title for i in self.p1.get_images()]
        self.assertEqual(titles, ["Image 2", "Image 3", "Image 1"])

        # By default variants use it own images instead the parent product
        titles = [i.title for i in self.v1.get_images()]
        self.assertEqual(titles, ["Image 4", "Image 5"])

        # Clear the variant images
        self.v1.images.clear()

        # We have to sent product_changed in order to refresh cache
        product_changed.send(self.v1)

        # Now variant has to use the images of parent product
        titles = [i.title for i in self.v1.get_images()]
        self.assertEqual(titles, ["Image 2", "Image 3", "Image 1"])

    def test_get_sub_images(self):
        """
        """
        titles = [i.title for i in self.p1.get_sub_images()]
        self.assertEqual(titles, ["Image 2", "Image 3"])

        # We change the position of the image
        self.i1.position = 5
        self.i1.save()

        # We have to sent product_changed in order to refresh cache
        product_changed.send(self.p1)

        # We get another order of the images
        titles = [i.title for i in self.p1.get_sub_images()]
        self.assertEqual(titles, ["Image 3", "Image 1"])

        # By default variants use it own images instead the parent product
        titles = [i.title for i in self.v1.get_sub_images()]
        self.assertEqual(titles, ["Image 5"])

        # Clear the variant images
        self.v1.images.clear()

        # We have to sent product_changed in order to refresh cache
        product_changed.send(self.v1)

        # Now variant has to use the images of parent product
        titles = [i.title for i in self.v1.get_sub_images()]
        self.assertEqual(titles, ["Image 3", "Image 1"])

    def test_get_data_for_meta_info(self):
        r = self.p1._get_data_for_meta_info().keys()
        self.assertIn('shop', r)
        self.assertIn('category', r)
        self.assertIn('product', r)
        self.assertIn('name', r)
        self.assertIn('shop_name', r)
        self.assertIn('manufacturer', r)

        r = self.p1._get_data_for_meta_info(request={}).keys()
        self.assertIn('shop', r)
        self.assertIn('category', r)
        self.assertIn('product', r)
        self.assertIn('name', r)
        self.assertIn('shop_name', r)
        self.assertIn('manufacturer', r)
        self.assertIn('request', r)

    def test_get_meta_title(self):
        """Tests the correct return of meta title, foremost the replacement
        of P-Cart specific tag {{ name }}.
        """
        self.p1.meta_title = "T1 T2"
        self.assertEqual(self.p1.get_meta_title(), "T1 T2")

        self.p1.meta_title = "{{ name }} T1 T2"
        self.assertEqual(self.p1.get_meta_title(), "Product 1 T1 T2")

        self.p1.meta_title = "T1 {{ name }} T2"
        self.assertEqual(self.p1.get_meta_title(), "T1 Product 1 T2")

        self.p1.meta_title = "T1 T2 {{ name }}"
        self.assertEqual(self.p1.get_meta_title(), "T1 T2 Product 1")

        self.p1.meta_title = ''
        self.p1.save()
        self.assertEqual(
            self.p1.meta_title, "{{ name }} - {{ shop_name }}")
        self.assertEqual(self.p1.get_meta_title(), "Product 1 - P-Cart")

    def test_get_meta_keywords_1(self):
        """Tests the correct return of meta keywords, foremost the replacement
        of P-Cart specific tags for the meta fields.
        """
        self.p1.meta_keywords = "KW1 KW2 KW3"
        self.p1.save()
        self.assertEqual(self.p1.get_meta_keywords(), "KW1 KW2 KW3")

        # Test including of the name
        self.p1.meta_keywords = "{{ name }} KW1 KW2 KW3"
        self.p1.save()
        self.assertEqual(self.p1.get_meta_keywords(), "Product 1 KW1 KW2 KW3")

        self.p1.meta_keywords = "KW1 {{ name }} KW2 KW3"
        self.p1.save()
        self.assertEqual(self.p1.get_meta_keywords(), "KW1 Product 1 KW2 KW3")

        self.p1.meta_keywords = "KW1 KW2 KW3 {{ name }}"
        self.p1.save()
        self.assertEqual(self.p1.get_meta_keywords(), "KW1 KW2 KW3 Product 1")

        self.p1.meta_keywords = "{{ name }}"
        self.p1.save()
        self.assertEqual(self.p1.get_meta_keywords(), "Product 1")

        # Test including of the description
        self.p1.meta_keywords = "{{ product.short_description }} KW1 KW2 KW3"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_keywords(),
            "Short description product 1 KW1 KW2 KW3")

        self.p1.meta_keywords = "KW1 {{ product.short_description }} KW2 KW3"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_keywords(),
            "KW1 Short description product 1 KW2 KW3")

        self.p1.meta_keywords = "KW1 KW2 KW3 {{ product.short_description }}"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_keywords(),
            "KW1 KW2 KW3 Short description product 1")

        self.p1.meta_keywords = "{{ product.short_description }}"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_keywords(),
            "Short description product 1")

    def test_get_meta_description(self):
        """Tests the correct return of meta description, foremost the
        replacement of P-Cart specific tags for the meta fields.
        """
        self.p1.meta_description = "Meta description"
        self.p1.save()
        self.assertEqual(self.p1.get_meta_description(), "Meta description")

        # Test the including of name
        self.p1.meta_description = "{{ name }} Meta description"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_description(), "Product 1 Meta description")

        self.p1.meta_description = "Meta {{ name }} description"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_description(), "Meta Product 1 description")

        self.p1.meta_description = "Meta description {{ name }}"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_description(), "Meta description Product 1")

        self.p1.meta_description = "{{ name }}"
        self.p1.save()
        self.assertEqual(self.p1.get_meta_description(), "Product 1")

        # Test the including of short description
        self.p1.meta_description = \
            "{{ product.short_description }} Meta description"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_description(),
            "Short description product 1 Meta description")

        self.p1.meta_description = \
            "Meta {{ product.short_description }} description"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_description(),
            "Meta Short description product 1 description")

        self.p1.meta_description = \
            "Meta description {{ product.short_description }}"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_description(),
            "Meta description Short description product 1")

        self.p1.meta_description = "{{ product.short_description }}"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_description(),
            "Short description product 1")

    def test_get_meta_seo_text(self):
        """Tests the correct return of meta seo text, foremost the
        replacement of P-Cart specific tags for the meta fields.
        """
        self.p1.meta_seo_text = "Meta seo text"
        self.p1.save()
        self.assertEqual(self.p1.get_meta_seo_text(), "Meta seo text")

        # Test the including of name
        self.p1.meta_seo_text = "{{ name }} Meta seo text"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_seo_text(), "Product 1 Meta seo text")

        self.p1.meta_seo_text = "Meta {{ name }} seo text"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_seo_text(), "Meta Product 1 seo text")

        self.p1.meta_seo_text = "Meta seo text {{ name }}"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_seo_text(), "Meta seo text Product 1")

        self.p1.meta_seo_text = "{{ name }}"
        self.p1.save()
        self.assertEqual(self.p1.get_meta_seo_text(), "Product 1")

        # Test the including of short seo text
        self.p1.meta_seo_text = \
            "{{ product.short_description }} Meta seo text"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_seo_text(),
            "Short description product 1 Meta seo text")

        self.p1.meta_seo_text = \
            "Meta {{ product.short_description }} seo text"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_seo_text(),
            "Meta Short description product 1 seo text")

        self.p1.meta_seo_text = \
            "Meta seo text {{ product.short_description }}"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_seo_text(),
            "Meta seo text Short description product 1")

        self.p1.meta_seo_text = "{{ product.short_description }}"
        self.p1.save()
        self.assertEqual(
            self.p1.get_meta_seo_text(),
            "Short description product 1")

    def test_get_name(self):
        """
        """
        # Test product
        self.assertEqual(self.p1.get_name(), u"Product 1")

        # Test variant. By default the name of a variant is returned
        self.assertEqual(self.v1.get_name(), u"Variant 1")

        # Now we clear name of the variant
        self.v1.name = ''
        self.v1.save()

        # Now we get the name of the parent product
        self.assertEqual(self.v1.get_name(), u"Product 1")

        # Now we clear use %P for insert the parent product name
        self.v1.name = 'Product 1 Red'
        self.v1.save()

        # Now we get the name of the parent product
        self.assertEqual(self.v1.get_name(), u"Product 1 Red")

    def test_get_price(self):
        """
        """
        # Test product
        self.assertEqual(self.p1.get_price(self.request), 1.0)

        # Test variant. Variant always return its own price
        self.assertEqual(self.v1.get_price(self.request), 2.0)

        # Now we set variant price to 0.
        self.v1.price = 0
        self.v1.save()

        # Now we get the zero price
        self.assertEqual(self.v1.get_price(self.request), 0)

    def test_get_standard_price(self):
        """Test the price vs. standard price for a product.
        """
        # By default get_standard_price returns then normal
        # price of the product
        standard_price = self.p1.get_standard_price(self.request)
        self.assertEqual(standard_price, 1.0)

        # Switch to for sale
        self.p1.for_sale = True
        self.p1.save()

        # If the product is for sale ``get_price`` returns the for sale price
        price = self.p1.get_price(self.request)
        self.assertEqual(price, 0.5)

        # But ``get_standard_price`` returns still the normal price
        standard_price = self.p1.get_standard_price(self.request)
        self.assertEqual(standard_price, 1.0)

    def test_get_for_sale_price(self):
        """Test the price vs. for sale price for a product.
        """
        price = self.p1.get_price(self.request)
        self.assertEqual(price, 1.0)

        standard_price = self.p1.get_for_sale_price(self.request)
        self.assertEqual(standard_price, 0.5)

        # Switch to for sale
        self.p1.for_sale = True
        self.p1.save()

        # If the product is for sale ``get_price`` returns the for sale price
        price = self.p1.get_price(self.request)
        self.assertEqual(price, 0.5)

        standard_price = self.p1.get_for_sale_price(self.request)
        self.assertEqual(standard_price, 0.5)

    def test_get_sku(self):
        """
        """
        # Test product
        self.assertEqual(self.p1.get_sku(), u"SKU P1")

        # Test variant. By default the sku of a variant is *not* inherited
        self.assertEqual(self.v1.get_sku(), "SKU V1")

        # Now we switch to active sku.
        self.v1.sku = ''
        self.v1.save()

        # Now we get the sku of the parent product
        self.assertEqual(self.v1.get_sku(), "SKU P1")

    def test_get_related_products(self):
        """
        """
        names = [p.name for p in self.p1.get_related_products()]
        self.assertEqual(names, ["Product 2", "Product 3"])

        names = [p.name for p in self.v1.get_related_products()]
        self.assertEqual(names, ["Product 2", "Product 3"])

        names = [p.name for p in self.p2.get_related_products()]
        self.assertEqual(names, [])

        names = [p.name for p in self.p3.get_related_products()]
        self.assertEqual(names, [])

    def test_has_related_products(self):
        """
        """
        result = self.p1.has_related_products()
        self.assertEqual(result, True)

        result = self.p2.has_related_products()
        self.assertEqual(result, False)

        result = self.p3.has_related_products()
        self.assertEqual(result, False)

    def test_has_variants(self):
        """
        """
        result = self.p1.has_variants()
        self.assertEqual(result, True)

        result = self.p2.has_variants()
        self.assertEqual(result, False)

    def test_get_variants(self):
        """
        """
        variants = self.p1.get_variants()

        self.assertEqual(len(variants), 2)
        self.failIf(self.v1 not in variants)
        self.failIf(self.v2 not in variants)

    def test_sub_type(self):
        """Tests the sub type of products.
        """
        self.assertEqual(self.p1.is_standard(), False)
        self.assertEqual(self.p1.is_product_with_variants(), True)
        self.assertEqual(self.p1.is_variant(), False)

        self.assertEqual(self.p2.is_standard(), True)
        self.assertEqual(self.p2.is_product_with_variants(), False)
        self.assertEqual(self.p2.is_variant(), False)

        self.assertEqual(self.v1.is_standard(), False)
        self.assertEqual(self.v1.is_product_with_variants(), False)
        self.assertEqual(self.v1.is_variant(), True)

    def setupAttachments(self):
        # Assign attachments to products
        self.attachment_P1_1_data = dict(
            title='Attachment P1-1',
            product=self.p1,
        )
        self.attachment_P1_1 = ProductAttachment.objects.create(
            **self.attachment_P1_1_data)

        self.attachment_P1_2_data = dict(
            title='Attachment P1-2',
            product=self.p1,
        )
        self.attachment_P1_2 = ProductAttachment.objects.create(
            **self.attachment_P1_2_data)

        self.attachment_V1_data = dict(
            title='Attachment V1',
            product=self.v1,
        )
        self.attachment_V1 = ProductAttachment.objects.create(
            **self.attachment_V1_data)

    def test_has_attachments(self):
        self.assertEqual(self.v1.has_attachments(), True)
        self.assertEqual(self.p2.has_attachments(), False)

    def test_get_attachments(self):
        # retrieve attachments
        match_titles = [self.attachment_P1_1_data['title'],
                        self.attachment_P1_2_data['title']]
        attachments = self.p1.get_attachments()
        attachments_titles = [x.title for x in attachments]
        self.assertEqual(match_titles, attachments_titles)

        # check data
        first = attachments[0]
        for k, v in self.attachment_P1_1_data.items():
            self.assertEqual(getattr(first, k), v)

        second = attachments[1]
        for k, v in self.attachment_P1_2_data.items():
            self.assertEqual(getattr(second, k), v)

        # retrieve variant attachment
        attachments = self.v1.get_attachments()
        attachments_titles = [x.title for x in attachments]
        match_titles = [self.attachment_V1_data['title']]
        self.assertEqual(attachments_titles, match_titles)

        # delete variant attachment: we should get parent attachments
        self.attachment_V1.delete()
        pattachments = [x.title for x in self.p1.get_attachments()]
        vattachments = [x.title for x in self.v1.get_attachments()]
        self.assertEqual(pattachments, vattachments)

        # position
        self.attachment_P1_1.position = 20
        self.attachment_P1_1.save()
        self.attachment_P1_2.position = 10
        self.attachment_P1_2.save()
        attachments_titles = [x.title for x in self.p1.get_attachments()]
        match_titles = [self.attachment_P1_2_data['title'],
                        self.attachment_P1_1_data['title']]
        self.assertEqual(match_titles, attachments_titles)

    def test_get_type_of_quantity_field(self):
        result = self.p1.get_type_of_quantity_field()
        self.assertEqual(result, QUANTITY_FIELD_INTEGER)

        result = self.v1.get_type_of_quantity_field()
        self.assertEqual(result, QUANTITY_FIELD_INTEGER)

        self.p1.type_of_quantity_field = QUANTITY_FIELD_DECIMAL_1
        self.p1.save()

        result = self.p1.get_type_of_quantity_field()
        self.assertEqual(result, QUANTITY_FIELD_DECIMAL_1)
        result = self.v1.get_type_of_quantity_field()
        self.assertEqual(result, QUANTITY_FIELD_DECIMAL_1)

        self.p1.type_of_quantity_field = QUANTITY_FIELD_DECIMAL_2
        self.p1.save()

        result = self.p1.get_type_of_quantity_field()
        self.assertEqual(result, QUANTITY_FIELD_DECIMAL_2)
        result = self.v1.get_type_of_quantity_field()
        self.assertEqual(result, QUANTITY_FIELD_DECIMAL_2)

    def test_get_clean_quantity(self):
        result = self.p1.get_clean_quantity(1)
        self.assertEqual(result, 1)
        result = self.v1.get_clean_quantity(1)
        self.assertEqual(result, 1)

        result = self.p1.get_clean_quantity("1")
        self.assertEqual(result, 1)
        result = self.v1.get_clean_quantity("1")
        self.assertEqual(result, 1)

        result = self.p1.get_clean_quantity(1.0)
        self.assertEqual(result, 1)
        result = self.v1.get_clean_quantity(1.0)
        self.assertEqual(result, 1)

        result = self.p1.get_clean_quantity("1.0")
        self.assertEqual(result, 1)
        result = self.v1.get_clean_quantity("1.0")
        self.assertEqual(result, 1)

        result = self.p1.get_clean_quantity("A")
        self.assertEqual(result, 1)
        result = self.v1.get_clean_quantity("A")
        self.assertEqual(result, 1)

        self.p1.type_of_quantity_field = QUANTITY_FIELD_DECIMAL_1
        self.p1.save()

        result = self.p1.get_clean_quantity(1)
        self.assertEqual(result, "1.0")
        result = self.v1.get_clean_quantity(1)
        self.assertEqual(result, "1.0")

        result = self.p1.get_clean_quantity("1")
        self.assertEqual(result, "1.0")
        result = self.v1.get_clean_quantity("1")
        self.assertEqual(result, "1.0")

        result = self.p1.get_clean_quantity(1.0)
        self.assertEqual(result, "1.0")
        result = self.v1.get_clean_quantity(1.0)
        self.assertEqual(result, "1.0")

        result = self.p1.get_clean_quantity("1.0")
        self.assertEqual(result, "1.0")
        result = self.v1.get_clean_quantity("1.0")
        self.assertEqual(result, "1.0")

        result = self.p1.get_clean_quantity("A")
        self.assertEqual(result, "1.0")
        result = self.v1.get_clean_quantity("A")
        self.assertEqual(result, "1.0")

        self.p1.type_of_quantity_field = QUANTITY_FIELD_DECIMAL_2
        self.p1.save()

        result = self.p1.get_clean_quantity(1)
        self.assertEqual(result, "1.00")
        result = self.v1.get_clean_quantity(1)
        self.assertEqual(result, "1.00")

        result = self.p1.get_clean_quantity("1")
        self.assertEqual(result, "1.00")
        result = self.v1.get_clean_quantity("1")
        self.assertEqual(result, "1.00")

        result = self.p1.get_clean_quantity(1.0)
        self.assertEqual(result, "1.00")
        result = self.v1.get_clean_quantity(1.0)
        self.assertEqual(result, "1.00")

        result = self.p1.get_clean_quantity("1.0")
        self.assertEqual(result, "1.00")
        result = self.v1.get_clean_quantity("1.0")
        self.assertEqual(result, "1.00")

        result = self.p1.get_clean_quantity("A")
        self.assertEqual(result, "1.00")
        result = self.v1.get_clean_quantity("A")
        self.assertEqual(result, "1.00")

    def test_apply_property_set(self):
        """
        """
        # Create new property set from the properties of Product 1
        ps = self.p1.create_property_set_from_product('Property set 1')

        # Apply the property set to Product 2
        self.p2.apply_property_set(ps)
        ppvs = self.p2.property_values.all()
        self.assertEqual(ppvs[0].property, self.ppg1)
        self.assertEqual(ppvs[1].property, self.pp11)
        self.assertEqual(ppvs[2].property, self.pp12)
        self.assertEqual(ppvs[3].property, self.pp13)
        self.assertEqual(ppvs[4].property, self.ppg2)
        self.assertEqual(ppvs[5].property, self.pp21)
        self.assertEqual(ppvs[6].property, self.pp22)
        self.assertEqual(ppvs[7].property, self.pp23)

        # Change the property positions for the property set
        ps_rels = ps.propertysetrelation_set.order_by('position')
        ps_rel4 = ps_rels[4]
        ps_rel4.position = 1
        ps_rel4.save()
        ps_rel5 = ps_rels[5]
        ps_rel5.position = 2
        ps_rel5.save()
        ps_rel6 = ps_rels[6]
        ps_rel6.position = 3
        ps_rel6.save()
        ps_rel7 = ps_rels[7]
        ps_rel7.position = 4
        ps_rel7.save()
        ps_rel0 = ps_rels[0]
        ps_rel0.position = 5
        ps_rel0.save()
        ps_rel1 = ps_rels[1]
        ps_rel1.position = 6
        ps_rel1.save()
        ps_rel2 = ps_rels[2]
        ps_rel2.position = 7
        ps_rel2.save()
        ps_rel3 = ps_rels[3]
        ps_rel3.position = 8
        ps_rel3.save()

        # Apply the property set again
        self.p2.apply_property_set(ps)
        ppvs = self.p2.property_values.all()
        self.assertEqual(ppvs[0].property, self.ppg2)
        self.assertEqual(ppvs[1].property, self.pp21)
        self.assertEqual(ppvs[2].property, self.pp22)
        self.assertEqual(ppvs[3].property, self.pp23)
        self.assertEqual(ppvs[4].property, self.ppg1)
        self.assertEqual(ppvs[5].property, self.pp11)
        self.assertEqual(ppvs[6].property, self.pp12)
        self.assertEqual(ppvs[7].property, self.pp13)

    def test_create_property_set_from_product(self):
        """
        """
        from mock import patch

        ps = self.p1.create_property_set_from_product('Property set 1')

        ps_rels = ps.propertysetrelation_set.order_by('position')
        self.assertEqual(ps_rels[0].property, self.ppg1)
        self.assertEqual(ps_rels[1].property, self.pp11)
        self.assertEqual(ps_rels[2].property, self.pp12)
        self.assertEqual(ps_rels[3].property, self.pp13)
        self.assertEqual(ps_rels[4].property, self.ppg2)
        self.assertEqual(ps_rels[5].property, self.pp21)
        self.assertEqual(ps_rels[6].property, self.pp22)
        self.assertEqual(ps_rels[7].property, self.pp23)

        # What if property set already exists?
        with patch('lfs.catalog.models.logger') as mock_logger:
            ps = self.p1.create_property_set_from_product('Property set 1')
            self.assertTrue(mock_logger.error.called)
            self.assertIn(
                u'уже существует',
                mock_logger.error.call_args[0][0])

    def test_get_manufacturer(self):
        """
        """
        from ..manufacturer.models import Manufacturer
        self.assertEqual(self.p1.get_manufacturer(), None)

        m = Manufacturer.objects.create(name='Manufacturer 1')
        self.p1.manufacturer = m
        self.p1.save()

        self.assertEqual(self.p1.get_manufacturer(), m)
        self.assertEqual(self.v1.get_manufacturer(), m)

        m2 = Manufacturer.objects.create(name='Manufacturer 2')
        self.v1.manufacturer = m2
        self.v1.save()
        self.assertEqual(self.v1.get_manufacturer(), m2)

    def test_get_price_calculator(self):
        from ..default_price import DefaultPriceCalculator
        from ..core.utils import import_symbol
        shop = get_default_shop(self.request)

        self.assertTrue(isinstance(
            self.p1.get_price_calculator(
                self.request), DefaultPriceCalculator))
        self.assertTrue(isinstance(
            self.p1.get_price_calculator(self.request),
            import_symbol(shop.price_calculator)))

        self.p1.price_calculator = 'lfs.default_price.DefaultPriceCalculator'
        self.p1.save()
        self.assertTrue(isinstance(
            self.p1.get_price_calculator(
                self.request), DefaultPriceCalculator))

    def test_get_price_unit(self):
        """
        """
        self.assertEqual(self.p1.get_price_unit(), '')

        self.p1.price_unit = u'шт'
        self.p1.save()
        self.assertEqual(self.p1.get_price_unit(), u'шт')
        self.assertEqual(self.v1.get_price_unit(), u'шт')

        self.v1.price_unit = u'pcs'
        self.v1.save()
        self.assertEqual(self.v1.get_price_unit(), u'шт')

    def test_get_unit(self):
        """
        """
        self.assertEqual(self.p1.get_unit(), '')

        self.p1.unit = u'шт'
        self.p1.save()
        self.assertEqual(self.p1.get_unit(), u'шт')
        self.assertEqual(self.v1.get_unit(), u'шт')

        self.v1.unit = u'pcs'
        self.v1.save()
        self.assertEqual(self.v1.get_unit(), u'шт')

    def test_get_properties_groups(self):
        pgs = self.p1.get_properties_groups()
        self.assertEqual(pgs, [self.ppg1, self.ppg2])

        pgs = self.p2.get_properties_groups()
        self.assertEqual(pgs, [])

    def test_get_properties_for_group(self):
        self.assertEqual(
            self.p1.get_properties_for_group(self.ppg1),
            [self.pp11, self.pp12, self.pp13])
        self.assertEqual(
            self.p1.get_properties_for_group(self.ppg2),
            [self.pp21, self.pp22, self.pp23])
        self.assertEqual(
            self.p2.get_properties_for_group(self.ppg1),
            [])

    def test_get_property_values(self):
        pvs = self.p1.get_property_values()
        self.assertEqual(pvs[0], self.ppgv1)
        self.assertEqual(pvs[1], self.ppv11)
        self.assertEqual(pvs[2], self.ppv12)
        self.assertEqual(pvs[3], self.ppv13)
        self.assertEqual(pvs[4], self.ppgv2)
        self.assertEqual(pvs[5], self.ppv21)
        self.assertEqual(pvs[6], self.ppv22)
        self.assertEqual(pvs[7], self.ppv23)

        self.assertEqual(
            self.p2.get_property_values(), [])

    def test_get_properties(self):
        pps = self.p1.get_properties()
        self.assertEqual(pps[0], self.ppg1)
        self.assertEqual(pps[1], self.pp11)
        self.assertEqual(pps[2], self.pp12)
        self.assertEqual(pps[3], self.pp13)
        self.assertEqual(pps[4], self.ppg2)
        self.assertEqual(pps[5], self.pp21)
        self.assertEqual(pps[6], self.pp22)
        self.assertEqual(pps[7], self.pp23)

        self.assertEqual(
            self.p2.get_properties(), [])

    def test_get_property_values(self):
        self.assertEqual(
            self.p1.get_property_value(self.ppg1),
            self.ppgv1)
        self.assertEqual(
            self.p1.get_property_value(self.pp11),
            self.ppv11)
        self.assertEqual(
            self.p1.get_property_value(self.pp12),
            self.ppv12)
        self.assertEqual(
            self.p1.get_property_value(self.pp13),
            self.ppv13)
        self.assertEqual(
            self.p1.get_property_value(self.ppg2),
            self.ppgv2)
        self.assertEqual(
            self.p1.get_property_value(self.pp21),
            self.ppv21)
        self.assertEqual(
            self.p1.get_property_value(self.pp22),
            self.ppv22)
        self.assertEqual(
            self.p1.get_property_value(self.pp23),
            self.ppv23)

        self.assertEqual(
            self.p2.get_property_value(self.pp11),
            None)

    def test_get_group_of_property(self):
        self.assertEqual(
            self.p1.get_group_of_property(self.pp11),
            self.ppg1)
        self.assertEqual(
            self.p1.get_group_of_property(self.pp12),
            self.ppg1)
        self.assertEqual(
            self.p1.get_group_of_property(self.pp13),
            self.ppg1)
        self.assertEqual(
            self.p1.get_group_of_property(self.pp21),
            self.ppg2)
        self.assertEqual(
            self.p1.get_group_of_property(self.pp22),
            self.ppg2)
        self.assertEqual(
            self.p1.get_group_of_property(self.pp23),
            self.ppg2)

        self.assertEqual(
            self.p1.get_group_of_property(self.ppg1),
            None)
        self.assertEqual(
            self.p1.get_group_of_property(self.ppg2),
            None)

        self.assertEqual(
            self.p2.get_group_of_property(self.ppg1),
            None)
        self.assertEqual(
            self.p2.get_group_of_property(self.pp11),
            None)

        ProductPropertyValue.objects.create(
            product=self.p2, property=self.pp11)
        self.assertEqual(
            self.p2.get_group_of_property(self.pp11),
            None)

    def test_get_cheapest_variant(self):
        self.assertEqual(
            self.p1.get_cheapest_variant(self.request),
            self.v1)

        self.v1.price = 0
        self.v1.save()
        self.assertEqual(
            self.p1.get_cheapest_variant(self.request),
            self.v2)

        self.assertEqual(
            self.p2.get_cheapest_variant(self.request),
            None)

        self.assertEqual(
            self.v1.get_cheapest_variant(self.request),
            None)
        self.assertEqual(
            self.v2.get_cheapest_variant(self.request),
            None)

    def test_get_cheapest_for_sale_price(self):
        self.assertEqual(
            self.p1.get_cheapest_for_sale_price(self.request),
            {'price': 1.0, 'starting_from': True})

        self.assertEqual(
            self.v1.get_cheapest_for_sale_price(self.request),
            {'price': 0, 'starting_from': False})
        self.assertEqual(
            self.v2.get_cheapest_for_sale_price(self.request),
            {'price': 0, 'starting_from': False})
        self.assertEqual(
            self.p2.get_cheapest_for_sale_price(self.request),
            {'price': 0, 'starting_from': False})

    def test_get_cheapest_standard_price(self):
        self.assertEqual(
            self.p1.get_cheapest_standard_price(self.request),
            {'price': 2.0, 'starting_from': True})

        self.assertEqual(
            self.v1.get_cheapest_standard_price(self.request),
            {'price': 0, 'starting_from': False})
        self.assertEqual(
            self.v2.get_cheapest_standard_price(self.request),
            {'price': 0, 'starting_from': False})
        self.assertEqual(
            self.p2.get_cheapest_standard_price(self.request),
            {'price': 0, 'starting_from': False})

    def test_get_cheapest_price(self):
        self.assertEqual(
            self.p1.get_cheapest_price(self.request),
            {'price': 2.0, 'starting_from': True})

        self.assertEqual(
            self.v1.get_cheapest_price(self.request),
            {'price': 0, 'starting_from': False})
        self.assertEqual(
            self.v2.get_cheapest_price(self.request),
            {'price': 0, 'starting_from': False})

        self.v2.for_sale = True
        self.v2.save()

        self.assertEqual(
            self.p1.get_cheapest_price(self.request),
            {'price': 1.0, 'starting_from': True})

        self.assertEqual(
            self.v1.get_cheapest_price(self.request),
            {'price': 0, 'starting_from': False})
        self.assertEqual(
            self.v2.get_cheapest_price(self.request),
            {'price': 0, 'starting_from': False})

        self.assertEqual(
            self.p2.get_cheapest_price(self.request),
            {'price': 0, 'starting_from': False})

    def test_get_min_variant_status(self):
        self.assertEqual(
            self.p1.get_min_variant_status(),
            self.s2)

    def test_get_max_variant_status(self):
        self.assertEqual(
            self.p1.get_max_variant_status(),
            self.s1)

    def test_get_static_block(self):
        """
        """
        sb1 = StaticBlock.objects.create(
            name='Static block 1',
            html='Body 1')
        sb2 = StaticBlock.objects.create(
            name='Static block 2',
            html='Body 2')

        self.p1.static_block = sb1
        self.p1.save()

        self.assertEqual(self.p1.get_static_block(), sb1)
        # Test the cache
        self.assertEqual(self.p1.get_static_block(), sb1)
        self.assertEqual(self.v1.get_static_block(), sb1)

        self.v1.static_block = sb2
        self.v1.save()
        self.assertEqual(self.v1.get_static_block(), sb1)

        self.v1.active_static_block = True
        self.v1.save()
        self.assertEqual(self.v1.get_static_block(), sb2)

    def test_get_parent_for_portlets(self):
        shop = get_default_shop()

        self.assertEqual(self.v1.get_parent_for_portlets(), self.p1)
        self.assertEqual(self.v2.get_parent_for_portlets(), self.p1)

        self.assertEqual(self.p1.get_parent_for_portlets(), self.c111)
        self.assertEqual(self.p2.get_parent_for_portlets(), self.c111)
        self.assertEqual(self.p3.get_parent_for_portlets(), shop)

        # Here we see a big problem because p2 is in two categories
        # self.assertEqual(self.p2.get_parent_for_portlets(), self.c2)

    def test_get_template_name(self):
        self.assertEqual(
            self.p1.get_template_name(),
            "%s/%s" % (PRODUCT_PATH, "product_inline.html"))

        self.p1.template = None
        self.p1.save()
        self.assertEqual(
            self.p1.get_template_name(),
            "%s/%s" % (PRODUCT_PATH, "product_inline.html"))

    def test_get_product_lists(self):
        from ..marketing.models import ProductList, ProductListItem

        pl = ProductList.objects.create(name='Product list 1')
        pli1 = ProductListItem.objects.create(
            product_list=pl,
            product=self.p1)

        self.assertEqual(
            list(self.p1.get_product_lists()),
            [pl])

    def test_get_id(self):
        settings.SSHOP_USE_1C = False
        self.assertEqual(self.p1.get_id(), 1)
        self.p1.id_1c = '00235'
        self.p1.save()

        settings.SSHOP_USE_1C = True
        self.assertEqual(self.p1.get_id(), '00235')


class ProductAccessoriesTestCase(TestCase):
    """Tests ProductAccessories (surprise, surprise).
    """
    fixtures = ['lfs_shop.xml']

    def setUp(self):
        """
        """
        self.p1 = Product.objects.create(
            name=u"Product 1", slug=u"product-1", price=1.0, active=True)
        self.p2 = Product.objects.create(
            name=u"Product 2", slug=u"product-2", price=2.0, active=True)
        self.p3 = Product.objects.create(
            name=u"Product 3", slug=u"product-3", price=3.0, active=True)

        self.request = RequestFactory().get("/")
        self.request.session = SessionStore()

    def test_defaults(self):
        """Tests default values after creation.
        """
        pa = ProductAccessories.objects.create(
            product=self.p1, accessory=self.p2)
        self.assertEqual(unicode(pa), '%s -> %s' % (
            pa.product.name, pa.accessory.name))
        self.assertEqual(pa.position, 999)
        self.assertEqual(pa.quantity, 1)

    def test_get_price(self):
        """Tests the calculation of the total price of a product accessory.
        """
        # Product 1 gets two accessories
        pa1 = ProductAccessories.objects.create(
            product=self.p1, accessory=self.p2, position=1, quantity=1)
        pa2 = ProductAccessories.objects.create(
            product=self.p1, accessory=self.p3, position=2, quantity=2)

        self.assertEqual(pa1.get_price(self.request), 2.0)
        self.assertEqual(pa2.get_price(self.request), 6.0)


class ProductStatusTestCase(TestCase):
    fixtures = ['lfs_shop.xml']

    def test_product_statuses(self):
        s1 = ProductStatus.objects.get(pk=1)
        self.assertEqual(s1.name, 'In stock')
        self.assertEqual(s1.css_class, '')
        self.assertEqual(s1.description, '')
        self.assertEqual(s1.order, 1)
        self.assertEqual(s1.show_buy_button, True)
        self.assertEqual(s1.show_ask_button, True)
        self.assertEqual(s1.is_visible, True)
        self.assertEqual(s1.is_searchable, True)
        self.assertEqual(s1.delivery_time, None)

        s2 = ProductStatus.objects.get(pk=2)
        self.assertEqual(s2.name, 'For order')
        self.assertEqual(s2.css_class, '')
        self.assertEqual(s2.description, '')
        self.assertEqual(s2.order, 2)
        self.assertEqual(s2.show_buy_button, False)
        self.assertEqual(s2.show_ask_button, True)
        self.assertEqual(s2.is_visible, True)
        self.assertEqual(s2.is_searchable, True)
        self.assertEqual(s2.delivery_time, None)

        s3 = ProductStatus.objects.get(pk=3)
        self.assertEqual(s3.name, 'Archive')
        self.assertEqual(s3.css_class, '')
        self.assertEqual(s3.description, '')
        self.assertEqual(s3.order, 3)
        self.assertEqual(s3.show_buy_button, False)
        self.assertEqual(s3.show_ask_button, True)
        self.assertEqual(s3.is_visible, False)
        self.assertEqual(s3.is_searchable, False)
        self.assertEqual(s3.delivery_time, None)

    def test_add_new_status(self):
        s = ProductStatus.objects.create(
            name='Status 1',
            order=4,
        )
        self.assertEqual(unicode(s), 'Status 1')

        s.css_class = 'label-success'
        s.description = 'Sample description'

        self.assertEqual(unicode(s.css_class), 'label-success')
        self.assertEqual(unicode(s.description), 'Sample description')


class PropertyTestCase(TestCase):
    """
    """
    def test_unicode(self):
        ppg1 = Property.objects.create(
            name='Property group 1', is_group=True)
        pp1 = Property.objects.create(
            name='Property 1')

        self.assertEqual(unicode(ppg1), u'Property group 1 [Группа]')
        self.assertEqual(unicode(pp1), u'Property 1')

    def test_save(self):
        from django.db import IntegrityError
        ppg1 = Property.objects.create(
            name='Property group 1', is_group=True)
        pp1 = Property.objects.create(
            name='Property 1')

        self.assertEqual(ppg1.identificator, 'property-group-1')
        self.assertEqual(pp1.identificator, 'property-1')

        ppg2 = Property.objects.create(
            name='Color', is_group=True)
        pp2 = Property.objects.create(
            name='Color')

        self.assertEqual(ppg2.identificator, 'color')
        self.assertEqual(pp2.identificator, 'color_1')

        # What if I try to create new property with same name
        with self.assertRaises(IntegrityError):
            ppg3 = Property.objects.create(
                name='Color', is_group=True)

        with self.assertRaises(IntegrityError):
            pp3 = Property.objects.create(
                name='Color')

        pp4 = Property.objects.create(
            name='Another color', identificator='color')
        self.assertEqual(pp4.identificator, 'color_2')


class PropertyOptionTestCase(TestCase):
    """
    """
    def test_unicode(self):
        po = PropertyOption.objects.create(
            name='Property option 1')
        self.assertEqual(unicode(po), 'Property option 1')


class ProductPropertyValueTestCase(TestCase):
    """
    """
    fixtures = ['lfs_shop.xml']

    def setUp(self):
        """
        """
        # Get status "In stock"
        self.s1 = ProductStatus.objects.get(pk=1)

        # A product with properties
        self.p1 = Product.objects.create(
            name=u"Product 1",
            slug=u"product-1",
            sku=u"SKU P1",
            description=u"Description",
            short_description=u"Short description product 1",
            meta_description=u"Meta description product 1",
            meta_keywords=u"Meta keywords product 1",
            sub_type=STANDARD_PRODUCT,
            price=1.0,
            for_sale_price=0.0,
            active=True,
            status=self.s1)

        # Create properties
        self.pp11 = Property.objects.create(
            name='Property 11', identificator='property_11')
        self.pp12 = Property.objects.create(
            name='Property 12', identificator='property_12')

        self.ppv11 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp11,
            value='Test value 11',
            position=1)
        self.ppv12 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp12,
            value='Test value 12',
            position=2)

        self.c1 = Category.objects.create(
            name="Category 1", slug="category-1")
        self.c1.products = (self.p1,)

    def test_unicode(self):
        self.assertEqual(
            unicode(self.ppv11),
            'Product 1/Property 11: Test value 11')

    def test_get_type(self):
        self.assertEqual(self.ppv11.get_type(), None)
        pt1 = PropertyType.objects.create(
            property=self.pp11,
            type=PROPERTY_TYPE_STRING)
        pt1.categories = (self.c1,)

        self.assertEqual(self.ppv11.get_type(), pt1)

        pt2 = PropertyType.objects.create(
            property=self.pp11,
            type=PROPERTY_TYPE_STRING)
        pt2.categories = (self.c1,)
        self.assertEqual(self.ppv11.get_type(), None)

    def test_fill_float(self):
        self.assertEqual(self.ppv11.get_type(), None)
        self.assertEqual(
            self.ppv11.value_as_float, 0)

        pt1 = PropertyType.objects.create(
            property=self.pp11,
            type=PROPERTY_TYPE_FLOAT)
        pt1.categories = (self.c1,)

        self.ppv11.value = '10.34'
        self.ppv11.save()
        self.assertEqual(
            self.ppv11.value_as_float, 10.34)

        self.ppv11.value = '10,34'
        self.ppv11.save()
        self.assertEqual(
            self.ppv11.value_as_float, 10.34)

        self.ppv11.value = ' 234 '
        self.ppv11.save()
        self.assertEqual(
            self.ppv11.value_as_float, 234.0)

        self.ppv11.value = '1024 Mb'
        self.ppv11.save()
        self.assertEqual(
            self.ppv11.value_as_float, 1024.0)


class ImageTestCase(TestCase):
    """
    """
    def test_unicode(self):
        i1 = Image.objects.create(title="Image 1", position=1)
        self.assertEqual(unicode(i1), 'Image 1')


class StaticBlockTestCase(TestCase):
    """
    """
    def test_unicode(self):
        sb1 = StaticBlock.objects.create(
            name="Static block 1",
            html="Test content")
        self.assertEqual(unicode(sb1), 'Static block 1')


class PropertyValueIconTestCase(TestCase):
    """
    """
    fixtures = ['lfs_shop.xml']

    def setUp(self):
        """
        """
        self.request = RequestFactory().get("/")
        self.request.session = SessionStore()

        # Get status "In stock"
        self.s1 = ProductStatus.objects.get(pk=1)
        # Get status "For order"
        self.s2 = ProductStatus.objects.get(pk=2)
        # Get status "Archive"
        self.s3 = ProductStatus.objects.get(pk=3)

        # A product with properties
        self.p1 = Product.objects.create(
            name=u"Product 1",
            slug=u"product-1",
            sku=u"SKU P1",
            description=u"Description",
            short_description=u"Short description product 1",
            meta_description=u"Meta description product 1",
            meta_keywords=u"Meta keywords product 1",
            price=1.0,
            active=True,
            status=self.s1)

        # Products without properties
        self.p2 = Product.objects.create(
            name=u"Product 2", slug=u"product-2", active=True)
        self.p3 = Product.objects.create(
            name=u"Product 3", slug=u"product-3", active=True)

        # Create property groups
        self.ppg1 = Property.objects.create(
            name='Property group 1',
            identificator='property_group_1',
            is_group=True)
        self.ppg2 = Property.objects.create(
            name='Property group 2',
            identificator='property_group_2',
            is_group=True)

        # Create properties
        self.pp11 = Property.objects.create(
            name='Property 11', identificator='property_11')
        self.pp12 = Property.objects.create(
            name='Property 12', identificator='property_12')
        self.pp13 = Property.objects.create(
            name='Property 13', identificator='property_13')

        self.pp21 = Property.objects.create(
            name='Property 21', identificator='property_21')
        self.pp22 = Property.objects.create(
            name='Property 22', identificator='property_22')
        self.pp23 = Property.objects.create(
            name='Property 23', identificator='property_23')

        self.ppgv1 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.ppg1,
            position=1)
        self.ppv11 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp11,
            value='Value 11',
            position=2)
        self.ppv12 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp12,
            value='Value 12',
            position=3)
        self.ppv13 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp13,
            value='Value 13',
            position=4)
        self.ppgv2 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.ppg2,
            position=5)
        self.ppv21 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp21,
            value='Value 21',
            position=6)
        self.ppv22 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp22,
            value='Value 22',
            position=7)
        self.ppv23 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp23,
            value='Value 23',
            position=8)

        # Create some categories
        self.c1 = Category.objects.create(
            name="Category 1", slug="category-1")
        self.c11 = Category.objects.create(
            name="Category 11", slug="category-11", parent=self.c1)
        self.c111 = Category.objects.create(
            name="Category 111", slug="category-111", parent=self.c11)
        self.c2 = Category.objects.create(
            name="Category 2", slug="category-2")
        Category.objects.rebuild()

        # Assign products to categories. Note: p2 has two top level categories
        self.c111.products = (self.p1, self.p2)
        self.c2.products = (self.p2, )

    def test_is_number_property(self):
        from mock import patch
        pvi1 = PropertyValueIcon.objects.create(
            category=self.c111,
            title='Icon 1',
            position=1)
        pvi1.properties = (self.pp11,)
        self.assertFalse(pvi1.is_number_property())

        pvi1.properties = (self.pp11, self.pp12)
        self.assertFalse(pvi1.is_number_property())

        pt1 = PropertyType.objects.create(
            property=self.pp11,
            type=PROPERTY_TYPE_FLOAT)
        pt1.categories = (self.c111,)

        pvi1.properties = (self.pp11,)
        self.assertTrue(pvi1.is_number_property())

        pvi1.properties = (self.pp11, self.pp12)
        self.assertFalse(pvi1.is_number_property())

        pt2 = PropertyType.objects.create(
            property=self.pp11,
            type=PROPERTY_TYPE_FLOAT)
        pt2.categories = (self.c111,)
        pvi1.properties = (self.pp11,)
        with patch('lfs.catalog.models.logger') as mock_logger:
            isn = pvi1.is_number_property()
            self.assertFalse(isn)
            self.assertIn(
                u'more than one',
                mock_logger.error.call_args[0][0])

    def test_parse(self):
        pvi1 = PropertyValueIcon.objects.create(
            category=self.c111,
            title='Icon 1',
            expression='Value 11',
            position=1)
        pvi1.properties = (self.pp11,)

        pvi1.parse()
        self.assertEqual(
            list(pvi1.products.all()),
            [self.p1])

        pvi1.expression = r'\w* \d+'
        pvi1.parse()
        self.assertEqual(
            list(pvi1.products.all()),
            [self.p1])

        pt1 = PropertyType.objects.create(
            property=self.pp11,
            type=PROPERTY_TYPE_FLOAT)
        pt1.categories = (self.c111,)
        self.ppv11.value = ' 23,40 m'
        self.ppv11.save()

        pvi1.expression = '== 23.40'
        pvi1.parse()
        self.assertEqual(
            list(pvi1.products.all()),
            [self.p1])

        pvi1.expression = '!= 23.40'
        pvi1.parse()
        self.assertEqual(
            list(pvi1.products.all()),
            [])

        pvi1.expression = '< 50'
        pvi1.parse()
        self.assertEqual(
            list(pvi1.products.all()),
            [self.p1])

        pvi1.expression = '<= 50'
        pvi1.parse()
        self.assertEqual(
            list(pvi1.products.all()),
            [self.p1])

        pvi1.expression = '> 50'
        pvi1.parse()
        self.assertEqual(
            list(pvi1.products.all()),
            [])

        pvi1.expression = '>= 50'
        pvi1.parse()
        self.assertEqual(
            list(pvi1.products.all()),
            [])

        pvi1.expression = '''
        > 10
        < 30
        '''
        pvi1.parse()
        self.assertEqual(
            list(pvi1.products.all()),
            [self.p1])

        pvi1.expression = '''
        < 10
        > 30
        '''
        pvi1.parse()
        self.assertEqual(
            list(pvi1.products.all()),
            [])


class PropertyTypeTestCase(TestCase):
    """
    """
    fixtures = ['lfs_shop.xml']

    def setUp(self):
        self.request = RequestFactory().get("/")
        self.request.session = SessionStore()

        # Get status "In stock"
        self.s1 = ProductStatus.objects.get(pk=1)
        # Get status "For order"
        self.s2 = ProductStatus.objects.get(pk=2)
        # Get status "Archive"
        self.s3 = ProductStatus.objects.get(pk=3)

        # A product with properties
        self.p1 = Product.objects.create(
            name=u"Product 1",
            slug=u"product-1",
            sku=u"SKU P1",
            description=u"Description",
            short_description=u"Short description product 1",
            meta_description=u"Meta description product 1",
            meta_keywords=u"Meta keywords product 1",
            price=1.0,
            active=True,
            status=self.s1)

        # Products without properties
        self.p2 = Product.objects.create(
            name=u"Product 2", slug=u"product-2", active=True)
        self.p3 = Product.objects.create(
            name=u"Product 3", slug=u"product-3", active=True)

        # Create property groups
        self.ppg1 = Property.objects.create(
            name='Property group 1',
            identificator='property_group_1',
            is_group=True)
        self.ppg2 = Property.objects.create(
            name='Property group 2',
            identificator='property_group_2',
            is_group=True)

        # Create properties
        self.pp11 = Property.objects.create(
            name='Property 11', identificator='property_11')
        self.pp12 = Property.objects.create(
            name='Property 12', identificator='property_12')
        self.pp13 = Property.objects.create(
            name='Property 13', identificator='property_13')

        self.pp21 = Property.objects.create(
            name='Property 21', identificator='property_21')
        self.pp22 = Property.objects.create(
            name='Property 22', identificator='property_22')
        self.pp23 = Property.objects.create(
            name='Property 23', identificator='property_23')

        self.ppgv1 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.ppg1,
            position=1)
        self.ppv11 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp11,
            value='Value 11',
            position=2)
        self.ppv12 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp12,
            value='Value 12',
            position=3)
        self.ppv13 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp13,
            value='Value 13',
            position=4)
        self.ppgv2 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.ppg2,
            position=5)
        self.ppv21 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp21,
            value='Value 21',
            position=6)
        self.ppv22 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp22,
            value='Value 22',
            position=7)
        self.ppv23 = ProductPropertyValue.objects.create(
            product=self.p1,
            property=self.pp23,
            value='Value 23',
            position=8)

        # Create some categories
        self.c1 = Category.objects.create(
            name="Category 1", slug="category-1")
        self.c11 = Category.objects.create(
            name="Category 11", slug="category-11", parent=self.c1)
        self.c111 = Category.objects.create(
            name="Category 111", slug="category-111", parent=self.c11)
        self.c2 = Category.objects.create(
            name="Category 2", slug="category-2")
        Category.objects.rebuild()

        # Assign products to categories. Note: p2 has two top level categories
        self.c111.products = (self.p1, self.p2)
        self.c2.products = (self.p2, )

    def test_unicode(self):
        """
        """
        pt1 = PropertyType.objects.create(
            property=self.pp11,
            type=PROPERTY_TYPE_FLOAT)
        self.assertEqual(unicode(pt1), 'Property 11')

    def test_is_number(self):
        """
        """
        pt1 = PropertyType.objects.create(
            property=self.pp11,
            type=PROPERTY_TYPE_FLOAT)
        self.assertTrue(pt1.is_number())
        pt2 = PropertyType.objects.create(
            property=self.pp12,
            type=PROPERTY_TYPE_INTEGER)
        self.assertTrue(pt2.is_number())
        pt3 = PropertyType.objects.create(
            property=self.pp13,
            type=PROPERTY_TYPE_CHOICE)
        self.assertFalse(pt3.is_number())

    def test_do_operation(self):
        """
        """
        from mock import patch

        pt1 = PropertyType.objects.create(
            property=self.pp11,
            type=PROPERTY_TYPE_FLOAT)

        self.assertEqual(
            pt1.do_operation('  AAA  ', '@strip'),
            'AAA')
        self.assertEqual(
            pt1.do_operation('  BBB', '@strip'),
            'BBB')
        self.assertEqual(
            pt1.do_operation('CCC   ', '@strip'),
            'CCC')

        self.assertEqual(
            pt1.do_operation('23,56', '@replace:,:.'),
            '23.56')
        self.assertEqual(
            pt1.do_operation('23,568,954', '@replace:,'),
            '23568954')
        self.assertEqual(
            pt1.do_operation('AsBsC', '@replace:s'),
            'ABC')

        self.assertEqual(
            pt1.do_operation('aBcdeF', '@lower'),
            'abcdef')
        self.assertEqual(
            pt1.do_operation('aBcdeF', '@upper'),
            'ABCDEF')

        self.assertEqual(
            pt1.do_operation({
                'value': '  Hi! ',
                'unit': 'blah',
            }, '@strip:value'),
            {
                'value': 'Hi!',
                'unit': 'blah',
            })
        self.assertEqual(
            pt1.do_operation({
                'value': '56,34',
                'unit': 'Mb',
            }, '@replace:value:,:.'),
            {
                'value': '56.34',
                'unit': 'Mb',
            })
        self.assertEqual(
            pt1.do_operation({
                'value': '56,344,578',
                'unit': 'Mb',
            }, '@replace:value:,'),
            {
                'value': '56344578',
                'unit': 'Mb',
            })
        self.assertEqual(
            pt1.do_operation({
                'value': '56,34',
                'unit': 'Mb',
            }, '@lower:unit'),
            {
                'value': '56,34',
                'unit': 'mb',
            })
        self.assertEqual(
            pt1.do_operation({
                'value': '56,34',
                'unit': 'Mb',
            }, '@upper:unit'),
            {
                'value': '56,34',
                'unit': 'MB',
            })

        with patch('lfs.catalog.models.logger') as mock_logger:
            opr = pt1.do_operation({
                'value': '  56,34 ',
                'unit': 'Mb',
            }, '@strip:ttt')
            self.assertEqual(
                opr,
                {
                    'value': '  56,34 ',
                    'unit': 'Mb',
                })
            self.assertIn(
                u'Invalid group \'ttt\'',
                mock_logger.error.call_args[0][0])

        with patch('lfs.catalog.models.logger') as mock_logger:
            opr = pt1.do_operation({
                'value': '56,34',
                'unit': 'Mb',
            }, '@replace:number:,:.')
            self.assertEqual(
                opr,
                {
                    'value': '56,34',
                    'unit': 'Mb',
                })
            self.assertIn(
                u'Invalid group \'number\'',
                mock_logger.error.call_args[0][0])

        with patch('lfs.catalog.models.logger') as mock_logger:
            opr = pt1.do_operation({
                'value': '56,344,876',
                'unit': 'Mb',
            }, '@replace:number:,')
            self.assertEqual(
                opr,
                {
                    'value': '56,344,876',
                    'unit': 'Mb',
                })
            self.assertIn(
                u'Invalid group \'number\'',
                mock_logger.error.call_args[0][0])

        with patch('lfs.catalog.models.logger') as mock_logger:
            opr = pt1.do_operation({
                'value': '56,34',
                'unit': 'Mb',
            }, '@lower:number')
            self.assertEqual(
                opr,
                {
                    'value': '56,34',
                    'unit': 'Mb',
                })
            self.assertIn(
                u'Invalid group \'number\'',
                mock_logger.error.call_args[0][0])

        with patch('lfs.catalog.models.logger') as mock_logger:
            opr = pt1.do_operation({
                'value': '56,34',
                'unit': 'Mb',
            }, '@upper:number')
            self.assertEqual(
                opr,
                {
                    'value': '56,34',
                    'unit': 'Mb',
                })
            self.assertIn(
                u'Invalid group \'number\'',
                mock_logger.error.call_args[0][0])

    def test_validate_regex(self):
        """
        """
        from mock import patch

        pt1 = PropertyType.objects.create(
            property=self.pp11,
            regex='',
            type=PROPERTY_TYPE_STRING)
        self.assertTrue(pt1.validate_regex(self.ppv11)[0])


class MiscTestCase(TestCase):
    """
    """
    def test_get_unique_id_str(self):
        from lfs.catalog.models import get_unique_id_str
        id = get_unique_id_str()
        self.failUnless(isinstance(id, str))
        self.assertEqual(len(id), len("dad27436-3468-4d27-97e4-5fd761db85da"))


class CategoryViewTestCase(TestCase):
    """
    """
    fixtures = ['lfs_shop.xml', 'lfs_user.xml']

    def setUp(self):
        """
        """
        from django.contrib.auth.models import User
        self.factory = RequestFactory()

        self.user = User.objects.get(pk=1)

        # Create a category hierachy
        self.c1 = Category.objects.create(
            name="Category 1", slug="category-1",
            short_description="Short description category 1")
        self.c11 = Category.objects.create(
            name="Category 11",
            slug="category-11", parent=self.c1)
        self.c111 = Category.objects.create(
            name="Category 111",
            slug="category-111", parent=self.c11)
        self.c12 = Category.objects.create(
            name="Category 12",
            slug="category-12", parent=self.c1)
        Category.objects.rebuild()

        # Get status "In stock"
        self.s1 = ProductStatus.objects.get(pk=1)
        # Get status "For order"
        self.s2 = ProductStatus.objects.get(pk=2)
        # Get status "Archive"
        self.s3 = ProductStatus.objects.get(pk=3)

    def generate_products(self):
        self.p111s = []
        for i in xrange(500):
            # A product with properties
            p = Product.objects.create(
                name=u"Product %s" % i,
                slug=u"product-%s" % i,
                sku=u"SKU P%s" % i,
                description=u"Description",
                short_description=u"Short description product %s" % i,
                meta_description=u"Meta description product %s" % i,
                meta_keywords=u"Meta keywords product %s" % i,
                price=1.0,
                active=True,
                status=self.s1)
            self.p111s.append(p)
        self.c111.products = self.p111s

    def test_set_sorting(self):
        """
        """
        from .views import set_sorting
        request = self.factory.get('/set-sorting?sorting=effective_price')
        request.session = SessionStore()

        response = set_sorting(request)
        self.assertEqual(response.status_code, 405)

        request = self.factory.post(
            '/set-sorting',
            {'sorting': 'effective_price'})
        request.session = SessionStore()

        response = set_sorting(request)
        self.assertEqual(response.status_code, 403)

        response = self.client.post(
            '/set-sorting',
            {'sorting': 'effective_price'},
            HTTP_REFERER='/category-1')
        self.assertRedirects(
            response, '/category-1',
            status_code=302, target_status_code=200)

        self.assertEqual(
            self.client.session['sorting'], 'effective_price')

        response = self.client.post(
            '/set-sorting',
            {},
            HTTP_REFERER='/category-1')
        self.assertRedirects(
            response, '/category-1',
            status_code=302, target_status_code=200)

        self.assertNotIn('sorting', self.client.session)

    def test_set_page_mode(self):
        """
        """
        from .views import set_page_mode
        request = self.factory.get(
            '/set-page-mode?mode=table')
        request.session = SessionStore()

        response = set_page_mode(request)
        self.assertEqual(response.status_code, 403)

        request = self.factory.get(
            '/set-page-mode')
        request.session = SessionStore()

        response = set_page_mode(request)
        self.assertEqual(response.status_code, 403)

        request = self.factory.post(
            '/set-page-mode',
            {'mode': 'list'})
        request.session = SessionStore()

        response = set_page_mode(request)
        self.assertEqual(response.status_code, 405)

        response = self.client.get(
            '/set-page-mode?mode=table',
            HTTP_REFERER='/category-1')
        self.assertRedirects(
            response, '/category-1',
            status_code=302, target_status_code=200)

        self.assertEqual(
            self.client.session['page_mode'], 'table')

        response = self.client.post(
            '/set-page-mode',
            HTTP_REFERER='/category-1')
        self.assertEqual(response.status_code, 405)

    def test_category_products(self):
        """
        """
        from .views import category_products

        request = self.factory.get('/category-111')
        request.session = SessionStore()

        result = category_products(request, 'category-111')
        self.assertIn('id="products"', result[0])

        # Fill the category with products
        self.generate_products()
        # Invalidate cache
        category_changed.send(self.c111)

        result = category_products(request, 'category-111')
        self.assertIn('id="products"', result[0])
        self.assertIn('class="products-navigation"', result[0])
