# coding: utf-8
from diagnostic.base import BaseDiagnosticTest
from django.utils.translation import ugettext as _


class StandardWithVariantsTest(BaseDiagnosticTest):
    """
    """
    def get_description(self):
        return _(u'Variants may not have standard product as parent.')

    def how_to_fix(self):
        return _(u'Convert standard products to product with variants.')

    def _get_items_with_problems(self):
        from .models import Product
        from .settings import VARIANT, STANDARD_PRODUCT
        all_variants = Product.objects.filter(
            sub_type=VARIANT, parent__sub_type=STANDARD_PRODUCT)
        v_ids = all_variants.values_list('id', flat=True)
        if v_ids:
            parents = Product.objects.filter(variants__id__in=v_ids).distinct()
            return parents
        else:
            return Product.objects.none()

    def get_report(self):
        _items = self._get_items_with_problems()
        _report = '<ul>'
        for i in _items:
            _report += '<li><a href="%s" target="_blank">%s</a></li>' % (
                '/superadmin/catalog/product/%d/' % i.pk, i.get_name())
        _report += '</ul>'
        return _report

    def check(self):
        _items = self._get_items_with_problems()
        if _items:
            p_count = _items.count()
            return (
                False,
                _(u'There are %d STANDARD_PRODUCTs with variants.') % p_count)
        else:
            return (True, _(u'OK'))

    def fix(self, solution=0):
        from .settings import PRODUCT_WITH_VARIANTS
        _items = self._get_items_with_problems()
        if _items:
            for p in _items:
                p.sub_type = PRODUCT_WITH_VARIANTS
                p.save()
            return (True, _(u'Fixed'))

        return (False, _(u'Failed'))


class VariantWithVariantsTest(BaseDiagnosticTest):
    """
    """
    def get_description(self):
        return _(u'Variants may not have variant as parent.')

    def has_multiple_solutions(self):
        return True

    def how_to_fix(self):
        return [
            _(u'Convert variants for variant to standard products and clear parent attribute.'),
            _(u'Convert parent variant to product with variants.'),
        ]

    def _get_items_with_problems(self):
        from .models import Product
        from .settings import VARIANT
        all_variants = Product.objects.filter(
            sub_type=VARIANT, parent__sub_type=VARIANT)
        v_ids = all_variants.values_list('id', flat=True)
        if v_ids:
            parents = Product.objects.filter(variants__id__in=v_ids).distinct()
            return parents
        else:
            return Product.objects.none()

    def check(self):
        _items = self._get_items_with_problems()
        if _items:
            p_count = _items.count()
            return (
                False,
                _(u'There are %d VARIANTs with variants.') % p_count)
        else:
            return (True, _(u'OK'))

    def get_report(self):
        _items = self._get_items_with_problems()
        _report = '<ul>'
        for i in _items:
            _report += '<li><pre><p>%s:</p><a href="%s" target="_blank">%s</a><p>%s:</p>' % (
                _(u'Variant product with variants'),
                '/superadmin/catalog/product/%d/' % i.pk,
                i.get_name(), _(u'His variants'))
            _report += '<ul>'
            for v in i.variants.all():
                _report += '<li><a href="%s" target="_blank">%s</a></li>' % (
                    '/superadmin/catalog/product/%d/'
                    % v.pk, v.get_name())
            _report += '</ul></pre></li>'
        _report += '</ul>'
        return _report

    def fix(self, solution=0):
        from .settings import STANDARD_PRODUCT, PRODUCT_WITH_VARIANTS
        _items = self._get_items_with_problems()
        if _items:
            if solution == 0:
                for p in _items:
                    variants = p.variants.all()
                    for v in variants:
                        v.sub_type = STANDARD_PRODUCT
                        v.parent = None
                        v.save()

                    if p.parent is None:
                        p.sub_type = STANDARD_PRODUCT
                        p.save()
                return (True, _(u'Fixed'))
            elif solution == 1:
                for p in _items:
                    p.sub_type = PRODUCT_WITH_VARIANTS
                    p.save()
                return (True, _(u'Fixed'))

        return (False, _(u'Failed'))


class NotActiveParentButActiveChildrenTest(BaseDiagnosticTest):
    """
    """
    def get_description(self):
        return _(u'Variants could be not active when parent is not active.')

    def has_multiple_solutions(self):
        return True

    def how_to_fix(self):
        return [
            _(u'If parent product is inactive than we should make all variants be inactive too.'),
            _(u'If product has active variants than it should be active too.'),
        ]

    def _get_items_with_problems(self):
        from .models import Product
        from .settings import VARIANT
        all_variants = Product.objects.filter(
            sub_type=VARIANT, active=True, parent__active=False)
        v_ids = all_variants.values_list('id', flat=True)
        if v_ids:
            parents = Product.objects.filter(variants__id__in=v_ids).distinct()
            return parents
        else:
            return Product.objects.none()

    def check(self):
        _items = self._get_items_with_problems()
        if _items:
            p_count = _items.count()
            return (
                False,
                _(u'There are %d inactive parent products with active variants.') % p_count)
        else:
            return (True, _(u'OK'))

    def get_report(self):
        _items = self._get_items_with_problems()
        _report = '<ul>'
        for i in _items:
            _report += '<li><a href="%s" target="_blank">%s</a></li>' % (
                '/superadmin/catalog/product/%d/' % i.pk, i.get_name())
        _report += '</ul>'
        return _report

    def fix(self, solution=0):
        _items = self._get_items_with_problems()
        if _items:
            if solution == 0:
                for p in _items:
                    variants = p.variants.all()
                    for v in variants:
                        v.active = False
                        v.save()
                    p.active = False
                    p.save()
                return (True, _(u'Fixed'))
            elif solution == 1:
                for p in _items:
                    p.active = True
                    p.save()
                return (True, _(u'Fixed'))

        return (False, _(u'Failed'))


class PropertiesWithBlankNamesTest(BaseDiagnosticTest):
    """
    """
    def get_description(self):
        return _(u'Product properties may not have the strange names.')

    def how_to_fix(self):
        return _(u'Delete properties with strange names')

    def _get_items_with_problems(self):
        from django.db.models import Q
        from .models import Property
        _ids_1 = list(Property.objects.exclude(
            name__iregex=r"[a-zа-я]+").values_list('id', flat=True))
        _ids_2 = list(Property.objects.filter(
            name__iregex=r"^[a-zа-я ]*[\[\]\\\/\^\$\.\|\?\*\+\(\)\{\}!<>%,&=~@#№:_]{2,}[a-zа-я ]*$").values_list('id', flat=True))
        _ids = list(set(_ids_1 + _ids_2))
        _items = Property.objects.filter(id__in=_ids)
        return _items

    def check(self):
        _items = self._get_items_with_problems()
        if _items:
            p_count = _items.count()
            return (
                False,
                _(u'There are %d properties with strange names.') % p_count)
        else:
            return (True, _(u'OK'))

    def get_report(self):
        _items = self._get_items_with_problems()
        _report = '<ul>'
        for i in _items:
            _report += '<li><pre><a href="%s" target="_blank">%s</a></pre></li>' % (
                '/superadmin/catalog/property/%d/' % i.pk, i.name)
        _report += '</ul>'
        return _report

    def fix(self, solution=0):
        _items = self._get_items_with_problems()
        if _items:
            for p in _items:
                p.delete()
            return (True, _(u'Fixed'))

        return (False, _(u'Failed'))

class HasParentTest(BaseDiagnosticTest):
    """
    """
    def get_description(self):
        return _(u'Product-variant must has parent')

    def how_to_fix(self):
        return _(u'Delete product-variant without parent')

    def _get_items_with_problems(self):
        from .models import Product
        _items = Product.objects.filter(sub_type='2', parent=None)
        return _items

    def check(self):
        _items = self._get_items_with_problems()
        if _items:
            p_count = _items.count()
            return (
                False,
                _(u'There are %d product-variant without parent.') % p_count)
        else:
            return (True, _(u'OK'))

    def get_report(self):
        _items = self._get_items_with_problems()
        _report = '<ul>'
        for i in _items:
            _report += '<li><pre><a href="%s" target="_blank">%s</a></pre></li>' % (
                '/superadmin/catalog/product/%d/' % i.pk, i.name)
        _report += '</ul>'
        return _report

    def fix(self, solution=0):
        _items = self._get_items_with_problems()
        if _items:
            for p in _items:
                p.delete()
            return (True, _(u'Fixed'))

        return (False, _(u'Failed'))

class StatusForParentsTest(BaseDiagnosticTest):
    """
    """
    def get_description(self):
        return _(u'Status for product with variants must be the maximum status from the its variants.')

    def how_to_fix(self):
        return _(u'Set status as maximum of the children.')

    def _get_items_with_problems(self):
        from django.db import connection
        cursor = connection.cursor()
        cursor.execute("""
        SELECT
            variant.id,
            variant.parent_id,
            variant_status.order,
            parent_status.order
        FROM
            catalog_product AS variant
        JOIN catalog_product AS parent ON variant.parent_id = parent.id
        INNER JOIN catalog_productstatus AS variant_status ON variant.status_id = variant_status.id
        INNER JOIN catalog_productstatus AS parent_status ON parent.status_id = parent_status.id
        WHERE
            variant.sub_type='2' AND
            variant.active=TRUE AND
            parent.active=TRUE AND
            variant_status.is_visible=TRUE AND
            parent_status.is_visible=TRUE AND
            (variant_status.order < parent_status.order)
        ORDER BY variant.parent_id ASC;
        """)
        return cursor.fetchall()

    def check(self):
        _items = self._get_items_with_problems()
        if _items:
            p_count = len(_items)
            return (
                False,
                _(u'There are %d variants with incorrect parent status.') % p_count)
        else:
            return (True, _(u'OK'))

    def get_report(self):
        from .models import Product
        _items = self._get_items_with_problems()
        _ids = [i[0] for i in _items]
        products = Product.objects.filter(id__in=_ids)

        _report = '<ul>'
        for p in products:
            _report += _(u'<li>Variant <a href="{0}" target="_blank">{1}</a> has status "{2}" but parent <a href="{3}" target="_blank">{4}</a> has status "{5}"</li>').format(
                '/superadmin/catalog/product/%d/' % p.pk,
                p.get_name(),
                p.status.name,
                '/superadmin/catalog/product/%d/' % p.parent_id,
                p.parent.get_name(),
                p.parent.status.name)
        _report += '</ul>'
        return _report

    def fix(self, solution=0):
        from .models import Product
        _items = self._get_items_with_problems()
        _ids = list(set([i[1] for i in _items]))
        if _ids:
            print(_ids)
            products = Product.objects.filter(id__in=_ids)
            for p in products:
                p.save()
            return (True, _(u'Fixed'))

        return (False, _(u'Failed'))


class ZeroPriceTest(BaseDiagnosticTest):
    """
    """
    def get_description(self):
        return _(u'Shop may not has active products with zero price.')

    def how_to_fix(self):
        return _(u'Make products with zero price inactive.')

    def _get_items_with_problems(self):
        from .models import Product
        from .settings import VARIANT, STANDARD_PRODUCT
        products = Product.objects.filter(
            sub_type__in=[VARIANT, STANDARD_PRODUCT],
            active=True,
            status__is_visible=True,
            effective_price=0)
        return products

    def check(self):
        _items = self._get_items_with_problems()
        if _items:
            p_count = _items.count()
            return (
                False,
                _(u'There are %d active products with zero price.') % p_count)
        else:
            return (True, _(u'OK'))

    def get_report(self):
        _items = self._get_items_with_problems()

        _report = '<ul>'
        for p in _items:
            _report += u'<li><a href="{0}" target="_blank">{1}</a></li>'.format(
                '/superadmin/catalog/product/%d/' % p.pk,
                p.get_name(),
            )
        _report += '</ul>'
        return _report

    def fix(self, solution=0):
        _items = self._get_items_with_problems()
        if _items:
            for p in _items:
                p.active = False
                p.save()
            return (True, _(u'Fixed'))

        return (False, _(u'Failed'))


class ProductWithoutStatusTest(BaseDiagnosticTest):
    """
    """
    def get_description(self):
        return _(u'Product must has status.')

    def how_to_fix(self):
        return _(u'Set the minimal available status for that profucts.')

    def _get_items_with_problems(self):
        from .models import Product
        products = Product.objects.filter(
            active=True,
            status=None)
        return products

    def check(self):
        _items = self._get_items_with_problems()
        if _items:
            p_count = _items.count()
            return (
                False,
                _(u'There are %d active products without status.') % p_count)
        else:
            return (True, _(u'OK'))

    def get_report(self):
        _items = self._get_items_with_problems()

        _report = '<ul>'
        for p in _items:
            _report += u'<li><a href="{0}" target="_blank">{1}</a></li>'.format(
                '/superadmin/catalog/product/%d/' % p.pk,
                p.get_name(),
            )
        _report += '</ul>'
        return _report

    def fix(self, solution=0):
        from .models import ProductStatus
        _items = self._get_items_with_problems()
        if _items:
            status = ProductStatus.objects.order_by('-order')[0]
            for p in _items:
                p.status = status
                p.save()
            return (True, _(u'Fixed'))

        return (False, _(u'Failed'))


class WrongProductPropertiesTest(BaseDiagnosticTest):
    """
    """
    def get_description(self):
        return _(u'Check for propuct to non-existent property values')

    def _get_items_with_problems(self):
        from lfs.catalog.models import ProductPropertyValue, PropertyType
        from .settings import PROPERTY_TYPE_CHOICE
        bad_products = []
        for p_type in PropertyType.objects.filter(type=PROPERTY_TYPE_CHOICE):
            values = [
                pt['property_option__name']
                for pt in p_type.propertyoptionrelation_set.all()
                .values('property_option__name')]
            products = [
                ppv
                for ppv in ProductPropertyValue.objects.filter(
                    property__identificator=p_type.property.identificator)
                .exclude(value__in=values).values(
                    'product__id', 'product__name')]
            bad_products.extend(products)
        bad_products = {
            product['product__id']:
            product for product in bad_products}.values()
        return bad_products

    def check(self):
        _items = self._get_items_with_problems()
        if _items:
            p_count = len(_items)
            return (
                False,
                _(u'There are %d products with non-existent property values.')
                % p_count)
        else:
            return (True, _(u'OK'))

    def get_report(self):
        _items = self._get_items_with_problems()
        _report = '<ul>'
        for i in _items:
            _report += u'<li><a href="{0}" target="_blank">{1}</a></li>'\
                .format(
                    '/superadmin/catalog/product/%d/' % i['product__id'],
                    i['product__name'],
                )
        _report += '</ul>'
        return _report
