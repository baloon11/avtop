# coding: utf-8
import re
from django.utils.translation import ugettext as _


def atof(text):
    try:
        return float(text)
    except ValueError:
        return text


def natural_keys(obj):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [
        atof(c) for c in re.split('([\d.]+)', obj.property_option.name.replace(',', '.'))]


class BasePropertyOptionSorter(object):
    """Basic class for implement the sorting method for filter options.
    """
    name = ''

    def sort(self, queryset):
        raise NotImplementedError


class AscPropertyOptionSorter(BasePropertyOptionSorter):
    """Sorting from A to Z
    """
    name = _(u'ASC')

    def sort(self, queryset):
        i = 0
        options = queryset.order_by('property_option__name')
        for o in options.iterator():
            o.position = i
            o.save()
            i += 1


class DescPropertyOptionSorter(BasePropertyOptionSorter):
    """Sorting from Z to A
    """
    name = _(u'DESC')

    def sort(self, queryset):
        i = 0
        options = queryset.order_by('-property_option__name')
        for o in options.iterator():
            o.position = i
            o.save()
            i += 1


class NumberAZPropertyOptionSorter(BasePropertyOptionSorter):
    """Sorting from 0 to 999
    """
    name = _(u'Number ASC')

    def sort(self, queryset):
        ordered = sorted(queryset, key=natural_keys)
        i = 0
        for o in ordered:
            o.position = i
            o.save()
            i += 1


class NumberZAPropertyOptionSorter(BasePropertyOptionSorter):
    """Sorting from 999 to 0
    """
    name = _(u'Number DESC')

    def sort(self, queryset):
        ordered = sorted(queryset, key=natural_keys, reverse=True)
        i = 0
        for o in ordered:
            o.position = i
            o.save()
            i += 1
