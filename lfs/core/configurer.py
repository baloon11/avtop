# coding: utf-8
import pytz

from django import forms
from django.conf import settings
from django.utils.translation import ugettext as _
from codemirror.widgets import CodeMirrorTextarea
from django.forms.widgets import Textarea

from adminconfig.utils import BaseConfig

# General options


class GeneralShopConfigForm(forms.Form):
    """This form describes the configuration fields
    for general options.

    project_identifier
        Unique identifier of current project at Sitepanel.

    project_secret_code
        Sitepanel API's secret code for current project.

    language_code
        Default language code.

    time_zone
        Default project's time zone.

    theme
        Name of the module which contains the visual theme for current project.

    task_manager
        Active task manager for manage background tasks.

    flush_job_count
        Default count of jobs in queue which managing for one flush
        queue launch.

    autocancel_timeout
        Time in minutes for autocancel "freezed" jobs.
    """
    TIMEZONES = ((x, x) for x in pytz.all_timezones)
    THEMES = getattr(settings, 'AVAILABLE_SHOP_THEMES', (None, u'Basic theme'))

    project_identifier = forms.CharField(label=_(u'Project identifier'))
    project_secret_code = forms.CharField(label=_(u'Secret code'))

    language_code = forms.ChoiceField(
        label=_(u'Default language'), choices=settings.LANGUAGES)
    time_zone = forms.ChoiceField(
        label=_(u'Default timezone'), choices=TIMEZONES)

    theme = forms.ChoiceField(label=_(u'Theme'), choices=THEMES)
    task_manager = forms.ChoiceField(
        label=_(u'Background task manager'), choices=settings.TASK_MANAGERS)
    flush_job_count = forms.IntegerField(
        label=_(u'Default jobs per flush queue'))
    autocancel_timeout = forms.IntegerField(
        label=_(u'Time in minutes for cancel jobs with "running" status.'))
    cart_block_discount = forms.BooleanField(
        label=_(u'Calculate total price with discounts in cart block'),
        required=False
    )
    url_case_insensitive = forms.BooleanField(
        label=_(u'Case-insensitive in the urls'),
        help_text=_(u'do a 301 redirect to the URL in lower case'),
        required=False
    )
    reply_to_customer = forms.BooleanField(
        label=_(u'Reply to customer email'),
        required=False
    )

    def clean_theme(self):
        """Validate the possibility to install the theme module.
        """
        theme = self.cleaned_data['theme']
        try:
            __import__(theme)
            return theme
        except ImportError:
            raise forms.ValidationError(
                _(u'Theme "%(theme)s" does not exist.') % {
                    'theme': theme,
                })


class GeneralShopConfig(BaseConfig):
    """Configurator for general options in config.
    """
    form_class = GeneralShopConfigForm
    block_name = 'general'

    def __init__(self):
        super(GeneralShopConfig, self).__init__()

        self.default_data = {
            'LANGUAGE_CODE': settings.LANGUAGE_CODE,
            'TIME_ZONE': settings.TIME_ZONE,
            'SHOP_THEME': settings.SHOP_THEME,
            'PROJECT_IDENTIFIER': settings.PROJECT_IDENTIFIER,
            'PROJECT_SECRET_CODE': settings.PROJECT_SECRET_CODE,
            'ACTIVE_TASK_MANAGER': settings.ACTIVE_TASK_MANAGER,
            'DEFAULT_FLUSH_JOB_COUNT': settings.DEFAULT_FLUSH_JOB_COUNT,
            'AUTO_CANCEL_TASK_TIMEOUT': settings.AUTO_CANCEL_TASK_TIMEOUT,
            'CART_BLOCK_DISCOUNT': False,
            'URL_CASE_INSENSITIVE': False,
            'EXCEPTIONS_FOR_URL_CASE_INSENSITIVE': u'',
            'REPLY_TO_CUSTOMER': False,
        }

        self.option_translation_table = (
            ('LANGUAGE_CODE', 'language_code'),
            ('TIME_ZONE', 'time_zone'),
            ('SHOP_THEME', 'theme'),
            ('PROJECT_IDENTIFIER', 'project_identifier'),
            ('PROJECT_SECRET_CODE', 'project_secret_code'),
            ('ACTIVE_TASK_MANAGER', 'task_manager'),
            ('DEFAULT_FLUSH_JOB_COUNT', 'flush_job_count'),
            ('AUTO_CANCEL_TASK_TIMEOUT', 'autocancel_timeout'),
            ('CART_BLOCK_DISCOUNT', 'cart_block_discount'),
            ('URL_CASE_INSENSITIVE', 'url_case_insensitive'),
            (
                'EXCEPTIONS_FOR_URL_CASE_INSENSITIVE',
                'url_case_insensitive_exception'
            ),
            ('REPLY_TO_CUSTOMER', 'reply_to_customer'),
        )

# SEO options


class SeoConfigForm(forms.Form):
    # use_django_templates = forms.BooleanField(
    #     label=_(u'Use Django templates'), required=False)
    show_seo_text_on_filtered_pages = forms.BooleanField(
        label=_(u'Show seo text on filtered pages'),
        required=False
    )
    category_meta_h1 = forms.CharField(
        label=_(u'Category META H1'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    product_meta_h1 = forms.CharField(
        label=_(u'Product META H1'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    shop_meta_title = forms.CharField(
        label=_(u'Shop META title'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    category_meta_title = forms.CharField(
        label=_(u'Category META title'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    product_meta_title = forms.CharField(
        label=_(u'Product META title'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    page_meta_title = forms.CharField(
        label=_(u'Page META title'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    other_meta_title = forms.CharField(
        label=_(u'Other META title'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)

    shop_meta_keywords = forms.CharField(
        label=_(u'Shop META keywords'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    category_meta_keywords = forms.CharField(
        label=_(u'Category META keywords'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    product_meta_keywords = forms.CharField(
        label=_(u'Product META keywords'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    page_meta_keywords = forms.CharField(
        label=_(u'Page META keywords'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    other_meta_keywords = forms.CharField(
        label=_(u'Other META keywords'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)

    shop_meta_description = forms.CharField(
        label=_(u'Shop META description'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    category_meta_description = forms.CharField(
        label=_(u'Category META description'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    product_meta_description = forms.CharField(
        label=_(u'Product META description'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    page_meta_description = forms.CharField(
        label=_(u'Page META description'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    other_meta_description = forms.CharField(
        label=_(u'Other META description'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)

    shop_meta_text = forms.CharField(
        label=_(u'Shop SEO text'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    category_meta_text = forms.CharField(
        label=_(u'Category SEO text'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    product_meta_text = forms.CharField(
        label=_(u'Product SEO text'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    page_meta_text = forms.CharField(
        label=_(u'Page SEO text'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)
    other_meta_text = forms.CharField(
        label=_(u'Other SEO text'),
        widget=CodeMirrorTextarea(
            mode='xml',
            config={'fixedGutter': True, 'lineWrapping': True}),
        required=False)


class SeoConfig(BaseConfig):
    """Configurator for general options in config.
    """
    form_class = SeoConfigForm
    block_name = 'seo'

    def __init__(self):
        super(SeoConfig, self).__init__()

        self.default_data = {
            # 'SEO_USE_DJANGO_TEMPLATES': getattr(
            #     settings, 'SEO_USE_DJANGO_TEMPLATES', False),
            'SHOW_SEO_TEXT_ON_FILTERED_PAGES': getattr(
                settings, 'SHOW_SEO_TEXT_ON_FILTERED_PAGES'),
            'DEFAULT_CATEGORY_META_H1_TEMPLATE': getattr(
                settings, 'DEFAULT_CATEGORY_META_H1_TEMPLATE', ''),
            'DEFAULT_PRODUCT_META_H1_TEMPLATE': getattr(
                settings, 'DEFAULT_PRODUCT_META_H1_TEMPLATE', ''),
            'DEFAULT_SHOP_META_TITLE_TEMPLATE': getattr(
                settings, 'DEFAULT_SHOP_META_TITLE_TEMPLATE', ''),
            'DEFAULT_CATEGORY_META_TITLE_TEMPLATE': getattr(
                settings, 'DEFAULT_CATEGORY_META_TITLE_TEMPLATE', ''),
            'DEFAULT_PRODUCT_META_TITLE_TEMPLATE': getattr(
                settings, 'DEFAULT_PRODUCT_META_TITLE_TEMPLATE', ''),
            'DEFAULT_PAGE_META_TITLE_TEMPLATE': getattr(
                settings, 'DEFAULT_PAGE_META_TITLE_TEMPLATE', ''),
            'DEFAULT_OTHER_META_TITLE_TEMPLATE': getattr(
                settings, 'DEFAULT_OTHER_META_TITLE_TEMPLATE', ''),

            'DEFAULT_SHOP_META_KEYWORDS_TEMPLATE': getattr(
                settings, 'DEFAULT_SHOP_META_KEYWORDS_TEMPLATE', ''),
            'DEFAULT_CATEGORY_META_KEYWORDS_TEMPLATE': getattr(
                settings, 'DEFAULT_CATEGORY_META_KEYWORDS_TEMPLATE', ''),
            'DEFAULT_PRODUCT_META_KEYWORDS_TEMPLATE': getattr(
                settings, 'DEFAULT_PRODUCT_META_KEYWORDS_TEMPLATE', ''),
            'DEFAULT_PAGE_META_KEYWORDS_TEMPLATE': getattr(
                settings, 'DEFAULT_PAGE_META_KEYWORDS_TEMPLATE', ''),
            'DEFAULT_OTHER_META_KEYWORDS_TEMPLATE': getattr(
                settings, 'DEFAULT_OTHER_META_KEYWORDS_TEMPLATE', ''),

            'DEFAULT_SHOP_META_DESCRIPTION_TEMPLATE': getattr(
                settings, 'DEFAULT_SHOP_META_DESCRIPTION_TEMPLATE', ''),
            'DEFAULT_CATEGORY_META_DESCRIPTION_TEMPLATE': getattr(
                settings, 'DEFAULT_CATEGORY_META_DESCRIPTION_TEMPLATE', ''),
            'DEFAULT_PRODUCT_META_DESCRIPTION_TEMPLATE': getattr(
                settings, 'DEFAULT_PRODUCT_META_DESCRIPTION_TEMPLATE', ''),
            'DEFAULT_PAGE_META_DESCRIPTION_TEMPLATE': getattr(
                settings, 'DEFAULT_PAGE_META_DESCRIPTION_TEMPLATE', ''),
            'DEFAULT_OTHER_META_DESCRIPTION_TEMPLATE': getattr(
                settings, 'DEFAULT_OTHER_META_DESCRIPTION_TEMPLATE', ''),

            'DEFAULT_SHOP_META_TEXT_TEMPLATE': getattr(
                settings, 'DEFAULT_SHOP_META_TEXT_TEMPLATE', ''),
            'DEFAULT_CATEGORY_META_TEXT_TEMPLATE': getattr(
                settings, 'DEFAULT_CATEGORY_META_TEXT_TEMPLATE', ''),
            'DEFAULT_PRODUCT_META_TEXT_TEMPLATE': getattr(
                settings, 'DEFAULT_PRODUCT_META_TEXT_TEMPLATE', ''),
            'DEFAULT_PAGE_META_TEXT_TEMPLATE': getattr(
                settings, 'DEFAULT_PAGE_META_TEXT_TEMPLATE', ''),
            'DEFAULT_OTHER_META_TEXT_TEMPLATE': getattr(
                settings, 'DEFAULT_OTHER_META_TEXT_TEMPLATE', ''),
        }

        self.option_translation_table = (
            # ('SEO_USE_DJANGO_TEMPLATES', 'use_django_templates'),
            (
                'SHOW_SEO_TEXT_ON_FILTERED_PAGES',
                'show_seo_text_on_filtered_pages'
            ),
            ('DEFAULT_CATEGORY_META_H1_TEMPLATE', 'category_meta_h1'),
            ('DEFAULT_PRODUCT_META_H1_TEMPLATE', 'product_meta_h1'),
            ('DEFAULT_SHOP_META_TITLE_TEMPLATE', 'shop_meta_title'),
            ('DEFAULT_CATEGORY_META_TITLE_TEMPLATE', 'category_meta_title'),
            ('DEFAULT_PRODUCT_META_TITLE_TEMPLATE', 'product_meta_title'),
            ('DEFAULT_PAGE_META_TITLE_TEMPLATE', 'page_meta_title'),
            ('DEFAULT_OTHER_META_TITLE_TEMPLATE', 'other_meta_title'),

            ('DEFAULT_SHOP_META_KEYWORDS_TEMPLATE', 'shop_meta_keywords'),
            (
                'DEFAULT_CATEGORY_META_KEYWORDS_TEMPLATE',
                'category_meta_keywords'),
            (
                'DEFAULT_PRODUCT_META_KEYWORDS_TEMPLATE',
                'product_meta_keywords'),
            ('DEFAULT_PAGE_META_KEYWORDS_TEMPLATE', 'page_meta_keywords'),
            ('DEFAULT_OTHER_META_KEYWORDS_TEMPLATE', 'other_meta_keywords'),

            (
                'DEFAULT_SHOP_META_DESCRIPTION_TEMPLATE',
                'shop_meta_description'),
            (
                'DEFAULT_CATEGORY_META_DESCRIPTION_TEMPLATE',
                'category_meta_description'),
            (
                'DEFAULT_PRODUCT_META_DESCRIPTION_TEMPLATE',
                'product_meta_description'),
            (
                'DEFAULT_PAGE_META_DESCRIPTION_TEMPLATE',
                'page_meta_description'),
            (
                'DEFAULT_OTHER_META_DESCRIPTION_TEMPLATE',
                'other_meta_description'),

            ('DEFAULT_SHOP_META_TEXT_TEMPLATE', 'shop_meta_text'),
            ('DEFAULT_CATEGORY_META_TEXT_TEMPLATE', 'category_meta_text'),
            ('DEFAULT_PRODUCT_META_TEXT_TEMPLATE', 'product_meta_text'),
            ('DEFAULT_PAGE_META_TEXT_TEMPLATE', 'page_meta_text'),
            ('DEFAULT_OTHER_META_TEXT_TEMPLATE', 'other_meta_text'),
        )


class CompatibilityConfigForm(forms.Form):
    use_topseller_and_featured = forms.BooleanField(
        label=_(u'Use topseller and featured lists'), required=False)
    use_parent_status = forms.BooleanField(
        label=_(u'Use parent product status'), required=False)
    cheapest_price_depends_sorting = forms.BooleanField(
        label=_(u'Cheapest price depends sorting'), required=False)


class CompatibilityConfig(BaseConfig):
    """Configurator for compatibility options in config.
    """
    form_class = CompatibilityConfigForm
    block_name = 'compatibility'

    def __init__(self):
        super(CompatibilityConfig, self).__init__()

        self.default_data = {
            'COMPATIBILITY_USE_TOPSELLER_AND_FEATURED': False,
            'COMPATIBILITY_USE_PARENT_STATUS': True,
            'COMPATIBILITY_CHEAPEST_PRICE_DEPENDS_SORTING': True,
        }

        self.option_translation_table = (
            (
                'COMPATIBILITY_USE_TOPSELLER_AND_FEATURED',
                'use_topseller_and_featured'),
            (
                'COMPATIBILITY_USE_PARENT_STATUS',
                'use_parent_status'),
            (
                'COMPATIBILITY_CHEAPEST_PRICE_DEPENDS_SORTING',
                'cheapest_price_depends_sorting'),
        )


class PaginationConfigForm(forms.Form):
    template = forms.CharField(
        label=_(u'Name of pagination'),
        required=False,
        help_text=_(
            u'You can use next variables: start_number, end_number, page_number'
        )
    )


class PaginationConfig(BaseConfig):
    """Configurator for pagination options in config.
    """
    form_class = PaginationConfigForm
    block_name = 'pagination'

    def __init__(self):
        super(PaginationConfig, self).__init__()

        self.default_data = {
            'PAGINATION_PAGE_NAME_TEMPLATE': 'page_number',
        }

        self.option_translation_table = (
            (
                'PAGINATION_PAGE_NAME_TEMPLATE', 'template'),
        )
