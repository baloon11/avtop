# coding: utf-8
from django.conf import settings
from django.core.cache import cache
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes import generic
from django.core.validators import MaxValueValidator, MinValueValidator

from mptt.models import MPTTModel, TreeForeignKey

from .fields.thumbs import ImageWithThumbsField
from .settings import (
    ACTION_TYPE_CHOICES,
    MENU_TYPE_CHOICES,
    POSITION_CHOICES,
)
from ..fields.models.fields_objects import FieldsObjects

from sshop_currencies.models import Currency

from south.modelsinspector import add_introspection_rules

add_introspection_rules(
    [], ["^lfs\.core\.fields\.thumbs\.ImageWithThumbsField"])


class ActionGroup(models.Model):
    """Actions of a action group can be displayed on several parts of the web
    page.

    **Attributes**:

    name
        The name of the group.
    """
    name = models.CharField(
        _(u"Name"), blank=True, max_length=100, unique=True)
    type = models.IntegerField(
        _(u'Render type'), default=1, choices=ACTION_TYPE_CHOICES)

    class Meta:
        ordering = ("name", )
        verbose_name = _(u'Action group')
        verbose_name_plural = _(u'Action groups')

    def __unicode__(self):
        return self.name

    def get_actions(self):
        """Returns the actions of this group.
        """
        return self.actions.filter(active=True)


class Action(MPTTModel):
    """A action is a link which belongs to a action groups.

    **Attributes**:

    group
        The belonging group.

    title
        The title of the menu tab.

    link
        The link to the object.

    active
        If true the tab is displayed.

    position
        the position of the tab within the menu.

    parent
        Parent tab to create a tree.
    """
    active = models.BooleanField(_(u"Active"), default=False)
    title = models.CharField(_(u"Title"), max_length=40)
    link = models.CharField(_(u"Link"), blank=True, max_length=100)
    group = models.ForeignKey(
        ActionGroup, verbose_name=_(u"Group"), related_name="actions")
    parent = TreeForeignKey(
        "self", verbose_name=_(u"Parent"),
        blank=True, null=True, related_name='children')

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _(u'Action')
        verbose_name_plural = _(u'Actions')


class Shop(models.Model):
    """Holds all shop related information.

    At the moment there must be exactly one shop with id == 1. (This may be
    changed in future to provide multi shops.)

    Instance variables:

    - name
       The name of the shop. This is used for the the title of the HTML pages

    - shop_owner
       The shop owner. This is displayed within several places for instance the
       checkout page

    - from_email
       This e-mail address is used for the from header of all outgoing e-mails

    - notification_emails
       This e-mail addresses are used for incoming notification e-mails, e.g.
       received an order. One e-mail address per line.

    - send_email_as
        A type of sending emails: text, html or html with images

    - description
       A description of the shop

    - image
      An image which can be used as default image if a category doesn't
      have one

    - product_cols, product_rows, category_cols
       Upmost format information, which defines how products and categories are
       displayed within several views. These may be inherited by categories and
       sub categories.

    - google_analytics_id
       Used to generate google analytics tracker code and e-commerce code. the
       id has the format UA-xxxxxxx-xx and is provided by Google.

    - ga_site_tracking
       If selected and the google_analytics_id is given google analytics site
       tracking code is inserted into the HTML source code.

    - ga_ecommerce_tracking
       If selected and the google_analytics_id is given google analytics
       e-commerce tracking code is inserted into the HTML source code.

    - countries
       Selected countries will be offered to the shop customer tho choose for
       shipping and invoice address.

    - default_country
       This country will be used to calculate shipping price if the shop
       customer doesn't have select a country yet.

    - price_calculator
        Class that implements lfs.price.PriceCalculator for calculating product
        price. This is the default price calculator for all products.

    - checkout_type
       Decides whether the customer has to login, has not to login or has the
       choice to to login or not to be able to check out.

    - confirm_toc
       If this is activated the shop customer has to confirm terms and
       conditions to checkout.

    - default_currency
        This is a default currency will be used for making price badges.

    - menu_type
        Type of main menu

    - prerendered_menu
        HTML code of the main menu

    - featured_badge
        Badge for featured products

    - topseller_badge
        Badge for topseller products

    - watermark_image

    - watermark_opacity

    - watermark_position

    """
    name = models.CharField(_(u"Name"), max_length=30)
    shop_owner = models.CharField(_(u"Shop owner"), max_length=100, blank=True)
    from_email = models.EmailField(_(u"From e-mail address"))
    notification_emails = models.TextField(_(u"Notification email addresses"))

    description = models.TextField(_(u"Description"), blank=True)
    image = models.ImageField(
        _(u"Logo"), upload_to="logos",
        blank=True, null=True)

    # Google Analytics
    google_analytics_id = models.CharField(
        _(u"Google Analytics ID"), blank=True, max_length=20)
    ga_site_tracking = models.BooleanField(
        _(u"Google Analytics Site Tracking"), default=False)
    ga_ecommerce_tracking = models.BooleanField(
        _(u"Google Analytics E-Commerce Tracking"), default=False)

    # TODO: redesign it
    menu_type = models.IntegerField(
        _(u'Menu type'), default=1, choices=MENU_TYPE_CHOICES)
    max_menu_item_count = models.IntegerField(
        _(u'Max item count'), default=5, help_text=_(u'For simple menu only'))
    prerendered_menu = models.TextField(_(u'Prerendered menu'), blank=True)

    # Badges
    featured_badge = ImageWithThumbsField(
        _(u"Badge for featured products"),
        upload_to="images", blank=True, null=True)
    topseller_badge = ImageWithThumbsField(
        _(u"Badge for topseller products"),
        upload_to="images", blank=True, null=True)

    # Search configs
    # TODO: move it to config
    default_search = models.CharField(
        choices=settings.SEARCH_CONFIGS, max_length=100,
        default=settings.SEARCH_CONFIGS[0][0],
        verbose_name=_(u'Search config')
    )

    search_field_text = models.CharField(
        _(u"Text in the search field"),
        blank=True,
        default=_(u'Search products...'),
        max_length=50)

    # Watermark
    # TODO: move it to config
    watermark_image = ImageWithThumbsField(
        _(u'Watermark image'),
        upload_to="images",
        blank=True, null=True)
    watermark_opacity = models.FloatField(
        _(u'Watermark opacity'),
        default=1,
        help_text='0.0-1.0',
        validators = [MinValueValidator(0.0), MaxValueValidator(1.0)])
    watermark_position = models.IntegerField(
        _(u'Watermark position'),
        default=1, choices=POSITION_CHOICES)

    # Category view
    product_cols = models.IntegerField(_(u"Product cols"), default=1)
    product_rows = models.IntegerField(_(u"Product rows"), default=10)
    category_cols = models.IntegerField(_(u"Category cols"), default=1)

    # Price calculator
    price_calculator = models.CharField(
        choices=settings.LFS_PRICE_CALCULATORS, max_length=255,
        default=settings.LFS_PRICE_CALCULATORS[0][0],
        verbose_name=_(u"Price calculator"))

    # Currency
    default_currency = models.ForeignKey(
        Currency, verbose_name=_(u"Default currency"), blank=True, null=True)

    # SEO
    meta_title = models.TextField(
        _(u"Meta title"), blank=True,
        default=getattr(
            settings, 'DEFAULT_SHOP_META_TITLE_TEMPLATE', '{{shop_name}}'),
    )
    meta_keywords = models.TextField(
        _(u"Meta keywords"),
        default=getattr(
            settings, 'DEFAULT_SHOP_META_KEYWORDS_TEMPLATE', ''),
        blank=True)
    meta_description = models.TextField(
        _(u"Meta description"),
        default=getattr(
            settings, 'DEFAULT_SHOP_META_DESCRIPTION_TEMPLATE', ''),
        blank=True)
    meta_seo_text = models.TextField(
        _(u"Meta SEO text"),
        default=getattr(settings, 'DEFAULT_SHOP_META_TEXT_TEMPLATE', ''),
        blank=True)

    # Customer data
    fields_objects = generic.GenericRelation(
        FieldsObjects,
        object_id_field="content_id", content_type_field="content_type")
    extra = models.TextField(_(u"Extra"), blank=True)

    # Addresses
    address_form_fields = models.CharField(
        _(u'Address fields'),
        help_text=_(u'Field identifiers separated by comma.'),
        max_length=255, blank=True)
    template_of_address = models.TextField(
        _(u'Template of presentation current address'),
        blank=True, null=True)

    # Registration
    registration_form_fields = models.CharField(
        _(u'Registration fields'),
        help_text=_(u'Field identifiers separated by comma.'),
        max_length=255, blank=True)
    ask_email_when_register = models.BooleanField(
        _(u'Ask email when register'),
        default=False,
        help_text=_(u'When login is not email.'))
    ask_phone_when_register = models.BooleanField(
        _(u'Ask phone when register'),
        default=False,
        help_text=_(u'When login is not phone.'))
    require_accept_rules = models.BooleanField(
        _(u'Require accept user`s rules'), default=False)
    link_to_rules = models.CharField(
        _(u'Text of link to rules'), max_length=255, blank=True)
    template_of_discount_code = models.CharField(
        _(u'Template of discount code in shop'),
        max_length=30,
        blank=True,  # TODO: need remove this
        default='WWWWWW')
    template_msg_of_registration = models.TextField(
        _(u'Template message of success registration'), blank=True)

    # Cart
    checkout_form_fields = models.CharField(
        _(u'Checkout fields'),
        help_text=_(u'Field identifiers separated by comma.'),
        max_length=255, blank=True)
    ask_email_when_checkout = models.BooleanField(
        _(u'Ask email when checkout'),
        default=False)
    ask_phone_when_checkout = models.BooleanField(
        _(u'Ask phone when register'),
        default=True)
    order_form_fields = models.CharField(
        _(u'Order specified fields'),
        help_text=_(u'Field identifiers separated by comma.'),
        max_length=255, blank=True)

    show_regular_customer_tab = models.BooleanField(
        _(u'Show "I\'m registered user" tab'), default=True)
    make_bg_registration = models.BooleanField(
        _(u'Do background registration'), default=True)
    allow_previous_data = models.BooleanField(
        _(u'Link order when data are the same'),
        default=False,
        help_text=_(u'Check your configuration for correct customer finder.'))
    cart_require_accept_rules = models.BooleanField(
        _(u'Require accept user`s rules'),
        default=False)
    cart_link_to_rules = models.CharField(
        _(u'Text of link to rules'), max_length=255, blank=True)

    class Meta:
        permissions = (
            ("manage_shop", _(u"Manage shop")),
            ("manage_portlets", _(u'Can manage portlets')),
            ("manage_criteria", _(u'Can manage criteria')),
            ("manage_fields", _(u'Can manage fields')),
        )
        verbose_name = _(u'Shop')
        verbose_name_plural = _(u'Shop')

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        """
        Overwritten to save seo data.
        """
        if not self.meta_title:
            self.meta_title = getattr(
                settings, 'DEFAULT_SHOP_META_TITLE_TEMPLATE', '')
        if not self.meta_keywords:
            self.meta_keywords = getattr(
                settings, 'DEFAULT_SHOP_META_KEYWORDS_TEMPLATE', '')
        if not self.meta_description:
            self.meta_description = getattr(
                settings, 'DEFAULT_SHOP_META_DESCRIPTION_TEMPLATE', '')
        if not self.meta_seo_text:
            self.meta_seo_text = getattr(
                settings, 'DEFAULT_SHOP_META_TEXT_TEMPLATE', '')
        super(Shop, self).save(*args, **kwargs)

    def prerender_menu(self, save=True):
        from lfs.catalog.models import Category
        from django.template.loader import render_to_string

        if self.menu_type == 1:  # Simple menu
            categories = Category.objects.filter(level=0)
            tabs1 = [{
                'title': c.name,
                'link': c.get_absolute_url(),
                'category': c
            } for c in categories[:self.max_menu_item_count]]
            tabs2 = [{
                'title': c.name,
                'link': c.get_absolute_url(),
                'category': c
            } for c in categories[self.max_menu_item_count:]]

            self.prerendered_menu = render_to_string('lfs/shop/tabs.html', {
                'tabs1': tabs1,
                'tabs2': tabs2,
            })
            if save:
                self.save()
        elif self.menu_type == 2:  # With sublevels
            categories = Category.objects.filter(level__gte=1, level__lte=2)
            self.prerendered_menu = render_to_string('lfs/shop/tabs_2.html', {
                'categories': categories,
            })
            if save:
                self.save()
        elif self.menu_type == 3:  # With infinite sublevels
            categories = Category.objects.all()
            self.prerendered_menu = render_to_string('lfs/shop/tabs_3.html', {
                'categories': categories,
            })
            if save:
                self.save()
        return True

    def get_format_info(self):
        """Returns the global format info.
        """
        return {
            "product_cols": self.product_cols,
            "product_rows": self.product_rows,
            "category_cols": self.category_cols,
        }

    def get_default_country(self):
        """Returns the default country of the shop.
        """
        cache_key = "%s-default-country-%s" % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.id)
        default_country = cache.get(cache_key)
        if default_country:
            return default_country

        default_country = self.default_country
        cache.set(cache_key, default_country)

        return default_country

    def get_notification_emails(self):
        """Returns the notification e-mail addresses as list
        """
        import re
        adresses = re.split("[\s,]+", self.notification_emails)
        return adresses

    def get_parent_for_portlets(self):
        """Implements contract for django-portlets. Returns
        always None as there is no parent for a shop.
        """
        return None

    def _get_data_for_meta_info(self):
        """
        Get common data for meta templates.
        """
        data = {
            'name': self.name,
            'shop_name': self.name,
        }
        if getattr(settings, 'SEO_USE_DJANGO_TEMPLATES', False):
            # Add additional data if Django templates are used
            data.update({
                'shop': self,
            })
        return data

    def get_meta_title(self):
        """Returns the meta title of the shop.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_title, self._get_data_for_meta_info())

    def get_meta_keywords(self):
        """Returns the meta keywords of the shop.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_keywords, self._get_data_for_meta_info())

    def get_meta_description(self):
        """Returns the meta description of the shop.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_description, self._get_data_for_meta_info())

    def get_meta_seo_text(self):
        """
        Returns the seo text of the shop. Takes care whether the
        product is a variant and meta description are active or not.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_seo_text, self._get_data_for_meta_info())


from monkeys import *
