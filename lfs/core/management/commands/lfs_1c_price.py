# -*- coding: utf-8 -*-

# python imports
import xml.sax

# django imports
from django.core.management.base import BaseCommand
from django.conf import settings
from django import db

# SShop imports
from lfs.catalog.models import Product



categories = {}


def parse_product_price(text):
    '''
        This function contain class for parse
        products from XML file from 1C
    '''

    class XMLReader(xml.sax.ContentHandler):
        def __init__(self, imported=False, mywin=None):
            xml.sax.ContentHandler.__init__(self)

        def startDocument(self):
            print 'Start products price processed'
            # stores the tag to be processed
            self.data = ''
            self.code_1c_ = False
            self.code_1c = ''
            self.price = ''
            self.price_ = False
            self.currency_ = False
            self.currency = ''


        def update_price(self):
            print u'\tProcess product {product}'.format(product=self.code_1c)

            try:
                price = float(self.price.replace(',','.'))
                Product.objects.filter(uid=self.code_1c).update(**{'price':price})

            except Exception, e:
                print e
                try:
                    open('error.txt','a').write(str(e)+str(self.code_1c)+'\n')
                except:
                    pass
                print 'ERROR: Create product. Error_20'

        def endDocument(self):
            pass

        def characters(self, data):
            if self.code_1c_:
                self.code_1c = data
            elif self.currency_:
                self.currency = data
            elif self.price_:
                self.price = data

        def startElement(self, name, attrs):
            if name == u'Код1с':
                self.code_1c_ = True
            elif name == u'ЦенаЗаЕдиницу':
                self.price_ = True
            elif name == u'Валюта':
                self.currency_ = True

        def endElement(self, name):
            if name == u'Предложение':
                self.update_price()
            if name == u'Код1с':
                self.code_1c_ = False
            elif name == u'ЦенаЗаЕдиницу':
                self.price_ = False
            elif name == u'Валюта':
                self.currency_ = False

    d = xml.sax.parseString(text, XMLReader())
    del d
    db.reset_queries()



class Command(BaseCommand):
    args = ''
    help = 'Import 1C XML data to LFS'

    def handle(self, *args, **options):
        print 'Update 1C XML product price to SShop\n'

        try:
            path_to_file = args[0]
        except IndexError:
            print 'ERROR: Input path to XML file with data'

        f = open(path_to_file, 'r')
        offer = False
        text = ''

        for i in f.xreadlines():
            if '<Предложение>' in i:
                offer = True

            if offer:
                text += i

            if '</Предложение>' in i:
                offer = False
                parse_product_price(text)
                text = ''
        f.close()

        print 'Import Finish'
