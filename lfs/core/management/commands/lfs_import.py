# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    args = ''
    help = 'Import 1C XML data to LFS'

    def handle(self, *args, **options):
        from sshop_import.models import Task
        from sshop_import.scripts.parse import parse_product

        try:
            task = Task.objects.filter(finish=False,performed=False)[0]
            task.performed = True
            task.save()

            finish = parse_product(task.path)
            task.performed = False
            if finish:
                task.finish = True
            task.save()
        except Exception, e:
            print e

        print 'Import Finish'