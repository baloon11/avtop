# -*- coding: utf-8 -*-

# python
from optparse import make_option

# django imports
from django.core.management.base import BaseCommand
from django.conf import settings
from django.contrib.sites.models import Site

SHOP_DESCRIPTION = """
<h1 class="first-heading">Мы рады приветствовать вас на сайте нашего магазина!</h1>
"""


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--domain', '-d', dest='domain',
            # required=True,
            # TODO: domain must be required option
            default='p-cart.com',
            help='Set domain of current shop instance'),
    )
    help = 'Initializes P-Cart engine'

    def handle(self, *args, **options):
        import os
        from lfs.core.models import ActionGroup
        # from lfs.core.models import Action
        # from lfs.core.models import Application
        # from lfs.core.models import Country
        from lfs.core.models import Shop
        from lfs.core.utils import import_symbol

        from portlets.models import Slot
        from portlets.models import PortletAssignment

        # from lfs.portlet.models import CartPortlet
        from lfs.portlet.models import CategoriesPortlet
        # from lfs.portlet.models import PagesPortlet
        from lfs.payment.models import PaymentMethod
        # from lfs.page.models import Page
        from lfs.shipping.models import ShippingMethod

        from sshop_currencies.models import Currency
        from lfs.catalog.models import ProductStatus, SortType

        # from lfs.mail.models import MailTemplate

        # from robots.models import Rule, Url

        from django.contrib.auth.models import User
        from lfs.order.models import OrderStatus
        from django.utils import translation
        from django.utils.translation import ugettext_lazy as _
        from django.contrib.contenttypes.models import ContentType
        from lfs.fields.models import CharField, FieldsObjects
        from lfs.catalog.models import StaticBlock
        from tasks.api import TaskQueueManager


        # StaticBlock
        StaticBlock.objects.get_or_create(name="header")
        StaticBlock.objects.get_or_create(name="footer")

        # Country - model was deleted
        # usa = Country.objects.create(code="us", name="USA")

        # dollar = Currency.objects.create(name="Dollar", code="usd", abbr="USD")
        uah = Currency.objects.create(
            name=u"Гривня",
            code=u"UAH",
            abbr=u"грн",
            format_str=u"%(value).0f %(abbr)s",
            coeffitient=1.0
        )

        # Shop
        shop = Shop.objects.create(
            name='P-Cart',
            shop_owner='Pink Pony',
            from_email='pink@pony.com',
            notification_emails='pink@pony.com',
            description=SHOP_DESCRIPTION,
            template_of_discount_code='WWWWWW',
            # default_country=usa, - filed was deleted
            default_currency=uah,
            search_field_text=u'Поиск по товарам...',
            product_cols=2,
            category_cols=2,
            meta_seo_text=u'Рады приветствовать вас в интернет-магазине  {{ shop_name }}',
            template_msg_of_registration=u'Регистрация прошла успешно!',
            allow_previous_data=True,
        )
        full_name = CharField.objects.create(
            field_name=u'ФИО',
            identificator=u'full_name',
            required=True,

        )
        FieldsObjects.objects.create(field=full_name, content=shop)

        address = CharField.objects.create(
            field_name=u'Адрес',
            identificator=u'address',
        )
        FieldsObjects.objects.create(field=address, content=shop)

        shop.checkout_form_fields = 'full_name'
        shop.save()

        # shop.email = True
        # shop.email_required = True
        # may be move it filed to config.json?
        # shop.template_of_discount_code = 'XYWAWAWA'
        # shop.address_fields = 1
        # shop.invoice_countries.add(usa)
        # shop.shipping_countries.add(usa)
        # shop.save()

        User.objects.create_superuser('sbits', 'dev@the7bits.com', 'Lol_7bits')
        User.objects.create_superuser('manager', 'manager@the7bits.com', 'zsergb')

        # Actions
        # tabs = ActionGroup.objects.create(name="Tabs")
        link_bar = ActionGroup.objects.create(name="Link_bar")
        footer = ActionGroup.objects.create(name="Footer")
        # Action.objects.create(group=link_bar, title="Contact", link="/contact", active=True, position=1)
        # Action.objects.create(group=footer, title="Terms and Conditions", link="/page/terms-and-conditions", active=True, position=1)
        # Action.objects.create(group=footer, title="Imprint", link="/page/imprint", active=True, position=2)

        # Portlets
        left_slot = Slot.objects.create(name="Left")
        right_slot = Slot.objects.create(name="Right")
        middle_slot = Slot.objects.create(name="Middle", type_of_slot=5)

        # # cart_portlet = CartPortlet.objects.create(title="Cart")
        # # PortletAssignment.objects.create(slot=right_slot, content=shop, portlet=cart_portlet)

        categories_portlet = CategoriesPortlet.objects.create(title="Categories")
        PortletAssignment.objects.create(slot=left_slot, content=shop, portlet=categories_portlet)

        # # pages_portlet = PagesPortlet.objects.create(title="Information")
        # # PortletAssignment.objects.create(slot=left_slot, content=shop, portlet=pages_portlet)

        # Payment methods
        PaymentMethod.objects.create(
            name=u"Наличными",
            currency=uah,
            priority=1,
            active=1,
            deletable=0
        )
        # pm.id=1; pm.save()

        # Shipping methods
        ShippingMethod.objects.create(name="Самовывоз", priority=1, active=1)

        # Pages
        # p = Page.objects.create(title="Root", slug="", active=1, exclude_from_navigation=1)
        # p.id = 1; p.save()
        # p = Page.objects.create(title="Terms and Conditions", slug="terms-and-conditions", active=1, body="Enter your terms and conditions here.")
        # p.id = 2; p.save()
        # p = Page.objects.create(title="Imprint", slug="imprint", active=1, body="Enter your imprint here.")
        # p.id = 3; p.save()

        # Order Numbers
        ong = import_symbol(settings.LFS_ORDER_NUMBER_GENERATOR)
        ong.objects.create(id="order_number")

        # Application object
        # Application.objects.create(version="0.7")

        # Default product statuses
        ProductStatus.objects.create(
            name=u"В наличии", css_class='label-success')
        ProductStatus.objects.create(name=u"Ожидается", show_buy_button=False)
        ProductStatus.objects.create(
            name=u"Нет в наличии", show_buy_button=False)
        ProductStatus.objects.create(
            name=u"Архив", show_buy_button=False, show_ask_button=False)

        # Default product sortings
        SortType.objects.create(
            name=u'По возрастанию цены',
            sortable_fields='status__order,effective_price,id')
        SortType.objects.create(
            name=u'По убыванию цены',
            sortable_fields='status__order,-effective_price,id')
        SortType.objects.create(
            name=u'По алфавиту',
            sortable_fields='status__order,name,id')
        SortType.objects.create(
            name=u'Обратно алфавиту',
            sortable_fields='status__order,-name,id')
        # SortType.objects.create(name=u'По статусу и цене', sortable_fields='status__order, -effective_price')

        # Mail templates
        # folder = 'basic_theme/templates/lfs/mail/'
        # files = [folder + a for a in os.listdir(folder)]
        # for f in files:
        #     name, file_type = f.split('/')[-1].split('.')
        #     if file_type not in ['txt', 'html']:
        #         continue
        #     mail_template, created = MailTemplate.objects.get_or_create(name=name)
        #     content = open(f, 'r')
        #     if file_type == 'txt':
        #         mail_template.text_template = content.read()
        #     elif file_type == 'html':
        #         mail_template.html_template = content.read()
        #     mail_template.save()
        #     content.close()

        # Replace the default domain of the real domain
        site = Site.objects.get_current()
        site.domain = options['domain']
        site.name = options['domain']
        site.save()

        translation.activate(settings.LANGUAGE_CODE)

        or_st, created = OrderStatus.objects.get_or_create(
            identifier="submitted")
        or_st.name = _(u"Submitted")
        or_st.save()

        or_st, created = OrderStatus.objects.get_or_create(identifier="paid")
        or_st.name = _(u"Paid")
        or_st.save()

        or_st, created = OrderStatus.objects.get_or_create(identifier="sent")
        or_st.name = _(u"Sent")
        or_st.save()

        or_st, created = OrderStatus.objects.get_or_create(identifier="closed")
        or_st.name = _(u"Closed")
        or_st.save()

        or_st, created = OrderStatus.objects.get_or_create(
            identifier="canceled")
        or_st.name = _(u"Canceled")
        or_st.save()

        or_st, created = OrderStatus.objects.get_or_create(
            identifier="payment_failed")
        or_st.name = _(u"Payment Failed")
        or_st.save()

        or_st, created = OrderStatus.objects.get_or_create(
            identifier="payment_flagged")
        or_st.name = _(u"Payment Flagged")
        or_st.save()

        task_manager = TaskQueueManager()
        task_manager.schedule(
            'lfs.core.jobs.clear_old_logs', repeat=24*60)
