# coding: utf-8
import locale

from django import template
from django.conf import settings
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.template import Node, TemplateSyntaxError
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from lfs.catalog.models import Category
from lfs.catalog.models import Product
from lfs.catalog.settings import PRODUCT_TYPE_LOOKUP
from lfs.core.models import Action
from lfs.page.models import Page
from lfs.payment.models import PaymentMethod
from lfs.catalog.models import SortType


register = template.Library()


@register.inclusion_tag(
    'lfs/portlets/category_children.html', takes_context=True)
def category_children(context, categories):
    """
    """
    return {"categories": categories}


@register.inclusion_tag(
    'lfs/shop/google_analytics_tracking.html', takes_context=True)
def google_analytics_tracking(context):
    """Returns google analytics tracking code which has been entered to the
    shop.
    """
    from lfs.core.utils import get_default_shop
    shop = get_default_shop(context.get("request"))
    return {
        "ga_site_tracking": shop.ga_site_tracking,
        "google_analytics_id": shop.google_analytics_id,
    }


@register.inclusion_tag(
    'lfs/shop/google_analytics_ecommerce.html', takes_context=True)
def google_analytics_ecommerce(context, clear_session=True):
    """Returns google analytics e-commerce tracking code. This should be
    displayed on the thank-you page.
    """
    from lfs.core.utils import get_default_shop
    request = context.get("request")
    order = request.session.get("order")
    shop = get_default_shop(request)

    # The order is removed from the session. It has been added after the order
    # has been payed within the checkout process. See order.utils for more.
    if clear_session and "order" in request.session:
        del request.session["order"]

    if "voucher" in request.session:
        del request.session["voucher"]

    return {
        "order": order,
        "ga_ecommerce_tracking": shop.ga_ecommerce_tracking,
        "google_analytics_id": shop.google_analytics_id,
    }


@register.inclusion_tag('lfs/catalog/sorting.html', takes_context=True)
def sorting(context):
    """
    """
    request = context.get('request')
    return {
        "current": request.session.get("sorting"),
        "sortings": SortType.objects.all().order_by('order'),
    }


@register.inclusion_tag('lfs/catalog/breadcrumbs.html', takes_context=True)
def breadcrumbs(context, obj):
    """
    """
    if isinstance(obj, Category):
        cache_key = "%s-category-breadcrumbs-%s" % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX, obj.slug)
        objects = cache.get(cache_key)
        if objects is not None:
            return objects

        objects = []
        while obj is not None:
            objects.insert(0, {
                "name": obj.name,
                "url": obj.get_absolute_url(),
            })
            obj = obj.parent

        result = {
            "objects": objects,
            "STATIC_URL": context.get("STATIC_URL"),
        }
        cache.set(cache_key, result)
    elif isinstance(obj, Product):
        request = context.get("request")
        category = obj.get_current_category(request)
        if category is None:
            return []
        else:
            objects = [{
                "name": obj.get_name(),
                "url": obj.get_absolute_url(),
            }]
            while category is not None:
                objects.insert(0, {
                    "name": category.name,
                    "url": category.get_absolute_url(),
                })
                category = category.parent
        result = {
            "objects": objects,
            "STATIC_URL": context.get("STATIC_URL"),
        }
    elif isinstance(obj, Page):
        objects = []
        # TODO: when page groups will be ready it needs to be fixed
        objects.append({
            "name": _(u"Information"),
            "url": reverse("lfs_pages")})
        objects.append({"name": obj.title})
        result = {
            "objects": objects,
            "STATIC_URL": context.get("STATIC_URL"),
        }
    else:
        result = {
            "objects": ({"name": obj},),
            "STATIC_URL": context.get("STATIC_URL"),
        }
    return result


class ActionsNode(Node):
    """
    Node for do_actions.
    """
    def __init__(self, group_name):
        self.group_name = group_name

    def render(self, context):
        context["actions"] = Action.objects.filter(
            active=True, group__name=self.group_name, level=0)
        return ''


def do_actions(parser, token):
    """
    Returns the actions for the group with the given id.
    """
    bits = token.contents.split()
    len_bits = len(bits)
    if len_bits != 2:
        raise TemplateSyntaxError(
            _('%s tag needs group id as argument') % bits[0])
    return ActionsNode(bits[1])
register.tag('actions', do_actions)


@register.inclusion_tag(
    'lfs/catalog/top_level_categories.html', takes_context=True)
def top_level_categories(context):
    """Displays the top level categories.
    """
    from lfs.catalog.utils import get_current_top_category
    request = context.get("request")
    obj = context.get("product") or context.get("category")

    categories = []
    top_category = get_current_top_category(request, obj)
    for category in Category.objects.filter(parent=None)[:4]:
        if top_category:
            current = top_category.id == category.id
        else:
            current = False
        categories.append({
            "url": category.get_absolute_url(),
            "name": category.name,
            "current": current,
        })
    return {
        "categories": categories,
    }


class TopLevelCategory(Node):
    """Calculates the current top level category.
    """
    def render(self, context):
        from lfs.catalog.utils import get_current_top_category
        request = context.get("request")
        obj = context.get("product") or context.get("category")
        top_level_category = get_current_top_category(request, obj)
        context["top_level_category"] = top_level_category.name
        return ''


def do_top_level_category(parser, token):
    """Calculates the current top level category.
    """
    bits = token.contents.split()
    len_bits = len(bits)
    if len_bits != 1:
        raise TemplateSyntaxError(_('%s tag needs no argument') % bits[0])
    return TopLevelCategory()
register.tag('top_level_category', do_top_level_category)


class CartInformationNode(Node):
    """
    """
    def render(self, context):
        from lfs.cart.utils import get_cart
        request = context.get("request")
        cart = get_cart(request)
        if cart is None:
            amount_of_items = 0
            price = 0.0
        else:
            amount_of_items = cart.get_amount_of_items()
            price = cart.get_price(request, total=True)
        context["cart_amount_of_items"] = amount_of_items
        context["cart_price"] = price
        return ''


def do_cart_information(parser, token):
    """Calculates cart informations.
    """
    bits = token.contents.split()
    len_bits = len(bits)
    if len_bits != 1:
        raise TemplateSyntaxError(_('%s tag needs no argument') % bits[0])
    return CartInformationNode()
register.tag('cart_information', do_cart_information)


class CurrentCategoryNode(Node):
    """
    """
    def render(self, context):
        request = context.get("request")
        product = context.get("product")
        context["current_category"] = \
            product.get_current_category(request)
        return ''


def do_current_category(parser, token):
    """Calculates current category.
    """
    bits = token.contents.split()
    len_bits = len(bits)
    if len_bits != 2:
        raise TemplateSyntaxError(
            _('%s tag needs product as argument') % bits[0])
    return CurrentCategoryNode()
register.tag('current_category', do_current_category)


# TODO: Move this to shop utils or similar
def get_slug_from_request(request):
    """Returns the slug of the currently displayed category.
    """
    slug = request.path.split("/")[-1]
    try:
        int(slug)
    except ValueError:
        pass
    else:
        slug = request.path.split("/")[-2]
    return slug


@register.filter
def currency(value, request=None, grouping=True):
    """
    e.g.
    import locale
    locale.setlocale(locale.LC_ALL, 'de_CH.UTF-8')
    currency(123456.789)  # Fr. 123'456.79
    currency(-123456.789) # <span class="negative">Fr. -123'456.79</span>
    """
    from lfs.core.utils import get_default_shop
    if not value:
        value = 0.0

    shop = get_default_shop(request)
    try:
        #TODO: should be redesigned
        s = locale.format('%.2f', value)
        if grouping:
            #TODO: we can add converter here
            s += u' %s' % shop.default_currency.abbr
        result = s
    except ValueError:
        result = value

    # add css class if value is negative
    if value < 0:
        # replace the minus symbol if needed
        if result[-1] == '-':
            length = len(locale.nl_langinfo(locale.CRNCYSTR))
            result = '%s-%s' % (result[0:length], result[length:-1])
        return mark_safe('<span class="negative">%s</span>' % result)
    return result


@register.filter
def payment_currency(value, payment_name, request=None, grouping=True):
    """
    e.g.
    import locale
    locale.setlocale(locale.LC_ALL, 'de_CH.UTF-8')
    currency(123456.789)  # Fr. 123'456.79
    currency(-123456.789) # <span class="negative">Fr. -123'456.79</span>
    """
    if not value:
        value = 0.0

    currency = PaymentMethod.objects.get(name=payment_name).currency
    value *= currency.coeffitient
    try:
        #TODO: should be redesigned
        s = locale.format('%.2f', value)
        if grouping:
            #TODO: we can add converter here
            s += u' %s' % currency.abbr
        result = s
    except ValueError:
        result = value

    # add css class if value is negative
    if value < 0:
        # replace the minus symbol if needed
        if result[-1] == '-':
            length = len(locale.nl_langinfo(locale.CRNCYSTR))
            result = '%s-%s' % (result[0:length], result[length:-1])
        return mark_safe('<span class="negative">%s</span>' % result)
    return result


@register.filter
def decimal_l10n(value, digits=2):
    """Returns the decimal value of value based on current locale.
    """
    try:
        value = float(value)
    except ValueError:
        return value

    format_str = "%%.%sf" % digits
    return locale.format_string(format_str, value)


@register.filter
def quantity(quantity):
    """Removes the decimal places when they are zero.

    Means "1.0" is transformed to "1", whereas "1.1" is not transformed at all.
    """
    if str(quantity).find(".0") == -1:
        return quantity
    else:
        return int(float(quantity))


@register.filter
def sub_type_name(sub_type, arg=None):
    """Returns the sub type name for the sub type with passed sub_type id.
    """
    try:
        return PRODUCT_TYPE_LOOKUP[sub_type]
    except KeyError:
        return ""


@register.filter
def multiply(score, pixel):
    """Returns the result of score * pixel
    """
    return score * pixel


@register.filter
def col_width_in_percent(cols):
    """Returns width of the column in percents
    """
    return int(1. / cols * 100)


@register.simple_tag
def query_update(query, key, value):
    """
    Build the GET query by taking the actual query and adding the
    new parameter.

    href='?{% query_update request.GET 'page' next %}'
    """
    query = query.copy()
    query[key] = value
    return query.urlencode()


@register.simple_tag
def show_level(level):
    """
    """
    return "---" * (level - 1)


@register.simple_tag
def sub(a, b):
    """
    """
    return a - b
