# coding: utf-8
import logging
import datetime
import os
import sys
import urllib
import random
import Image
import ImageEnhance
import cStringIO

from collections import deque
from itertools import count
from django.core.files.uploadedfile import TemporaryUploadedFile
from django.core.files import File
from django.conf import settings
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.utils import simplejson
from django.utils.functional import Promise
from django.utils.encoding import force_unicode
from django.contrib.redirects.models import Redirect
from django.core.cache import cache
from django.core import urlresolvers

# import lfs.catalog.utils
from lfs.catalog.models import Category
from lfs.caching.utils import lfs_get_object_or_404
from lfs.core.models import Shop


logger = logging.getLogger('sshop')


def l10n_float(string):
    """Takes a country specfic decimal value as string and returns a float.
    """
    # TODO: Implement a proper transformation with babel or similar
    if settings.LANGUAGE_CODE in ['de', 'ru', 'uk']:
        string = string.replace(",", ".")
    try:
        return float(string)
    except ValueError:
        return 0.0


def get_default_shop(request=None):
    """Returns the default shop.
    """
    if request:
        try:
            return request.shop
        except AttributeError:
            pass

    cache_key = "%s-default-shop" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX,)
    shop = cache.get(cache_key)
    if shop:
        return shop

    try:
        shop = Shop.objects.get(pk=1)
    except Shop.DoesNotExist, e:
        shop = Shop.objects.all()[0]

    if request:
        request.shop = shop

    cache.set(cache_key, shop)
    return shop


def lfs_quote(string, encoding="utf-8"):
    """Encodes passed string to passed encoding before quoting with
    urllib.quote().
    """
    return urllib.quote(string.encode(encoding))


def import_module(module):
    """Imports module with given dotted name.
    """
    try:
        module = sys.modules[module]
    except KeyError:
        __import__(module)
        module = sys.modules[module]
    return module


def import_symbol(symbol):
    """Imports symbol with given dotted name.
    """
    module_str, symbol_str = symbol.rsplit('.', 1)
    module = import_module(module_str)
    return getattr(module, symbol_str)


def set_message_to(response, msg):
    """Sets message cookie with passed message to passed response.
    """
    # We just keep the message two seconds.
    max_age = 2
    expires = datetime.datetime.strftime(
        datetime.datetime.utcnow() +
        datetime.timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")

    response.set_cookie(
        "message", lfs_quote(msg), max_age=max_age, expires=expires)
    return response


def set_message_cookie(url, msg):
    """Returns a HttpResponseRedirect object with passed url and set cookie
    ``message`` with passed message.
    """
    # We just keep the message two seconds.
    max_age = 2
    expires = datetime.datetime.strftime(
        datetime.datetime.utcnow() +
        datetime.timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")

    response = HttpResponseRedirect(url)
    response.set_cookie(
        "message", lfs_quote(msg), max_age=max_age, expires=expires)
    return response


def render_to_ajax_response(html=[], message=None):
    """Encodes given html and message to JSON and returns a HTTP response.
    """
    result = simplejson.dumps(
        {"message": message, "html": html}, cls=LazyEncoder)

    return HttpResponse(result)


def get_redirect_for(path):
    """Returns redirect path for the passed path.
    """
    try:
        redirect = Redirect.objects.get(
            site=settings.SITE_ID, old_path=path)
    except Redirect.DoesNotExist:
        return ""
    else:
        return redirect.new_path


def set_redirect_for(old_path, new_path):
    """Sets redirect path for the passed path.
    """
    try:
        redirect = Redirect.objects.get(
            site=settings.SITE_ID, old_path=old_path)
        redirect.new_path = new_path
        redirect.save()
    except Redirect.DoesNotExist:
        redirect = Redirect.objects.create(
            site_id=settings.SITE_ID, old_path=old_path, new_path=new_path)


def remove_redirect_for(path):
    """Removes the redirect path for given path.
    """
    try:
        redirect = Redirect.objects.get(site=settings.SITE_ID, old_path=path)
    except Redirect.DoesNotExist:
        return False
    else:
        redirect.delete()
        return True


def set_category_levels():
    """Sets the category levels based on the position in hierarchy.
    """
    for category in Category.objects.all():
        category.level = len(category.get_ancestors())
        category.save()


def get_start_day(date):
    """Takes a string such as ``2009-07-23`` and returns datetime object of
    this day.
    """
    year, month, day = date.split("-")
    start = datetime.datetime(int(year), int(month), int(day))
    return start


def get_end_day(date):
    """Takes a string such as ``2009-07-23`` and returns a datetime object with
    last valid second of this day: 23:59:59.
    """
    year, month, day = date.split("-")
    end = datetime.datetime(int(year), int(month), int(day))
    end = end + datetime.timedelta(1) - datetime.timedelta(microseconds=1)
    return end


def getLOL(objects, objects_per_row=3):
    """Returns a list of list of the passed objects with passed objects per
    row.
    """
    result = []
    row = []
    for i, object in enumerate(objects):
        row.append(object)
        if (i + 1) % objects_per_row == 0:
            result.append(row)
            row = []

    if len(row) > 0:
        result.append(row)

    return result


class LazyEncoder(simplejson.JSONEncoder):
    """Encodes django's lazy i18n strings.
    """
    def default(self, obj):
        if isinstance(obj, Promise):
            return force_unicode(obj)
        return obj


def define_page_range(current_page, total_pages, window=6):
    """ Returns range of pages that contains current page and few pages
    before and after it.

        @current_page - starts from 1
        @tota_pages - total number of pages
        @window - maximum number of pages shown with current page - should
        be even

        Examples (cucumber style):
             Given window = 6
             When current_page is 8
             and total_pages = 20
             Then I should see: 5 6 7 [8] 9 10 11

             Given window = 6
             When current_page is 8
             and total_pages = 9
             Then I should see: 3 4 5 6 7 [8] 9

             Given window = 6
             When current_page is 1
             and total_pages = 9
             Then I should see: [1] 2 3 4 5 6 7
    """
    # maximum length of page range is window + 1
    maxlen = window + 1
    page_range = deque(maxlen=maxlen)

    # minimum possible index is either: (current_page - window) or 1
    window_start = (current_page - window) \
        if (current_page - window) > 0 else 1

    # maximum possible index is current_page + window or total_pages
    window_end = total_pages if (current_page + window) > \
        total_pages else (current_page + window)

    # if we have enough pages then we should end at preffered end
    preffered_end = current_page + int(window / 2.0)

    for i in count(window_start):
        if i > window_end:
            # if we're on first page then our window will be [1] 2 3 4 5 6 7
            break
        elif i > preffered_end and len(page_range) == maxlen:
            # if we have enough pages already then stop at preffered_end
            break
        page_range.append(i)
    return list(page_range)


def lfs_pagination(request, current_page, url='', getparam='start'):
    """Prepare data for pagination

       @page - number of current page (starting from 1)
       @paginator - paginator object, eg. Paginator(contact_list, 25)
    """
    paginator = current_page.paginator
    current_page_no = current_page.number

    has_next = current_page.has_next()
    has_prev = current_page.has_previous()

    page_range = define_page_range(current_page.number, paginator.num_pages)

    first = 1
    last = paginator.num_pages

    if first in page_range:
        first = None

    if last in page_range:
        last = None
    formated_page_range = []
    pagination_template = settings.PAGINATION_PAGE_NAME_TEMPLATE
    if len(page_range) > 1:
        if first:
            formated_page_range.append((
                page_range[0],
                pagination_template.replace(
                    'start_number', str((page_range[0]-1)*paginator.per_page+1)
                ).replace(
                    'end_number', str(page_range[0]*paginator.per_page)
                ).replace(
                    'page_number', str(page_range[0])
                )
            ))
        else:
            formated_page_range.append((
                page_range[0],
                pagination_template.replace(
                    'start_number', str(page_range[0])
                ).replace(
                    'end_number', str(page_range[0]*paginator.per_page)
                ).replace(
                    'page_number', str(page_range[0])
                )
            ))
        for i in range(1, len(page_range)-1):
            formated_page_range.append((
                page_range[i],
                pagination_template.replace(
                    'start_number', str(page_range[i-1]*paginator.per_page+1)
                ).replace(
                    'end_number', str(page_range[i]*paginator.per_page)
                ).replace(
                    'page_number', str(page_range[i])
                )
            ))
        if paginator.count <= page_range[-1]*paginator.per_page:
            last_number = paginator.count
        else:
            last_number = page_range[-1]*paginator.per_page
        formated_page_range.append((
            page_range[-1],
            pagination_template.replace(
                'start_number', str(page_range[-2]*paginator.per_page+1)
            ).replace(
                'end_number', str(last_number)
            ).replace(
                'page_number', str(page_range[-1])
            )
        ))
    to_return = {
        'page_range': page_range,
        'current_page': current_page_no,
        'total_pages': paginator.num_pages,
        'has_next': has_next,
        'has_prev': has_prev,
        'next': current_page_no + 1,
        'prev': current_page_no - 1,
        'url': url,
        'getparam': getparam,
        'first_page': first,
        'last_page': last,
        'getvars': '',
        'per_page': paginator.per_page,
        'formated_page_range': formated_page_range
    }

    getvars = request.GET.copy()
    if getparam in getvars:
        del getvars[getparam]

    if getattr(settings, 'USE_HR_URLS', False):
        import urlparse
        _dict = {}
        if '?' in url:
            _dict = dict(urlparse.parse_qs(url.split('?')[1]))
            to_return['url'] = url.split('?')[0]

        for key in getvars.keys():
            if key not in _dict:
                getvars.pop(key)

    if len(getvars.keys()) > 0:
        to_return['getvars'] = "&%s" % getvars.urlencode().replace('%2C', ',')
    return to_return


def resize(file_content, image, w, h):
    image_w, image_h = image.size

    size = []
    if image_w > w or image_h > h:
        if image_w > image_h and image_w - w > image_h - h:
            z = (image_w * 1.0) / w
            size = [w, int(image_h / z)]
        else:
            z = (image_h * 1.0) / h
            size = [int(image_w / z), h]

    if size:
        image = image.resize(size, Image.ANTIALIAS)
        # WTF???
        path = os.path.join(
            getattr(
                settings, 'MEDIA_ROOT', '/'), 'uploads', file_content.name)
        image.save(path)
        file_content = File(open(path, 'rb'))
    return image, file_content


# DEPRECATED: use directly in 1C import
def add_watermark(file_content):
    logger.info('Before add watermark')
    image = Image.open(file_content.file)

    w, h = getattr(settings, 'RESIZE_IMAGE', (None, None))
    if w and h:
        image, file_content = resize(file_content, image, w, h)

    shop = get_default_shop()

    try:
        watermark = Image.open(shop.watermark_image.file)
        opacity = shop.watermark_opacity
        position = shop.watermark_position
    except:
        return file_content

    assert opacity >= 0 and opacity <= 1
    if opacity < 1:
        if watermark.mode != 'RGBA':
            watermark = watermark.convert('RGBA')
        else:
            watermark = watermark.copy()
        alpha = watermark.split()[3]
        alpha = ImageEnhance.Brightness(alpha).enhance(opacity)
        watermark.putalpha(alpha)

    image_w, image_h = image.size
    mark_w, mark_h = watermark.size

    if position == 1:
        pos = (0, 0)
    elif position == 2:
        pos = (image_w - mark_w, 0)
    elif position == 3:
        pos = (0, image_h - mark_h)
    elif position == 4:
        pos = (image_w - mark_w, image_h - mark_h)
    elif position == 5:
        pos = (image_w / 2 - mark_w / 2, image_h / 2 - mark_h / 2)
    elif position == 6:
        pos = (image_w / 2 - mark_w / 2, 0)
    elif position == 7:
        pos = (image_w / 2 - mark_w / 2, image_h - mark_h)
    elif position == 8:
        pos = (image_w - mark_w, image_h / 2 - mark_h / 2)
    elif position == 9:
        pos = (0, image_h / 2 - mark_h / 2)

    layer = Image.new('RGBA', image.size, (0, 0, 0, 0))
    layer.paste(watermark, pos)
    new_content = Image.composite(layer,  image,  layer)

    output = cStringIO.StringIO()
    if '.png' in file_content.name:
        format = 'PNG'
    else:
        format = 'JPEG'
    new_content.save(output, format=format)

    content = TemporaryUploadedFile(
        name=file_content.name,
        size=file_content.size,
        content_type="application/gzip",
        charset='utf-8')
    content.write(output.getvalue())
    content.seek(0)
    output.close()
    logger.info('After add watermark')
    return content


def _get_named_patterns():
    """Returns list of (pattern-name, pattern) tuples
    """
    resolver = urlresolvers.get_resolver(None)
    patterns = sorted([
        (key, value[0][0][0])
        for key, value in resolver.reverse_dict.items()
        if isinstance(key, basestring)
    ])
    return patterns


def check_pattern_exist(pattern, obj=None):
    """ Return True, if url `pattern` exist, else - False
    """
    from lfs.catalog.models import Category, Product

    all_patterns = [i[1] for i in _get_named_patterns()]

    if pattern in all_patterns or (pattern+'/') in all_patterns:
        return True

    try:
        category = Category.objects.get(slug=pattern)
        if isinstance(obj, Category) and obj.id == category.id:
            return False
        else:
            return True
    except Category.DoesNotExist:
        pass

    try:
        product = Product.objects.get(slug=pattern)
        if isinstance(obj, Product) and obj.id == product.id:
            return False
        else:
            return True
    except Product.DoesNotExist:
        pass

    return False


def make_similar_unique_url(url):
    """ Get some url, and return a new similar url that
        will be unique in the project
    """
    f = True
    cnt = 1
    new_url = url
    logger.info('Make similar unique url for %s' % url)
    while f:
        if check_pattern_exist(new_url):
            new_url = '%s_%d' % (url, cnt)
            cnt += 1
        else:
            f = False
    logger.info('Found similar unique url - %s' % new_url)
    return new_url


def generate_referral_code(template):
    """ Take template of referral code and
        generate referral code for customer

    X - цифра
    A - велика латинська літера
    a - мала латинська літера
    W - велика латинська літера або цифра
    w - мала латинська літера або цифра
    Y - мала або велика латинська літера, або цифра
    """
    X = range(48, 58)   # list ASCII codes of numbers
    A = range(65, 91)   # list ASCII codes of upper chars
    a = range(97, 123)  # list ASCII codes of lower chars
    W = A + X           # list ASCII codes of upper chars and numbers
    w = a + X           # list ASCII codes of lower chars and numbers
    Y = A + a + X       # list ASCII codes of upper and lower char and numbers

    result = ''

    for k in template:
        if k == 'X':
            result += chr(random.choice(X))
        elif k == 'A':
            result += chr(random.choice(A))
        elif k == 'a':
            result += chr(random.choice(a))
        elif k == 'W':
            result += chr(random.choice(W))
        elif k == 'w':
            result += chr(random.choice(w))
        elif k == 'Y':
            result += chr(random.choice(Y))
        else:
            result += k

    return result


def render_meta_info(text, data):
    # Always use Django templates from 0.3 release
    # use_django_templates = getattr(
    #     settings, 'SEO_USE_DJANGO_TEMPLATES', False)

    # if use_django_templates:
    try:
        from django.template import Context, Template
        context = Context(data)
        template = Template(text)
        return template.render(context)
    except Exception as e:
        request = context['request']
        if request.user.is_authenticated() and request.user.is_superuser:
            error = e.message
        else:
            error = ''
        return error
    # else:
    #     temp = text
    #     for x in data.keys():
    #         temp = temp.replace('{{ '+x+' }}', data[x])
    #     return temp
