Feature: Test profile
    Test profile functionals

    Scenario: You orders is empty
        Given I load a browser "firefox"
        Then I go to "/login"
        Then I fill in the field "username" with "user1@testmail.com"
        And I fill in the field "password" with "123456"
        And I click on the button with name "login_submit"
        Then I go to "/my-orders"
        And I should see "У вас пока нет заказов." anywhere in the page
        Then I close a browser

    Scenario: Some order in you orders
        Given I load a browser "firefox"
        Then I sign in as "user1@testmail.com" with password "123456"
        Then I go to "/product/product-slug-1-1"
        And I click on the button with name "add-to-cart"
        Then I go to "/checkout-dispatcher"
        Then I wait "5" seconds
        And I fill in the field "invoice_firstname" with "Master"
        And I fill in the field "invoice_lastname" with "Joda"
        And I fill in the field "invoice_phone" with "+34(569)394-9432"
        And I click on the button with ID "sumbit_order"
        Then I go to "/my-orders"
        Then I wait "3" seconds
        And I should see "Состояние" anywhere in the page
        Then I close a browser
