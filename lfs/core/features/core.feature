Feature: Test core app
    First of all
    We need test shop object
    And other fundamental parts of project

    Scenario: Test shop
        Given I have the default shop
        Then I see the shop name "7shop"
        Then I see the shop owner "7bits"
        Then I see "Welcome to Aquashop!" in shop description

    Scenario: Test the login form
        Given I load a browser "firefox"
        Then I go to "/login"
        Then I fill in the field "username" with "admin@testmail.com"
        And I fill in the field "password" with "123456"
        And I click on the button with name "login_submit"
        Then I wait "5" seconds
        Then I should see "admin" somewhere in the page
        Then I close a browser

    Scenario: Test the registration form
        Given I load a browser "firefox"
        Then I go to "/login"
        Then I fill in the field "email" with "test@testmail.com"
        And I fill in the field "password_1" with "123456"
        And I fill in the field "password_2" with "123456"
        And I click on the button with name "register_submit"
        Then I wait "2" seconds
        Then I should see the link to "/logout"
        And I should see "test@testmail.com" somewhere in the page
        And I should see user email "test@testmail.com" in database
        Then I close a browser

    Scenario: Check on existing links: login, logout, manage and account as simple user.
        Given I load a browser "firefox"
        And I go to "/"
        Then I should see the link to "/login"
        Given I sign in as "user1@testmail.com" with password "123456"
        Then I wait "2" seconds
        Then I should see the link to "/logout"
        And I should see the link to "/my-account"
        And I should not see the link to "/manage/"
        Then I logout
        And I should not see the link to "/logout"
        And I should not see the link to "/my-account"
        And I should not see the link to "/manage/"
        Then I close a browser

    Scenario: Check on existing links: login, logout, manage and account as superuser.
        Given I load a browser "firefox"
        And I go to "/"
        Then I should see the link to "/login"
        Given I sign in as "admin@testmail.com" with password "123456"
        Then I wait "2" seconds
        Then I should see the link to "/logout"
        And I should see the link to "/my-account"
        And I should see the link to "/manage/"
        Then I logout
        And I should not see the link to "/logout"
        And I should not see the link to "/my-account"
        And I should not see the link to "/manage/"
        Then I close a browser

    Scenario: Test email notification after registration
        Given I load a browser "firefox"
        And I go to "/login"
        Then I fill in the field "email" with "test@testmail.com"
        And I fill in the field "password_1" with "123456"
        And I fill in the field "password_2" with "123456"
        And I click on the button with name "register_submit"
        Then I should see an email is sent to "test@testmail.com" with subject "Добро пожаловать в 7shop"
        Then I wait "5" seconds
        Then I close a browser

    Scenario: Test email notification after changing the password
        Given I load a browser "firefox"
        And I go to "/login"
        Given I sign in as "admin@testmail.com" with password "123456"
        Then I wait "2" seconds
        Then I go to "/my-password"
        Then I fill in the field "old_password" with "123456"
        And I fill in the field "new_password1" with "new_test123"
        And I fill in the field "new_password2" with "new_test123"
        And I click on the button with name "change_password"
        Then I should see an email is sent to "admin@testmail.com" with subject "Ваш пароль успешно изменен"
        Then I wait "5" seconds
        Then I close a browser

    Scenario: Test email notification after changing the email
        Given I load a browser "firefox"
        And I go to "/login"
        Given I sign in as "admin@testmail.com" with password "123456"
        Then I wait "2" seconds
        Then I go to "/my-email"
        Then I fill in the field "email" with "admin1@testmail.com"
        And I click on the button with name "change_email"
        Then I should see an email is sent to "admin1@testmail.com" with subject "Вы удачно изменили адрес электронной почты"
        Then I wait "5" seconds
        Then I close a browser

    Scenario: Test password changing form
        Given I load a browser "firefox"
        And I go to "/login"
        Given I sign in as "admin@testmail.com" with password "123456"
        Then I wait "2" seconds
        Then I go to "/my-password"
        Then I fill in the field "old_password" with "123456"
        And I fill in the field "new_password1" with "new_test123"
        And I fill in the field "new_password2" with "new_test123"
        And I click on the button with name "change_password"
        Then I wait "2" seconds
        Then I logout
        Then I go to "/login"
        Given I sign in as "admin@testmail.com" with password "new_test123"
        Then I wait "2" seconds
        And I should see the link to "/logout"
        Then I close a browser

    Scenario: Test email changing form
        Given I load a browser "firefox"
        And I go to "/login"
        Given I sign in as "admin@testmail.com" with password "123456"
        Then I wait "2" seconds
        Then I go to "/my-email"
        Then I fill in the field "email" with "admin1@testmail.com"
        And I click on the button with name "change_email"
        Then I wait "2" seconds
        Then I logout
        Then I go to "/login"
        Given I sign in as "admin1@testmail.com" with password "123456"
        Then I wait "2" seconds
        And I should see the link to "/logout"
        Then I close a browser
