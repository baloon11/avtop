# coding: utf-8
import logging

from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template import loader
from .decorators import ajax_required
from django.shortcuts import redirect
from django.contrib.auth import logout
from ..core.decorators import sbits_cache_page
from ..core.templatetags.sshop_tags import (
    price,
    standard_price,
    for_sale_price,
    price_difference,
    cheapest_price,
    most_expensive_price,
)
from lfs.catalog.models import Product
from django.utils import simplejson
logger = logging.getLogger("sshop")
from django.views.decorators.csrf import ensure_csrf_cookie

@sbits_cache_page()
@ensure_csrf_cookie
def shop_view(request, template_name="lfs/shop/shop.html"):
    """
    Displays the shop.
    """
    ### ------ Jardin hack ------ ###
    def_category = getattr(settings, 'SSHOP_DEFAULT_SLUG', False)
    if def_category:
        from lfs.catalog.views import category_product_url_proxy
        return category_product_url_proxy(request, def_category)
    ### ------ end of hack ------ ###
    return render_to_response(template_name, RequestContext(request, {
        'main_page': True,
        'page_type': 'main',
        'service_page': False,
    }))


@ajax_required
def top_bar_view(request, template_name="lfs/top_bar.html"):
    """
    Displays the top bar.
    """
    return render_to_response(template_name, RequestContext(request, {}))


def server_error(request):
    t = loader.get_template('500.html')
    return HttpResponse(t.render(RequestContext(request)), status=500)


def logout_view(request):
    logout(request)
    return redirect('lfs_shop_view')


@ajax_required
def get_products_prices(request):
    current_prices = []
    append_cp = current_prices.append
    for p in request.POST:
        try:
            price_function = eval(request.POST[p])
            current_price = price_function(
                {'request': request},
                Product.objects.get(id=int(p))
            )
            append_cp({
                'xpath': '.__discover--price[data-product-id=%s][data-product-type=%s]' % (
                    p,
                    request.POST[p]
                ),
                'html': current_price
            })
        except Exception, e:
            logger.error(str(e))
    response = simplejson.dumps({'prices': current_prices})
    return HttpResponse(response)
