# coding: utf-8
import simplejson
import pytils

from django.utils.translation import ugettext as _
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
from django.db.models import Count
from django.core.paginator import Paginator, EmptyPage, InvalidPage

from ..core.utils import LazyEncoder
from ..catalog.models import (
    Category,
    ProductPropertyValue,
    ProductStatus,
)
from ..catalog.settings import STANDARD_PRODUCT, VARIANT
from ..manufacturer.models import Manufacturer
from .models import Filter, FilterOption
from .utils import invalidate_filter_cache
from django.contrib.admin.models import LogEntry, CHANGE
from django.contrib.contenttypes.models import ContentType
from django.utils.encoding import force_unicode
from django.contrib.admin.models import User

FILTER_ACTIONS = (
    ('add', _(u'Add to filter')),
    ('as_one', _(u'As one option')),
    ('split_for_values', _(u'Split for values')),
    ('split_for_properties', _(u'Split for properties')),
)

ARRAY_DELIMITERS = [',', '/', ';']


def _render_options(request, filter_id):
    from ..core.utils import import_symbol
    current_filter = Filter.objects.get(pk=filter_id)
    all_filter_options = FilterOption.objects.filter(
        filter=current_filter).order_by('position')

    start = request.GET.get('start')
    # Calculates parameters for display.
    try:
        start = int(start)
    except (ValueError, TypeError):
        start = 1
    amount = getattr(settings, 'ADMIN_FILTER_OPTION_PER_PAGE', 10)
    paginator = Paginator(all_filter_options, amount)
    try:
        current_page = paginator.page(start)
    except (EmptyPage, InvalidPage):
        current_page = paginator.page(paginator.num_pages)

    filter_options = current_page.object_list

    sorters = []
    for s in settings.FILTER_SORTERS:
        if s is not None:
            sorter_class = import_symbol(s[1])
            sorters.append({
                'code': s[0],
                'class_name': s[1],
                'name': sorter_class.name,
            })
        else:
            sorters.append({
                'class_name': 'divider',
            })

    return render_to_string(
        'admin/filter_includes/filter_options_%s_%s.html' % (
            current_filter.special_filter,
            current_filter.type,
        ), {
            'filter': current_filter,
            'filter_options': filter_options,
            'paginator': paginator,
            'current_page': current_page,
            'sorters': sorters,
        })


def _render_filters(request, category_id):
    category = Category.objects.get(pk=category_id)
    filters = Filter.objects.filter(category=category).order_by('position')

    opened_filters = request.session.get('admin_opened_filters', {})
    c_filters = opened_filters.get(int(category_id), [])
    for f in filters:
        if f.pk in c_filters:
            f.prerendered_options = _render_options(request, f.pk)

    return render_to_string('admin/filter_includes/filters_list.html', {
        'category': category,
        'filters': filters,
    })


@permission_required(
    "filters.change_filter",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def filters_for_category(request, category_id):
    return HttpResponse(_render_filters(request, category_id))


@permission_required(
    "filters.change_filteroption",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def option_list_for_filter(request):
    print request.GET
    if 'filter_id' in request.GET:
        filter_id = request.GET['filter_id']
        if 'json_result' in request.GET:
            return HttpResponse(simplejson.dumps({
                "html": [[
                    "#filter-options-wrapper-%s" % filter_id,
                    _render_options(request, filter_id)]]
            },
                cls=LazyEncoder
            ))
        else:
            return HttpResponse(_render_options(request, int(filter_id)))
    else:
        return HttpResponse(_(u'Filter is is not specified.'))


def options_for_filter(request, filter_id):
    if request.user.has_perm('filters.change_filter'):
        is_preload = int(request.GET.get('preload', '0')) > 0
        if is_preload:
            opened_filters = request.session.get('admin_opened_filters', {})
            current_filter = Filter.objects.get(pk=filter_id)
            category_id = current_filter.category_id
            if category_id in opened_filters:
                c_filters = opened_filters[category_id]
            else:
                c_filters = []

            if current_filter.pk not in c_filters:
                c_filters.append(current_filter.pk)
                opened_filters[category_id] = c_filters

            request.session['admin_opened_filters'] = opened_filters

        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % filter_id,
                _render_options(request, filter_id)]]
        },
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % filter_id,
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


def close_options_for_filter(request, filter_id):
    opened_filters = request.session.get('admin_opened_filters', {})
    current_filter = Filter.objects.get(pk=filter_id)
    category_id = current_filter.category_id
    if category_id in opened_filters:
        c_filters = opened_filters[category_id]
    else:
        c_filters = []

    if current_filter.pk in c_filters:
        c_filters.remove(current_filter.pk)
        opened_filters[category_id] = c_filters

    request.session['admin_opened_filters'] = opened_filters
    result = simplejson.dumps({
        "success": True,
    },
        cls=LazyEncoder
    )
    return HttpResponse(result)


def move_filter(request, filter_id):
    """
    Moves a filter up/down in portlet.

    **Parameters:**

        filter_id
            The id of filter which should be moved.

    **Query String:**

        direction
            The direction to which the portlet should be moved. One of 0 (up)
            or 1 (down).
    """
    if request.user.has_perm('filters.change_filter'):
        try:
            f = Filter.objects.get(pk=filter_id)
        except Filter.DoesNotExist:
            return HttpResponse(_(u'Filter does not exist.'))

        category_id = f.category_id
        direction = request.GET.get("direction", "0")
        if direction == "1":
            f.position += 15
        else:
            f.position -= 15
            if f.position < 0:
                f.position = 10
        f.save()

        update_filter_positions(f.category)
        invalidate_filter_cache(category_slug=f.category.slug)

        result = simplejson.dumps({
            "html": [[
                "#filters-list-wrapper",
                _render_filters(request, category_id)]]
        },
            cls=LazyEncoder
        )
        if request.user.is_authenticated:
            LogEntry.objects.log_action(
                user_id=request.user.pk,
                content_type_id=ContentType.objects.get_for_model(
                    f).pk,
                object_id=f.pk,
                object_repr=force_unicode(f),
                action_flag=CHANGE,
                change_message=_("Filter was moved")
            )
    else:
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % filter_id,
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


def delete_filters_from_category(request, category_id):
    if request.user.has_perm('filters.delete_filter'):
        category = Category.objects.get(pk=category_id)
        filters = Filter.objects.filter(category=category)
        filters.delete()
        invalidate_filter_cache(category_slug=category.slug)
        result = simplejson.dumps({
            "html": [[
                "#filters-list-wrapper",
                _render_filters(request, category_id)]],
        },
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#filters-list-wrapper",
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


def delete_filter(request, filter_id):
    """Deletes the filter.
    """
    if request.user.has_perm('filters.delete_filter'):
        try:
            f = Filter.objects.get(pk=filter_id)
        except Filter.DoesNotExist:
            pass
        else:
            category_id = f.category_id
            f.delete()
            update_filter_positions(category_id)
            invalidate_filter_cache(category_slug=f.category.slug)
            result = simplejson.dumps({
                "html": [[
                    "#filters-list-wrapper",
                    _render_filters(request, category_id)]],
            },
                cls=LazyEncoder
            )
            if request.user.is_authenticated:
                LogEntry.objects.log_action(
                    user_id=request.user.pk,
                    content_type_id=ContentType.objects.get_for_model(
                        f).pk,
                    object_id=f.pk,
                    object_repr=force_unicode(f),
                    action_flag=CHANGE,
                    change_message=_("Filter was delete")
                )
            return HttpResponse(result)
    else:
        result = simplejson.dumps({
            "html": [[
                "#filters-list-wrapper",
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
        return HttpResponse(result)


def parse_filter(request, filter_id):
    """Parse the filter.
    """
    if request.user.has_perm('filters.change_filter'):
        try:
            f = Filter.objects.get(pk=filter_id)
        except Filter.DoesNotExist:
            pass
        else:
            category_id = f.category_id
            f.parse()
            invalidate_filter_cache(category_slug=f.category.slug)
            result = simplejson.dumps({
                "html": [[
                    "#filters-list-wrapper",
                    _render_filters(request, category_id)]],
            },
                cls=LazyEncoder
            )
            if request.user.is_authenticated:
                LogEntry.objects.log_action(
                    user_id=request.user.pk,
                    content_type_id=ContentType.objects.get_for_model(
                        f).pk,
                    object_id=f.pk,
                    object_repr=force_unicode(f),
                    action_flag=CHANGE,
                    change_message=_("Filter was parsed")
                )
            return HttpResponse(result)
    else:
        result = simplejson.dumps({
            "html": [[
                "#filters-list-wrapper",
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
        return HttpResponse(result)


def parse_filters_for_category(request, category_id):
    """Parse all filters.
    """
    if request.user.has_perm('filters.change_filter'):
        category = Category.objects.get(pk=category_id)
        filters = Filter.objects.filter(category=category)
        for f in filters:
            f.parse()
            if request.user.is_authenticated:
                LogEntry.objects.log_action(
                    user_id=request.user.pk,
                    content_type_id=ContentType.objects.get_for_model(
                        f).pk,
                    object_id=f.pk,
                    object_repr=force_unicode(f),
                    action_flag=CHANGE,
                    change_message=_("Filter was parsed")
                )
        invalidate_filter_cache(category_slug=category.slug)
        result = simplejson.dumps({
            "html": [[
                "#filters-list-wrapper",
                _render_filters(request, category_id)]],
        },
            cls=LazyEncoder
        )
        return HttpResponse(result)
    else:
        result = simplejson.dumps({
            "html": [[
                "#filters-list-wrapper",
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
        return HttpResponse(result)


def move_filter_option(request, option_id):
    """
    Moves a filter option up/down in portlet.

    **Parameters:**

        option_id
            The id of filter option which should be moved.

    **Query String:**

        direction
            The direction to which the portlet should be moved. One of 0 (up)
            or 1 (down).
    """
    try:
        o = FilterOption.objects.get(pk=option_id)
    except FilterOption.DoesNotExist:
        return ""

    if request.user.has_perm('filters.change_filteroption'):
        direction = request.GET.get("direction", "0")
        if direction == "1":
            o.position += 15
        else:
            o.position -= 15
            if o.position < 0:
                o.position = 10
        o.save()

        update_filteroption_positions(o.filter)
        invalidate_filter_cache(category_slug=o.filter.category.slug)

        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%d" % o.filter_id,
                _render_options(request, o.filter_id)]]
        },
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % o.filter_id,
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


def parse_filter_option(request, option_id):
    """Parses the filter option.
    """
    try:
        o = FilterOption.objects.get(pk=option_id)
    except FilterOption.DoesNotExist:
        return HttpResponse(_(u'Filter option does not exist.'))

    if request.user.has_perm('filters.change_filteroption'):
        o.parse()
        invalidate_filter_cache(category_slug=o.filter.category.slug)
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%d" % o.filter_id,
                _render_options(request, o.filter_id)]]},
            cls=LazyEncoder
        )
        if request.user.is_authenticated:
            LogEntry.objects.log_action(
                user_id=request.user.pk,
                content_type_id=ContentType.objects.get_for_model(
                    o).pk,
                object_id=o.pk,
                object_repr=force_unicode(o),
                action_flag=CHANGE,
                change_message=_("Filter option was parsed")
            )
    else:
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % o.filter_id,
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


def combine_filter_option(request, option_id):
    """
    Combine a filter option up/down with another one.

    **Parameters:**

        option_id
            The id of filter option which should be combined with.

    **Query String:**

        direction
            The direction to which the option should be combined. One of 0 (up)
            or 1 (down).
    """
    try:
        o = FilterOption.objects.get(pk=option_id)
    except FilterOption.DoesNotExist:
        return HttpResponse(_(u'Filter option does not exist.'))

    if request.user.has_perm('filters.change_filteroption'):
        direction = request.GET.get("direction", "0")
        if direction == "1":
            target = FilterOption.objects.get(
                filter=o.filter, position=o.position+10)
        else:
            target = FilterOption.objects.get(
                filter=o.filter, position=o.position-10)

        target.title = target.title + '+' + o.title

        if target.operator_type == 10:
            target.regex = target.regex + '|' + o.regex
        else:
            target.regex = target.regex + '\n' + o.regex

        if target.operator_type == 0:
            target.operator_type = 5
        target.save()
        o.delete()

        update_filteroption_positions(o.filter)
        invalidate_filter_cache(category_slug=o.filter.category.slug)

        return parse_filter_option(request, target.id)
    else:
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % o.filter_id,
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
        return HttpResponse(result)


def sort_filter(request, filter_id):
    """Sort options for current filter.
    """
    from ..core.utils import import_symbol
    if request.user.has_perm('filters.change_filteroption'):
        f = Filter.objects.get(pk=filter_id)
        options = FilterOption.objects.filter(filter=f)

        sort_type = request.GET.get('sort_type', 'asc')
        sort_methods = dict(
            (x for x in settings.FILTER_SORTERS if x is not None))
        sorter_class_name = sort_methods[sort_type]
        sorter_class = import_symbol(sorter_class_name)

        sorter = sorter_class()
        sorter.sort(options)

        update_filteroption_positions(f)
        if f.sorting == sort_type:
            f.sorting = ''
        else:
            f.sorting = sort_type
        f.save()
        invalidate_filter_cache(category_slug=f.category.slug)
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % filter_id,
                _render_options(request, filter_id)]],
            "close-dialog": True,
            "message": _(u"Filter options has been deleted.")},
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % filter_id,
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


def clear_filter(request, filter_id):
    """Remove all options from current filter.
    """
    if request.user.has_perm('filters.change_filter'):
        f = Filter.objects.get(pk=filter_id)
        options = FilterOption.objects.filter(filter=f)
        options.delete()
        invalidate_filter_cache(category_slug=f.category.slug)
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % filter_id,
                _render_options(request, filter_id)]],
            "close-dialog": True,
            "message": _(u"Filter options has been deleted.")},
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % filter_id,
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


def delete_filter_option(request, option_id):
    """Deletes the filter option.
    """
    try:
        o = FilterOption.objects.get(pk=option_id)
    except FilterOption.DoesNotExist:
        return HttpResponse(_(u'Filter option does not exist.'))

    if request.user.has_perm('filters.delete_filteroption'):
        o.delete()
        update_filteroption_positions(o.filter)
        invalidate_filter_cache(category_slug=o.filter.category.slug)
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % o.filter_id,
                _render_options(request, o.filter_id)]],
            "close-dialog": True,
            "message": _(u"Filter option has been deleted.")},
            cls=LazyEncoder
        )
        if request.user.is_authenticated:
            LogEntry.objects.log_action(
                user_id=request.user.pk,
                content_type_id=ContentType.objects.get_for_model(
                    o).pk,
                object_id=o.pk,
                object_repr=force_unicode(o),
                action_flag=CHANGE,
                change_message=_("Filter option was delete")
            )
    else:
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % o.filter_id,
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


def autofill_filter(request, filter_id):
    """Autofill the filter.
    """
    try:
        f = Filter.objects.get(pk=filter_id)
    except Filter.DoesNotExist:
        pass

    if request.user.has_perm('filters.change_filter'):
        category_id = f.category_id

        f.autofill()

        update_filter_positions(category_id)
        invalidate_filter_cache(category_slug=f.category.slug)
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%d" % f.pk,
                _render_options(request, f.pk)]]},
            cls=LazyEncoder
        )
        if request.user.is_authenticated:
            LogEntry.objects.log_action(
                user_id=request.user.pk,
                content_type_id=ContentType.objects.get_for_model(
                    f).pk,
                object_id=f.pk,
                object_repr=force_unicode(f),
                action_flag=CHANGE,
                change_message=_("Autofill filter")
            )
    else:
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % filter_id,
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)


@permission_required(
    "filters.change_filter",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def autoconfigure_filter(
        request, filter_id,
        template_name="admin/filter_includes/autoconfigurer.html"):
    """Form and logic to edit the filter.
    """
    import operator
    import itertools

    try:
        f = Filter.objects.get(pk=filter_id)
    except Filter.DoesNotExist:
        return ""

    result = []
    if f.special_filter == 0:   # Property
        t_values = ProductPropertyValue.objects.filter(
            property__in=f.properties.all(),
            product__categories=f.category,
            product__sub_type__in=[STANDARD_PRODUCT, VARIANT],
            product__active=True,
            product__status__is_visible=True,
        ).distinct().select_related()\
            .values('value', 'property__name')\
            .annotate(value_count=Count('value'))
        t_values = sorted(t_values, key=lambda x: x['value'])
        for id, iter in itertools.groupby(
                t_values, operator.itemgetter('value')):
            idList = list(iter)
            result.append({
                'value': id,
                'properties': set([z['property__name'] for z in idList]),
                'count': sum([z['value_count'] for z in idList]),
            })
        result = sorted(result, key=lambda x: x['count'] * -1)
    if f.special_filter == 20:   # Manufacturer
        t_values = Manufacturer.objects.filter(
            products__categories=f.category,
            products__sub_type__in=[STANDARD_PRODUCT, VARIANT],
            products__active=True,
            products__status__is_visible=True,
        ).distinct().select_related()\
            .values('name')\
            .annotate(name_count=Count('name'))
        result = [{
            'value': x['name'],
            'properties': [_(u'Manufacturer')],
            'count': x['name_count'],
        } for x in t_values]
        result = sorted(result, key=lambda x: x['count'] * -1)
    if f.special_filter == 30:   # Status
        t_values = ProductStatus.objects.filter(
            product__in=f.category.products.all(),
            product__sub_type__in=[STANDARD_PRODUCT, VARIANT],
            product__active=True,
            product__status__is_visible=True,
        ).distinct().select_related()\
            .values('name')\
            .annotate(name_count=Count('name'))
        result = [{
            'value': x['name'],
            'properties': [_(u'Status')],
            'count': x['name_count'],
        } for x in t_values]
        result = sorted(result, key=lambda x: x['count'] * -1)
    if request.user.is_authenticated:
        LogEntry.objects.log_action(
            user_id=request.user.pk,
            content_type_id=ContentType.objects.get_for_model(
                f).pk,
            object_id=f.pk,
            object_repr=force_unicode(f),
            action_flag=CHANGE,
            change_message=_("Auto configure filter")
        )

    return render_to_response(template_name, RequestContext(request, {
        'filter': f,
        'variants': result,
        'filter_actions': FILTER_ACTIONS,
        'is_popup': True,
    }))


@permission_required(
    "filters.change_filter",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def autoconfigurer_action(request, filter_id):
    """Form and logic to edit the filter.
    """
    try:
        f = Filter.objects.get(pk=filter_id)
    except Filter.DoesNotExist:
        return ""

    if request.method == "POST":
        action = request.POST.get('action', '')

        if action == 'add':
            values = request.POST.getlist('value', [])
            try:
                values.remove('select-all-values')
            except:
                pass
            for value in values:
                if getattr(settings, 'FILTER_REPLACE_DOT_COMMA', False):
                    # self.title = self.title.replace('.', '-').replace(',', '-')
                    identificator = f.template % pytils.translit.slugify(
                        value.replace('.', '-').replace(',', '-'))
                else:
                    identificator = f.template % pytils.translit.slugify(
                        value
                    )
                identificator = identificator[:40].lower()
                option = FilterOption.objects.get_or_create(
                    identificator=identificator,
                    filter=f)[0]
                # option = FilterOption(
                #     identificator='',
                #     filter=f,
                #     title=value,
                #     regex=value,
                #     operator_type=0,
                # )
                # option.filter = f
                option.title = value
                option.regex = value
                option.operator_type = 0
                option.save()
                option.parse()
                update_filteroption_positions(f)
                invalidate_filter_cache(category_slug=f.category.slug)
        elif action == 'as_one':
            values = request.POST.getlist('value', [])
            try:
                values.remove('select-all-values')
            except:
                pass
            if getattr(settings, 'FILTER_REPLACE_DOT_COMMA', False):
                # self.title = self.title.replace('.', '-').replace(',', '-')
                identificator = f.template % pytils.translit.slugify(
                    '+'.join(values)[:240].replace('.', '-').replace(',', '-'))
            else:
                identificator = f.template % pytils.translit.slugify(
                    '+'.join(values)[:240]
                )
            identificator = identificator[:40].lower()
            option = FilterOption.objects.get_or_create(
                identificator=identificator,
                filter=f)[0]
            # option = FilterOption(
            #     identificator='',
            #     filter=f,
            #     title='+'.join(values)[:240],
            #     regex='\n'.join(values),
            #     operator_type=5,
            # )
            # option.filter = f
            option.title = '+'.join(values)[:240]
            option.regex = '\n'.join(values)
            option.operator_type = 5
            option.save()
            option.parse()
            update_filteroption_positions(f)
            invalidate_filter_cache(category_slug=f.category.slug)
        elif action == 'split_for_properties':
            values = request.POST.getlist('value', [])
            try:
                values.remove('select-all-values')
            except:
                pass
            if values:
                value = values[0]
            else:
                return ''

            p_properties = f.properties.filter(
                property_values__value=value,
            ).distinct()
            for p_property in p_properties:
                if getattr(settings, 'FILTER_REPLACE_DOT_COMMA', False):
                    # self.title = self.title.replace('.', '-').replace(',', '-')
                    identificator = f.template % pytils.translit.slugify(
                        p_property.name.replace('.', '-').replace(',', '-'))
                else:
                    identificator = f.template % pytils.translit.slugify(
                        p_property.name
                    )
                identificator = identificator[:40].lower()
                option = FilterOption.objects.get_or_create(
                    identificator=identificator,
                    filter=f)[0]
                # option = FilterOption(
                #     identificator='',
                #     filter=f,
                #     title=p_property.name,
                #     regex=p_property.name+'~~*~~'+value,
                #     operator_type=15,
                # )
                # option.filter = f
                option.title = p_property.name
                option.regex = p_property.name+'~~*~~'+value
                option.operator_type = 15
                option.save()
                option.parse()
            update_filteroption_positions(f)
            invalidate_filter_cache(category_slug=f.category.slug)
        elif action == 'split_for_values':
            values = request.POST.getlist('value', [])
            try:
                values.remove('select-all-values')
            except:
                pass
            option_names = set([])
            for value in values:
                character = ''
                char_count = 0
                # Finding array delimiter strategy
                for d in ARRAY_DELIMITERS:
                    m = value.count(d)
                    if m > char_count:
                        char_count = m
                        character = d
                if character != '':
                    items = filter(
                        lambda y: y != '',
                        map(lambda x: x.strip(), value.split(character)))
                else:
                    items = [value.strip()]
                option_names.update(items)

            names = list(option_names)
            for name in names:
                if getattr(settings, 'FILTER_REPLACE_DOT_COMMA', False):
                    # self.title = self.title.replace('.', '-').replace(',', '-')
                    identificator = f.template % pytils.translit.slugify(
                        name.replace('.', '-').replace(',', '-'))
                else:
                    identificator = f.template % pytils.translit.slugify(
                        name
                    )
                identificator = identificator[:40].lower()
                option = FilterOption.objects.get_or_create(
                    identificator=identificator,
                    filter=f)[0]
                # option = FilterOption(
                #     identificator='',
                #     filter=f,
                #     title=name,
                #     regex=r'\m'+name+r'\M',  # For PostgreSQL
                #     operator_type=10,
                # )
                # option.filter = f
                option.title = name
                option.regex = r'\m'+name+r'\M'
                option.operator_type = 10
                option.save()
                option.parse()
            update_filteroption_positions(f)
            invalidate_filter_cache(category_slug=f.category.slug)

        return HttpResponse(
            '<!DOCTYPE html><html><head><title></title></head><body>'
            '<script type="text/javascript">'
            'opener.on_filter_option_popup_close(window, "%s");'
            '</script></body></html>' % filter_id)
    else:
        return autoconfigure_filter(request, filter_id)


def update_filter_positions(category):
    """Updates the filter positions for a category.

    **Parameters:**

        category
            Category which contains the filters.
    """
    for i, obj in enumerate(Filter.objects.filter(category=category)):
        obj.position = (i + 1) * 10
        obj.save()


def update_filteroption_positions(f):
    """Updates the filter option positions for a filter.

    **Parameters:**

        f
            Filter which contains the options.
    """
    for i, obj in enumerate(FilterOption.objects.filter(filter=f)):
        obj.position = (i + 1) * 10
        obj.save()


def regenerate_filteroptions(request, filter_id):
    import operator
    import itertools
    if request.user.has_perm('filters.change_filter'):
        f = Filter.objects.get(pk=filter_id)
        options = FilterOption.objects.filter(filter=f)
        options.delete()
        invalidate_filter_cache(category_slug=f.category.slug)
        result = []
        if f.special_filter == 0:   # Property
            t_values = ProductPropertyValue.objects.filter(
                property__in=f.properties.all(),
                product__categories=f.category,
                product__sub_type__in=[STANDARD_PRODUCT, VARIANT],
                product__active=True,
                product__status__is_visible=True,
            ).distinct().select_related()\
                .values('value', 'property__name')\
                .annotate(value_count=Count('value'))
            t_values = sorted(t_values, key=lambda x: x['value'])
            for id, iter in itertools.groupby(
                    t_values, operator.itemgetter('value')):
                idList = list(iter)
                result.append({
                    'value': id,
                    'properties': set([z['property__name'] for z in idList]),
                    'count': sum([z['value_count'] for z in idList]),
                })
            result = sorted(result, key=lambda x: x['count'] * -1)
        if f.special_filter == 20:   # Manufacturer
            t_values = Manufacturer.objects.filter(
                products__categories=f.category,
                products__sub_type__in=[STANDARD_PRODUCT, VARIANT],
                products__active=True,
                products__status__is_visible=True,
            ).distinct().select_related()\
                .values('name')\
                .annotate(name_count=Count('name'))
            result = [{
                'value': x['name'],
                'properties': [_(u'Manufacturer')],
                'count': x['name_count'],
            } for x in t_values]
            result = sorted(result, key=lambda x: x['count'] * -1)
        if f.special_filter == 30:   # Status
            t_values = ProductStatus.objects.filter(
                product__in=f.category.products.all(),
                product__sub_type__in=[STANDARD_PRODUCT, VARIANT],
                product__active=True,
                product__status__is_visible=True,
            ).distinct().select_related()\
                .values('name')\
                .annotate(name_count=Count('name'))
            result = [{
                'value': x['name'],
                'properties': [_(u'Status')],
                'count': x['name_count'],
            } for x in t_values]
            result = sorted(result, key=lambda x: x['count'] * -1)
        for value in result:
            if getattr(settings, 'FILTER_REPLACE_DOT_COMMA', False):
                # self.title = self.title.replace('.', '-').replace(',', '-')
                identificator = f.template % pytils.translit.slugify(
                    value['value'].replace('.', '-').replace(',', '-'))
            else:
                identificator = f.template % pytils.translit.slugify(
                    value['value']
                )
            identificator = identificator[:40].lower()
            option = FilterOption.objects.get_or_create(
                identificator=identificator,
                filter=f)[0]
            # option = FilterOption(
            #     identificator='',
            #     filter=f,
            #     title=value,
            #     regex=value,
            #     operator_type=0,
            # )
            # option.filter = f
            option.title = value['value']
            option.regex = value['value']
            option.operator_type = 0
            option.save()
            option.parse()
            update_filteroption_positions(f)
            invalidate_filter_cache(category_slug=f.category.slug)
    return HttpResponse(
        simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % filter_id,
                _render_options(request, filter_id)]],
            "close-dialog": True,
            "message": _(u"Filter options has been deleted.")},
            cls=LazyEncoder
        ))


def clear_filteroptins_without_products(request, filter_id):
    """Remove all options from current filter.
    """
    if request.user.has_perm('filters.change_filter'):
        f = Filter.objects.get(pk=filter_id)
        options = FilterOption.objects.filter(filter=f)
        for fo in options:
            if fo.products.count() == 0:
                fo.delete()
        invalidate_filter_cache(category_slug=f.category.slug)
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % filter_id,
                _render_options(request, filter_id)]],
            "close-dialog": True,
            "message": _(u"Filter options has been deleted.")},
            cls=LazyEncoder
        )
    else:
        result = simplejson.dumps({
            "html": [[
                "#filter-options-wrapper-%s" % filter_id,
                render_to_string(
                    'adminextras/ajax-forbidden.html',
                    RequestContext(request))]]
        },
            cls=LazyEncoder
        )
    return HttpResponse(result)
