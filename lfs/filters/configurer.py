# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from adminconfig.utils import BaseConfig


class FiltersConfigForm(forms.Form):
    use_new_system = forms.BooleanField(
        label=_(u'Replace dot and comma'),
        required=False
    )
    show_checked_filter_option_without_products = forms.BooleanField(
        label=_(u'Show checked filter options without products'),
        required=False
    )
    filter_ajax_loading = forms.BooleanField(
        label=_(u'Filter ajax loading'),
        required=False
    )


class FiltersConfig(BaseConfig):
    form_class = FiltersConfigForm
    block_name = 'filters'

    def __init__(self):
        super(FiltersConfig, self).__init__()

        self.default_data = {
            'FILTER_REPLACE_DOT_COMMA': False,
            'FILTER_SHOW_CHECKED_OPTIONS_WITHOUT_PRODUCTS': False,
            'FILTER_AJAX_LOADING': False,
        }

        self.option_translation_table = (
            ('FILTER_REPLACE_DOT_COMMA', 'use_new_system'),
            (
                'FILTER_SHOW_CHECKED_OPTIONS_WITHOUT_PRODUCTS',
                'show_checked_filter_option_without_products'
            ),
            ('FILTER_AJAX_LOADING', 'filter_ajax_loading')
        )
