from listeners import *
from django.conf import settings

if not hasattr(settings, 'FILTER_REPLACE_DOT_COMMA'):
    settings.FILTER_REPLACE_DOT_COMMA = False
