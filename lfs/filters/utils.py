# coding:utf-8
import logging

from django.conf import settings
from django.core.cache import cache
from lfs.catalog.models import (
    Category,
    ProductPropertyValue,
    PropertyValueIcon,
)
from ..catalog.settings import STANDARD_PRODUCT, VARIANT
from lfs.filters.models import Filter, FilterOption
from lfs.manufacturer.models import Manufacturer
from django.db.models import Count
from django.utils.translation import ugettext_lazy as _

logger = logging.getLogger('sshop')


# TODO: move to caching
def invalidate_filter_cache(category_slug):
    cache_keys = cache.get(
        '%s-productfilters-keys-%s' % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX, category_slug), [])
    cache.delete_many(cache_keys)
    cache.set(
        '%s-productfilters-keys-%s' % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX, category_slug), [])


def update_filters(category_id):
    from lfs.core.utils import import_symbol
    try:
        category = Category.objects.get(pk=category_id)
        filters = category.filter_set.all()
        for _filter in filters:
            if _filter.type == 20:  # Slider
                _filter.autofill()
            for option in _filter.filteroption_set.all():
                try:
                    option.parse()
                except:
                    pass
            if _filter.auto_update_add and _filter.type != 20:
                auto_add_options(_filter.id)

            # Sort filter options
            if _filter.sorting:
                try:
                    sort_methods = dict(
                        (x for x in settings.FILTER_SORTERS if x is not None))
                    sorter_class_name = sort_methods[_filter.sorting]
                    sorter_class = import_symbol(sorter_class_name)

                    sorter = sorter_class()
                    options = FilterOption.objects.filter(filter=_filter)
                    sorter.sort(options)
                except Exception, e:
                    logger.error(unicode(e))

        invalidate_filter_cache(category.slug)

        v_icons = PropertyValueIcon.objects.filter(category=category)
        for vi in v_icons:
            vi.parse()
    except Exception, e:
        logger.error(unicode(e))

    return True


def auto_add_options(filter_id):
    import operator
    import itertools

    f = Filter.objects.get(pk=filter_id)
    fo = FilterOption.objects.filter(filter=f)
    products = f.category.products.filter(
        active=True, status__is_visible=True).exclude(filteroption__in=fo)

    result = []
    if f.special_filter == 0:   # Property
        t_values = ProductPropertyValue.objects.filter(
            product__in=products,
            product__sub_type__in=[STANDARD_PRODUCT, VARIANT],
            property__in=f.properties.all(),
        ).distinct().select_related()\
            .values('value', 'property__name')\
            .annotate(value_count=Count('value'))

        t_values = sorted(t_values, key=lambda x: x['value'])
        for id, iter in itertools.groupby(
                t_values, operator.itemgetter('value')):
            idList = list(iter)
            result.append({
                'value': id,
                'properties': [z['property__name'] for z in idList],
                'count': sum([z['value_count'] for z in idList]),
            })
        result = sorted(result, key=lambda x: x['count'] * -1)
    if f.special_filter == 20:   # Manufacturer
        t_values = Manufacturer.objects.filter(
            products__in=products,
        ).distinct().select_related()\
            .values('name')\
            .annotate(name_count=Count('name'))
        result = [{
            'value': x['name'],
            'properties': [_(u'Manufacturer')],
            'count': x['name_count'],
        } for x in t_values]
        result = sorted(result, key=lambda x: x['count'] * -1)

    for res in result:
        if not FilterOption.objects.filter(
                title__iexact=res['value'], filter=f):
            value = res['value']
            option = FilterOption(
                identificator='',
                filter=f,
                title=value,
                regex=value,
                operator_type=0,
            )
            option.save()
            option.parse()

    update_filteroption_positions(f)


def update_filteroption_positions(f):
    """Updates the filter option positions for a filter.

    **Parameters:**

        f
            Filter which contains the options.
    """
    for i, obj in enumerate(FilterOption.objects.filter(filter=f)):
        obj.position = (i + 1) * 10
        obj.save()
