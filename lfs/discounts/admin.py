# coding: utf-8
from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import Discount


class DiscountForm(forms.ModelForm):
    class Meta:
        model = Discount


class DiscountAdmin(admin.ModelAdmin):
    list_display = ['name', 'type', 'value', 'discount_calculator', 'sku']
    search_fields = ['name']
    list_filter = ['type']

    add_fieldsets = [
        (None, {
            'fields': ['name', 'value', 'type', 'discount_calculator', 'sku']
        }),
    ]
    edit_fieldsets = [
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': ['name', 'value', 'type', 'discount_calculator', 'sku']
        }),
    ]

    add_suit_form_tabs = None
    edit_suit_form_tabs = (
        ('general', _(u'General')),
        ('criteria', _(u'Criteria')),
    )

    add_suit_form_includes = None
    edit_suit_form_includes = (
        ('admin/criteria_includes/criteria.html', 'top', 'criteria'),
    )

    class Media:
        css = {
            "all": ("/static/css/loadover.css",)
        }
        js = (
            '/static/jquery/jquery.cookie.js',
            '/static/js/sco.collapse.js',
            '/static/admin/js/ajax_links.js',
            '/static/js/loadover.js',
        )

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.suit_form_includes = self.edit_suit_form_includes
            self.suit_form_tabs = self.edit_suit_form_tabs
            self.fieldsets = self.edit_fieldsets
        else:
            self.suit_form_includes = self.add_suit_form_includes
            self.suit_form_tabs = self.add_suit_form_tabs
            self.fieldsets = self.add_fieldsets
        return DiscountForm


admin.site.register(Discount, DiscountAdmin)
