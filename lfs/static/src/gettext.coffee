window.gettext = (x) ->
  try
    return window[__locale][x] or x
  catch error
    return x
