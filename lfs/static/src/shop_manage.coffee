
# Mediator is a special object for communication between system components

class Mediator
    constructor: ->
        @channels = {}

    subscribe: (channel, fn) ->
        @channels[channel] ?= []
        @channels[channel].push 
            context: @
            callback: fn
        @

    publish: (channel, args...) ->
        if not @channels[channel] then false
        for subscription in @channels[channel]
            subscription.callback(subscription.context, args)
        @

@mediator = new Mediator

icon_click_event = ->
    objs = $('.icon-chevron-right, .icon-chevron-down')
    objs.unbind('click');
    objs.click ->
        icon = $(@)
        if icon.hasClass('icon-chevron-down')
            $(@).closest('li').attr('class', 'mjs-nestedSortable-branch mjs-nestedSortable-collapsed')
            icon.attr('class', 'icon-chevron-right pointer')
            $(@).closest('li').children('ol').each (i, el)->
                if typeof($(el).attr('id')) == "undefined"
                    $(el).remove()
        else
            id = $(@).closest('li').attr('id').replace('category_', '')
            link = $('#collapse-head-' + id).attr('data')
            if $('#inner-' + id).length == 0
                $(@).closest('li').append('<ol id="inner-'+ id + '" class="menu"></ol>')
                $('#inner-' + id).load link, null, ->
                    $('#collapse-head-' + $('#collapse_current_id')[0].value).addClass('selected')
                    mediator.publish 'category_collapsed_event'
                    
                    for a in $('.icon-chevron-right.pointer')
                        id = $(a).attr('id').replace('icon-','')
                        if $('#parents_id')[0].value.indexOf(id.toString()) > -1
                            $('#icon-' + id).click()
                    true
            $(@).closest('li').attr('class', 'mjs-nestedSortable-branch mjs-nestedSortable-extended')
            icon.attr('class', 'icon-chevron-down pointer')
            true

mediator.subscribe 'icon_click_event', icon_click_event


remove_icons = ->
    setTimeout ->
      $('.icon-chevron-down').each (i, el) ->
        if $(el.parentElement.parentElement).children('ol').length == 0
            $(el).attr('class','')
        true
    , 500
    
mediator.subscribe 'remove_icons', remove_icons
    

category_collapsed_event = ->
    mediator.publish 'icon_click_event'

    $('.collapse-link').click ->
        old_id = $('#collapse_current_id')[0].value
        new_id = $(@).attr('id').replace('collapse-head-','')
        $('.update-id').each ->
            $(@).attr('href', $(@).attr('href').replace(old_id, new_id))

        window.history.pushState(null, null, $(@).attr('data-click'));

        $.get $(@).attr('data-href'), $(@).serialize(), (data) ->
            data = $.parseJSON(data);
            $(html[0]).html(html[1]) for html in data["html"]
            $('#delete_category').attr('dialog_message', data["dialog_message"])
            update_editor()
            $('#category-tabs').tabs({ active: 0 });
            true
        $('.selected').removeClass('selected')
        $(@).addClass('selected')
        $('#collapse_current_id')[0].value = new_id
        true

mediator.subscribe 'category_collapsed_event', category_collapsed_event

modal = ->
    $(".modal-form").submit (e)->
       e.preventDefault()
       true
    $(".modal-form .btn").click ->
        data = $(".modal-form").ajaxSubmit
            url : $(".modal-form").attr("data"),
            "success" : (data) ->
                $('#sshop-modal .modal-body').html data
                $('#sshop-modal .modal-header h3').html($('div.header h3').html())
                $('div.header').remove()
                mediator.publish 'modal_show'
                false
   false

mediator.subscribe 'modal_show', modal

category_load = ->
    parents = $('#parents_id')[0].value
    $('#icon-' + parents.split(',')[0]).click()

    $(".modal-link").click ->
        $('#sshop-modal .modal-footer').addClass('hide')
        $('#sshop-modal .modal-body').load $(@).attr('href'), null, ->
            $('#sshop-modal .modal-header h3').html($('div.header h3').html())
            $('div.header').remove()
            mediator.publish 'modal_show'
            false
        $('#sshop-modal').modal 'show'
        false

mediator.subscribe 'category_load', category_load