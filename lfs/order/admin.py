# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext as _

from genericadmin.admin import GenericAdminModelAdmin

from .models import Order, OrderItem, OrderStatus
from .forms import OrderForm, OrderItemForm


class OrderItemInlines(admin.StackedInline):
    model = OrderItem
    form = OrderItemForm
    extra = 0
    fields = [
        'product',
        'product_name',
        'product_sku',
        'product_price',
        'product_amount',
        'price',
    ]


class OrderAdmin(admin.ModelAdmin):
    list_display = [
        'number',
        'created',
        'customer_email',
        'customer_phone',
        'price',
        'status',
        'export_status',
    ]
    inlines = [OrderItemInlines]
    change_form_template = 'admin/order_change_form.html'
    date_hierarchy = 'created'
    list_filter = ('status', 'export_status')
    search_fields = [
        'number',
        'customer_email',
        'customer_phone',
        # 'price',
    ]
    form = OrderForm
    readonly_fields = ['uuid', 'created', 'state_modified']
    fieldsets = (
        (None, {
            'fields': [
                'number',
                'price',
                'uuid',
                'created',
                'export_status',
                'status',
                'state_modified'],
        }),
        (_(u'Customer'), {
            'classes': ('collapse',),
            'fields': [
                'customer',
                'customer_email',
                'customer_phone',
                'user',
                'session',
                'customer_data',
            ],
        }),
        (_(u'Shipping'), {
            'classes': ('collapse',),
            'fields': [
                'shipping_method',
                'shipping_price',
                'shipping_data',
            ],
        }),
        (_(u'Payment'), {
            'classes': ('collapse',),
            'fields': [
                'payment_method',
                'payment_price',
                'payment_data',
                'pay_link',
            ],
        }),
        (_(u'Voucher'), {
            'classes': ('collapse',),
            'fields': ['voucher_number', 'voucher_price'],
        }),
        (_(u'Extra'), {
            'classes': ('collapse',),
            'fields': ['message', 'extra_data'],
        }),

    )


admin.site.register(Order, OrderAdmin)


class OrderStatusAdmin(GenericAdminModelAdmin):
    prepopulated_fields = {"identifier": ("name", )}
    list_display = [
        'name',
        'identifier',
    ]

admin.site.register(OrderStatus, OrderStatusAdmin)
