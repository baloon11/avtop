# coding: utf-8
from django import forms
import autocomplete_light
from codemirror.widgets import CodeMirrorTextarea


class OrderForm(forms.ModelForm):
    class Meta:
        widgets = {
            'customer':
            autocomplete_light.ChoiceWidget('CustomerAutocomplete'),
            'user':
            autocomplete_light.ChoiceWidget('UserAutocomplete'),
            'customer_data': CodeMirrorTextarea(mode='javascript', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'shipping_data': CodeMirrorTextarea(mode='javascript', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'payment_data': CodeMirrorTextarea(mode='javascript', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'extra_data': CodeMirrorTextarea(mode='javascript', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
        }


class OrderItemForm(forms.ModelForm):
    class Meta:
        widgets = {
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete'),
        }
