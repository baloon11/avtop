# coding: utf-8
from django.conf.urls.defaults import *

# These url patterns use for admin interface
urlpatterns = patterns(
    'lfs.order.views',
    url(
        r'^order-preview/(?P<order_id>[-\d]*)/$',
        'order_preview', name="admin_order_preview"),
)
