# coding: utf-8
import uuid
# import re

from django.conf import settings
from django.db import models
from django.utils.timezone import localtime
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField

from ..catalog.models import Product
from ..shipping.models import ShippingMethod
from ..payment.models import PaymentMethod
from ..customer.models import Customer
from .settings import ORDER_STATES
from .settings import SUBMITTED
from ..payment.utils import get_pay_link


ORDER_EXPORT_STATUSES = (
    (0, _(u'No')),
    (5, _(u'Processing')),
    (10, _(u'Success')),
)


def get_unique_id_str():
    return str(uuid.uuid4())


def get_default_status():
    default = getattr(settings, 'DEFAULT_ORDER_STATUS', None)
    if default is None:
        if OrderStatus.objects.count() == 0:
            o, created = OrderStatus.objects.get_or_create(
                name='', identifier="submitted")
            return o
        else:
            return OrderStatus.objects.all()[0]
    else:
        return OrderStatus.objects.get(pk=default)


class OrderStatus(models.Model):
    """
    Current status of the order.

    name
        Status name.

    identifier
        The identifier of status

    """
    name = models.CharField(_(u"Name"), max_length=70)
    identifier = models.SlugField(_(u'Identifier'))

    class Meta:
        verbose_name = _(u'Order status')
        verbose_name_plural = _(u'Order statuses')

    def __unicode__(self):
        return self.name


class Order(models.Model):
    """An order is created when products have been sold.

    **Parameters**:

    number
        The unique order number of the order which is the reference for the
        customer.

    voucher_number, voucher_value, voucher_tax

        Storing this information here assures that we have it all time, even
        when the involved voucher will be deleted.

    requested_delivery_date
        a buyer requested delivery date (e.g. for a florist to deliver flowers
        on a specific date)

    pay_link
        A link to re-pay the order (e.g. for PayPal)

    """
    number = models.CharField(_(u'Number'), max_length=30)
    created = models.DateTimeField(_(u"Created"), auto_now_add=True)
    customer = models.ForeignKey(Customer, blank=True, null=True)

    customer_email = models.CharField(
        _(u"Email"), max_length=50, blank=True, null=True)
    customer_phone = models.CharField(
        _(u"Phone"), max_length=50, blank=True, null=True)

    customer_data = JSONField(_(u'Customer data'), default=dict(), blank=True)

    price = models.FloatField(_(u"Price"), default=0.0)

    shipping_data = JSONField(_(u'Shipping data'), default=dict(), blank=True)
    shipping_method = models.ForeignKey(
        ShippingMethod,
        verbose_name=_(u"Shipping Method"), blank=True, null=True)
    shipping_price = models.FloatField(_(u"Shipping Price"), default=0.0)

    payment_data = JSONField(_(u'Payment data'), default=dict(), blank=True)

    payment_method = models.ForeignKey(
        PaymentMethod,
        verbose_name=_(u"Payment Method"), blank=True, null=True)
    payment_price = models.FloatField(_(u"Payment Price"), default=0.0)
    voucher_number = models.CharField(
        _(u"Voucher number"), blank=True, max_length=100)
    voucher_price = models.FloatField(_(u"Voucher value"), default=0.0)

    message = models.TextField(_(u"Message"), blank=True)
    pay_link = models.TextField(_(u"pay_link"), blank=True)

    uuid = models.CharField(
        max_length=50, editable=False, unique=True, default=get_unique_id_str)

    extra_data = JSONField(_(u'Extra data'), default=dict(), blank=True)

    export_status = models.IntegerField(
        _(u"Export status"), choices=ORDER_EXPORT_STATUSES, default=0)

    # TODO: remove null in 0.3.2
    status = models.ForeignKey(
        OrderStatus, verbose_name=_(u'State'), null=True,
        default=get_default_status)
    # DEPRECATED in 0.3.2
    state = models.PositiveSmallIntegerField(
        _(u"State"), choices=ORDER_STATES, default=SUBMITTED)
    state_modified = models.DateTimeField(
        _(u"State modified"), auto_now_add=True)

    # DEPRECATED in 0.3.1
    user = models.ForeignKey(
        User, verbose_name=_(u"User"), blank=True, null=True)
    # DEPRECATED in 0.3.1
    session = models.CharField(_(u"Session"), blank=True, max_length=100)

    # DEPRECATED in 0.3.1
    shipping_info = models.TextField(_(u"Shipping info"), blank=True)
    # DEPRECATED in 0.3.1
    payment_info = models.TextField(_(u"Payment info"), blank=True)

    # DEPRECATED in 0.3.1
    requested_delivery_date = models.DateTimeField(
        _(u"Delivery Date"), null=True, blank=True)

    class Meta:
        ordering = ("-created", )
        verbose_name = _(u'Order')
        verbose_name_plural = _(u'Orders')

    def __unicode__(self):
        return localtime(self.created).strftime("%x %X")

    def save(self, *args, **kwargs):
        from ..fields.utils import get_field_titles
        from django.template.loader import render_to_string

        shipping_titles = get_field_titles(self.shipping_method)
        shipping = []
        for item in self.shipping_data:
            shipping.append({
                'identificator': item,
                'title': shipping_titles.get(item),
                'value': self.shipping_data[item],
            })

        self.shipping_info = render_to_string(
            'lfs/order/shipping_info.html',
            {
            'fields': shipping,
            'shipping_method': self.shipping_method,
            })

        payment_titles = get_field_titles(self.payment_method)
        payment = []
        for item in self.payment_data:
            payment.append({
                'identificator': item,
                'title': payment_titles.get(item),
                'value': self.payment_data[item],
            })

        self.payment_info = render_to_string(
            'lfs/order/payment_info.html',
            {
            'fields': payment,
            'payment_method': self.payment_method,
            })
        super(Order, self).save(*args, **kwargs)

    def get_customer_data(self):
        from ..fields.utils import get_field_titles
        from ..core.utils import get_default_shop
        customer_items = []
        shop = get_default_shop()
        customer_fields = shop.checkout_form_fields.split(',')
        field_titles = get_field_titles(shop)
        for item in customer_fields:
            customer_items.append({
                'identificator': item,
                'title': field_titles.get(item),
                'value': self.customer_data.get(item),
            })
        return customer_items

    def get_full_customer_data(self):
        from ..fields.utils import get_field_titles
        from ..core.utils import get_default_shop
        customer_items = []
        shop = get_default_shop()
        customer_info = self.customer.get_info()

        address_fields = shop.address_form_fields.split(',')
        field_titles = get_field_titles(shop)
        for item in field_titles.keys():
            value = self.customer_data.get(item)
            if value is not None and item not in address_fields:
                customer_items.append({
                    'identificator': item,
                    'title': field_titles.get(item),
                    'value': value,
                    'original_value': customer_info.get(item),
                })
        return customer_items

    def get_order_data(self):
        from ..fields.utils import get_field_titles
        from ..core.utils import get_default_shop
        order_items = []
        shop = get_default_shop()
        order_fields = shop.order_form_fields.split(',')
        field_titles = get_field_titles(shop)
        for item in order_fields:
            order_items.append({
                'identificator': item,
                'title': field_titles.get(item),
                'value': self.extra_data.get(item),
            })
        return order_items

    def get_pay_link(self, request):
        """Returns a pay link for the selected payment method.
        """
        return get_pay_link(request, self.payment_method, self)

    def get_name(self):
        order_name = ""
        for order_item in self.items.all():
            if order_item.product is not None:
                order_name = order_name + order_item.product.get_name() + ", "

        order_name.strip(', ')
        return order_name

    def get_formatted_phone_number(self):
        """ Get formatted phone number like "+38(055)912-12-34"
        """
        templates = getattr(
            settings,
            'CUSTOMER_PHONE_TEMPLATE',
            '+%s%s(%s%s%s)%s%s%s-%s%s-%s%s'
        ).split(',')

        for template in templates:
            try:
                formatted_phone = template % tuple(self.customer_phone)
                return formatted_phone
            except TypeError:
                pass

        return self.customer_phone

    def get_shipping_info(self):
        from ..fields.utils import get_field_titles
        field_titles = get_field_titles(self.shipping_method)
        values = []
        for obj in self.shipping_data:
            values.append(
                '%s: %s' % (field_titles[obj], self.shipping_data[obj])
            )
        return ', '.join(values)

    def get_state_display(self):
        return self.status.name


class OrderItem(models.Model):
    """An order items holds the sold product, its amount and some other
    relevant product values like the price at the time the product has
    been sold.
    """
    order = models.ForeignKey(Order, related_name="items")
    price = models.FloatField(_(u"Price"), default=0.0)
    # A optional reference to the origin product. This is optional in case the
    # product has been deleted. TODO: Decide: Are products able to be delete?
    product = models.ForeignKey(
        Product,
        verbose_name=_(u'Product'),
        blank=True,
        null=True, on_delete=models.SET_NULL)

    # Values of the product at the time the orders has been created
    product_amount = models.FloatField(
        _(u"Product quantity"), blank=True, null=True)
    product_sku = models.CharField(
        _(u"Product SKU"), blank=True, max_length=100)
    product_name = models.CharField(
        _(u"Product name"), blank=True, max_length=300)
    product_price = models.FloatField(
        _(u"Product price"), default=0.0)

    class Meta:
        verbose_name = _(u'Order item')
        verbose_name_plural = _(u'Order items')

    def __unicode__(self):
        return "%s" % self.product_name

    def save(self, *args, **kwargs):
        if self.product_amount and self.product_price:
            self.order.price -= self.price
            self.price = self.product_amount * self.product_price
            self.order.price += self.price
            self.order.save()
        super(OrderItem, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.order.price -= self.price
        self.order.save()
        super(OrderItem, self).delete(*args, **kwargs)

    @property
    def amount(self):
        return self.product.get_clean_quantity(self.product_amount)
