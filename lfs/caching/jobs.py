# coding: utf-8


def update_category_cache_job(*category_ids, **kwargs):
    from ..catalog.models import Category
    from .listeners import update_category_cache
    categories = Category.objects.filter(id__in=category_ids).distinct()
    for category in categories:
        update_category_cache(category)

update_category_cache_job.allow_concatenate_args = True


def update_product_cache_job(*product_ids, **kwargs):
    from ..catalog.models import Product
    from .listeners import update_product_cache
    products = Product.objects.filter(id__in=product_ids).distinct()
    for product in products:
        update_product_cache(product)

update_product_cache_job.allow_concatenate_args = True
