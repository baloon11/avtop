# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes import generic
from django.conf import settings
from ..catalog.models import Product, Image



class Topseller(models.Model):
    """Selected products are in any case among topsellers.
    """
    product = models.ForeignKey(Product, verbose_name=_(u"Product"))
    position = models.PositiveSmallIntegerField(_(u"Position"), default=1)

    class Meta:
        verbose_name = _(u'Top seller')
        verbose_name_plural = _(u'Top sellers')
        ordering = ["position"]

    def __unicode__(self):
        return "%s (%s)" % (self.product.name, self.position)


class ProductSales(models.Model):
    """Stores totals sales per product.
    """
    product = models.ForeignKey(Product, verbose_name=_(u"Product"))
    sales = models.IntegerField(_(u"sales"), default=0)

    class Meta:
        verbose_name = _(u'Sales of product')
        verbose_name_plural = _(u'Sales of products')

    def __unicode__(self):
        return self.product.name


class FeaturedProduct(models.Model):
    """Featured products are manually selected by the shop owner
    """
    product = models.ForeignKey(Product, verbose_name=_(u"Product"))
    position = models.PositiveSmallIntegerField(_(u"Position"), default=1)
    active = models.BooleanField(_(u"Active"), default=True)

    class Meta:
        verbose_name = _(u'Featured product')
        verbose_name_plural = _(u'Featured products')
        ordering = ["position"]

    def __unicode__(self):
        return "%s (%s)" % (self.product.name, self.position)


class ProductList(models.Model):
    name = models.CharField(_(u'List name'), max_length=100)

    position = models.PositiveSmallIntegerField(_(u"Position"), default=1)

    identifier = models.CharField(
        _(u'Identifier'), max_length=100, default='')  # TODO: must be unique
    images = generic.GenericRelation(
        Image, verbose_name=_(u"Images"),
        object_id_field="content_id", content_type_field="content_type")

    class Meta:
        verbose_name = _(u'Product list')
        verbose_name_plural = _(u'Product lists')

    def __unicode__(self):
        return self.name

    def get_image(self):
        # Return Top image in position: 10
        try:
            return self.images.all().order_by('position')[0]
        except:
            return None


class ProductListItem(models.Model):
    product_list = models.ForeignKey(
        ProductList, verbose_name=_(u'Product list'))
    product = models.ForeignKey(Product, verbose_name=_(u'Product'))
    position = models.PositiveSmallIntegerField(_(u"Position"), default=1)

    class Meta:
        verbose_name = _(u'Product from list')
        verbose_name_plural = _(u'Products form list')
        ordering = ['position']

    def __unicode__(self):
        return self.product.name
