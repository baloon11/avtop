# coding: utf-8


def update_product_sales_job(**kwargs):
    from .utils import calculate_product_sales
    calculate_product_sales()
