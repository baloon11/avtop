# coding: utf-8
import logging

from .models import ShippingMethod


logger = logging.getLogger('sshop')


def update_to_valid_shipping_method(request, customer, save=False):
    """After this has been called the given customer has a valid shipping
    method in any case.
    """
    valid_sms = get_valid_shipping_methods(request)

    if customer.selected_shipping_method not in valid_sms:
        customer.selected_shipping_method = \
            get_default_shipping_method(request)
        if save:
            customer.save()


def get_valid_shipping_methods(request, product=None):
    """Returns a list of all valid shipping methods for the passed request.
    """
    from ..criteria.utils import is_valid as _is_valid
    result = []
    for sm in ShippingMethod.objects.filter(active=True):
        if _is_valid(request, sm, product):
            result.append(sm)
    return result


def get_first_valid_shipping_method(request, product=None):
    """Returns the valid shipping method with the highest priority.
    """
    from ..criteria.utils import get_first_valid
    active_shipping_methods = ShippingMethod.objects.filter(active=True)
    return get_first_valid(request, active_shipping_methods, product)


def get_default_shipping_method(request):
    """Returns the default shipping method for the passed request.

    At the moment is this the first valid shipping method, but this could be
    made more explicit in future.
    """
    from ..criteria.utils import get_first_valid
    active_shipping_methods = ShippingMethod.objects.filter(active=True)
    return get_first_valid(request, active_shipping_methods)


def get_selected_shipping_method(request):
    """Returns the selected shipping method for the passed request.

    This could either be an explicitely selected shipping method of the current
    user or the default shipping method.
    """
    from ..customer.utils import get_customer
    customer = get_customer(request)
    if customer:
        update_to_valid_shipping_method(request, customer)
    if customer and customer.selected_shipping_method:
        return customer.selected_shipping_method
    else:
        return get_default_shipping_method(request)


def get_shipping_costs(request, shipping_method):
    """Returns a dictionary with the shipping price and tax for the passed
    request and shipping method.

    The format of the dictionary is: {"price" : 0.0, "tax" : 0.0}
    """
    from ..criteria.utils import get_first_valid
    if shipping_method is None:
        return {
            "price": 0.0,
        }

    price = get_first_valid(
        request,
        shipping_method.prices.all())  # TODO: check active

    if price is None:
        price = shipping_method.get_price(request)
        return {
            "price": price,
        }
    else:
        return {
            "price": price.price,
        }
