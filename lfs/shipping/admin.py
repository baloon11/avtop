# coding: utf-8
from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from suit.admin import SortableModelAdmin, SortableTabularInline
from suit_ckeditor.widgets import CKEditorWidget

from .models import ShippingMethod
from .models import ShippingMethodPrice

from codemirror.widgets import CodeMirrorTextarea
from adminextras.admin import LinkedInline


class ShippingMethodForm(forms.ModelForm):
    class Meta:
        model = ShippingMethod
        widgets = {
            'description': CKEditorWidget(),
            'note': CKEditorWidget(),
            'extra': CodeMirrorTextarea(config={'fixedGutter': True}),
        }

class ShippingMethodPriceForm(forms.ModelForm):
    class Meta:
        model = ShippingMethodPrice

class ShippingMethodPriceInline(SortableTabularInline, LinkedInline):
    model = ShippingMethodPrice
    suit_classes = 'suit-tab suit-tab-prices'
    sortable = 'priority'
    fields = ('price',)
    extra = 0


class ShippingMethodAdmin(SortableModelAdmin, admin.ModelAdmin):
    list_display = ['name', 'active']
    search_fields = ['name']
    sortable = 'priority'

    add_suit_form_tabs = None
    edit_suit_form_tabs = (
        ('general', _(u'General')),
        ('criteria', _(u'Criteria')),
        ('fields', _(u'Fields')),
        ('prices', _(u'Prices')),
    )

    add_inlines = []
    edit_inlines = [ShippingMethodPriceInline]

    add_fieldsets = [
        (None, {
            'fields': [
                'active', 'priority', 'name', 'image', 'description', 'note',
                'price', 'delivery_time', 'price_calculator']
        }),
    ]
    edit_fieldsets = [
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': [
                'active', 'priority', 'name', 'image', 'description', 'note',
                'price', 'delivery_time', 'price_calculator', 'extra']
        }),
    ]

    add_suit_form_includes = None
    edit_suit_form_includes = (
        ('admin/fields_includes/fields.html', 'top', 'fields'),
        ('admin/criteria_includes/criteria.html', 'top', 'criteria'),
    )

    class Media:
        css = {
            "all": ("/static/css/loadover.css",)
        }
        js = (
            '/static/jquery/jquery.cookie.js',
            '/static/js/sco.collapse.js',
            '/static/admin/js/ajax_links.js',
            '/static/js/loadover.js',
        )

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.suit_form_includes = self.edit_suit_form_includes
            self.suit_form_tabs = self.edit_suit_form_tabs
            self.fieldsets = self.edit_fieldsets
            self.inlines = self.edit_inlines
        else:
            self.suit_form_includes = self.add_suit_form_includes
            self.suit_form_tabs = self.add_suit_form_tabs
            self.fieldsets = self.add_fieldsets
            self.inlines = self.add_inlines
        return ShippingMethodForm

class ShippingMethodPriceAdmin(admin.ModelAdmin):
    list_display = ('price', 'shipping_method', 'active', 'priority')

    add_fieldsets = [
        (None, {
            'fields': ['active', 'priority', 'price', 'shipping_method', ],
        }),
    ]
    edit_fieldsets = [
        (None, {
            'classes': ('suit-tab suit-tab-price',),
            'fields': ['active', 'priority', 'price', 'shipping_method'],
        }),
    ]

    add_suit_form_tabs = None
    edit_suit_form_tabs = (
        ('price', _(u'Price')),
        ('criteria', _(u'Criteria')),
    )

    add_suit_form_includes = None
    edit_suit_form_includes = (
        ('admin/criteria_includes/criteria.html', 'top', 'criteria'),
    )

    class Media:
        css = {
            "all": ("/static/css/loadover.css",)
        }
        js = (
            '/static/jquery/jquery.cookie.js',
            '/static/admin/js/ajax_links.js',
            '/static/js/loadover.js',
        )

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.suit_form_includes = self.edit_suit_form_includes
            self.suit_form_tabs = self.edit_suit_form_tabs
            self.fieldsets = self.edit_fieldsets
        else:
            self.suit_form_includes = self.add_suit_form_includes
            self.suit_form_tabs = self.add_suit_form_tabs
            self.fieldsets = self.add_fieldsets
        return ShippingMethodPriceForm


admin.site.register(ShippingMethod, ShippingMethodAdmin)
admin.site.register(ShippingMethodPrice, ShippingMethodPriceAdmin)
