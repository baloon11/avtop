# coding: utf-8
import json

from django.conf import settings
from django.utils import simplejson
from django.http import HttpResponse
from django.template import RequestContext
from django.template.loader import render_to_string

# from portlets.models import PortletAssignment
from ..caching.utils import lfs_get_object
from ..catalog.models import Product


def ajax_recent_products_render(request):
    # id = request.GET['id']
    slug_not_to_display = request.GET.get('slug_not_to_display', None)
    title = request.GET.get('title', '')
    slot_name = request.GET.get('slot_name', None)
    # obj = PortletAssignment.objects.get(pk=id)
    limit = settings.LFS_RECENT_PRODUCTS_LIMIT
    if slug_not_to_display:
        limit += 1
    products = []
    for slug in request.session.get("RECENT_PRODUCTS", [])[:limit]:
        if slug == slug_not_to_display:
            continue
        product = lfs_get_object(Product, slug=slug)
        if product:
            products.append(product)
    html = render_to_string(
        "lfs/portlets/recent_products.html",
        RequestContext(request, {
            "ajax": True,
            "title": title,
            "products": products,
            "slot_name": slot_name,
        }))
    response = simplejson.dumps({'html': html})
    return HttpResponse(response)
