# coding: utf-8
from django.utils.translation import ugettext_lazy as _
import autocomplete_light

from django.contrib.auth.models import User
from .models import (
    Customer,
)


class UserAutocomplete(autocomplete_light.AutocompleteModelTemplate):
    search_fields = ['username']
    autocomplete_js_attributes = {
        'placeholder': _(u'Type the username...'),
    }

autocomplete_light.register(User, UserAutocomplete)


class CustomerAutocomplete(autocomplete_light.AutocompleteModelTemplate):
    search_fields = ['user__username', 'session']
    autocomplete_js_attributes = {
        'placeholder': _(u'Type the customer username or session...'),
    }

autocomplete_light.register(Customer, CustomerAutocomplete)
