# coding: utf-8
from django import forms
from django.forms.widgets import CheckboxInput
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _


class AcceptRulesInput(CheckboxInput):
    def __init__(
            self,
            custom_text=_(u'I confirm terms and conditions'),
            *args, **kwargs):
        super(AcceptRulesInput, self).__init__(*args, **kwargs)
        self.custom_text = custom_text

    def render(self, name='accept_rules', value='', attrs=None):
        html_code = u"""
        <input type="checkbox" name="{name}" id="{id}" />
        {custom_text}"""\
        .format(
            name='require_accept_rules',
            id='require_accept_rules', custom_text=self.custom_text)

        return mark_safe(html_code)


class AcceptRulesField(forms.Field):
    """ Custom field for "Accept rules" with custom text
    """
    def __init__(self, custom_text, *args, **kwargs):
        widget = AcceptRulesInput(custom_text)
        super(AcceptRulesField, self).__init__(widget=widget, *args, **kwargs)

    def clean(self, value):
        if value:
            return value
        else:
            raise forms.ValidationError(
                _(u'Please confirm our terms and conditions'))
