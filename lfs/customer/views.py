# coding: utf-8
import datetime
from urlparse import urlparse

from django.conf import settings
from django.db.models import Q
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User, UserManager
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _
from django.http import Http404, HttpResponseForbidden
from django.utils import timezone
from django import forms

from ..core.utils import (
    get_default_shop,
    set_message_cookie,
)
from .forms import (
    EmailForm,
    RegisterForm,
    EditAccountForm,
    PhoneForm,
    AddressForm,
)
from .models import (
    Customer,
    Address,
    Phone,
    EmailAddress,
)
from ..order.models import Order


def login(request, template_name="lfs/customer/login.html"):
    """Custom view to login or register/login a user.

    The reason to use a custom login method are:

      * validate checkout type
      * integration of register and login form

    It uses Django's standard AuthenticationForm, though.
    """
    shop = get_default_shop(request)

    # If only anonymous checkout is allowed this view doesn't exists :)
    if not getattr(settings, 'CUSTOMER_AUTH_ENABLED', True):
        raise Http404

    # print request.get_full_path()

    # Using Djangos default AuthenticationForm
    login_form = AuthenticationForm()
    login_form.fields["username"].label = \
        getattr(settings, 'CUSTOMER_AUTH_LABEL', _(u'Username'))
    login_form.fields["username"].widget = forms.TextInput(
        attrs={
            'placeholder':
            getattr(settings, 'CUSTOMER_AUTH_PLACEHOLDER', '')
        })
    register_form = RegisterForm(request=request)
    if settings.CUSTOMER_AUTH_BY == 'phone':
        login_form.fields["username"].help_text = \
            getattr(settings, 'CUSTOMER_PHONE_HELP_TEXT', '')
        register_form.fields["username"].help_text = \
            getattr(settings, 'CUSTOMER_PHONE_HELP_TEXT', '')

    if request.POST.get("action") == "login":
        login_form = AuthenticationForm(data=request.POST)
        login_form.fields["username"].label = \
            getattr(settings, 'CUSTOMER_AUTH_LABEL', _(u'Username'))
        login_form.fields["username"].widget = forms.TextInput(
            attrs={
                'placeholder':
                getattr(settings, 'CUSTOMER_AUTH_PLACEHOLDER', '')
            })

        if login_form.is_valid():
            redirect_to = request.POST.get("next")
            # Light security check -- make sure redirect_to isn't garbage.
            if not redirect_to or '//' in redirect_to or ' ' in redirect_to:
                redirect_to = getattr(settings, 'LOGIN_REDIRECT_URL', '/')

            from django.contrib.auth import login
            login(request, login_form.get_user())
            messages.success(request, _(u"You have been logged in."))
            return HttpResponseRedirect(redirect_to)

    elif request.POST.get("action") == "register":
        register_form = RegisterForm(request=request, data=request.POST)
        if register_form.is_valid():
            username = register_form.data.get('username', None)
            phone = register_form.data.get('phone', None)
            email = register_form.data.get('email', None)
            password = register_form.data.get("password_1")
            first_name = register_form.data.get('first_name', '')
            last_name = register_form.data.get('last_name', '')
            #  Create user
            if getattr(settings, 'CUSTOMER_AUTH_BY', 'email') == 'email':
                email = username
            elif getattr(settings, 'CUSTOMER_AUTH_BY', 'email') == 'phone':
                phone = username

            now = timezone.now()
            if not username:
                raise ValueError('The given username must be set')
            email = UserManager.normalize_email(email)
            user = User(username=username, email=email,
                        is_staff=False, is_active=True, is_superuser=False,
                        last_login=now, date_joined=now,
                        first_name=first_name, last_name=last_name)

            user.set_password(password)
            user.save()
            # At this moment post_save signal creates the Customer

            address_fields = shop.address_form_fields.split(',')
            reg_fields = shop.registration_form_fields.split(',')

            if Customer.objects.filter(user=user).exists():
                customer = Customer.objects.get(user=user)
                register_form_data = {}
                for item in reg_fields:
                    if item not in address_fields:
                        value = register_form.cleaned_data.get(item)
                        if value:
                            register_form_data.update({item: value})

                customer.set_info(register_form_data)
                customer.save()

                address_data = {}
                for item in reg_fields:
                    if item in address_fields:
                        value = register_form.cleaned_data.get(item)
                        if value:
                            address_data.update({item: value})

                if address_data:
                    Address.objects.create(
                        customer=customer,
                        fields=address_data,
                        default=True)

                if phone:
                    if getattr(settings, 'REGISTRATION_VALIDATE_PHONE', True):
                        import phonenumbers
                        phone_obj = phonenumbers.parse(
                            phone,
                            getattr(
                                settings, 'CUSTOMER_PHONE_NUMBER_REGION', 'UA'),
                        )
                        phone_str = '%s%s' % (
                            phone_obj.country_code,
                            phone_obj.national_number,
                        )
                        phone_type = phonenumbers.number_type(phone_obj)
                        p_type = 0
                        if phone_type == 0:
                            p_type = 5  # Landline
                        elif phone_type == 1:
                            p_type = 0  # Mobile
                    else:
                        phone_str = str(phone)
                        p_type = 0

                    Phone.objects.create(
                        customer=customer,
                        number=phone_str,
                        default=True,
                        type=p_type,
                    )

                if email:
                    EmailAddress.objects.create(
                        customer=customer,
                        email=email,
                        default=True,
                        verified=False,
                    )

            # Log in user
            from django.contrib.auth import authenticate
            from django.contrib.auth import login
            user = authenticate(username=username, password=password)
            login(request, user)
            redirect_to = request.POST.get("next")
            if not redirect_to or '//' in redirect_to \
                    or ' ' in redirect_to:
                redirect_to = getattr(settings, 'REGISTER_REDIRECT_URL', '/')

            messages.success(
                request, _(u"You have been registered and logged in."))
            return HttpResponseRedirect(redirect_to)

    # Get next_url
    next_url = request.REQUEST.get("next")
    if next_url is None:
        next_url = request.META.get("HTTP_REFERER")
    if next_url is None:
        next_url = getattr(settings, 'LOGIN_REDIRECT_URL', '/')

    # Get just the path of the url.
    # See django.contrib.auth.views.login for more
    next_url = urlparse(next_url)
    next_url = next_url[2]

    try:
        login_form_errors = login_form.errors["__all__"]
    except KeyError:
        login_form_errors = None

    return render_to_response(template_name, RequestContext(request, {
        "login_form": login_form,
        "login_form_errors": login_form_errors,
        "register_form": register_form,
        "next_url": next_url,
        'page_type': 'login',
        'service_page': True,
    }))


def update_register_form(
        request,
        template_name='lfs/customer/register_form.html'):

    form = RegisterForm(request=request)
    return render_to_response(template_name, RequestContext(request, {
        "register_form": form,
    }))


def logout(request):
    """Custom method to logout a user.

    The reason to use a custom logout method is just to provide a login and a
    logoutmethod on one place.
    """
    from django.contrib.auth import logout
    logout(request)

    return set_message_cookie(
        reverse("lfs_shop_view"),
        msg=_(u"You have been logged out."))


@login_required
def orders(request, template_name="lfs/customer/orders.html"):
    """Displays the orders of the current user
    """
    orders = Order.objects.filter(user=request.user)

    if request.method == "GET":
        date_filter = request.session.get("my-orders-date-filter")
    else:
        date_filter = request.POST.get("date-filter")
        if date_filter:
            request.session["my-orders-date-filter"] = date_filter
        else:
            try:
                del request.session["my-orders-date-filter"]
            except KeyError:
                pass
    try:
        date_filter = int(date_filter)
    except (ValueError, TypeError):
        date_filter = None
    else:
        now = datetime.datetime.now()
        start = now - datetime.timedelta(days=date_filter*30)
        orders = orders.filter(created__gte=start)

    options = []
    for value in [1, 3, 6, 12]:
        selected = True if value == date_filter else False
        options.append({
            "value": value,
            "selected": selected,
        })

    return render_to_response(template_name, RequestContext(request, {
        "orders": orders,
        "options": options,
        "date_filter": date_filter,
        'page_type': 'orders',
        'service_page': True,
    }))


@login_required
def order(request, id, template_name="lfs/customer/order.html"):
    """
    """
    orders = Order.objects.filter(user=request.user)
    order = get_object_or_404(Order, pk=id, user=request.user)

    return render_to_response(template_name, RequestContext(request, {
        "current_order": order,
        "orders": orders,
        'page_type': 'order',
        'service_page': True,
    }))


@login_required
def account(request, template_name="lfs/customer/account.html"):
    """Displays the main screen of the current user's account.
    """
    from ..fields.utils import get_field_object_list
    user = request.user
    customer = Customer.objects.get(user=user)
    shop = get_default_shop(request)

    address_fields = shop.address_form_fields.split(',')
    disabled_fields = [
        'first_name',
        'last_name',
        'full_name',
        'date_of_birth',
        'referral_code']
    field_list = get_field_object_list(shop, request=request)
    fields = [
        x for x in field_list
        if x['name'] not in address_fields and
        x['name'] not in disabled_fields]

    additional_fields = []
    info = customer.get_info()

    for f in fields:
        if f['name'] in info:
            additional_fields.append({
                'label': f['obj'].label,
                'value': info[f['name']]
            })

    phones = Phone.objects.filter(customer=customer)
    addresses = Address.objects.filter(customer=customer)
    emails = EmailAddress.objects.filter(customer=customer)
    has_unverified_email = False
    for e in emails:
        if not e.verified:
            has_unverified_email = True
            break

    return render_to_response(template_name, RequestContext(request, {
        'shop': shop,
        'user': user,
        'has_fields': bool(field_list),
        'customer': customer,
        'phones': phones,
        'addresses': addresses,
        'emails': emails,
        'has_unverified_email': has_unverified_email,
        'additional_fields': additional_fields,
        'page_type': 'account',
        'service_page': True,
    }))


@login_required
def edit_account(request, template_name="lfs/customer/edit_account.html"):
    """Displays the form for edit account data.
    """
    user = request.user
    customer = Customer.objects.get(user=user)
    shop = get_default_shop(request)

    initial_data = customer.get_info()
    if request.POST:
        form = EditAccountForm(
            request=request,
            data=request.POST)
        if form.is_valid():
            form_data = {}
            for item in form.fields.keys():
                value = form.cleaned_data.get(item)
                if value is not None:
                    form_data.update({item: value})
            customer.set_info(form_data)
            customer.save()
            messages.success(request, _(u'Account settings was saved.'))
            return HttpResponseRedirect(reverse('lfs_my_account'))
    else:
        form = EditAccountForm(request=request, initial=initial_data)

    return render_to_response(template_name, RequestContext(request, {
        'shop': shop,
        'user': user,
        'customer': customer,
        'form': form,
        'page_type': 'account',
        'service_page': True,
    }))


def update_account_form(
        request,
        template_name='lfs/customer/form_edit.html'):
    form = EditAccountForm(request=request)
    return render_to_response(template_name, RequestContext(request, {
        "form": form,
    }))


@login_required
def add_email(request, template_name='lfs/customer/form_add_email.html'):
    """ Add new customer email
    """
    if request.method == 'POST':
        form = EmailForm(request.POST)
        if form.is_valid():
            e = form.save(commit=False)
            customer = Customer.objects.get(user=request.user)
            if customer.emails.filter(email=e.email).exists():
                messages.error(request, _(u'Dublicate email.'))
            else:
                e.customer = customer
                try:
                    EmailAddress.objects.get(customer=customer, default=True)
                except EmailAddress.DoesNotExist:
                    e.default = True
                e.save()
                messages.success(request, _(u'New email was added.'))
                return HttpResponseRedirect(reverse('lfs_my_account'))
    else:
        form = EmailForm()

    return render_to_response(template_name, RequestContext(request, {
        'form': form,
        'page_type': 'account',
        'service_page': True,
    }))


@login_required
def remove_email(
        request, email_id):
    """ Remove customer phone number
    """
    email = EmailAddress.objects.get(id=email_id)
    if email.default:
        return HttpResponseForbidden(_(u'Can not remove default email.'))
    elif email.customer.user != request.user:
        return HttpResponseForbidden(_(u'It is not your email.'))

    email.delete()
    messages.success(request, _(u'Email was removed.'))
    return HttpResponseRedirect(reverse('lfs_my_account'))


@login_required
def set_default_email(
        request, email_id):
    """ Set default email
    """
    email = EmailAddress.objects.get(id=email_id)
    if email.default:
        return HttpResponseForbidden(_(u'Email is already default.'))
    elif email.customer.user != request.user:
        return HttpResponseForbidden(_(u'It is not your email.'))

    customer = email.customer

    same_emails = EmailAddress.objects.filter(
        email=email.email, default=True)
    if same_emails:
        messages.error(
            request,
            _(
                u'Your cannot use %s as default email because it already '
                u'used by other user.') % email.email)
        return HttpResponseRedirect(reverse('lfs_my_account'))

    if User.objects.filter(
            Q(email=email.email) | Q(username=email.email)).count() > 0:
        messages.error(
            request,
            _(
                u'Your cannot use %s as default email because it already '
                u'used by other user.') % email.email)
        return HttpResponseRedirect(reverse('lfs_my_account'))

    default_emails = EmailAddress.objects.filter(
        customer=customer, default=True)
    default_emails.update(default=False)

    email.default = True
    email.save()
    # Need to change login
    if getattr(settings, 'CUSTOMER_AUTH_BY', 'email') == 'email':
        user = request.user
        user.username = email.email
        user.email = email.email
        user.save()
        messages.success(
            request,
            _(
                u'Now your default email is %s. Look that your login is also'
                u' changed and now is the same as your new default email.'
            ) % email.email)
    else:
        messages.success(
            request, _(u'Now your default email is %s.') % email.email)

    return HttpResponseRedirect(reverse('lfs_my_account'))


@login_required
def add_phone(request, template_name='lfs/customer/form_add_phone.html'):
    """ Add new customer phone number
    """
    if request.method == 'POST':
        form = PhoneForm(request.POST)
        if form.is_valid():
            p = form.save(commit=False)
            customer = Customer.objects.get(user=request.user)
            p.customer = customer
            try:
                Phone.objects.get(customer=customer, default=True)
            except Phone.DoesNotExist:
                p.default = True
            p.save()
            messages.success(request, _(u'Phone number was added.'))
            return HttpResponseRedirect(reverse('lfs_my_account'))
    else:
        form = PhoneForm()

    return render_to_response(template_name, RequestContext(request, {
        'form': form,
        'page_type': 'account',
        'service_page': True,
    }))


@login_required
def remove_phone(
        request, phone_id):
    """ Remove customer phone number
    """
    phone = Phone.objects.get(id=phone_id)
    if phone.default:
        return HttpResponseForbidden(_(u'Can not remove default phone.'))
    elif phone.customer.user != request.user:
        return HttpResponseForbidden(_(u'It is not your phone.'))

    phone.delete()
    messages.success(request, _(u'Phone number was removed.'))
    return HttpResponseRedirect(reverse('lfs_my_account'))


@login_required
def set_default_phone(
        request, phone_id):
    """ Set default customer phone number
    """
    phone = Phone.objects.get(id=phone_id)
    if phone.default:
        return HttpResponseForbidden(_(u'Phone is already default.'))
    elif phone.customer.user != request.user:
        return HttpResponseForbidden(_(u'It is not your phone.'))

    customer = phone.customer

    existing_phones = Phone.objects.filter(number=phone.number, default=True)
    if existing_phones:
        messages.error(
            request,
            _(
                u'Your cannot use %s as default phone because it already '
                u'used by other user.') % phone.number)
        return HttpResponseRedirect(reverse('lfs_my_account'))

    try:
        User.objects.get(username=phone.number)
        messages.error(
            request,
            _(
                u'Your cannot use %s as default phone because it already '
                u'used by other user.') % phone.number)
        return HttpResponseRedirect(reverse('lfs_my_account'))
    except User.DoesNotExist:
        pass

    default_phones = Phone.objects.filter(
        customer=customer, default=True)
    default_phones.update(default=False)

    phone.default = True
    phone.save()
    # Need to change login
    if getattr(settings, 'CUSTOMER_AUTH_BY', 'email') == 'phone':
        user = request.user
        user.username = phone.number
        user.save()
        messages.success(
            request,
            _(
                u'Now your default phone is %s. Look that your login is also'
                u' changed and now is the same as your new default phone.'
            ) % phone.number)
    else:
        messages.success(
            request, _(u'Now your default phone number is %s.') % phone.number)
    return HttpResponseRedirect(reverse('lfs_my_account'))


@login_required
def add_address(request, template_name='lfs/customer/form_add_address.html'):
    """ Render form for add new customer address
    """
    shop = get_default_shop(request)

    if request.method == 'POST':
        form = AddressForm(request, data=request.POST)

        if form.is_valid():
            customer = Customer.objects.get(user=request.user)
            address_fields = shop.address_form_fields.split(',')
            address_data = {}
            for item in address_fields:
                value = form.cleaned_data.get(item)
                if value:
                    address_data.update({item: value})

            hash_str = hash(frozenset(address_data.items()))

            if not Address.objects.filter(
                    customer=customer, hash_str=hash_str).exists():
                need_default = not Address.objects.filter(
                    customer=customer,
                    default=True).exists()
                if address_data:
                    Address.objects.create(
                        customer=customer,
                        fields=address_data,
                        default=need_default)
                messages.success(request, _(u'New address was added.'))
                return HttpResponseRedirect(reverse('lfs_my_account'))
            else:
                messages.error(
                    request, _(u'You can not add dublicate addresses.'))
    else:
        form = AddressForm(request)

    return render_to_response(template_name, RequestContext(request, {
        'form': form,
        'page_type': 'account',
        'service_page': True,
    }))


def update_address_form(
        request,
        template_name='lfs/customer/address_form.html'):
    form = AddressForm(request=request)
    return render_to_response(template_name, RequestContext(request, {
        "form": form,
    }))


@login_required
def set_default_address(
        request, address_id):
    """ Set default customer address
    """
    address = Address.objects.get(id=address_id)
    if address.default:
        return HttpResponseForbidden(_(u'Address is already default.'))
    elif address.customer.user != request.user:
        return HttpResponseForbidden(_(u'It is not your address.'))

    customer = address.customer
    default_addresses = Address.objects.filter(
        customer=customer, default=True)
    default_addresses.update(default=False)

    address.default = True
    address.save()

    messages.success(
        request, _(u'Default address was changed.'))
    return HttpResponseRedirect(reverse('lfs_my_account'))


@login_required
def remove_address(
        request, address_id):
    """ Remove customer address
    """
    address = Address.objects.get(id=address_id)
    if address.default:
        return HttpResponseForbidden(_(u'Can not remove default address.'))
    elif address.customer.user != request.user:
        return HttpResponseForbidden(_(u'It is not your address.'))

    address.delete()
    messages.success(request, _(u'Address was removed.'))
    return HttpResponseRedirect(reverse('lfs_my_account'))


@login_required
def password(request, template_name="lfs/customer/password.html"):
    """Changes the password of current user.
    """
    from ..core.signals import customer_change_password
    if request.method == "POST":
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, _(u'Your password was changed.'))
            customer_change_password.send(request.user)
            return HttpResponseRedirect(reverse("lfs_my_password"))
    else:
        form = PasswordChangeForm(request.user)

    return render_to_response(template_name, RequestContext(request, {
        "form": form,
        'page_type': 'password',
        'service_page': True,
    }))


def confirm_email(request, id, code):
    """ Confirm customer email
    """
    try:
        customer = EmailAddress.objects.get(
            id=id,
            verification_code=code)
        customer.verified = True
        customer.save()
        messages.success(
            request, _(u'Email address verified successfully.'))
    except EmailAddress.DoesNotExist:
        messages.error(
            request, _(u'Email address verification error.'))

    redirect_to = reverse('lfs_my_account')
    return HttpResponseRedirect(redirect_to)
