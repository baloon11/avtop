# coding: utf-8
import logging
from django.conf import settings
from .models import Customer


logger = logging.getLogger('sshop')


def get_or_create_customer(request):
    """Get or creates the customer object.
    """
    customer = get_customer(request)
    if customer is None:
        customer = request.customer = create_customer(request)
    return customer


def create_customer(request):
    """Creates a customer for the given request (which means for the current
    logged in user/or the session user).

    This shouldn't be called directly. Instead get_or_create_customer should be
    called.
    """
    customer = Customer(session=request.session.session_key)
    if request.user.is_authenticated():
        customer.user = request.user
    customer.save()
    return customer


def get_customer(request):
    """Returns the customer for the given request (which means for the current
    logged in user/or the session user).
    """
    try:
        return request.customer
    except AttributeError:
        customer = request.customer = _get_customer(request)
        return customer


def _get_customer(request):
    user = request.user
    if user.is_authenticated():
        try:
            return Customer.objects.get(user=user)
        except Customer.DoesNotExist:
            return None
        except Customer.MultipleObjectsReturned:
            logger.error(
                'User with pk=%s had more than one customer. Deleted.',
                [user.id])
            customers = Customer.objects.filter(user=user).order_by('id')
            c = customers[0]
            customers.exclude(id=c.id).delete()
            return c
    else:
        session_key = request.session.session_key
        try:
            return Customer.objects.get(session=session_key)
        except Customer.DoesNotExist:
            return None
        except Customer.MultipleObjectsReturned:
            logger.error(
                'User with session=%s had more than one customer. Deleted.',
                [session_key])
            customers = Customer.objects.filter(
                session=session_key).order_by('id')
            c = customers[0]
            customers.exclude(id=c.id).delete()
            return c


def update_customer_after_login(request):
    """Updates the customer after login.

    1. If there is no session customer, nothing has to be done.
    2. If there is a session customer and no user customer we
       assign the session
       customer to the current user.
    3. If there is a session customer and a user customer we copy the session
       customer information to the user customer and delete the session
       customer
    """
    try:
        session_customer = Customer.objects.get(
            session=request.session.session_key)

        try:
            user_customer = Customer.objects.get(user=request.user)
        except Customer.DoesNotExist:
            session_customer.user = request.user
            session_customer.save()
        else:
            if getattr(settings, 'CUSTOMER_AUTH_MERGE', True) and \
                    (session_customer.id != user_customer.id):
                from ..order.models import Order
                if not user_customer.first_name:
                    user_customer.first_name = session_customer.first_name
                if not user_customer.last_name:
                    user_customer.last_name = session_customer.last_name
                if not user_customer.full_name:
                    user_customer.full_name = session_customer.full_name
                if not user_customer.date_of_birth:
                    user_customer.date_of_birth = \
                        session_customer.date_of_birth
                if not user_customer.ref_code:
                    user_customer.ref_code = session_customer.ref_code
                user_customer.saving_account = user_customer.saving_account + \
                    session_customer.saving_account
                # We ignore extra data for customer here

                user_customer.selected_shipping_method = \
                    session_customer.selected_shipping_method
                user_customer.selected_payment_method = \
                    session_customer.selected_payment_method
                user_customer.save()

                for phone in session_customer.phones.all():
                    user_customer.add_phone(phone.number)
                for email in session_customer.emails.all():
                    user_customer.add_email(email.email)
                # We ignore address merging here

                Order.objects.filter(customer=session_customer).update(
                    customer=user_customer,
                    user=user_customer.user)
                session_customer.delete()
    except Customer.DoesNotExist:
        pass
