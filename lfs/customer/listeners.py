# coding: utf-8
import datetime
import hashlib

from django.contrib.auth.models import User
from django.db.models.signals import post_save

from ..core.signals import customer_added


def user_created_listener(sender, instance, **kwargs):
    from ..core.utils import (
        generate_referral_code,
        get_default_shop,
    )
    from .models import Customer

    try:
        shop = get_default_shop()
    except IndexError:
        # Помилка виникає, коли створюється user (і викликається цей сигнал),
        # а об'єкт Shop ще не створений. Така біда настає при запуску
        # команди `./manage.py syncdb` і створення юзера під час її виконання.
        return

    if not Customer.objects.filter(user=instance).exists():
        customer = Customer.objects.create()
        customer.user = instance
        f = True
        ref_code = None
        while f:
            ref_code = generate_referral_code(shop.template_of_discount_code)
            try:
                z = Customer.objects.get(ref_code=ref_code)
            except Customer.DoesNotExist:
                f = False
        # d = datetime.datetime.now()
        customer.ref_code = ref_code
        customer.save()
        customer_added.send(instance)
post_save.connect(user_created_listener, sender=User)

"""
See `email_added_listener` definition in lfs.customer.models
for preventing ciclic import reason.
"""
