# coding: utf-8
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Manufacturer(models.Model):
    """The manufacturer is the unique creator of a product.
    """
    name = models.CharField(_(u"Name"), max_length=50)
    is_popular = models.BooleanField(_(u'Popular'), default=False)
    image = models.ImageField(
        _(u"Logo"),
        upload_to="manufacturer-images", blank=True, null=True)

    class Meta:
        ordering = ('name',)
        verbose_name = _(u'Manufacturer')
        verbose_name_plural = _(u'Manufacturers')

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        """Returns the absolute url of the manufacturer
        """
        return reverse(
            "lfs_manufacturer",
            kwargs={"manufacturer_id": self.id})
